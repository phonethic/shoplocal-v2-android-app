package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.shoplocal.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SharedListViewAdapter extends ArrayAdapter<String>{

	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<String> mAppName; 
	private ArrayList<String> mPackageName;
	private ArrayList<Drawable> mAppIcon = new ArrayList<Drawable>();
	public SharedListViewAdapter(Context context, int resource, LayoutInflater inflater, ArrayList<String> appName, ArrayList<String> packageNames, ArrayList<Drawable> appIcon) {
		super(context, resource);
		mContext = context;
		mInflater = inflater; 
		mAppName = appName;
		mPackageName = packageNames;
		mAppIcon = appIcon;
	}
	
	
	public int getCount() {
		return mAppName.size();
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=mInflater.inflate(R.layout.sharedialoglistviewlayout, null);
			holder.tv = (TextView) convertView.findViewById(R.id.textViewAppName);
			holder.iv = (ImageView) convertView.findViewById(R.id.imageViewAppIcon);
			convertView.setTag(holder);
		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		
		hold.tv.setText(mAppName.get(position));
		hold.iv.setImageDrawable(mAppIcon.get(position));
		
		//iv.setImageDrawable();
		
		return convertView;
	}
	
	static class ViewHolder
	{
		TextView tv;
		ImageView iv;
	}
}
