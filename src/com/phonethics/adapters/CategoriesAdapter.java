package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.phonethics.shoplocal.Config;
import com.phonethics.shoplocal.R;

public class CategoriesAdapter extends ArrayAdapter<String> {

	ArrayList<String> category_name;
	Activity context;
	LayoutInflater inflate;
	Typeface tf;
	Animation anim_fromRight,anim_fromLeft;
	String isFav="";
	String TAG="CATEG";

	public CategoriesAdapter(Activity context, int resource,
			int textViewResourceId, ArrayList<String> category_name) {
		super(context, resource, textViewResourceId, category_name);
		this.context=context;
		this.category_name=category_name;
		inflate=context.getLayoutInflater();
		//getting font from asset directory.
		tf=Typeface.createFromAsset(context.getAssets(), "fonts/DinDisplayProLight.otf");
		anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
		anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
	}
	public int getCount() {
		return category_name.size();
	}

	public String getItem(int position) {
		return category_name.get(position);
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}
	public View getView(final int position, View convertView, ViewGroup parent) {
		//inflate a view and build ui for list.
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflate.inflate(R.layout.categorylistlayout,null);
			holder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);
			holder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
			holder.categoryLayout=(RelativeLayout)convertView.findViewById(R.id.categoryLayout);
			holder.txtCategory.setTypeface(tf);
			convertView.setTag(holder);
			
		}
		final ViewHolder hold=(ViewHolder)convertView.getTag();
		hold.txtCategory.setText(category_name.get(position));
		hold.chkBox.setVisibility(View.GONE);
		return convertView;
	}

	 class ViewHolder
	{
		ImageView imgStoreLogo;
		TextView txtCategory;
		CheckBox chkBox;
		RelativeLayout categoryLayout;
	}
}
