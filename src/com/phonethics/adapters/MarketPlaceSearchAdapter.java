package com.phonethics.adapters;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonethics.shoplocal.ListClickListener;
import com.phonethics.shoplocal.R;

public class MarketPlaceSearchAdapter extends ArrayAdapter<String>{

	private ArrayList<String> mArrStoreName;
	private ArrayList<String> mArrDistance;
	private ArrayList<String> mArrLogo;
	private ArrayList<String> mArrHasOffer;
	private ArrayList<String> mArrOfferTitle;
	private ArrayList<String> mArrDescription;
	private ArrayList<String>mArrTotalLike;
	private ArrayList<String> mArrId;
	private Activity mContext;
	private LayoutInflater mInflate;
	private String photo;
	private Typeface tf;
	
	private ListClickListener mLsClick;

	public MarketPlaceSearchAdapter(Activity context, int resource,
			int textViewResourceId, ArrayList<String> mArrId, ArrayList<String> mArrStoreName,ArrayList<String>mArrLogo,ArrayList<String>mArrHasOffer,
			ArrayList<String>mArrOfferTitle,ArrayList<String>mArrDescription,ArrayList<String>mArrDistance,ArrayList<String>mArrTotalLike,ListClickListener mLsClick) {
		super(context, resource, textViewResourceId, mArrStoreName);

		this.mContext=context;
		this.mArrStoreName=mArrStoreName;
		this.mArrDistance=mArrDistance;
		this.mArrLogo=mArrLogo;
		this.mArrDescription=mArrDescription;
		this.mArrOfferTitle=mArrOfferTitle;
		this.mArrHasOffer=mArrHasOffer;
		this.mArrTotalLike=mArrTotalLike;
		this.mLsClick=mLsClick;
		this.mArrId=mArrId;
		mInflate=context.getLayoutInflater();
		tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
	}

	@Override
	public int getCount() {
		return mArrStoreName.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView=LayoutInflater.from(mContext).inflate(R.layout.layout_marketplace,null);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();

		if(holder == null) {
			holder=new ViewHolder();
			holder.imgPlaceLogo=(ImageView)convertView.findViewById(R.id.imgPlaceLogo);
			holder.txtStoreName=(TextView)convertView.findViewById(R.id.txtStoreName);
			holder.txtStoreDistance=(TextView)convertView.findViewById(R.id.txtStoreDistance);
			holder.txtStoreDescription=(TextView)convertView.findViewById(R.id.txtStoreDescription);
			holder.txtStoreLikeCount=(TextView)convertView.findViewById(R.id.txtStoreLikeCount);
			holder.imgOfferBand=(ImageView)convertView.findViewById(R.id.imgOfferBand);
			
			holder.txtStoreName.setTypeface(tf, Typeface.BOLD);
			holder.txtStoreDescription.setTypeface(tf);
			holder.txtStoreDistance.setTypeface(tf);
			convertView.setTag(holder);
		}
		
		holder.txtStoreName.setText(mArrStoreName.get(position));
		holder.txtStoreDescription.setText(mArrDescription.get(position));
		holder.txtStoreLikeCount.setText(mArrTotalLike.get(position));
		holder.imgOfferBand.setVisibility(View.GONE);
		
		holder.txtStoreDistance.setVisibility(View.VISIBLE);
		try
		{
		if(mArrDistance.get(position)!=null && !mArrDistance.get(position).equalsIgnoreCase("0")) {
			double distance = Double.parseDouble(mArrDistance.get(position));
			if(distance < 1 && distance >0) {
				DecimalFormat format = new DecimalFormat("##.###");
				String formatted = format.format(distance*100);
				holder.txtStoreDistance.setText(""+formatted+" m.");
			} else {
				DecimalFormat format = new DecimalFormat("##.###");
				String formatted = format.format(distance);
				debug((distance*100)+"");
				if((distance*100)>8000) {
					holder.txtStoreDistance.setVisibility(View.GONE);
				} else {
					holder.txtStoreDistance.setVisibility(View.VISIBLE);
					holder.txtStoreDistance.setText(""+formatted+" km.");
				}
			}
		}
		}catch(NumberFormatException nb){
			nb.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		holder.imgPlaceLogo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				callStore(position);
			}
		});
		
		if(mArrHasOffer.get(position).equalsIgnoreCase("1")){
			holder.imgOfferBand.setVisibility(View.VISIBLE);
		}else{
			holder.imgOfferBand.setVisibility(View.GONE);
		}
		

		
		return convertView;
	}
	
	void callStore(int position)
	{
		mLsClick.listItemClick(position, mArrId.get(position),"call");
	}
	
	void debug(String msg){
		Log.d("Adapter ", "Adapter "+msg);
	}
	
	private class ViewHolder {
		ImageView imgPlaceLogo;
		ImageView imgOfferBand;
		TextView txtStoreName;
		TextView txtStoreDescription;
		TextView txtStoreDistance;
		TextView txtStoreLikeCount;
	}


	
}
