package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.phonethics.shoplocal.R;

public class LocationListAdapter extends ArrayAdapter<String> {

	
	ArrayList<String> locations;
	Activity context;
	LayoutInflater inflator;
	Typeface tf;

	public LocationListAdapter(Activity context, int resource,
			int textViewResourceId, ArrayList<String> locations) {
		super(context, resource, textViewResourceId, locations);
		this.locations=locations;
		this.context=context;
		inflator=context.getLayoutInflater();
		tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
	}

	
	public int getCount() {
		return locations.size();
	}

	
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflator.inflate(R.layout.layout_location_row, null);
			holder.txtLocationRow=(TextView)convertView.findViewById(R.id.txtLocationRow);
			holder.txtLocationRow.setTypeface(tf);
			convertView.setTag(holder);
		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		hold.txtLocationRow.setText(locations.get(position));
		return convertView;
	}
}

 class ViewHolder
{
	TextView txtLocationRow;
}
