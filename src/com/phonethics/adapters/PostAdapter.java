package com.phonethics.adapters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.phonethics.shoplocal.Config;
import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.ShowPlaceMessage;
import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PostAdapter extends ArrayAdapter<String> {
	ArrayList<String> TITLE,distance,logo,photo,date,TOTAL_LIKE, TOTAL_VIEWS, TOTAL_SHARE, isOffer;
	Activity context;
	LayoutInflater inflate;
	boolean mbShowTotalView;
	
	public PostAdapter(Activity context, int resource,
			int textViewResourceId, ArrayList<String> TITLE,ArrayList<String>photo,ArrayList<String>date,
			ArrayList<String>TOTAL_LIKE, ArrayList<String>TOTAL_VIEWS, ArrayList<String>TOTAL_SHARE, ArrayList<String> isOffer, boolean mbShowTotalView) {
		super(context, resource, textViewResourceId, TITLE);
		this.TITLE=TITLE;
		this.photo=photo;
		this.context=context;
		this.date=date;
		this.TOTAL_LIKE=TOTAL_LIKE;
		this.TOTAL_VIEWS = TOTAL_VIEWS;
		this.TOTAL_SHARE = TOTAL_SHARE;
		this.mbShowTotalView=mbShowTotalView;
		this.isOffer = isOffer;
		inflate=context.getLayoutInflater();
	}
	public int getCount() {
		return TITLE.size();
	}

	public String getItem(int position) {
		return TITLE.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			ViewHolder holder=new ViewHolder();
			convertView=inflate.inflate(R.layout.offers_broadcast_layout,null);

			holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
			holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
			holder.offerLogo = (ImageView) convertView.findViewById(R.id.offerLogo);
			holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
			holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
			holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
			holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
			holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
			holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);
			convertView.setTag(holder);
		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		hold.txtTitle.setText(TITLE.get(position));
		try{
			DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dt2=new SimpleDateFormat("MMM");
			Date date_con = (Date) dt.parse(date.get(position).toString());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_con);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);

			Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+date.get(position));
			hold.txtDate.setText(day+"");
			hold.txtMonth.setText(dt2.format(date_con)+"");
			hold.txtBroadCastStoreTotalLike.setText(TOTAL_LIKE.get(position));
			
			hold.txtBroadCastStoreTotalViews.setVisibility(View.GONE);
			if(mbShowTotalView==true){
				hold.txtBroadCastStoreTotalViews.setVisibility(View.VISIBLE);
				hold.txtBroadCastStoreTotalViews.setText(TOTAL_VIEWS.get(position));
			}else{
				hold.txtBroadCastStoreTotalViews.setVisibility(View.GONE);
			}
			
			Log.i("TOTAL_VIEWS ADAPTER", "TOTAL_VIEWS ADAPTER "+TOTAL_VIEWS.get(position));
			hold.txtBroadCastStoreTotalShare.setText(TOTAL_SHARE.get(position));
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		if (isOffer.get(position).equals("1")) {
			hold.offerLogo.setVisibility(View.VISIBLE);
		} else {
			hold.offerLogo.setVisibility(View.INVISIBLE);
		}
		try{
			if(photo.get(position).toString().equalsIgnoreCase("notfound")){
				hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
				hold.viewOverLay.setVisibility(View.INVISIBLE);
				hold.offerLogo.setVisibility(View.INVISIBLE);
			}
			else{
				/*hold.offerLogo.setVisibility(View.VISIBLE);*/
				String photo_source=photo.get(position).toString().replaceAll(" ", "%20");
				hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
				try{
					Picasso.with(context).load(Config.IMAGE_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgBroadcastLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return convertView;
	}
	
	class ViewHolder
	{
		TextView txtTitle,txtDate,txtMonth;
		ImageView imgBroadcastLogo, offerLogo;
		View viewOverLay;
		TextView txtBroadCastStoreTotalLike;
		TextView txtBroadCastStoreTotalViews;
		TextView txtBroadCastStoreTotalShare;
	}
}
