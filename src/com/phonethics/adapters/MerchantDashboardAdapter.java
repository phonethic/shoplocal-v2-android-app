package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.shoplocal.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MerchantDashboardAdapter extends ArrayAdapter<String>{

	private Activity mContext;
	private LayoutInflater mInflater;
	private ArrayList<Integer> mIcon;
	private ArrayList<String> mMenu;
	public MerchantDashboardAdapter(Activity context, int resource, ArrayList<String> mMenu, ArrayList<Integer> mIcon) {
		super(context, resource);
		mContext = context;
		mInflater =mContext.getLayoutInflater(); 
		this.mMenu=mMenu;
		this.mIcon=mIcon;
	}
	
	
	public int getCount() {
		return mMenu.size();
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=mInflater.inflate(R.layout.layout_dashboard_row, null);
			holder.tv = (TextView) convertView.findViewById(R.id.txtMenu);
			holder.iv = (ImageView) convertView.findViewById(R.id.imgIcon);
			convertView.setTag(holder);
		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		
		hold.tv.setText(mMenu.get(position));
		hold.iv.setImageResource(mIcon.get(position));
		
		//iv.setImageDrawable();
		
		return convertView;
	}
	
	static class ViewHolder
	{
		TextView tv;
		ImageView iv;
	}
}
