package com.phonethics.adapters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonethics.shoplocal.Config;
import com.phonethics.shoplocal.ListClickListener;
import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.ShowPlaceMessage;
import com.squareup.picasso.Picasso;

public class MyShoplocalAdapter extends ArrayAdapter<String> {

	private int miCount=0;

	private Typeface  tf = null;

	private Activity mActivity;

	private ArrayList<String> mArrPostId=new ArrayList<String>();
	private ArrayList<String> mArrPostTitle=new ArrayList<String>();
	private ArrayList<String> mArrPostDesc=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrHasOFfer=new ArrayList<String>();
	private ArrayList<String> mArrImage=new ArrayList<String>();
	private ArrayList<String> mArrPostDate=new ArrayList<String>();
	private ArrayList<String> mArrTotalLike=new ArrayList<String>();
	private ArrayList<String> mArrTotalShare=new ArrayList<String>();

	private ListClickListener mLsClick;
	public MyShoplocalAdapter(Activity context, int resource,
			int textViewResourceId, ArrayList<String> mArrPostId, ArrayList<String> mArrPostTitle,
			ArrayList<String> mArrPostDesc, ArrayList<String> mArrStoreName, ArrayList<String> mArrHasOFfer,
			ArrayList<String> mArrImage, ArrayList<String> mArrPostDate, ArrayList<String> mArrTotalLike,
			ArrayList<String> mArrTotalShare,  ListClickListener lsClick) {
		super(context, resource, textViewResourceId, mArrPostId);

		this.mArrPostId=mArrPostId;
		mActivity=context;
		tf=Typeface.createFromAsset(mActivity.getAssets(), "fonts/GOTHIC_0.TTF");
		this.mLsClick=lsClick;
		this.mArrPostTitle=mArrPostTitle;
		this.mArrPostDesc=mArrPostDesc;
		this.mArrStoreName=mArrStoreName;
		this.mArrHasOFfer=mArrHasOFfer;
		this.mArrImage=mArrImage;
		this.mArrPostDate=mArrPostDate;
		this.mArrTotalLike=mArrTotalLike;
		this.mArrPostTitle=mArrPostTitle;
		this.mArrTotalShare=mArrTotalShare;
		miCount=mArrPostId.size();
		
	}

	@Override
	public int getCount() {
		return mArrPostId.size();
	}
	
	private void showToast(String msg) {
		
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView=LayoutInflater.from(mActivity).inflate(R.layout.newsfeedrow,null);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();

		if(holder==null){
			holder=new ViewHolder();
			holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
			holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
			holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);
			holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
			holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);
			holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);
			holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);
			holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);
			holder.imgCallStore=(ImageView)convertView.findViewById(R.id.imgCallStore);


			holder.newsFeedOffersText.setTypeface(tf);
			holder.newsFeedOffersDesc.setTypeface(tf);
			holder.newsFeedTotalLike.setTypeface(tf);
			holder.newsFeedTotalShare.setTypeface(tf);
			holder.newsFeedStoreName.setTypeface(tf);
			convertView.setTag(holder);
		}
		
		String desc = mArrPostDesc.get(position).trim();
		String seperator = "\\\\'";
		String []tempText = desc.split(seperator);
		
        String b= "";
        for (String c : tempText) {
        	b +=c+"\'";
        }
        b = b.substring(0, b.length()-1);
		
		holder.newsFeedOffersDesc.setText(b/*mArrPostDesc.get(position)*/);
		

		desc = mArrPostTitle.get(position).trim();
		seperator = "\\\\'";
		tempText = desc.split(seperator);
		
        b= "";
        for (String c : tempText) {
        	b +=c+"\'";
        }
        b = b.substring(0, b.length()-1);
		holder.newsFeedOffersText.setText(b);
		
		holder.newsFeedTotalLike.setText(mArrTotalLike.get(position));
		holder.newsFeedTotalShare.setText(mArrTotalShare.get(position));
		
		desc = mArrStoreName.get(position).trim();
		seperator = "\\\\'";
		tempText = desc.split(seperator);
		
        b= "";
        for (String c : tempText) {
        	b +=c+"\'";
        }
        b = b.substring(0, b.length()-1);
		
		holder.newsFeedStoreName.setText(b/*mArrStoreName.get(position)*/);
		
		if(mArrHasOFfer.get(position).equalsIgnoreCase("1")){
			holder.newsIsOffer.setVisibility(View.VISIBLE);
		}else{
			holder.newsIsOffer.setVisibility(View.GONE);
		}
		try{
			Picasso.with(mActivity).load(Config.IMAGE_URL+mArrImage.get(position))
			.placeholder(R.drawable.ic_place_holder)
			.error(R.drawable.ic_place_holder)
			.into(holder.imgNewsFeedLogo);
		}
		catch(IllegalArgumentException illegalArg){
			illegalArg.printStackTrace();
		}
		catch(Exception e){ 
			e.printStackTrace();
		}
		try {
			DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dt2=new SimpleDateFormat("MMM");
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			DateFormat date_format=new SimpleDateFormat("hh:mm a");

			Date date_con = (Date) dt.parse(mArrPostDate.get(position));
			Date date=dateFormat.parse(mArrPostDate.get(position));
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_con);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			holder.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		holder.newsFeedTotalLike.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				likePost(position);
			}
		});
		holder.newsFeedTotalShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sharePost(position);
			}
		});

		holder.imgCallStore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				callStore(position);
			}
		});

		return convertView;
	}


	void likePost(int position)
	{
		mLsClick.listItemClick(position,mArrPostId.get(position), "like");
	}

	void sharePost(int position)
	{
		mLsClick.listItemClick(position,mArrPostId.get(position), "share");
	}
	void callStore(int position)
	{
		mLsClick.listItemClick(position, mArrPostId.get(position),"call");
	}


	private class ViewHolder
	{
		TextView newsFeedOffersDate;
		TextView newsFeedOffersMonth;
		TextView newsFeedOffersText;
		TextView newsFeedOffersDesc;
		TextView newsFeedTotalLike;
		TextView newsFeedTotalShare;
		ImageView imgNewsFeedLogo;
		View newsFeedviewOverLay;
		TextView newFeedband;
		ImageView imgCallStore;
		ImageView newsIsOffer;
		TextView newsFeedStoreName;
	}

}
