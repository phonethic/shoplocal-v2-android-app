package com.phonethics.adapters;

import java.util.ArrayList;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.phonethics.shoplocal.R;

public class NavigationActionListener extends ArrayAdapter<String>{

	ArrayList<String>	mArArea;
	Activity 			mContext;
	LayoutInflater 		mLayoutInflater;
	Typeface tf;
	public NavigationActionListener(Activity context, int resource,int textViewResourceId, ArrayList<String> name) {
		super(context, resource, textViewResourceId, name);
		this.mContext	= context;
		this.mArArea	= name;
		mLayoutInflater	= context.getLayoutInflater();
		tf=Typeface.createFromAsset(mContext.getAssets(), "fonts/GOTHIC_0.TTF");
	}

	@Override
	public int getCount() {
		return mArArea.size();
	}

	@Override
	public String getItem(int position) {
		return mArArea.get(position);
	}

	@Override
	public View getDropDownView(int position, View convertView,
			ViewGroup parent) {
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_menus, parent, false);
		}
		TextView txtMenu = (TextView) convertView.findViewById(R.id.listText);
		txtMenu.setTypeface(tf);
		txtMenu.setText(""+getItem(position));
		return convertView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_dropdown_item, parent, false);
		}
		TextView txtMenu = (TextView) convertView.findViewById(R.id.txtSpinner);
		txtMenu.setTypeface(tf);
		txtMenu.setText(""+getItem(position));
		return convertView;
	}




}
