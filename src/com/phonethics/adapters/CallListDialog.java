package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.shoplocal.R;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CallListDialog extends ArrayAdapter<String>{

	ArrayList<String> callNumbers;
	Activity context;
	LayoutInflater inflator;

	public CallListDialog(Activity context, int resource, ArrayList<String> callNumbers) {
		super(context, resource, callNumbers);

		this.context=context;
		inflator=context.getLayoutInflater();
		this.callNumbers=callNumbers;

	}

	@Override
	public int getCount() {

		return callNumbers.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		try
		{
			if(convertView==null)
			{
				CallListViewHolder holder=new CallListViewHolder();
				convertView=inflator.inflate(R.layout.calllistlayout, null);
				holder.contactNo=(TextView)convertView.findViewById(R.id.contactNo);

				convertView.setTag(holder);
			}

			CallListViewHolder hold=(CallListViewHolder) convertView.getTag();

			hold.contactNo.setText(callNumbers.get(position));
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return convertView;
	}
}

class CallListViewHolder
{
	TextView contactNo;
}

