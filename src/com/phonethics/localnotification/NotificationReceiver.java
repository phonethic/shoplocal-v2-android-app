package com.phonethics.localnotification;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.phonethics.shoplocal.CreateBroadCast;
import com.phonethics.shoplocal.CustomerProfile;
import com.phonethics.shoplocal.DBUtil;
import com.phonethics.shoplocal.EditStore;
import com.phonethics.shoplocal.EventTracker;
import com.phonethics.shoplocal.PlaceChooser;
import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.SessionManager;
import com.phonethics.shoplocal.StartUpActivity;

/**
 * Whenever notification is being clicked this class gets called
 *  
 * @author manoj
 *
 */
public class NotificationReceiver extends BroadcastReceiver {

	private int miRequestCode=0;
	private SessionManager mSessionManager;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrStoreLogo=new ArrayList<String>();
	private ArrayList<String> mArrStoreDesc=new ArrayList<String>();

	private DBUtil mDbUtil;
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		mSessionManager=new SessionManager(context);
		mDbUtil=DBUtil.getInstance(context);

		try{
			mArrStoreId=mDbUtil.getAllPlacesIDs();
			mArrStoreName=mDbUtil.getAllPlacesNames();
			mArrStoreLogo=mDbUtil.getAllPlacesPhotoUrl();
			mArrStoreDesc=mDbUtil.getAllPlacesDesc();
		}catch(Exception ex){
			ex.printStackTrace();
		}

		if(bundle!=null){
			miRequestCode=bundle.getInt("requestCode");
			cancelAlarm(miRequestCode, context);
		}


		boolean isShoplocalRunning = isRunning(context);

		if(!isShoplocalRunning){
			if(miRequestCode==100){
				editStore(context);
			}else if(miRequestCode==101){
				talkNow(context);
			}else if(miRequestCode==203){
				Intent launch = new Intent(Intent.ACTION_MAIN);
				launch.putExtra("requestCode", miRequestCode);
				launch.setClass(context, CustomerProfile.class);
				launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(launch);	
			}
			else{
				Intent launch = new Intent(Intent.ACTION_MAIN);
				launch.putExtra("requestCode", miRequestCode);
				launch.setClass(context, StartUpActivity.class);
				launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(launch);
			}
		}

	}

	void talkNow(final Context mContext)
	{
		//Talk Now
		if(mArrStoreId.size()==0){
			Intent intent=new Intent(mContext, EditStore.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("isNew", true);
			mContext.startActivity(intent);
		}
		else if(mArrStoreId.size()==1)
		{
			EventTracker.logEvent("Tab_TalkNow", false);
			Intent intent=new Intent(mContext, CreateBroadCast.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putStringArrayListExtra("SELECTED_STORE_ID",mArrStoreId);
			mContext.startActivity(intent);
		}
		else
		{
			EventTracker.logEvent("Tab_TalkNow", false);
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("choiceMode", 2);
			intent.putExtra("viewPost", 0);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			mContext.startActivity(intent);
		}

	}

	void editStore(final Context mContext){
		//Add Store
		if(mArrStoreId.size()==0){
						Intent intent=new Intent(mContext, EditStore.class);
						intent.putExtra("isNew", true);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						mContext.startActivity(intent);
		}
		//EditStore
		else if(mArrStoreId.size()==1){
			EventTracker.logEvent("Tab_EditStore",false );	
			Intent intent=new Intent(mContext, EditStore.class);
			intent.putExtra("isSplitLogin",true);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("STORE_NAME", mArrStoreName.get(0));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(0));
			intent.putExtra("STORE_ID", mArrStoreId.get(0));
			mContext.startActivity(intent);
		}
		//Place Chooser
		else{
			EventTracker.logEvent("Tab_EditStore",false );	
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("viewPost",2);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			mContext.startActivity(intent);
		}
	}

	/**
	 * 
	 * @param ctx
	 * @return true if the app is running or else false 
	 */
	public boolean isRunning(Context ctx) {
		ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

		for (RunningTaskInfo task : tasks) {
			if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
				return true;   
		}
		return false;
	}


	/**
	 * 
	 * @param requestCode Unique code of alarm that needs to be canceled.
	 * @param context
	 */
	void cancelAlarm(int requestCode,Context context){

		String ALARM_ACTION="com.phonethics.localnotification.ALARM_ACTION";
		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null){
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}

	/**
	 * 
	 * @param processName The name of the process that needs to be checked whether its running or not
	 * @param context
	 * @return true if process is running else false
	 */
	//	boolean isNamedProcessRunning(String processName,Context context){
	//		if (processName == null) return false;
	//
	//		ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
	//		List<RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
	//
	//		for (RunningAppProcessInfo process : processes)
	//		{
	//			if (processName.equals(process.processName)){
	//				return true;
	//			}
	//		}
	//		return false;
	//	}


}
