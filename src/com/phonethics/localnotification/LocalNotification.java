package com.phonethics.localnotification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.SessionManager;

/**
 * This class is Sets the Alarm 
 * @author manoj
 * 
 */
public class LocalNotification {

	private Context mContext;
	private DateFormat mFormat;

	//Alarm Manager
	private AlarmManager mAlarmManager;
	private int miAlarmType=AlarmManager.RTC_WAKEUP;
	private long miRepeatFor=0;

	private String ALARM_ACTION="com.phonethics.localnotification.ALARM_ACTION";

	//Preference Names
	private String IS_ALARM_FIRED="IS_ALARM_FIRED";

	//PREF FOR EDIT STORE
	private String EDIT_STORE_PREF="EDIT_STORE";
	private String EDIT_STORE_KEY_ID="PLACE_ID";
	private String EDIT_STORE_SETALARM="EDIT_STORE_SET_ALARM";

	//PREF FOR MERCHANT NOT POSTED STORE
	private String POST_STORE_PREF="POST_STORE_PREF";
	private String POST_PREF_DATE="POST_STORE_PREF_DATE";
	private String POST_PREF_SETALARM="POST_PREF_SET_ALARM";

	//PREF FOR FAV
	private String FAV_STORE_PREF="FAV_STORE_PREF";
	private String FAV_SETALARM="HAS_FAV";


	//PREF FOR Customer Profile
	private String PROFILE_PREF="PROFILE_PREF";
	private String PROFILE_SETALARM="IS_UPDATED";

	//PREF FOR APP LAUNCH
	private String APP_LAUNCH_PREF="APP_LAUNCH_PREF";

	//PREF FOR LOGIN
	private String APP_LOGIN_PREF="APP_LOGIN_PREF";

	//PREF Boolean
	private boolean mbIsPlaceAlarmReq=false;
	private boolean mbIsPlaceAlarmFired=false;
	private boolean mbIsFavAlarmFired=false;
	private boolean mbIsPostAlarmFired=false;
	private boolean mbIsProfilAlarmFired=false;
	private boolean mbHasFav=false;
	private boolean mbIsPosted=false;
	private boolean mbIsProfileUpdated=false;
	private boolean mbIsAppLaunchedFired=false;
	private boolean mbIsLoginAlarmFired=false;


	//Pref Vari
	private String msPlaceId="";
	private String msPostDate="";

	private SessionManager mSession;

	private long mlDiffSeconds;
	private long mlDiffMinutes;
	private long mlDiffHours;
	private long mlDiffDays;

	public LocalNotification(Context context)
	{
		mContext=context;
		mFormat=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
		mAlarmManager=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		mSession=new SessionManager(mContext);
	}


	/**
	 * 
	 * @param setFor For how many days alarm needs to be set.
	 * @param days After how many days alarm needs to repeat.
	 * @param requestCode unique code to create alarm.
	 * @param isRepeating is Alarm repeating or not.
	 */
	public void setLocalNotification(int setFor,int days,int requestCode,boolean isRepeating)
	{
		try
		{

			//Current Date
			Date date=new Date();

			//Adding the days to current date
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, setFor);

			long timeOrLengthOfWait=c.getTimeInMillis();

			//Firing Broadcast with date created
			Intent intentToFire= new Intent(ALARM_ACTION);
			intentToFire.putExtra("requestCode",requestCode);
			intentToFire.putExtra("dateCreated",mFormat.format(date).toString());

			//Creating pending intent with given request Code
			PendingIntent alarmIntent=PendingIntent.getBroadcast(mContext, requestCode, intentToFire,PendingIntent.FLAG_UPDATE_CURRENT);

			if(isRepeating){
				//Repeating Alarm for given days
				miRepeatFor=AlarmManager.INTERVAL_DAY*days;

				//Setting alarm
				mAlarmManager.setInexactRepeating(miAlarmType, timeOrLengthOfWait, miRepeatFor, alarmIntent);
			}
			else{
				Log.i("Notification ", "Notification  "+setFor+" "+days+" "+requestCode+" "+isRepeating);
				mAlarmManager.set(miAlarmType, timeOrLengthOfWait, alarmIntent);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void setWeeklyReminder(int requestCode,int hour)
	{
		try
		{
			//Current Date
			Date date=new Date();

			//Repeating Alarm for given days
			miRepeatFor=AlarmManager.INTERVAL_DAY*7;
			int day=Math.abs(Calendar.FRIDAY - (Calendar.DAY_OF_WEEK % 7));
			//Setting Alarm for particular Day
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, day);
			c.set(Calendar.HOUR, hour);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			
			long timeOrLengthOfWait=c.getTimeInMillis();

			Intent intentToFire= new Intent(ALARM_ACTION);
			intentToFire.putExtra("requestCode",requestCode);
			intentToFire.putExtra("dateCreated",mFormat.format(date).toString());

			//Creating pending intent with given request Code
			PendingIntent alarmIntent=PendingIntent.getBroadcast(mContext, requestCode, intentToFire,PendingIntent.FLAG_UPDATE_CURRENT);

			//Setting alarm
			mAlarmManager.setInexactRepeating(miAlarmType, timeOrLengthOfWait, miRepeatFor, alarmIntent);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void checkPref()
	{
		try
		{
			//Place Preference 
			SharedPreferences editStorePref=mContext.getSharedPreferences(EDIT_STORE_PREF, Context.MODE_PRIVATE);
			mbIsPlaceAlarmReq=editStorePref.getBoolean(EDIT_STORE_SETALARM, false);
			msPlaceId=editStorePref.getString(EDIT_STORE_KEY_ID, "");
			mbIsPlaceAlarmFired=editStorePref.getBoolean(IS_ALARM_FIRED, false);

			//Post Preference
			SharedPreferences postStorePref=mContext.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			msPostDate=postStorePref.getString(POST_PREF_DATE, "");
			mbIsPosted=postStorePref.getBoolean(POST_PREF_SETALARM, false);
			mbIsPostAlarmFired=postStorePref.getBoolean(IS_ALARM_FIRED, false);

			//Favourite Preference
			SharedPreferences favStorePref=mContext.getSharedPreferences(FAV_STORE_PREF, Context.MODE_PRIVATE);
			mbHasFav=favStorePref.getBoolean(FAV_SETALARM, false);
			mbIsFavAlarmFired=favStorePref.getBoolean(IS_ALARM_FIRED, false);

			//Profile Preference
			SharedPreferences profPref=mContext.getSharedPreferences(PROFILE_PREF, Context.MODE_PRIVATE);
			mbIsProfileUpdated=profPref.getBoolean(PROFILE_SETALARM, false);
			mbIsProfilAlarmFired=profPref.getBoolean(IS_ALARM_FIRED, false);

			//App Launch Preference
			SharedPreferences appLaunch=mContext.getSharedPreferences(APP_LAUNCH_PREF, Context.MODE_PRIVATE);
			mbIsAppLaunchedFired=appLaunch.getBoolean(IS_ALARM_FIRED, false);

			//App Login Preference
			SharedPreferences appLogin=mContext.getSharedPreferences(APP_LOGIN_PREF, Context.MODE_PRIVATE);
			mbIsLoginAlarmFired=appLogin.getBoolean(IS_ALARM_FIRED, false);

			Log.i("Check Prefs", "Check prefs "+mbIsPlaceAlarmReq+" "+mbIsPosted+" "+mbHasFav+" "+mbIsProfileUpdated);
			Log.i("Check Prefs", "Check prefs fired "+mbIsPlaceAlarmFired+" "+mbIsPostAlarmFired+" "+mbIsFavAlarmFired+" "+mbIsProfilAlarmFired+" "+mbIsAppLaunchedFired+" "+mbIsLoginAlarmFired);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void clearPlacePrefs()
	{
		try
		{
			SharedPreferences editStorePref=mContext.getSharedPreferences(EDIT_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=editStorePref.edit();
			editor.remove(EDIT_STORE_KEY_ID);
			editor.remove(EDIT_STORE_SETALARM);
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	public void clearPostPrefs()
	{
		try
		{
			SharedPreferences postStorePref=mContext.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=postStorePref.edit();
			editor.remove(POST_PREF_DATE);
			editor.remove(IS_ALARM_FIRED);
			editor.remove(POST_PREF_SETALARM);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void clearFavPrefs()
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(FAV_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(FAV_SETALARM);
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void clearProfilePrefs()
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(PROFILE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(PROFILE_SETALARM);
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void clearAppLaunchPrefs()
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(APP_LAUNCH_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void clearLoginPrefs()
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(APP_LOGIN_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}



	//Place Preference
	public void setPlacePref(String Place_Id,boolean setAlarm,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences editStorePref=mContext.getSharedPreferences(EDIT_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=editStorePref.edit();
			
			editor.putString(EDIT_STORE_KEY_ID, Place_Id);
			editor.putBoolean(EDIT_STORE_SETALARM, setAlarm);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Set Post Preference
	public void setPostPref(String postDate,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences postStorePref=mContext.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=postStorePref.edit();
			editor.putString(POST_PREF_DATE, postDate);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.putBoolean(POST_PREF_SETALARM, mbIsPosted);	
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void setIsPosted(boolean isPosted)
	{
		try
		{
			SharedPreferences postStorePref=mContext.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=postStorePref.edit();
			editor.putBoolean(POST_PREF_SETALARM, isPosted);	
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Set Favourite Preference
	public void setFavPref(boolean setAlarm,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(FAV_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(FAV_SETALARM, setAlarm);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Set Profile Preference
	public void setProfilePref(boolean setAlarm,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(PROFILE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(PROFILE_SETALARM, setAlarm);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Set App Launch Preference
	public void setAppLaunchPref(boolean setAlarm)
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(APP_LAUNCH_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(IS_ALARM_FIRED, setAlarm);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Set Login Preference
	public void setLoginPref(boolean setAlarm)
	{
		try
		{
			SharedPreferences favStorePref=mContext.getSharedPreferences(APP_LOGIN_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(IS_ALARM_FIRED, setAlarm);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	// Set Alarm Fired Toggle

	void setAlarmFired(int requestCode,boolean isAlarmFired)
	{
		try
		{
			String PREF_NAME="";
			//Merchant
			if(requestCode==Integer.parseInt(mContext.getString(R.string.Edit_Store_alert))){
				PREF_NAME=EDIT_STORE_PREF;
			}
			if(requestCode==Integer.parseInt(mContext.getString(R.string.Talk_Now_alert))){
				PREF_NAME=POST_STORE_PREF;
			}

			//Customer
			if(requestCode==Integer.parseInt(mContext.getString(R.string.Favourite_alert))){
				PREF_NAME=FAV_STORE_PREF;
			}
			if(requestCode==Integer.parseInt(mContext.getString(R.string.Customer_Profile_alert))){
				PREF_NAME=PROFILE_PREF;
			}

			if(requestCode==Integer.parseInt(mContext.getString(R.string.Login_alert))){
				PREF_NAME=APP_LOGIN_PREF;
			}
			
			if(requestCode==Integer.parseInt(mContext.getString(R.string.App_Launch))){
				PREF_NAME=APP_LAUNCH_PREF;
			}

			SharedPreferences pref=mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			Editor editor=pref.edit();
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


/**
 * Cancel Alarm based on request code.
 * @param requestCode
 * @param context
 */
	public void cancelAlarm(int requestCode,Context context){
		Log.i("DATE TIME DIFF", "Notification Broadcast fired Alarm Canceled "+requestCode);
		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled ");
			//Toast.makeText(context, "Alarm Canceled on Back Press",Toast.LENGTH_SHORT).show();
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}


	//Canceling All Alarm
	public void caneclAllAlarm(){
		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender1 = PendingIntent.getBroadcast(mContext, Integer.parseInt(mContext.getString(R.string.Edit_Store_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender1!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled "+mContext.getString(R.string.Edit_Store_alert));
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender1);
		}

		PendingIntent sender2 = PendingIntent.getBroadcast(mContext, Integer.parseInt(mContext.getString(R.string.Talk_Now_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender2!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled "+mContext.getString(R.string.Talk_Now_alert));
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender2);
		}

		/*PendingIntent sender3 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.TalkNow_Rem_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender3!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.TalkNow_Rem_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender3);
		}*/

		PendingIntent sender4 = PendingIntent.getBroadcast(mContext, Integer.parseInt(mContext.getString(R.string.Login_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender4!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled "+mContext.getString(R.string.Login_alert));
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender4);
		}


		PendingIntent sender5 = PendingIntent.getBroadcast(mContext, Integer.parseInt(mContext.getString(R.string.Favourite_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender5!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled "+mContext.getString(R.string.Favourite_alert));
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender5);
		}

		PendingIntent sender6 = PendingIntent.getBroadcast(mContext, Integer.parseInt(mContext.getString(R.string.Customer_Profile_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender6!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled "+mContext.getString(R.string.Customer_Profile_alert));
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender6);
		}

		/*PendingIntent sender7 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Offers_Stream_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender7!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Offers_Stream_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender7);
		}*/

		PendingIntent sender8 = PendingIntent.getBroadcast(mContext, Integer.parseInt(mContext.getString(R.string.App_Launch)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender8!=null){
			Log.i("DATE TIME DIFF", "Alarm Canceled "+mContext.getString(R.string.App_Launch));
			AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender8);
		}

	}

	public boolean isAppLaunchedFired() {
		return mbIsAppLaunchedFired;
	}

	public void setAppLaunchedFired(boolean isAppLaunchedFired) {
		this.mbIsAppLaunchedFired = isAppLaunchedFired;
	}

	public boolean isLoginAlarmFired() {
		return mbIsLoginAlarmFired;
	}

	public String getPlaceId() {
		return msPlaceId;
	}

	public String getPostDate(){
		return msPostDate;
	}

	public boolean placeAlarm()	{
		return mbIsPlaceAlarmReq;
	}

	public boolean postAlarm(){
		return mbIsPosted;
	}

	public boolean favPostAlarm(){
		return mbHasFav;
	}

	public boolean isPlaceAlarmFired() {
		return mbIsPlaceAlarmFired;
	}
	
	public boolean isFavAlarmFired() {
		return mbIsFavAlarmFired;
	}
	
	public boolean isPostAlarmFired() {
		return mbIsPostAlarmFired;
	}
	
	public boolean isProfilAlarmFired() {
		return mbIsProfilAlarmFired;
	}
	
	public boolean isProfileUpdated() {
		return mbIsProfileUpdated;
	}

	//Setting Alarm
	public void alarm()
	{
		try
		{
			checkPref();

			if(mSession.isLoggedIn()) //Merchant
			{
				if( isPlaceAlarmFired()==false)
				{
					if(placeAlarm() == true){
						//Remind Merchant for incomplete place details
						setLocalNotification(7, 0,Integer.parseInt(mContext.getString(R.string.Edit_Store_alert)), false);
					}
					else
					{
						cancelAlarm(Integer.parseInt(mContext.getString(R.string.Edit_Store_alert)),mContext);
					}
				}
				if(isPostAlarmFired() == false){
					if(postAlarm() == false){
						//Remind Merchant for posting offer
						setLocalNotification(3, 0,Integer.parseInt(mContext.getString(R.string.Talk_Now_alert)), false);
						setIsPosted(true);
					}
				}
			}
			if(mSession.isLoggedInCustomer()){
				if( isFavAlarmFired()==false)
				{
					if(favPostAlarm() == false){
						//Remind Customer that he has not marked any store favourite
						setLocalNotification(3, 0,Integer.parseInt(mContext.getString(R.string.Favourite_alert)), false);
					}
					else{
						cancelAlarm(Integer.parseInt(mContext.getString(R.string.Favourite_alert)),mContext);
					}
				}

				if( isProfilAlarmFired()==false){
					if(isProfileUpdated() == false ){
						//Customer has not updated his profile
						setLocalNotification(7, 0, Integer.parseInt(mContext.getString(R.string.Customer_Profile_alert)), false);
					}
					else{
						cancelAlarm(Integer.parseInt(mContext.getString(R.string.Customer_Profile_alert)),mContext);
					}
				}

				if(isLoginAlarmFired() == false){
					cancelAlarm(Integer.parseInt(mContext.getString(R.string.Login_alert)),mContext);
				}
			}
			else{

				if( isLoginAlarmFired()==false)	{
					//Customer launched the app but not signed in
					setLocalNotification(7, 0, Integer.parseInt(mContext.getString(R.string.Login_alert)), false);
				}

			}

			if( isAppLaunchedFired()==false){
				//App Not launched
				setLocalNotification(4, 0, Integer.parseInt(mContext.getString(R.string.App_Launch)), false);
			}

			//Clearing Merchant Prefs if Merchant is Logged out 
			if(!mSession.isLoggedIn()){
				clearPlacePrefs();
				clearPostPrefs();
			}
			//Clearing Customer Prefs if Customer is Logged out 
			if(!mSession.isLoggedInCustomer()){
				clearFavPrefs();
				clearProfilePrefs();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}


	public void checkDateDiff(String alarmDate)
	{
		try
		{
			//Getting Date time stamp of last saved area
			/*DateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");*/
			SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
			Date prevDate=format.parse(alarmDate);
			Date currentDate=format.parse(format.format(new Date()));
			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();
			mlDiffSeconds = diff / 1000 % 60;
			mlDiffMinutes = diff / (60 * 1000) % 60;
			mlDiffHours = diff / (60 * 60 * 1000) % 24;
			mlDiffDays = diff / (24 * 60 * 60 * 1000);
			Log.i("DATE TIME DIFF", "Notification Broadcast fired on DIFF DAYS "+mlDiffDays);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



}

//if([INUserDefaultOperations isMerchantLoggedIn]) { //If merchant is loggedin
//if(![INUserDefaultOperations isEditStoreNotificationAlreadySet])
//	[INUserDefaultOperations setEditStoreNotification]; //After 7 Days
//
//if(![INUserDefaultOperations isTalkNowNotificationAlreadySet])
//	[INUserDefaultOperations setTalkNowNotification]; // After 3 Days from today
//
//} else { // Reminders for Customers
//
//if([INUserDefaultOperations isCustomerLoggedIn]) {
//	if(![INUserDefaultOperations isCustomerSpecialDatesNotificationAlreadySet])
//		[INUserDefaultOperations setCustomerSpecialDatesNotification]; // After 7 days
//
//	if(![INUserDefaultOperations isCustomerFavouriteNotificationAlreadySet])
//		[INUserDefaultOperations setCustomerFavouriteNotification]; // After 3 days
//} else {
//	if(![INUserDefaultOperations isCustomerSignInNotificationAlreadySet])
//		[INUserDefaultOperations setCustomerSignInNotification]; // After 7 days
//}
//if(![INUserDefaultOperations isAppNotLaunchedNotificationAlreadySet])
//	[INUserDefaultOperations setAppNotLaunchedNotification]; // After 4 days
//}


//Remind on Every Friday to Merchant to post offers
/* setWeeklyReminder(Integer.parseInt(getString(R.string.TalkNow_Rem_alert)), 4);*/

//Remind on Every Friday to customer to check offers
/* setWeeklyReminder(Integer.parseInt(getString(R.string.Offers_Stream_alert)), 6);*/

