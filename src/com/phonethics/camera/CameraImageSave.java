package com.phonethics.camera;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class CameraImageSave {
	
	private String fileName = "temp_photo_crop.jpg";
	
	
	public void saveBitmapToFile(Bitmap bitmap){
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

		//String filePath = "/sdcard/temp_photo.jpg";
		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + fileName);
		
		try{
			f.createNewFile();
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
			fo.close();
		}
		catch(Exception e){ Log.e("Bitmap error","File not found to save image"); }
	}
	
	
	public void deleteFromFile(){
		
		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + fileName);
		
		f.delete();
		
	}
	
	public Bitmap getBitmapFromFile(int size){
		Bitmap bitmap = null;
		String filePath = Environment.getExternalStorageDirectory()
				+ File.separator + fileName;
		

		try{
			FileInputStream in = new FileInputStream(filePath);
			BufferedInputStream buf = new BufferedInputStream(in);
			bitmap = BitmapFactory.decodeStream(buf);
			bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
		}
		catch(Exception e){
			Log.e("Exception","getBitmapFromFile() -> CameraSaveImage.java");
			e.printStackTrace();
		}
		
		return bitmap;
	}
	
	public String getImagePath(){
		return Environment.getExternalStorageDirectory()
				+ File.separator + fileName;
	}
	
	//Getting Image 
	public  Bitmap getThumbnail(Context context,Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();

		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;

		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);


		input.close();
		return bitmap;
	}
	
	public Bitmap changeImageOrientation(int orientation, Bitmap bitmap){

		if(orientation == 0){
			bitmap = ImageEffects.rotate(bitmap, 90);
		}
		else if(orientation == 180){
			bitmap = ImageEffects.rotate(bitmap, -90);
		}
		else if(orientation == 90){
			bitmap = ImageEffects.rotate(bitmap, 180);
		}

		return bitmap;
	}
	
	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}


}
