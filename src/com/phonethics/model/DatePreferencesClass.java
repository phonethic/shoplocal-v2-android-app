package com.phonethics.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class DatePreferencesClass {

	private Activity mContext;
	private SharedPreferences mPrefs;
	private Editor mEditor;

	private String PREF_NAME;
	private String PREF_KEY;

	private long mlDiffSeconds;
	private long mlDiffMinutes;
	private long mlDiffHours;

	private long mlDiffDays=0;

	private SimpleDateFormat mFormat;

	private String date_time;

	public DatePreferencesClass(Activity context,String PREF_NAME,String PREF_KEY)
	{
		mContext = context;
		this.PREF_NAME=PREF_NAME;
		this.PREF_KEY=PREF_KEY;

		mFormat=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		
	}


	public long getDiffSeconds() {
		return mlDiffSeconds;
	}

	public long getDiffMinutes() {
		return mlDiffMinutes;
	}

	public long getDiffHours() {
		return mlDiffHours;
	}

	public long getDiffDays() {
		return mlDiffDays;
	}

	public void checkDateDiff()
	{
		try
		{
			//Getting Date time stamp of last saved area
			SharedPreferences prefs=mContext.getSharedPreferences(PREF_NAME, 1);

			date_time=prefs.getString(PREF_KEY, mFormat.format(new Date()).toString());

			Date prevDate=mFormat.parse(date_time);
			Date currentDate=mFormat.parse(mFormat.format(new Date()));

			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();

			mlDiffSeconds = diff / 1000 % 60;
			mlDiffMinutes = diff / (60 * 1000) % 60;
			mlDiffHours = diff / (60 * 60 * 1000) % 24;

			mlDiffDays = diff / (24 * 60 * 60 * 1000);

			Log.i("DATE TIME DIFF", "DATE TIME DIFF STAMP"+date_time);

//			Log.i("DATE TIME DIFF", "DATE TIME DIFF "+diffDays);



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void setDateInPref()
	{
		SharedPreferences prefs=mContext.getSharedPreferences(PREF_NAME,1);
		Editor editor=prefs.edit();
		editor.putString(PREF_KEY,mFormat.format(new Date()));
		editor.commit();
	}
	
	public boolean isPrefDatePresent()
	{
		SharedPreferences prefs=mContext.getSharedPreferences(PREF_NAME,1);
		String date=prefs.getString(PREF_KEY,"");
		if(date.length()==0)
		{
			return false;
		}
		return true;
	}




}
