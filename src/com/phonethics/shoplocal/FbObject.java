package com.phonethics.shoplocal;

public class FbObject {
	
	private String storeTitle = "";
	private String storeImage = "";
	private String storeUrl	= "";
	private String storeDescription = "";
	
	
	
	public String getStoreTitle() {
		return storeTitle;
	}
	public void setStoreTitle(String storeTitle) {
		this.storeTitle = storeTitle;
	}
	public String getStoreImage() {
		return storeImage;
	}
	public void setStoreImage(String storeImage) {
		this.storeImage = storeImage;
	}
	public String getStoreUrl() {
		return storeUrl;
	}
	public void setStoreUrl(String storeUrl) {
		this.storeUrl = storeUrl;
	}
	public String getStoreDescription() {
		return storeDescription;
	}
	public void setStoreDescription(String storeDescription) {
		this.storeDescription = storeDescription;
	}
	
	
	

}
