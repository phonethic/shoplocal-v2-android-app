package com.phonethics.shoplocal;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class SplitLoginSignUp extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener
{

	private boolean mbIsAddStore;
	private Activity mContext;

	private Dialog mTermsConditionDialog;
	private Dialog mPrivacyDialog;
	private Dialog mDisclaimerDialog;

	private Button mBtnDisclaimer;
	private Button mBtnTermAccept;
	private Button mBtnTermCancel;
	private Button mBtnPrivacyDone;
	private Button mBtnLogin;
	private Button mBtnSignup;

	private TextView mTxtDesclaimer;
	private TextView mTxtPrivacy;
	private TextView mTxtCountryPicker;
	private TextView mTxtForgotPassword;
	private TextView mTxtSignUpText;
	private TextView mTxtMerchantPlaceHolder;
	private TextView mTxtHeaderInfo;
	private ImageView mImgInfo;

	private WebView mPrivacyWebView;
	private WebView mTermsWebView;

	private EditText mEdtMerchantName;
	private EditText mEdtMobileNo;
	private EditText mEdtPassword;

	private ProgressBar mProgress;

	private CheckBox mCbxAccept;

	private String 	msJsonArray;
	private String msCountryCode;

	private AlertDialog mAlert = null;

	CountryAdapter	  mAdapter;
	ArrayList<String> mArrCountryarr;
	ArrayList<String> mArrCountryarrSearch;
	ArrayList<String> mArrCountryCode;
	ArrayList<String> mArrCountryCodeSearch;

	//User Name
	public String KEY_USER_NAME="mobileno";
	//Password
	public String KEY_PASSWORD="password";

	HashMap<String, String> user_details;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=this;
		setContentView(R.layout.activity_split_login_sign_up);
		hideDrawerLayout();
		setTitle("Merchant Login");

		mTermsConditionDialog=new Dialog(mContext);
		mPrivacyDialog=new Dialog(mContext);

		mPrivacyDialog.setContentView(R.layout.privacy_policy);
		mPrivacyDialog.setTitle("Privacy Policy");
		mPrivacyDialog.setCancelable(true);
		mPrivacyDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mTermsConditionDialog.setContentView(R.layout.termsandcondition);
		mTermsConditionDialog.setTitle("Terms & Conditions");
		mTermsConditionDialog.setCancelable(false);
		mTermsConditionDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mDisclaimerDialog=new Dialog(mContext);
		mDisclaimerDialog.setContentView(R.layout.disclaimer_dialog);
		mDisclaimerDialog.setTitle("Why should I login?");
		mDisclaimerDialog.setCancelable(true);
		mDisclaimerDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mCbxAccept=(CheckBox)mTermsConditionDialog.findViewById(R.id.chkBoxAccept);

		mTxtPrivacy=(TextView)mDisclaimerDialog.findViewById(R.id.privacy);
		mTxtCountryPicker=(TextView)findViewById(R.id.countryPicker);
		mTxtForgotPassword=(TextView)findViewById(R.id.forgotPassword);
		mTxtSignUpText=(TextView)findViewById(R.id.signUpText);
		mTxtMerchantPlaceHolder=(TextView)findViewById(R.id.txtMerchantPlaceHolder);
		mTxtHeaderInfo=(TextView)findViewById(R.id.txtHeaderInfo);
		mTxtDesclaimer=(TextView)mDisclaimerDialog.findViewById(R.id.desclaimerText);

		mBtnDisclaimer=(Button)mDisclaimerDialog.findViewById(R.id.btnDesclaimer);
		mBtnTermAccept=(Button)mTermsConditionDialog.findViewById(R.id.btnTermDone);
		mBtnTermCancel=(Button)mTermsConditionDialog.findViewById(R.id.btnTermCancel);
		mBtnPrivacyDone=(Button)mPrivacyDialog.findViewById(R.id.buttonOk);
		mBtnLogin=(Button)findViewById(R.id.btnLoginMerchant);
		mBtnSignup=(Button)findViewById(R.id.btnSingupMerchant);

		mImgInfo=(ImageView)findViewById(R.id.loginMerchant_info);

		mEdtMerchantName=(EditText)findViewById(R.id.edtDialogMerchantName);
		mEdtMobileNo=(EditText)findViewById(R.id.edtDialogMobileNo);
		mEdtPassword=(EditText)findViewById(R.id.edtDialogPassword);

		mTermsWebView=(WebView)mTermsConditionDialog.findViewById(R.id.tcWeb);
		mPrivacyWebView=(WebView)mPrivacyDialog.findViewById(R.id.privacyWeb);

		mProgress=(ProgressBar)findViewById(R.id.progLogin);

		mPrivacyWebView.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));
		mTermsWebView.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));

		mTxtPrivacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
		mTxtDesclaimer.setText(getResources().getString(R.string.mobile_desclaimer));
		mBtnLogin.setText("Proceed");




		/*		btnLoginCustomer=(Button)findViewById(R.id.btnLoginCustomer);


		btnSignUpCustomer=(Button)findViewById(R.id.btnSingupCustomer);*/


		/*signUpText.getBackground().setAlpha(130);
		forgotPassword.getBackground().setAlpha(130);*/

		mTxtSignUpText.setTypeface(tf);
		mTxtForgotPassword.setTypeface(tf);
		mTxtMerchantPlaceHolder.setTypeface(tf);
		mEdtMobileNo.setTypeface(tf);
		mEdtPassword.setTypeface(tf);
		mEdtMerchantName.setTypeface(tf);


		mTxtForgotPassword.setTextColor(Color.parseColor("#c4c4c4"));
		mTxtSignUpText.setTextColor(Color.parseColor("#c4c4c4"));
		mEdtMobileNo.setTextColor(Color.parseColor("#9c9c9c"));
		mEdtPassword.setTextColor(Color.parseColor("#9c9c9c"));
		mTxtMerchantPlaceHolder.setTextColor(Color.parseColor("#715b57"));
		mTxtCountryPicker.setTextColor(Color.parseColor("#9c9c9c"));
		mEdtMerchantName.setTextColor(Color.parseColor("#9c9c9c"));

		mEdtMerchantName.setHint("Owner/Manager's Name");
		mTxtForgotPassword.setText(Html.fromHtml("<u>"+getResources().getString(R.string.forgot_password)+"</u>"));

		mEdtMobileNo.setSelection(mEdtMobileNo.getText().length());
		mTxtForgotPassword.setVisibility(View.GONE);


		mImgInfo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.mobile_desclaimer))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				})
				;
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}
		});

		mBtnPrivacyDone.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mPrivacyDialog.dismiss();
			}
		});

		mTxtPrivacy.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mDisclaimerDialog.dismiss();
				mPrivacyDialog.show();
			}
		});

		mTxtHeaderInfo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mDisclaimerDialog.show();
			}
		});

		mBtnDisclaimer.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				mDisclaimerDialog.dismiss();
			}
		});
		//chkBoxAccept.setChecked(true);
		mBtnTermAccept.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mCbxAccept.setChecked(true);
				if(mCbxAccept.isChecked())
				{
					mTermsConditionDialog.dismiss();
				}
				proceedWithoutNext();

			}
		});
		mBtnTermCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mTermsConditionDialog.dismiss();
				mCbxAccept.setChecked(false);
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getString(R.string.terms_and_condition_retry))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Try again",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog2,int id) {
						mTermsConditionDialog.show();
					}
				})
				.setNegativeButton("Do not register",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog2,int id) {
						dialog2.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}
		});

		try
		{
			msJsonArray = getCountriesFromAsset();
			getCountryCode();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mTxtCountryPicker.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				parseJsonData(msJsonArray);
				mAlert.show();
			}
		});

		mBtnSignup.setText("Proceed");
		mBtnSignup.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mInternet.isNetworkAvailable())
				{
					proceed();
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		user_details=new HashMap<String, String>();
		user_details=mSessionManager.getUserDetails();

		if(user_details.get(KEY_USER_NAME)!="" && user_details.get(KEY_USER_NAME).toString().length()!=0)
		{
			Log.i("MOBILE PREFS", "MOBILE PREFS "+user_details.get(KEY_USER_NAME).toString().substring(msCountryCode.length()));
			Log.i("MOBILE PREFS", "MOBILE PREFS Name "+user_details.get(KEY_USER_NAME).toString());
			mEdtMobileNo.setText(user_details.get(KEY_USER_NAME).toString().substring(msCountryCode.length()));
			//password.setText(user_details.get(KEY_PASSWORD).toString());
		}
	}

	void getCountryCode()
	{
		SharedPreferences pref=getSharedPreferences("merchantCountryCode",0);
		msCountryCode=pref.getString("mCountryCode", "91");
		mTxtCountryPicker.setText("+"+msCountryCode);

	}

	public void parseJsonData(String jsonString){

		/*countryModels 		= new ArrayList<CountryModel>();*/
		mArrCountryarr 			= new ArrayList<String>();
		mArrCountryCode 		= new ArrayList<String>();
		try{
			JSONArray jArr = new JSONArray(jsonString);
			for(int i=0;i<jArr.length();i++){
				CountryModel	countryModel = new CountryModel();
				JSONObject jObj = jArr.getJSONObject(i);
				countryModel.setcName(jObj.getString("name"));
				mArrCountryarr.add(jObj.getString("name"));
				countryModel.setcCode(jObj.getString("dial_code"));
				mArrCountryCode.add(jObj.getString("dial_code").replace(" ", ""));
				countryModel.setcShort(jObj.getString("code"));
				/*countryModels.add(countryModel);*/
			}

			mArrCountryarrSearch = mArrCountryarr;
			mArrCountryCodeSearch = mArrCountryCode;
			initDialog();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void initDialog(){

		try
		{
			AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
			alertDialog.setTitle("Select County Code");
			alertDialog.setCancelable(true);
			View view = mContext.getLayoutInflater().inflate(R.layout.dialog_list, null);
			final ListView listView = (ListView) view.findViewById(R.id.list_county_code);
			final EditText editSearch = (EditText) view.findViewById(R.id.edit_search);

			listView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
						long arg3) {
					//setText(countryarrSearch.get(pos), countryCodeSearch.get(pos));

					setCountryCode(mArrCountryarrSearch.get(pos), mArrCountryCodeSearch.get(pos));
					mAlert.dismiss();

				}
			});
			mAdapter = new CountryAdapter(mContext, mArrCountryarr,mArrCountryCode);
			listView.setAdapter(mAdapter);
			editSearch.addTextChangedListener(new TextWatcher() {


				public void onTextChanged(CharSequence s, int start, int before, int count) {
					ArrayList<String> countryarrSearch_1 = new ArrayList<String>();
					ArrayList<String> countryCodeSearch_1 = new ArrayList<String>();

					int textLength  = editSearch.getText().toString().length();

					for(int i=0;i<mArrCountryarr.size();i++){
						if(textLength<= mArrCountryarr.get(i).length()){
							if(editSearch.getText().toString().equalsIgnoreCase((String)mArrCountryarr.get(i).subSequence(0, textLength))){
								countryarrSearch_1.add(mArrCountryarr.get(i));
								countryCodeSearch_1.add(mArrCountryCode.get(i));						
							}
						}
					}

					mArrCountryarrSearch = countryarrSearch_1;
					mArrCountryCodeSearch = countryCodeSearch_1;

					mAdapter = new CountryAdapter(mContext,  mArrCountryarrSearch,mArrCountryCodeSearch);
					mAdapter.notifyDataSetChanged();
					listView.setAdapter(mAdapter);
				}


				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}
				public void afterTextChanged(Editable s) {
				}
			});
			alertDialog.setView(view);
			mAlert = alertDialog.create();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void setCountryCode(String country, String code){
		try
		{
			mTxtCountryPicker.setText(code);
			msCountryCode=code.substring(1);
			setPrefernces();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setPrefernces()
	{
		SharedPreferences pref=getSharedPreferences("merchantCountryCode", 0);
		Editor editor=pref.edit();
		editor.putString("mCountryCode", msCountryCode);
		editor.commit();
	}

	public String getCountriesFromAsset(){
		String fileString = "";
		try{
			InputStream is = getAssets().open("county_code.txt");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			fileString = new String(buffer,"UTF-8");
			//Log.d("", "JsonString >> "+fileString);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return fileString;
	}

	void proceed()
	{
		if(mEdtMobileNo.getText().toString().length()==0){
			showToast(getResources().getString(R.string.mobileAlert));
		}
		else if(mEdtMobileNo.getText().toString().length()<10){
			showToast(getResources().getString(R.string.mobileLengthAlert));
		}
		else{
			if(mCbxAccept.isChecked()){
				user_details=mSessionManager.getUserDetails();
				registerMerchant();
			}
			else{
				mTermsConditionDialog.show();
			}
		}
	}

	void proceedWithoutNext()
	{
		if(mEdtMobileNo.getText().toString().length()==0){
		}
		else if(mEdtMobileNo.getText().toString().length()<10){
		}
		else
		{
			if(mCbxAccept.isChecked()){
				registerMerchant();
			}
		}
	}

	void registerMerchant()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				EventTracker.logEvent("MSignUp_Proceed", false);
				
				PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);

				JSONObject json = new JSONObject();
				json.put("mobile", msCountryCode+mEdtMobileNo.getText().toString());
				json.put("name", mEdtMerchantName.getText().toString());
				json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
						+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
				String msUrl=Config.MERCHANT_REGISTER_URL;
				new ShoplocalAsyncTask(this, "post", "merchant", false, json, "register").execute(msUrl);
				mBtnSignup.setEnabled(false);
			}catch(Exception ex){
				ex.printStackTrace();

			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishTo();
		return true;
	}

	@Override
	public void onAsyncStarted() {
		mProgress.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mBtnSignup.setEnabled(true);
		mProgress.setVisibility(View.GONE);
		if(tag.equalsIgnoreCase("register")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);

				String status=jsonObject.getString("success");
				String passmsg=jsonObject.getString("message");
				String error_code=jsonObject.getString("code");

				if(error_code.equalsIgnoreCase("-119")){ //New user
					EventTracker.logEvent("MSignUp_New", false);

					Log.i("Signup ", "MSignup success true");
				}
				else if(error_code.equalsIgnoreCase("-102")){ //Returning user
					EventTracker.logEvent("MSignUp_Return", false);
					Log.i("Signup ", "MSignup success false");
				}
				if(status.equalsIgnoreCase("true")){
					Intent intent=new Intent(this, SignupMerchant.class);
					intent.putExtra("server_message", passmsg);
					intent.putExtra("mobileno", msCountryCode+mEdtMobileNo.getText().toString());
					startActivityForResult(intent, 4);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else{
					showToast(passmsg);
				}
			}catch(Exception  ex)
			{
				ex.printStackTrace();
			}
		}
	}

	void debug(String msg){
		Log.i("Merchant Signup ","Merchant Signup "+msg);
	}

	@Override
	public void onError(int errorCode) {
		mBtnSignup.setEnabled(true);
		mProgress.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}

	public void onBackPressed() {
		finishTo();
	}

	void finishTo()
	{
		Intent intent=new Intent();
		intent.putExtra("isAddStore", mbIsAddStore);
		setResult(4, intent);
		this.finish();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mBtnSignup.setEnabled(true);
		if(requestCode==4){
			if(mSessionManager.isLoggedIn()){
				mbIsAddStore=data.getBooleanExtra("isAddStore",false);
				finishTo();
				overridePendingTransition(0,R.anim.shrink_fade_out_center);
			}
		}
	}
}
