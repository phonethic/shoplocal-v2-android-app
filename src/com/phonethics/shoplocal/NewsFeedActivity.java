package com.phonethics.shoplocal;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.CallListDialog;


public class NewsFeedActivity extends ShoplocalActivity implements ListClickListener,OnLastItemVisibleListener, OnRefreshListener<ListView>, OnItemClickListener, ShoplocalAsyncTaskStatusListener
{
	private PullToRefreshListView mList;
	private TextView mTxtCounter;
	private ProgressBar progress;
	private OffersAdapter mAdapter=null;
	private String msQuery="";

	private int index=-1;
	private int currentPage = 0;
	private int totalPages=0;

	private String msTempPostId="";
	private Activity mContext;

	private String msStoreId="";
	private String msPostId="";
	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msStoreName="";
	private String msImageLink="";
	private String msIsOffer="";
	private String msOfferValidTill="";
	private String msPostTitle="";
	private String msPostDescription="";
	private String msShareVia="";
	private String msCallType="";

	private ArrayList<String> mArrCall=new ArrayList<String>();

	private ProgressDialog mProgressDialog;

	private long mlBackPressed;

	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;

	private String msActiveAreaName="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		mContext=this;

		msActiveAreaName=mDbUtil.getActiveArea();

		mProgressDialog=new ProgressDialog(mActivity);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");

		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());
		progress=(ProgressBar)findViewById(R.id.progress);
		mList=(PullToRefreshListView)findViewById(R.id.dafault_listSearchResult);
		mTxtCounter=(TextView)findViewById(R.id.txtCounter);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		mList.setLayoutAnimation(controller);
		mAdapter=new OffersAdapter(this,mList,mTxtCounter,this);
		mList.setAdapter(mAdapter);
		mList.setOnLastItemVisibleListener(this);
		mList.setOnRefreshListener(this);
		mList.setOnItemClickListener(this);
		setTitle(getResources().getString(R.string.itemDiscover));
		createNavigationList();
		showSearchBox(true);

		rateDialog();

	}

	private void showProgressDialog(){
		mProgressDialog.show();
	}

	void rateDialog()
	{
		long sessionTime=getSessionLength(); //get last session length in seconds
		try {
			if(sessionTime>300 && isAppRated()==false){ 
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
				alertDialogBuilder.setTitle("Rate Shoplocal");
				alertDialogBuilder
				.setMessage("Dear user, \nPlease take a moment to rate this application. Thanks!")
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Rate Now",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						setAppRated(true);
						Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=com.phonethics.shoplocal"));
						startActivity(intent);
						dialog.dismiss();
					}
				})
				.setNegativeButton("Remind me later",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	public void onItemClick(AdapterView<?> arg0, View view, final int position, long arg3) {
		offerDetails(position);
		openPostDetails();
	}
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		if(mInternet.isNetworkAvailable()){
			mAdapter.setMbRefreshing(true);
			mAdapter.setmPage(0);
			mAdapter.getData(Config.OFFERS_STREAM_URL +"&area_id="+Config.MY_AREA + "&page=" + 0 
					+ "&count="+mAdapter.ITEMS_PER_PAGE,"",false,"Offers");
		}else{
			showToast(getString(R.string.noInternetConnection));
		}

	}
	@Override
	public void onLastItemVisible() {
		if(mInternet.isNetworkAvailable()){
			Cursor cursor=null;
			try{
				msQuery="Select * from OFFERS_RECORDS where area_id="+Config.MY_AREA;
				cursor=new ShoplocalCursorLoader(this, msQuery).loadInBackground();

				if(cursor!=null){

					cursor.moveToFirst();

					if(cursor.getCount()!=0){
						index=cursor.getColumnIndex("current_page");
						debug(cursor.getCount()+" index "+index);

						if(index>=0){
							currentPage=cursor.getInt(index);
						}

						index=cursor.getColumnIndex("total_page");

						if(index>=0){
							totalPages=cursor.getInt(index);
						}

						debug("current page "+currentPage+" total pages "+totalPages);

						if(currentPage<totalPages){
							mAdapter.setmPage(currentPage);
							mAdapter.getData(Config.OFFERS_STREAM_URL +"&area_id="+Config.MY_AREA + "&page=" + (currentPage+1) + "&count="+mAdapter.ITEMS_PER_PAGE,"",false,"offers");
						}
						else{
							debug("current page "+currentPage+" total pages "+totalPages);
						}
					}
					cursor.close();
				}
			}catch(CursorIndexOutOfBoundsException c){
				c.printStackTrace();
			}
			finally{
				cursor.close();
			}
		}else{
			showToast(getString(R.string.noInternetConnection));
		}
	}

	void debug(String s){
		Log.i("MarketPlaceActivity", "MarketPlaceActivity "+s);
	}


	void offerDetails(int position)
	{
		int index=position-1;
		String storeId="";
		String postId="";
		try
		{
			Cursor cursor=mAdapter.getCursor();
			if(cursor!=null && cursor.getCount()!=0){
				cursor.moveToPosition(index);
				msStoreName=cursor.getString(cursor.getColumnIndex("store_name"));
				msPostTitle=cursor.getString(cursor.getColumnIndex("title"));
				msPostDescription=cursor.getString(cursor.getColumnIndex("description"));
				msStoreId=cursor.getString(cursor.getColumnIndex("id"));
				msPostId=cursor.getString(cursor.getColumnIndex("post_id"));
				msMobNo1=cursor.getString(cursor.getColumnIndex("mob_no_1"));
				msMobNo2=cursor.getString(cursor.getColumnIndex("mob_no_2"));
				msMobNo3=cursor.getString(cursor.getColumnIndex("mob_no_3"));
				msTelNo1=cursor.getString(cursor.getColumnIndex("tel_no_1"));
				msTelNo2=cursor.getString(cursor.getColumnIndex("tel_no_2"));
				msTelNo3=cursor.getString(cursor.getColumnIndex("tel_no_3"));
				msImageLink=cursor.getString(cursor.getColumnIndex("image_url_1"));
				msIsOffer=cursor.getString(cursor.getColumnIndex("has_offer"));
				msOfferValidTill=cursor.getString(cursor.getColumnIndex("offer_date_time"));
			}
			mArrCall.clear();
			mCalldailog.setTitle(msStoreName);
			if(msMobNo1.length()>5){
				mArrCall.add("+"+msMobNo1);
			}
			if(msMobNo2.length()>5){
				mArrCall.add("+"+msMobNo2);
			}
			if(msMobNo3.length()>5){
				mArrCall.add("+"+msMobNo3);
			}
			if(msTelNo1.length()>5){
				mArrCall.add(msTelNo1);
			}
			if(msTelNo2.length()>5){
				mArrCall.add(msTelNo2);
			}
			if(msTelNo3.length()>5){
				mArrCall.add(msTelNo3);
			}
			mCallAdapter.notifyDataSetChanged();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void openPostDetails()
	{
		try
		{
			EventTracker.logEvent("OfferStream_Details", false);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		Intent intent=new Intent(this, OffersBroadCastDetails.class);
		intent.putExtra("storeName",msStoreName);
		intent.putExtra("title",msPostTitle);
		intent.putExtra("body",msPostDescription);
		intent.putExtra("PLACE_ID",msStoreId);
		intent.putExtra("POST_ID",msPostId);
		intent.putExtra("fromCustomer", true);
		intent.putExtra("mob1",msMobNo1);
		intent.putExtra("mob2",msMobNo2);
		intent.putExtra("mob3",msMobNo3);
		intent.putExtra("tel1",msTelNo1);
		intent.putExtra("tel2",msTelNo2);
		intent.putExtra("tel3",msTelNo3);
		intent.putExtra("photo_source",msImageLink);
		intent.putExtra("USER_LIKE","-1");
		startActivity(intent);
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	}
	@Override
	public void listItemClick(int position,String msPostId, String tag) {
		this.msTempPostId=msPostId;
		if(tag.equalsIgnoreCase("like")){
			offerDetails(position+1);
			if(mSessionManager.isLoggedInCustomer()){
				likePost(msPostId);
			}else{
				loginAlert(getResources().getString(R.string.postFavAlert), 1);
			}
		}else if(tag.equalsIgnoreCase("share")){
			offerDetails(position+1);
			if(mInternet.isNetworkAvailable()){
				showDialog();
			}else{
				showToast(getString(R.string.noInternetConnection));
			}
		}else if(tag.equalsIgnoreCase("call")){
			offerDetails(position+1);
			if(mArrCall.size()!=0){

				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("Source","OfferStream");
				callClicked.put("storeName",msStoreName);
				debug(msStoreName);
				EventTracker.logEvent("StoreCall", callClicked);

				msCallType="StoreCall";
				callContactApi(msPostId);
				mCalldailog.show();
			}else{
				showToast("No numbers to call");
			}
		}
	}

	void callContactApi(String postId)
	{
		try
		{
//			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);

					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put("post_id", postId);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);

					debug(jsonObject.toString());

					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
//			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {


			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("Source","OfferStream");
			callClicked.put("storeName",msStoreName);
			callClicked.put("Number",mArrCall.get(position));
			debug(msStoreName);
			EventTracker.logEvent("StoreCallDone", callClicked);

			msCallType="StoreCallDone";
			callContactApi(msPostId);
			likePlace(msStoreId);
			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();

		}

	}

	void likePost(String msPostId)
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				debug(msActiveAreaName);
				Map<String,String> eventLike=new HashMap<String, String>();
				eventLike.put("Source", "OfferStream");
				eventLike.put("AreaName",msActiveAreaName);
				EventTracker.logEvent("Offer_Like", eventLike);

				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("post_id", msTempPostId);
				json.put("auth_id", Config.CUST_AUTH_ID);
				String msUrl=Config.POST_LIKE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"likePost").execute(msUrl);
			}else
			{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	void likePlace(String postId)
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", postId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlaceOnCall").execute(msUrl);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	void sharePost(String msPostId)
	{
		try
		{
			try {
				if(mInternet.isNetworkAvailable()){

					Map<String,String> eventShare=new HashMap<String, String>();
					eventShare.put("Source", "OfferStream");
					eventShare.put("AreaName",msActiveAreaName);
					EventTracker.logEvent("Offer_Share", eventShare);

					JSONObject json = new JSONObject();
					json.put("post_id", msTempPostId);
					json.put("via", msShareVia);
					String msUrl=Config.POST_SHARE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"sharePost").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	@Override
	public void onAsyncStarted() {
		if(!mProgressDialog.isShowing()){
			showProgressDialog();
		}
	}
	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mProgressDialog.dismiss();
		if(tag.equalsIgnoreCase("likePost")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;
				if(status.equalsIgnoreCase("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){
					try
					{
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(msStoreName);
						if(msImageLink.length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+msImageLink);
						}
						fbObj.setStoreDescription(msPostDescription);
						String encodedurl = URLEncoder.encode(msStoreName,"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+msStoreId);
						//FacebookOperations.likeOnFb(mActivity, fbObj, getString(R.string.fb_store_object));
						FacebookOperations.FavThisOnFb(mActivity, fbObj, getString(R.string.fb_offer_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					String message=jsonObject.getString("message");
					showToast(message);
				}

			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}if(tag.equalsIgnoreCase("sharePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;

				if(status.equalsIgnoreCase("true")){

					if(mbFaceBookShare==false)
					{
						try
						{
							FbObject fbObj=new FbObject();
							fbObj.setStoreTitle(msStoreName);
							if(msImageLink.length()==0){
								fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
							}else{
								fbObj.setStoreImage(Config.IMAGE_URL+msImageLink);
							}
							fbObj.setStoreDescription(msPostDescription);
							String encodedurl = URLEncoder.encode(msStoreName,"UTF-8");
							debug(Config.FACEBOOK_SHARE_URL+msSlug);
							fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+msStoreId);
							FacebookOperations.shareThisInfo(mActivity, fbObj, getString(R.string.fb_offer_object));
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("likePlaceOnCall")){
			
		}
	}
	@Override
	public void onError(int errorCode) {
		mProgressDialog.dismiss();
		showToast(getResources().getString(R.string.errorMessage));
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==1)
				{
					Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
					mContext.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	void showDialog(){
		try
		{

			String shareBody= "";
			String validTill="";
			String contact="";
			String contact_lable="\nContact:";
			if(msIsOffer.length()!=0 && msIsOffer.equalsIgnoreCase("1")){
				try
				{
					DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
					DateFormat dt2=new SimpleDateFormat("MMM");
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

					Date date_con = (Date) dt.parse(msOfferValidTill.toString());
					Date date=dateFormat.parse(msOfferValidTill.toString());

					DateFormat date_format=new SimpleDateFormat("hh:mm a");
					Calendar cal = Calendar.getInstance();
					cal.setTime(date_con);
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH);
					int day = cal.get(Calendar.DAY_OF_MONTH);
					validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);

				}catch(Exception ex)
				{
					ex.printStackTrace();
					validTill="";
				}
			}
			else{
				validTill="";
			}
			try
			{
				if(msMobNo1.length()>0){
					contact="+"+msMobNo1;
				} 
				if(msMobNo2.length()>0){
					contact+=" +"+msMobNo2;
				}
				if(msMobNo3.length()>0){
					contact+=" +"+msMobNo3;
				}
				if(msTelNo1.length()>0){
					contact+=" "+msTelNo1;
				}
				if(msTelNo2.length()>0){
					contact+=" "+msTelNo2;
				}
				if(msTelNo3.length()>0){
					contact+=" "+msTelNo3;
				}
				if(contact.length()==0){
					contact_lable="";
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			shareBody=msStoreName + " - " + "Offers" + " : " + "\n\n"+msPostTitle + "\n" + msPostDescription +validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
			offerShareDialog(msStoreName, msImageLink, shareBody);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(requestCode==2){
			createDrawer();
		}
		if(requestCode==4){
			if(mSessionManager.isLoggedIn()){
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore){
					Intent intent=new Intent(mActivity, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mActivity.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mActivity.finish();
				}
				else{
					Intent intent=new Intent(mActivity,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}

		if(requestCode == 3){
			msShareVia="Android";
			sharePost(msTempPostId);
		}
		try
		{
			mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					msShareVia="Android-Facebook";
					sharePost(msTempPostId);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public void onBackPressed() {
		if(mlBackPressed+2000 >System.currentTimeMillis()){
			finishTo();
		}else{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			mlBackPressed=System.currentTimeMillis();
		}
	}

	void finishTo()
	{
		mLocalNotification.alarm();
		if(isAppRated()==false){
			setSession(getSessionLength()+getTimeDiff());//Add session  length. 
		}
		mDbUtil.close();
		AppLaunchPrefs.clearPrefs(mContext);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}	


}