package com.phonethics.shoplocal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.phonethics.adapters.LocationListAdapter;


public class LocationActivity extends ShoplocalActivity implements LocationListener , ShoplocalAsyncTaskStatusListener, OnItemClickListener, DBListener, UpadateAreaListener
{

	private GoogleMap supportMap;
	private LocationManager mLocationManager;
	private Location mLocation;

	private String msCountryCode;
	private String ISO_URL;
	private String msActiveAreaId="";
	private String msSelectedAreaId="";
	private String msSelectedAreaName="";
	private String AREA_DATE_TIME_STAMP="AREA_DATE_TIME_STAMP";
	private String ACTIVE_AREA_PREF="ACTIVE_AREA";  
	private String ACTIVE_AREA_KEY="AREA_ID";
	private String msDateTime;
	private String KEY="DATE_TIME_STAMP";

	private long mlDiffDays;


	private	SimpleDateFormat mFormat;

	private Dialog mLocationDialog;
	private Dialog mLocationPrompt;
	private Activity mContext;
	private AlertDialog.Builder alertDialog;
	private AlertDialog alert;

	private ListView mListLocation;

	private TextView mTxtPlaceChooser;
	private TextView mTxtMerchantCount;
	private TextView mTxtPromptTitle;
	private TextView mTxtPromptText;

	private Button mBtnGoto;
	private Button mBtnPromptFindMe;
	private Button mBtnPromptChoose;
	private Button mBtnPromptCancel;
	private Button mBtnPromptDone;

	private ProgressBar mProgress;

	private ArrayList<String>mArrId=new ArrayList<String>();
	private ArrayList<String>mArrAreaName=new ArrayList<String>();
	private ArrayList<String>mArrLatitude=new ArrayList<String>();
	private ArrayList<String>mArrLongitude=new ArrayList<String>();
	private ArrayList<String>mArrMerchantCount=new ArrayList<String>();

	private DrawerLayout mDrawerLayout;
	private View mFooterView;
	private ExpandableListView mDrawerList;

	private boolean mbIsGpsEnabled=false;
	private boolean mbIsNetworkEnabled=false;
	private boolean mbIsNearestApiCalled=false;
	private boolean mbShowPrompt=true;

	private Double mdLatitude=0.0;
	private Double mdLongitude=0.0;
	private long back_pressed;

	private int miAreaId=0;
	private String msNearestArea="";

	private	Map<String, String> eventTrackingParam=new HashMap<String, String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);
		mContext=this;

		setTitle(getResources().getString(R.string.itemChangeLocation));

		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		msCountryCode = tm.getSimCountryIso();
		ISO_URL = "iso-code=" + "in";
		debug(msCountryCode);
		debug("My area"+Config.MY_AREA);

		FragmentManager fmanager = getSupportFragmentManager();
		Fragment fragment = fmanager.findFragmentById(R.id.map);
		SupportMapFragment supportmapfragment = (SupportMapFragment)fragment;
		supportMap = supportmapfragment.getMap();
		supportMap.setMyLocationEnabled(true);
		supportMap.animateCamera(CameraUpdateFactory.zoomTo(13));

		mFormat=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerList=(ExpandableListView)findViewById(R.id.drawerList);

		setDrawer(mDrawerLayout,mDrawerList);

		mLocationDialog=new Dialog(mContext);
		mLocationDialog.setContentView(R.layout.layout_location_chooser_dialog);
		mLocationDialog.setCancelable(true);
		mLocationDialog.setTitle("Choose Location");
		mLocationDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mLocationPrompt=new Dialog(mContext);
		mLocationPrompt.setContentView(R.layout.layout_location_prompt);
		mLocationPrompt.setCancelable(true);
		mLocationPrompt.setTitle(getString(R.string.actionBarTitle));
		mLocationPrompt.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mListLocation=(ListView)mLocationDialog.findViewById(R.id.locationList);
		mListLocation.setOnItemClickListener(this);

		mBtnGoto=(Button)findViewById(R.id.gotoWelcome);
		mBtnPromptFindMe=(Button)mLocationPrompt.findViewById(R.id.btnPromptFindMe);
		mBtnPromptChoose=(Button)mLocationPrompt.findViewById(R.id.btnPromptChoose);
		mBtnPromptCancel=(Button)mLocationPrompt.findViewById(R.id.btnPromptCancel);
		mBtnPromptDone=(Button)mLocationPrompt.findViewById(R.id.btnPromptDone);

		mProgress=(ProgressBar)findViewById(R.id.locationProgress);

		mTxtPlaceChooser=(TextView)findViewById(R.id.textChooseLocation);
		mTxtMerchantCount=(TextView)findViewById(R.id.txtMerchantCount);
		mTxtPromptTitle=(TextView)mLocationPrompt.findViewById(R.id.txtPromptTitle);
		mTxtPromptText=(TextView)mLocationPrompt.findViewById(R.id.txtPromptText);

		mTxtPromptText.setTypeface(tf);
		mBtnPromptDone.setTypeface(tf);
		mBtnPromptCancel.setTypeface(tf);
		mBtnPromptChoose.setTypeface(tf);
		mBtnPromptFindMe.setTypeface(tf);

		//Map
		mLocationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		//		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);

		mLocation=mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		mbIsGpsEnabled=mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		mbIsNetworkEnabled=mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		initLocationAlert("Please enable location services.");
		//		showToast("GPS service "+mbIsGpsEnabled);
		//		showToast("Network Service "+mbIsNetworkEnabled);
		if(mbIsGpsEnabled==false){
			showLocationAlert();
		}else if(mbIsGpsEnabled==false && mbIsNetworkEnabled==false)
		{
			showLocationAlert();
		}


		try
		{
			//get active Area Id
			if(mDbUtil.getAreaRowCount()!=0){
				msActiveAreaId=mDbUtil.getActiveAreaID();
				//showActiveAreaPrompt();
				createDrawer();
			}
			//Getting Date time stamp of last saved area
			SharedPreferences prefs=getSharedPreferences(AREA_DATE_TIME_STAMP, MODE_PRIVATE);
			msDateTime=prefs.getString(KEY, mFormat.format(new Date()).toString());
			Date prevDate=mFormat.parse(msDateTime);
			Date currentDate=mFormat.parse(mFormat.format(new Date()));
			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();
			mlDiffDays = diff / (24 * 60 * 60 * 1000);
			debug(msDateTime);
			debug(mlDiffDays+"");

			if(mLocation==null){
				mLocation=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				//				showToast("location not found via GPS");
				if(mLocation!=null)
				{
					onLocationChanged(mLocation);
					//					showToast("location  found via Network");
				}else{
					//					showToast("Location not found");
					area();
				}
			}else
			{
				onLocationChanged(mLocation);
			}


			mTxtPlaceChooser.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(mArrId.size()!=0){
						mLocationDialog.show();
					}
				}
			});

			mBtnGoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(mDbUtil.getActiveAreaID().length()!=0){
						if(mLocationPrompt.isShowing()==true){
							mLocationPrompt.dismiss();
						}
						Intent intent=new Intent(mContext,MarketPlaceActivity.class);
						startActivity(intent);
						finish();
						//						toggleDrawer();
					}
					else{
						showToast("Please Select Location First");
					}
				}
			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mBtnPromptFindMe.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mLocationPrompt.dismiss();
				checkNearestArea();
			}
		});
		mBtnPromptChoose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mLocationDialog.show();
				mLocationPrompt.dismiss();
				mBtnPromptDone.setVisibility(View.VISIBLE);
				mBtnPromptCancel.setVisibility(View.GONE);
			}
		});
		mBtnPromptCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mLocationPrompt.dismiss();
				Intent intent=new Intent(mContext,MarketPlaceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		mBtnPromptDone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mbShowPrompt=false;
				mLocationPrompt.dismiss();
				//Updating Status of Selected Area to 1
				mDbUtil.updatePlaceStatus(LocationActivity.this,Integer.parseInt(msSelectedAreaId),1);
				//Saved selected Area id.
				Config.MY_AREA=msSelectedAreaId;
				Log.i("AREA NAME", "AREA NAME "+msSelectedAreaName+" AREA ID "+msSelectedAreaId);
				eventTrackingParam.put("Area_Name",msSelectedAreaName);
				eventTrackingParam.put("Area_ID",msSelectedAreaId);
				EventTracker.logEvent("Location_Select", eventTrackingParam);
				setActiveArea();
			}
		});
		
	}

	void area()
	{
		//Date time Stamp is older than a day refresh Location
		if(mlDiffDays>=1){
			if(mInternet.isNetworkAvailable()){
				hideDrawerLayout();
				mActionBar.setHomeButtonEnabled(false);
				if(mLocationPrompt.isShowing()){
					mLocationPrompt.dismiss();
				}
				checkNearestArea();	
			}
			else{
				showToast(getResources().getString(R.string.noInternetConnection));
				drawerModeToggle();
				//load Locally.
				setSelectedLocation();
				loadLocalAreas();

			}
		}
		else{
			//Toggle The Drawer
			drawerModeToggle();
			//If no records in table load from Internet
			if(mDbUtil.getAreaRowCount()==0){
				if(mInternet.isNetworkAvailable()){
					checkNearestArea();	
				}
				else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
			else{
				//load Locally.
				setSelectedLocation();
				loadLocalAreas();
			}
		}

	}
	void initLocationAlert(String msg)
	{
		alertDialog=new AlertDialog.Builder(mContext);
		alertDialog.setTitle("Location");
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("Enable",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);

			}
		});
		alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alert=alertDialog.create();
	}
	void showLocationAlert(){
		alert.show();
	}

	void setSelectedLocation()
	{
		try
		{
			//	Log.i("Active Area", "Active "+mDbUtil.getActiveArea()+" Length "+mDbUtil.getActiveArea().length());
			if(mDbUtil.getActiveArea().length()!=0){
				mTxtPlaceChooser.setText(mDbUtil.getActiveArea());
				mTxtMerchantCount.setText("Total Merchants "+mDbUtil.getMerchantCountForActiveArea());
			}
			else{
				mTxtPlaceChooser.setText("Choose your location");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}



	void drawerModeToggle()
	{
		try
		{
			Log.i("Area", "Current And Selected Area Id "+msActiveAreaId+" "+msSelectedAreaId);
			//if currently active id and user selected Id did not match then call Active Area Api
			Log.i("Area", "Current And Selected Area Id "+msActiveAreaId+" "+msSelectedAreaId);
			if(msSelectedAreaId.length()!=0)
			{
				if(mSessionManager.isLoggedInCustomer())
				{
					setActiveArea();
				}
			}

			String active_place;
			active_place=mDbUtil.getActiveAreaID();
			//		Log.i("Active Area Count", "Active Area Count in Drawer");
			Log.i("Active Area Count", "Active Area Count "+active_place);
			if(active_place.length()==0 && !msCountryCode.equalsIgnoreCase("in")){
				//showLocalAlert();
			}
			if(active_place.length()==0){
				hideDrawerLayout();
				mActionBar.setHomeButtonEnabled(false);
				mBtnGoto.setVisibility(View.GONE);
				//locationMap.setVisibility(View.GONE);
			}
			else{
				//				mActionBar.setIcon(R.drawable.ic_drawer);
				showDrawerLayout();
				mActionBar.setHomeButtonEnabled(true);
				mBtnGoto.setVisibility(View.VISIBLE);
				/*			locationMap.setVisibility(View.VISIBLE);*/
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void debug(String msg){
		Log.i("Location Screen", "Location Screen "+msg);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		toggleDrawer();
		return true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void onBackPressed() {
		if(back_pressed+2000 >System.currentTimeMillis()){
			mLocalNotification.alarm();
			if(isAppRated()==false){
				setSession(getSessionLength()+getTimeDiff());//Add session  length.
			}
			finish();
		}
		else{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}


	}

	@Override
	public void finish() {
		super.finish();
		if(mLocationDialog.isShowing()){
			mLocationDialog.dismiss();
		}
		if(mLocationPrompt.isShowing()){
			mLocationPrompt.dismiss();
		}
		if(alert.isShowing()){
			alert.dismiss();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2){
			createDrawer();
		}
		if(requestCode==4){
			if(mSessionManager.isLoggedIn()){
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore){
					Intent intent=new Intent(mActivity, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mActivity.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mActivity.finish();
				}
				else{
					Intent intent=new Intent(mActivity,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}

	}

	@Override
	public void onLocationChanged(Location location) {
		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
		supportMap.animateCamera(cameraUpdate);
		mdLatitude=location.getLatitude();
		mdLongitude=location.getLongitude();
		//		if(mbIsNearestApiCalled==false){
		//			mbIsNearestApiCalled=true;
		//			checkNearestArea();
		//		}else{
		//			promptAreaChange(false,miAreaId, msNearestArea);
		//		}
		area();
		//showToast(""+location.getLatitude()+" , "+location.getLongitude());
		mLocationManager.removeUpdates(this);
		setPref();

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	protected void onStop() {
		super.onStop();
		mLocationManager.removeUpdates(this);
	}

	void getAreas()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.AREA_URL+ISO_URL;
				new ShoplocalAsyncTask(this, "get", "customer", false, "areas").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void checkNearestArea()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{

				String msUrl=Config.NEAREST_AREA+"latitude="+mdLatitude+"&longitude="+mdLongitude;
				new ShoplocalAsyncTask(this, "get", "customer", false, "nearestArea").execute(msUrl);

			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void setActiveArea()
	{
		try
		{
			if(mSessionManager.isLoggedInCustomer()){
				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("auth_id", Config.CUST_AUTH_ID);
				json.put("active_area", msSelectedAreaId);
				String msUrl=Config.UPDATE_USER_PROFILE_URL;
				new ShoplocalAsyncTask(this, "put", "customer", false, json,"activeArea").execute(msUrl);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public void onAsyncStarted() {
		mProgress.setVisibility(View.VISIBLE);
	}


	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mProgress.setVisibility(View.GONE);
		if(tag.equalsIgnoreCase("areas")){

			try
			{
				JSONObject jsonobject=new JSONObject(sJson);
				JSONObject data = jsonobject.getJSONObject("data");
				JSONArray jsonArray = data.getJSONArray("areas");

				if(jsonArray == null)  return;

				int count = jsonArray.length();
				ContentValues[] values = new ContentValues[count];
				for(int i=0;i<jsonArray.length();i++){
					JSONObject areaObject=jsonArray.getJSONObject(i);
					if(areaObject!=null){
						ContentValues value=new ContentValues();
						mArrId.add(areaObject.getString("id"));
						mArrAreaName.add(areaObject.getString("sublocality"));
						value.put("area_id", areaObject.getInt("id"));
						value.put("area",  areaObject.getString("sublocality"));
						value.put("slug", areaObject.getString("slug"));
						value.put("locality",  areaObject.getString("locality"));
						value.put("latitude",  areaObject.getString("latitude"));
						value.put("longitude",  areaObject.getString("longitude"));
						value.put("pincode",  areaObject.getString("pincode"));
						value.put("city",  areaObject.getString("city"));
						value.put("state",  areaObject.getString("state"));
						value.put("country",  areaObject.getString("country"));
						value.put("iso_code",  areaObject.getString("iso_code"));
						value.put("merchant_count",  areaObject.getString("merchant_count"));
						value.put("published_state",  areaObject.getInt("published_status"));
						value.put("active_area",  0);
						values[i]=value;
					}

					
					 
					Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from AREAS").loadInBackground();
					
					if(c1!=null) {
						debug("deleted previous entries"); 
						mProgress.setVisibility(View.VISIBLE);
						mDbUtil.replaceOrUpdate(this,"AREAS", values,"areas");
					}

				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("activeArea")){
			try{
				JSONObject jsonobject=new JSONObject(sJson);
				String status=jsonobject.getString("success");
				if(status==null) return;

				if(status.equalsIgnoreCase("success")){
					debug("Area Updated");
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("nearestArea")){
			try{

				JSONObject jsonobject=new JSONObject(sJson);
				JSONObject data = jsonobject.getJSONObject("data");
				JSONArray jsonArray = data.getJSONArray("areas");

				if(jsonArray == null)  return;

				int count = jsonArray.length();
				ContentValues[] values = new ContentValues[count];
				for(int i=0;i<jsonArray.length();i++){
					JSONObject areaObject=jsonArray.getJSONObject(i);
					if(areaObject!=null){
						ContentValues value=new ContentValues();
						mArrId.add(areaObject.getString("id"));
						mArrAreaName.add(areaObject.getString("sublocality"));
						value.put("area_id", areaObject.getInt("id"));
						value.put("area",  areaObject.getString("sublocality"));
						value.put("slug", areaObject.getString("slug"));
						value.put("locality",  areaObject.getString("locality"));
						value.put("latitude",  areaObject.getString("latitude"));
						value.put("longitude",  areaObject.getString("longitude"));
						value.put("pincode",  areaObject.getString("pincode"));
						value.put("city",  areaObject.getString("city"));
						value.put("state",  areaObject.getString("state"));
						value.put("country",  areaObject.getString("country"));
						value.put("iso_code",  areaObject.getString("iso_code"));
						value.put(DBUtil.ID,areaObject.getString("shoplocal_place_id"));
						value.put("merchant_count",  areaObject.getString("merchant_count"));
						value.put("published_state",  areaObject.getInt("published_status"));
						value.put("active_area",  0);
						values[i]=value;
					}

				}

				Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from AREAS").loadInBackground();
				if(c1!=null){
					debug("deleted previous entries "+values.length); 
					mProgress.setVisibility(View.VISIBLE);
					mDbUtil.replaceOrUpdate(this,"AREAS", values,"areas");

				}

				miAreaId=Integer.parseInt(mArrId.get(0));
				msNearestArea=mArrAreaName.get(0);
				//Selecting default Area
				msSelectedAreaId=Integer.parseInt(mArrId.get(0))+"";
				Config.MY_AREA=miAreaId+"";

				promptAreaChange(miAreaId,msNearestArea);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	//load Areas from DB
	void loadLocalAreas()
	{
		mArrId.clear();
		mArrAreaName.clear();
		mArrLongitude.clear();
		mArrLatitude.clear();
		mArrMerchantCount.clear();

		Cursor cursor=null;
		try{
			
			String msQuery="Select area_id,area,latitude,longitude,merchant_count from AREAS where published_state=1";
			
			cursor=new ShoplocalCursorLoader(this, msQuery).loadInBackground();
			
			if(cursor!=null){
				for(cursor.moveToNext();!cursor.isAfterLast();cursor.moveToNext()){
					mArrId.add(cursor.getString(cursor.getColumnIndex("area_id")));
					mArrAreaName.add(cursor.getString(cursor.getColumnIndex("area")));
					mArrMerchantCount.add(cursor.getString(cursor.getColumnIndex("merchant_count")));
					mArrLatitude.add(cursor.getString(cursor.getColumnIndex("latitude")));
					mArrLongitude.add(cursor.getString(cursor.getColumnIndex("longitude")));
				}
			}
			mListLocation.setAdapter(new LocationListAdapter(mContext, 0, 0, mArrAreaName));
			
		}catch(CursorIndexOutOfBoundsException c){
			c.printStackTrace();
		}
		finally{
			cursor.close();
		}
	}

	@Override
	public void onCompleteInsertion(String tag) {
		//		showToast("Areas Inserted ");
		mProgress.setVisibility(View.GONE);		
		//Putting Date time stamp
		SharedPreferences prefs=getSharedPreferences(AREA_DATE_TIME_STAMP,MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putString(KEY,mFormat.format(new Date()));
		editor.commit();
		loadLocalAreas();
	}


	@Override
	public void onError(int errorCode) {
		mProgress.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}



	void setAreaTag(String area)
	{/*
		Log.i("URBAN", "URBAN SET TAG");
		boolean isMisMatch=false;

		//Adding Tags

		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
		//Getting All Tags
		Iterator<String> iterator=tags.iterator();

		//Traversing Tags to find out if there are any tags existing with own_id

		while(iterator.hasNext())
		{
			temp=iterator.next();

			Log.i("TAG", "Urban TAG PRINT "+temp);
			//If tag contains own_
			if(temp.startsWith("area_"))
			{
				//Replace area_ with "" and compare it with ID
				replace=temp.replaceAll("area_", "");
				tempIdList.add(replace);
				Log.i("TAG", "Urban TAG ID "+replace);

				if(!area.equalsIgnoreCase(replace))
				{
					isMisMatch=true;
					break;
				}
			}
		}

		if(isMisMatch==false && tempIdList.size()==0)
		{
			isMisMatch=true;
		}

		if(isMisMatch==true){
			//Clear area_ from Tag List
			for(int i=0;i<tempIdList.size();i++)
			{
				tags.remove("area_"+tempIdList.get(i));
			}
			tags.add("area_"+area);
			Log.i("TAG", "Urban TAG AFTER "+tags.toString());
			PushManager.shared().setTags(tags);
		}
	*/}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {

		try
		{
			msSelectedAreaId=mArrId.get(position);
			msSelectedAreaName=mArrAreaName.get(position);
			Log.i("Selected Area Id ", "Selected Area Id "+msSelectedAreaId);

			mLocationDialog.dismiss();
//			if(mbShowPrompt==true){
//				mLocationPrompt.show();
//			}else{
//				eventTrackingParam.put("Area_Name",msSelectedAreaName);
//				eventTrackingParam.put("Area_ID",msSelectedAreaId);
//				EventTracker.logEvent("Location_Select", eventTrackingParam);
//
//				//Updating Status of Selected Area to 1
//				mDbUtil.updatePlaceStatus(LocationActivity.this,Integer.parseInt(mArrId.get(position)),1);
//				//Saved selected Area id.
//				Config.MY_AREA=mArrId.get(position);
//			}
			
			eventTrackingParam.put("Area_Name",msSelectedAreaName);
			eventTrackingParam.put("Area_ID",msSelectedAreaId);
			EventTracker.logEvent("Location_Select", eventTrackingParam);

			//Updating Status of Selected Area to 1
			mDbUtil.updatePlaceStatus(LocationActivity.this,Integer.parseInt(mArrId.get(position)),1);
			//Saved selected Area id.
			Config.MY_AREA=mArrId.get(position);

			mTxtPromptText.setText("Set "+mArrAreaName.get(position)+"\' as your preffered local Area?");

			setActiveArea();


			//Putting Selected Area id in Preference so that it can be access across the app.
			//			SharedPreferences prefs=getSharedPreferences(ACTIVE_AREA_PREF,MODE_PRIVATE);
			//			Editor editor=prefs.edit();
			//			editor.putString(ACTIVE_AREA_KEY, mDbUtil.getActiveAreaID());
			//			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void showActiveAreaPrompt()
	{
		try
		{
			if(mDbUtil.getActiveAreaID().length()==0){
				mLocationDialog.show();
				mBtnPromptCancel.setVisibility(View.GONE);
				mBtnPromptDone.setVisibility(View.VISIBLE);
			}else{
				String currentArea=mDbUtil.getActiveArea();
				mTxtPromptText.setText("Your current location is "+currentArea);
				mLocationPrompt.show();				
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void promptCustomDialog(final int areaId,String areaName)
	{
		mLocationPrompt.show();
	}

	void promptAreaChange(final int areaId,String areaName){
		//showToast("Prompt area");
		//		String currentArea=mDbUtil.getActiveArea();
		//		int currentAreaId=Integer.parseInt(mDbUtil.getActiveAreaID());

		//		if(currentAreaId==areaId){
		//			showToast("");
		//		}

		try
		{

			String msg="";
			msg="Set \'"+areaName+"\' as your shoplocal Area?";
			AlertDialog.Builder alertDialog=new AlertDialog.Builder(mContext);
			alertDialog.setTitle("Location");
			alertDialog.setMessage(msg);
			alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					mBtnPromptCancel.setVisibility(View.GONE);
					mBtnPromptDone.setVisibility(View.VISIBLE);
					mDbUtil.updatePlaceStatus(LocationActivity.this,areaId,1);
					Config.MY_AREA=areaId+"";
					setActiveArea();

				}
			});
			alertDialog.setNegativeButton("Choose from list",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					mBtnPromptCancel.setVisibility(View.GONE);
					mBtnPromptDone.setVisibility(View.VISIBLE);
					mLocationDialog.show();
				}
			});
			AlertDialog alert=alertDialog.create();
			alert.show();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void areaUpdated(int id) {
		setSelectedLocation();
		setAreaTag(msSelectedAreaId);
		createDrawer();
		drawerModeToggle();
	}

	void setPref()
	{

		SharedPreferences prefs=getSharedPreferences("location", 0);
		Editor editor=prefs.edit();
		editor.putString("latitued", mdLatitude+"");
		editor.putString("longitude", mdLongitude+"");
		editor.putBoolean("isLocationFound",true);
		editor.commit();
		Log.i("LOCATION  ", "LOCATION COMMITED IN HOME GRID"+mdLatitude+","+mdLongitude);
	}



}
