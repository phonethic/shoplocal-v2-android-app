package com.phonethics.shoplocal;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.BadTokenException;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class LoginSignUpCustomer extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener 
{

	private boolean mbIsFacebook;

	private String 	msFbUserid="";
	private String 	msFbAccessToken="";

	private ProgressBar mProgress;

	private TextView mTxtLoginInfo;
	private TextView mTxtCountryPicker;
	private TextView mTxtPrivacy;
	private EditText mEdtMobileNo;

	private AlertDialog mAlert = null;


	private LoginButton mBtnSignUpFb;
	private Button mBtnSignUp;
	private Button mBtnDisclaimer;
	private Button mBtnTermAccept;
	private Button mBtnTermCancel;
	private Button mBtnPrivacyDone;
	private Button mBtnProceed;

	private WebView mPrivacyWebView;
	private WebView mTermsWebView;

	private CheckBox mCbxAccept;

	private RelativeLayout optionLayout;
	private RelativeLayout manualSignInLayout;

	private Dialog mTermsConditionDialog;
	private Dialog mPrivacyDialog;
	private Dialog mDisclaimerDialog;


	private Activity mContext;

	private boolean mbPendingPublishReauthorization = false;
	private static final List<String> PERMISSIONS = Arrays.asList("email");

	private String msJsonArray;
	private String msCountryCode;
	private String mActiveArea="";


	CountryAdapter	  mAdapter;
	ArrayList<String> mArrCountryarr;
	ArrayList<String> mArrCountryarrSearch;
	ArrayList<String> mArrCountryCode;
	ArrayList<String> mArrCountryCodeSearch;

	HashMap<String, String> user_details;

	public static final String KEY_USER_NAME_CUSTOMER="mobileno_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=this;
		setContentView(R.layout.activity_login_sign_up_customer);

		hideDrawerLayout();
		setTitle(getResources().getString(R.string.customer)+" login");
		Bundle b = getIntent().getExtras(); 

		if(b!=null){
			mbIsFacebook =  b.getBoolean("facebookLogin");
			//	isFromTour =  b.getBoolean("isFromTour",false);
			if(mbIsFacebook){
				login_facebook();
			}
			else{
				optionLayout.setVisibility(View.GONE);
				manualSignInLayout.setVisibility(View.VISIBLE);
			}
		}


		mTermsConditionDialog=new Dialog(mContext);
		mPrivacyDialog=new Dialog(mContext);

		mPrivacyDialog.setContentView(R.layout.privacy_policy);
		mPrivacyDialog.setTitle("Privacy Policy");
		mPrivacyDialog.setCancelable(true);
		mPrivacyDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mTermsConditionDialog.setContentView(R.layout.termsandcondition);
		mTermsConditionDialog.setTitle("Terms & Conditions");
		mTermsConditionDialog.setCancelable(false);
		mTermsConditionDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mDisclaimerDialog=new Dialog(mContext);
		mDisclaimerDialog.setContentView(R.layout.disclaimer_dialog);
		mDisclaimerDialog.setTitle("Why should I login?");
		mDisclaimerDialog.setCancelable(true);
		mDisclaimerDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mTermsWebView=(WebView)mTermsConditionDialog.findViewById(R.id.tcWeb);
		mPrivacyWebView=(WebView)mPrivacyDialog.findViewById(R.id.privacyWeb);


		mActiveArea=mDbUtil.getActiveAreaID();

		mProgress=(ProgressBar)findViewById(R.id.pBarFb);

		mPrivacyWebView.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));
		mTermsWebView.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));

		mTxtLoginInfo=(TextView)findViewById(R.id.txtMerchantField);
		mTxtCountryPicker=(TextView)findViewById(R.id.countryPicker);
		mTxtPrivacy=(TextView)mDisclaimerDialog.findViewById(R.id.privacy);

		mEdtMobileNo=(EditText)findViewById(R.id.edtCustomerMobileNo);

		optionLayout = (RelativeLayout)findViewById(R.id.optionLayout);
		manualSignInLayout = (RelativeLayout)findViewById(R.id.manualSignInLayout);

		mBtnSignUpFb = (LoginButton) findViewById(R.id.signUpFb);
		mBtnSignUp = (Button) findViewById(R.id.signUpCustomer);
		mBtnProceed=(Button)findViewById(R.id.btnSingupCustomer);
		mBtnDisclaimer=(Button)mDisclaimerDialog.findViewById(R.id.btnDesclaimer);
		mBtnTermAccept=(Button)mTermsConditionDialog.findViewById(R.id.btnTermDone);
		mBtnTermCancel=(Button)mTermsConditionDialog.findViewById(R.id.btnTermCancel);
		mBtnPrivacyDone=(Button)mPrivacyDialog.findViewById(R.id.buttonOk);

		//mBtnSignUpFb.setBackgroundResource(R.drawable.facebook_login_button);
		

		mCbxAccept=(CheckBox)mTermsConditionDialog.findViewById(R.id.chkBoxAccept);

		mEdtMobileNo.setTextColor(Color.parseColor("#9c9c9c"));

		mBtnSignUpFb.setTextSize(20);
		mBtnSignUp.setTextSize(20);

		mBtnSignUpFb.setText("Log in with Facebook");
		mBtnSignUp.setText("Sign up with shoplocal");
		mBtnSignUpFb.setText("");

		mTxtLoginInfo.setTypeface(tf);
		mTxtCountryPicker.setTypeface(tf);
		mTxtPrivacy.setTypeface(tf);
		mEdtMobileNo.setTypeface(tf);

		mBtnSignUpFb.setTypeface(tf);
		mBtnSignUp.setTypeface(tf);

		mTxtPrivacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));

		mBtnPrivacyDone.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mPrivacyDialog.dismiss();
			}
		});

		mTxtPrivacy.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mDisclaimerDialog.dismiss();
				mPrivacyDialog.show();
			}
		});

		mTxtLoginInfo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mDisclaimerDialog.show();
			}
		});

		mBtnDisclaimer.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				mDisclaimerDialog.dismiss();
			}
		});
		//chkBoxAccept.setChecked(true);
		mBtnTermAccept.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mCbxAccept.setChecked(true);
				if(mCbxAccept.isChecked())
				{
					mTermsConditionDialog.dismiss();
				}
				proceedWithoutNext();

			}
		});
		mBtnTermCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mTermsConditionDialog.dismiss();
				mCbxAccept.setChecked(false);
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getString(R.string.terms_and_condition_retry))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Try again",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog2,int id) {
						mTermsConditionDialog.show();
					}
				})
				.setNegativeButton("Do not register",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog2,int id) {
						dialog2.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}
		});

		try
		{
			msJsonArray = getCountriesFromAsset();
			getCountryCode();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mTxtCountryPicker.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				parseJsonData(msJsonArray);
				mAlert.show();
			}
		});

		user_details=new HashMap<String, String>();
		user_details=mSessionManager.getUserDetailsCustomer();
		Log.i("MOBILE PREFS", "MOBILE PREFS CUSTO"+user_details.get(KEY_USER_NAME_CUSTOMER).toString());
		if(user_details.get(KEY_USER_NAME_CUSTOMER).toString().length()!=0)
		{
			try
			{
				if(user_details.get(KEY_USER_NAME_CUSTOMER).toString().length()!=0){
					Log.i("MOBILE PREFS", "MOBILE PREFS "+user_details.get(KEY_USER_NAME_CUSTOMER).toString().substring(msCountryCode.length()));
					mEdtMobileNo.setText(user_details.get(KEY_USER_NAME_CUSTOMER).toString().substring(msCountryCode.length()));
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		mBtnSignUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EventTracker.logEvent("CSignup_ShoplocalLoginOption", false);
				optionLayout.setVisibility(View.GONE);
				manualSignInLayout.setVisibility(View.VISIBLE);
			}
		});

		mBtnSignUpFb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EventTracker.logEvent("CSignUp_FBOption", false);
				login_facebook();
			}
		});
		
		mBtnProceed.setText("Proceed");
		mBtnProceed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mInternet.isNetworkAvailable()){
					proceed();
				}
				else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});
	}

	void proceed()
	{
		try
		{

			if(mEdtMobileNo.getText().toString().length()==0){
				showToast( getResources().getString(R.string.mobileAlert));
			}
			else if(mEdtMobileNo.getText().toString().length()<10){
				showToast(getResources().getString(R.string.mobileLengthAlert));
			}
			else{
				if(mCbxAccept.isChecked()){
					user_details=mSessionManager.getUserDetailsCustomer();
				
					registerCustomer();
				}
				else{
					mTermsConditionDialog.show(); 
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}//end

	void proceedWithoutNext()
	{
		try
		{

			if(mEdtMobileNo.getText().toString().length()==0){
			}
			else if(mEdtMobileNo.getText().toString().length()<10){
			}
			else{
				if(mCbxAccept.isChecked()){
					user_details=mSessionManager.getUserDetailsCustomer();
					String user_name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();
					registerCustomer();
				}

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	} //ends

	void loginCustomer()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
				JSONObject json = new JSONObject();
				json.put("facebook_user_id",msFbUserid);
				json.put("facebook_access_token",msFbAccessToken);
				json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
						+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
				String msUrl=Config.LOGIN_URL;
				new ShoplocalAsyncTask(this, "post", "customer", false,json,"login").execute(msUrl);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void registerCustomer()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				EventTracker.logEvent("CSignUp_Proceed", false);
				
				PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
				JSONObject json = new JSONObject();
				json.put("mobile", msCountryCode+mEdtMobileNo.getText().toString());
				json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
						+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
				String msUrl=Config.REGISTER_USER_URL;
				
				
				new ShoplocalAsyncTask(this, "post", "customer", false, json, "register").execute(msUrl);
				mBtnProceed.setEnabled(false);
				
			}catch(Exception ex){
				ex.printStackTrace();

			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	public void parseJsonData(String jsonString){

		/*countryModels 		= new ArrayList<CountryModel>();*/
		mArrCountryarr 			= new ArrayList<String>();
		mArrCountryCode 		= new ArrayList<String>();
		try{
			JSONArray jArr = new JSONArray(jsonString);
			for(int i=0;i<jArr.length();i++){
				CountryModel	countryModel = new CountryModel();
				JSONObject jObj = jArr.getJSONObject(i);
				countryModel.setcName(jObj.getString("name"));
				mArrCountryarr.add(jObj.getString("name"));
				countryModel.setcCode(jObj.getString("dial_code"));
				mArrCountryCode.add(jObj.getString("dial_code").replace(" ", ""));
				countryModel.setcShort(jObj.getString("code"));
				/*countryModels.add(countryModel);*/
			}

			mArrCountryarrSearch = mArrCountryarr;
			mArrCountryCodeSearch = mArrCountryCode;
			initDialog();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void initDialog(){

		try
		{
			AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
			alertDialog.setTitle("Select County Code");
			alertDialog.setCancelable(true);
			View view = mContext.getLayoutInflater().inflate(R.layout.dialog_list, null);
			final ListView listView = (ListView) view.findViewById(R.id.list_county_code);
			final EditText editSearch = (EditText) view.findViewById(R.id.edit_search);

			listView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
						long arg3) {
					//setText(countryarrSearch.get(pos), countryCodeSearch.get(pos));

					setCountryCode(mArrCountryarrSearch.get(pos), mArrCountryCodeSearch.get(pos));
					mAlert.dismiss();

				}
			});
			mAdapter = new CountryAdapter(mContext, mArrCountryarr,mArrCountryCode);
			listView.setAdapter(mAdapter);
			editSearch.addTextChangedListener(new TextWatcher() {


				public void onTextChanged(CharSequence s, int start, int before, int count) {
					ArrayList<String> countryarrSearch_1 = new ArrayList<String>();
					ArrayList<String> countryCodeSearch_1 = new ArrayList<String>();

					int textLength  = editSearch.getText().toString().length();

					for(int i=0;i<mArrCountryarr.size();i++){
						if(textLength<= mArrCountryarr.get(i).length()){
							if(editSearch.getText().toString().equalsIgnoreCase((String)mArrCountryarr.get(i).subSequence(0, textLength))){
								countryarrSearch_1.add(mArrCountryarr.get(i));
								countryCodeSearch_1.add(mArrCountryCode.get(i));						
							}
						}
					}

					mArrCountryarrSearch = countryarrSearch_1;
					mArrCountryCodeSearch = countryCodeSearch_1;

					mAdapter = new CountryAdapter(mContext,  mArrCountryarrSearch,mArrCountryCodeSearch);
					mAdapter.notifyDataSetChanged();
					listView.setAdapter(mAdapter);
				}


				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}
				public void afterTextChanged(Editable s) {
				}
			});
			alertDialog.setView(view);
			mAlert = alertDialog.create();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void setCountryCode(String country, String code){
		try
		{
			mTxtCountryPicker.setText(code);
			msCountryCode=code.substring(1);
			setPrefernces();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setPrefernces()
	{
		SharedPreferences pref=getSharedPreferences("customerCountryCode", 0);
		Editor editor=pref.edit();
		editor.putString("cCountryCode", msCountryCode);
		editor.commit();
	}

	void getCountryCode()
	{
		SharedPreferences pref=getSharedPreferences("customerCountryCode",0);
		msCountryCode=pref.getString("cCountryCode", "91");
		mTxtCountryPicker.setText("+"+msCountryCode);
		Log.i("Countrycode found","Countrycode found "+msCountryCode);
	}

	public String getCountriesFromAsset(){
		String fileString = "";
		try{
			InputStream is = getAssets().open("county_code.txt");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			fileString = new String(buffer,"UTF-8");
			//Log.d("", "JsonString >> "+fileString);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return fileString;
	}



	public void login_facebook(){
		try
		{
			mProgress.setVisibility(View.VISIBLE);
			Session.openActiveSession(mContext, true, new Session.StatusCallback() {
				public void call(final Session session, SessionState state, Exception exception) {
					if(session.isOpened())
					{
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();
						if (!isSubsetOf(PERMISSIONS, permissions)) {
							mbPendingPublishReauthorization = true;
						}
						Request request = Request.newMeRequest(session,
								new Request.GraphUserCallback() {
							public void onCompleted(GraphUser user, Response response) {
								if(user!=null)
								{
									try{
										msFbUserid=user.getId();
										msFbAccessToken=session.getAccessToken();
										Log.i("User Id", "FbName : "+msFbAccessToken);
										Log.i("User Id ", "FbId : "+msFbUserid);
										mProgress.setVisibility(View.INVISIBLE);
										loginCustomer();
									}catch(Exception ex){
										ex.printStackTrace();
									}
								}

								else{
									//									EventTracker.logEvent("CSignup_LoginFailedFB", false);
									mProgress.setVisibility(View.INVISIBLE);
								}
							}

						});
						request.executeAsync();
					}

					if(session.isClosed()){
						mProgress.setVisibility(View.INVISIBLE);
						EventTracker.logEvent("CSignup_LoginFailedFB", false);
						try {

							if ((exception instanceof FacebookOperationCanceledException ||
									exception instanceof FacebookAuthorizationException)) {
								new AlertDialog.Builder(LoginSignUpCustomer.this)
								.setTitle("Login Failed")
								.setMessage(exception.getMessage().toString())
								.setPositiveButton("OK", null)
								.show();

							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}


			});
		}catch(NullPointerException npx)
		{
			npx.printStackTrace();
		}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishTo();
		return true;
	}

	@Override
	public void onAsyncStarted() {
		mProgress.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		mProgress.setVisibility(View.GONE);
		debug(sJson);
		if(tag.equalsIgnoreCase("register")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);

				String status=jsonObject.getString("success");
				String passmsg=jsonObject.getString("message");
				String error_code=jsonObject.getString("code");

				if(error_code.equalsIgnoreCase("-223")) //New user
				{
					EventTracker.logEvent("CSignUp_New", false);
				}
				else if(error_code.equalsIgnoreCase("-202")) //Returning user
				{
					EventTracker.logEvent("CSignUp_Return", false);
				}
				if(status.equalsIgnoreCase("true")){
					
					Intent intent=new Intent(this, SignupCustomer.class);
					intent.putExtra("server_message", passmsg);
					intent.putExtra("mobileno", msCountryCode+mEdtMobileNo.getText().toString());
					startActivityForResult(intent, 2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else{
					showToast(passmsg);
				}
			}catch(Exception  ex)
			{
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("login")){

			try
			{
				JSONObject jsonObject=new JSONObject(sJson);

				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");


				if(status==null) return;

				if(status.equalsIgnoreCase("true")){
					EventTracker.logEvent("CSignup_LoginSuccessFB", false);
					
					JSONObject jsonData=jsonObject.getJSONObject("data");
					String id=jsonData.getString("id");
					String auth_code=jsonData.getString("auth_code");
					mSessionManager.createLoginSessionCustomer("", "", id, auth_code,true);

					setCustomerTag(id);
					if(mActiveArea.length()!=0){
						setAreaTag(mActiveArea);
					}

					Intent intent=new Intent();
					mContext.setResult(2, intent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
				}
				else if(status.equalsIgnoreCase("false")){
					EventTracker.logEvent("CSignup_LoginFailedFB", false);
					showToast(msg);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void onError(int errorCode) {
		mProgress.setVisibility(View.GONE);
		mBtnProceed.setEnabled(true);
		showToast(getResources().getString(R.string.errorMessage));
	}

	void debug(String msg){
		Log.i("Customer", "Customer "+msg);
	}

	void setCustomerTag(String userId)
	{
		try
		{/*
			Log.i("URBAN", "URBAN SET TAG");
			boolean isMisMatch=false;

			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			ArrayList<String>tempIdList=new ArrayList<String>();

			String temp="";
			String replace="";
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();
			//Traversing Tags to find out if there are any tags existing with own_id
			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains customer_id_
				if(temp.startsWith("customer_id_"))
				{
					//Replace customer_id_ with "" and compare it with ID
					replace=temp.replaceAll("customer_id_", "");
					tempIdList.add(replace);
					Log.i("TAG", "Urban TAG ID "+replace);

					if(!userId.equalsIgnoreCase(replace))
					{
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false && tempIdList.size()==0)
			{
				isMisMatch=true;
			}


			if(isMisMatch==true)
			{

				//Clear customer_id_ from Tag List
				for(int i=0;i<tempIdList.size();i++)
				{
					tags.remove("customer_id_"+tempIdList.get(i));
				}
				tags.add("customer_id_"+userId);
				Log.i("TAG", "Urban TAG AFTER "+tags.toString());
				PushManager.shared().setTags(tags);
			}
		*/}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setAreaTag(String area)
	{/*
		Log.i("URBAN", "URBAN SET TAG");
		boolean isMisMatch=false;

		//Adding Tags

		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
		//Getting All Tags
		Iterator<String> iterator=tags.iterator();

		//Traversing Tags to find out if there are any tags existing with own_id

		while(iterator.hasNext())
		{
			temp=iterator.next();

			Log.i("TAG", "Urban TAG PRINT "+temp);
			//If tag contains own_
			if(temp.startsWith("area_"))
			{
				//Replace area_ with "" and compare it with ID
				replace=temp.replaceAll("area_", "");
				tempIdList.add(replace);
				Log.i("TAG", "Urban TAG ID "+replace);

				if(!area.equalsIgnoreCase(replace))
				{
					isMisMatch=true;
					break;
				}
			}
		}

		if(isMisMatch==false && tempIdList.size()==0)
		{
			isMisMatch=true;
		}

		if(isMisMatch==true)
		{
			//Clear area_ from Tag List
			for(int i=0;i<tempIdList.size();i++)
			{
				tags.remove("area_"+tempIdList.get(i));
			}
			tags.add("area_"+area);
			Log.i("TAG", "Urban TAG AFTER "+tags.toString());
			PushManager.shared().setTags(tags);
		}
	*/}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			mBtnProceed.setEnabled(true);
			if(requestCode==2)
			{
				if(mSessionManager.isLoggedInCustomer())
				{
					finish();
					overridePendingTransition(0,R.anim.shrink_fade_out_center);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void onBackPressed() {
		finishTo();
	}

	void finishTo()
	{
		Intent intent=new Intent();
		setResult(3, intent);
		this.finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
	}

}
