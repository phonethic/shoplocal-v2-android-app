package com.phonethics.shoplocal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStateChangeListener extends BroadcastReceiver {

	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		if(isNetworkAvailable(context)){
			if(AppLaunchPrefs.isAppLaunchedApiCalled(context)==false){
			AppLaunchPrefs.appLaunchApi(context);
			}
		}
	}
	
	
	public boolean isNetworkAvailable(Context context) {
		try
		{
			ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = cm.getActiveNetworkInfo();
			// if no network is available networkInfo will be null
			// otherwise check if we are connected
			if (networkInfo != null && networkInfo.isConnected()) {
				return true;
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	} 
}
