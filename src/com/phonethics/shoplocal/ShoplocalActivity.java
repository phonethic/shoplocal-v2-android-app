package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.adapters.NavigationActionListener;
import com.phonethics.adapters.SharedListViewAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.DatePreferencesClass;
import com.phonethics.model.ExpandibleDrawer;


public class ShoplocalActivity extends SherlockFragmentActivity implements OnClickListener, OnNavigationListener {
	protected LocalNotification mLocalNotification;
	protected UiLifecycleHelper mUiHelper;
	protected DatePreferencesClass mDatePref;

	protected final String MARKET_DATE_TIME_STAMP="MARKET_DATE_TIME_STAMP";
	protected final String KEY="MARKET_DATE_TIME";
	protected DrawerClass mDrawerClass;
	protected boolean mbShowMore = false;
	protected DBUtil mDbUtil;
	protected SessionManager mSessionManager;
	protected boolean mbIsSplitLogin;
	protected boolean mbOpenDrawer;
	protected String msActiveArea;
	protected String msDrawerItem;
	protected ActionBar mActionBar;
	protected DrawerLayout mDrawerLayout;
	protected View mFooterView;
	protected ExpandableListView mDrawerList;


	protected TextView list_item_footerMore;
	protected TextView list_item_footerLess;
	protected ActionBarDrawerToggle mDrawerToggle;
	protected Dialog mDialogShare ;

	protected RelativeLayout emptyData;
	//protected TextView txtEmptyMessage;

	protected ArrayList<String> packageNames = new ArrayList<String>();
	protected ArrayList<String> appName = new ArrayList<String>();
	protected ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	private ArrayList<ExpandibleDrawer> mDrawerMenu=new ArrayList<ExpandibleDrawer>();
	private ListView mListViewShare;
	private DrawerExpandibleAdapter mDrawerAdapter;

	private String mActionTitle="Shoplocal";

	protected boolean mbFacebook = false;
	protected FragmentActivity mActivity;
	private final String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download "
			+ "- Why don't you try and experience it yourself.";

	private String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";
	private String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	private String KEY_USER_ID="user_id";
	private String KEY_AUTH_ID="auth_id";
	private String TAG="ShoplocalActivity";

	protected RelativeLayout mLoginInfoLayout;
	protected TextView mTxtLoginMessage;
	protected TextView mEdtSearch;
	protected Button mButtonLogin;

	protected Typeface tf;

	protected boolean mbShowLoginLayout=false;

	protected NetworkCheck mInternet;

	protected int timeInSec=0;

	protected String msSlug;

	protected boolean mbFaceBookShare=false;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = this;

		setTheme(R.style.Theme_City_custom);
		setContentView(R.layout.activity_default_search_list);
		mInternet=new NetworkCheck(mActivity);
		tf=Typeface.createFromAsset(mActivity.getAssets(), "fonts/GOTHIC_0.TTF");
		mLocalNotification=new LocalNotification(getApplicationContext());
		mUiHelper = new UiLifecycleHelper(this, null);
		mUiHelper.onCreate(savedInstanceState);

		mDatePref=new DatePreferencesClass(this, MARKET_DATE_TIME_STAMP, KEY);
		mDrawerClass=new DrawerClass(this);
		mbShowMore = mDrawerClass.isShowMore();

		mDbUtil = DBUtil.getInstance(getApplicationContext());

		msSlug=mDbUtil.getActiveSlug();

		mSessionManager = new SessionManager(getApplicationContext());

		if(mSessionManager.isLoggedInCustomer()){
			HashMap<String,String>user_details=mSessionManager.getUserDetailsCustomer();
			Config.CUST_USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
			Config.CUST_AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
		}
		if(mSessionManager.isLoggedIn()){
			HashMap<String,String>user_details=mSessionManager.getUserDetails();
			Config.MERCHANT_USER_ID=user_details.get(KEY_USER_ID).toString();
			Config.MERCHANT_AUTH_ID=user_details.get(KEY_AUTH_ID).toString();
		}
		mLoginInfoLayout=(RelativeLayout)findViewById(R.id.loginInfoLayout);
		mTxtLoginMessage=(TextView)findViewById(R.id.txtLoginMessage);
		mButtonLogin=(Button)findViewById(R.id.buttonLogin);
		mEdtSearch=(TextView)findViewById(R.id.edtSearch);

		emptyData=(RelativeLayout)findViewById(R.id.emptyData);
		//txtEmptyMessage=(TextView)findViewById(R.id.txtEmptyMessage);

		mTxtLoginMessage.setTypeface(tf);
		mButtonLogin.setTypeface(tf);
		mButtonLogin.setOnClickListener(this);
		try {
			Bundle b=getIntent().getExtras();
			if(b!=null) {
				mbOpenDrawer=b.getBoolean("opeDrawer",false);
			}
			mbShowMore = mDrawerClass.isShowMore();
			msActiveArea=mDbUtil.getActiveAreaID();
			if(msActiveArea.length()!=0){
				if(Config.MY_AREA.length()==0)
				{
					Config.MY_AREA=msActiveArea;
				}
			}
			Config.debug("ShoplocalActivity", "My Area "+Config.MY_AREA);
			getLocationFromPrefs();
		}catch(Exception e) {
			e.printStackTrace();
		}


		mActionBar = getSupportActionBar();
		mActionBar.setTitle(getResources().getString(R.string.itemAllStores));
		mActionBar.show();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setIcon(R.drawable.ic_navigation_previous_item);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerList=(ExpandableListView)findViewById(R.id.drawerList);

		mFooterView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)mFooterView.findViewById(R.id.list_item_footerMore);

		mDrawerList.addFooterView(mFooterView);
		list_item_footerMore.setTypeface(tf);
		//		if(mbShowMore) {
		//			list_item_footerMore.setText("LESS");
		//		}else {
		//			list_item_footerMore.setText("MORE");
		//		}
		//
		//		list_item_footerMore.setOnClickListener(new OnClickListener() {
		//			public void onClick(View arg0) {
		//				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More")) {
		//					mbShowMore = true;
		//					list_item_footerMore.setText("LESS");
		//				} else {
		//					mbShowMore = false;
		//					list_item_footerMore.setText("MORE");
		//				}
		//				createDrawer();
		//			}
		//		});


		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  
				mDrawerLayout,         
				R.drawable.ic_navigation_previous_item,  
				R.string.drawer_open,  
				R.string.drawer_close  
				) {
			public void onDrawerClosed(View view) {
				mActionBar.setTitle(mActionTitle);
			}

			public void onDrawerOpened(View drawerView) {
				mActionBar.setTitle(mActionTitle);
				//				mDrawerLayout.bringChildToFront(drawerView);
				//				mDrawerLayout.requestLayout();
			}
		};


		mDrawerLayout.setDrawerListener(mDrawerToggle);
		createDrawer();

		if(mbOpenDrawer) {
			mDrawerLayout.openDrawer(Gravity.LEFT);
		}

		EventTracker.startLocalyticsSession(getApplicationContext());

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");

		final PackageManager pm = getPackageManager();
		//		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);

		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && mbFacebook == false){
					mbFacebook = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!mbFacebook) {
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}

		mDialogShare = new Dialog(this);
		mDialogShare.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		mDialogShare.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.activity_background));
		mDialogShare.setContentView(R.layout.sharedialog);
		mDialogShare.setTitle("Select an action");
		mListViewShare = (ListView) mDialogShare.findViewById(R.id.listViewForShare);
		mListViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));

	

		list_item_footerMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if(mSessionManager.isLoggedIn()) {
					try {

						long rowcount=mDbUtil.getMyPlaceCount();
						if(rowcount==0) {
							Log.i("DB ","DB Row count "+rowcount);
							SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
							Editor editor=prefs.edit();
							editor.putBoolean("isPlaceRefreshRequired", true);
							editor.commit();	
						}

					}catch(Exception ex) {
						ex.printStackTrace();
					}

					SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
					Editor 	editor=prefs.edit();
					editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
					editor.commit();

					Intent intent=new Intent(mActivity,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
				else{
					Intent intent=new Intent(mActivity,SplitLoginSignUp.class);
					startActivityForResult(intent, 4);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});
	}


	/**
	 * use to save time spent in particular activity.
	 * 
	 * @param timeDiff : time in milli sec
	 * 
	 */
	void setSession(long timeDiff)
	{
		Config.debug(TAG, "Shoplocal Session "+timeDiff);
		SharedPreferences prefs=getSharedPreferences(Config.APP_SESSION, 1);
		Editor edit=prefs.edit();
		edit.putLong(Config.APP_SESSION_KEY,timeDiff);
		edit.commit();
	}


	long getSessionLength()
	{
		SharedPreferences prefs=getSharedPreferences(Config.APP_SESSION, 1);
		return prefs.getLong(Config.APP_SESSION_KEY, 0);
	}
	boolean isAppRated()
	{
		boolean appRated=false;
		SharedPreferences prefs=getSharedPreferences(Config.APP_SESSION, 1);
		appRated=prefs.getBoolean(Config.APP_RATED_KEY, false);
		Config.debug(TAG, "IsApp Rated "+appRated);
		return appRated;
	}

	void setAppRated(boolean isAppRated){
		Config.debug(TAG, "Is App Rated "+isAppRated);
		SharedPreferences prefs=getSharedPreferences(Config.APP_SESSION, 1);
		Editor edit=prefs.edit();
		edit.putBoolean(Config.APP_RATED_KEY,isAppRated);
		edit.commit();

	}

	String getAppLaunchDateTime()
	{
		SharedPreferences prefs=getSharedPreferences(Config.APP_START_TIME, 1);
		return prefs.getString(Config.APP_START_TIME_KEY,"");
	}

	int getTimeDiff()
	{
		long lastLaunchTime=0;
		long currentTime=0;
		String launchDate=getAppLaunchDateTime(); //get Launch date time stamp from pref
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		try {
			Config.debug(TAG, "Launch Date "+launchDate);
			//Launch Time
			Date mDate=(Date)df.parse(launchDate);
			Calendar mLaunchDatecal=Calendar.getInstance();
			mLaunchDatecal.setTime(mDate);

			//Current Time
			Calendar mCal=Calendar.getInstance();
			lastLaunchTime=mLaunchDatecal.getTimeInMillis();
			currentTime=mCal.getTimeInMillis();

			timeInSec=(int)((currentTime-lastLaunchTime)/1000)%60; 
			Config.debug(TAG, "Date time Diff "+timeInSec);
		} catch (Exception e) {
			e.printStackTrace();
		}


		return timeInSec;
	}

	void clearSessionPref()
	{
		SharedPreferences prefs=getSharedPreferences(Config.APP_SESSION, 1);
		Editor edit=prefs.edit();
		edit.clear();
		edit.commit();
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventTracker.endFlurrySession(getApplicationContext());	
	}

	void setDrawer(DrawerLayout drawerLayout,ExpandableListView expList)
	{
		this.mDrawerLayout=drawerLayout;;
		this.mDrawerList=expList;
		mDrawerList.addFooterView(mFooterView);
		//		if(mbShowMore) {
		//			list_item_footerMore.setText("LESS");
		//		}else {
		//			list_item_footerMore.setText("MORE");
		//		}

	}

	protected void showSearchBox(final boolean isOfferSearch){
		mEdtSearch.setVisibility(View.VISIBLE);
		mEdtSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mActivity,SearchActivity.class);
				intent.putExtra("isOfferSearch", isOfferSearch);
				startActivity(intent);
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
			}
		});
	}

	
	protected void showEmptyLayout(boolean showLayout)
	{
		if(showLayout==true){
			emptyData.setVisibility(View.VISIBLE);
		}else{
			emptyData.setVisibility(View.GONE);
		}
	}

	void hideDrawerLayout()
	{
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	}
	void showDrawerLayout()
	{
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
	}
	void toggleDrawer()
	{
		mDrawerLayout.openDrawer(Gravity.LEFT);
		if(mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
			mDrawerLayout.closeDrawers();
		}
	}

	void showLoginLayout(boolean showLoginLayout)
	{
		mbShowLoginLayout=showLoginLayout;
		if(showLoginLayout==true){
			mLoginInfoLayout.setVisibility(View.VISIBLE);
		}else{
			mLoginInfoLayout.setVisibility(View.GONE);
		}
	}

	void setTitle(String title)
	{
		this.mActionTitle=title;
		mActionBar.setTitle(mActionTitle);
	}
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	protected void onResume() {
		super.onResume();
		mUiHelper.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		//		try
		//		{
		//		mDbUtil=DBUtil.getInstance(getApplicationContext());
		//		}catch(Exception ex){
		//			ex.printStackTrace();
		//		}
	}
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		mUiHelper.onPause();
		super.onPause();
	}


	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mUiHelper.onSaveInstanceState(outState);
	}
	public void onDestroy() {
		super.onDestroy();
		mUiHelper.onDestroy();
		//		mDbUtil.close();
	}

	@Override
	public void finish() {
		super.finish();
		//		mDbUtil.close();
	}

	void createNavigationList(){

		ArrayList<String> mAllAreas=new ArrayList<String>();
		mAllAreas.add(mDbUtil.getActiveArea());
		mAllAreas.add("Pick Location");
		NavigationActionListener mAdAction = new NavigationActionListener(mActivity, 0, 0, mAllAreas);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionBar.setListNavigationCallbacks(mAdAction, this);
		mActionBar.setSelectedNavigationItem(0);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
				mDrawerLayout.closeDrawers();
			}
		}

	
		return true;
	}
	protected void createDrawer(){
		try {
			mDrawerMenu.clear();
			mDrawerClass.setShowMore(mbShowMore);

			mDrawerMenu= mDrawerClass.getDrawerAdapter();
			mDrawerAdapter=new DrawerExpandibleAdapter(this, 0, mDrawerMenu);
			mDrawerList.setAdapter(mDrawerAdapter);

			mDrawerList.setOnChildClickListener(new ExpandedDrawerClickListener());
			mDrawerList.setOnGroupClickListener(new OnGroupClickListener() {
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					if(mDrawerMenu.get(groupPosition).getOpened_state()==1) {
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<mDrawerMenu.size();i++) {
				if(mDrawerMenu.get(i).getOpened_state()==1){
					mDrawerList.expandGroup(i);
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} 
	}
	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener {
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			msDrawerItem=mDrawerMenu.get(groupPosition).getItem_array().get(childPosition);
			navigate();
			return false;
		}
	}
	private void navigate() {
		Log.i("Shop", "ShoplocalActivity "+msDrawerItem+" ActionBar "+mActionBar.getTitle().toString());
		if(mActionBar.getTitle().toString().equalsIgnoreCase(msDrawerItem)){
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemDiscover))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			msDrawerItem="";
			Intent intent=new Intent(this,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			msDrawerItem="";
			Intent intent=new Intent(this,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemLogin))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			msDrawerItem="";
			Intent intent=new Intent(mActivity,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			msDrawerItem="";
			if(mSessionManager.isLoggedInCustomer())
			{
				Intent intent=new Intent(this,CustomerProfile.class);
				startActivity(intent);
				finish(); 
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else {
				login();
			}
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			msDrawerItem="";
			Intent intent=new Intent(this,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemSettings))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			msDrawerItem="";
			Intent intent=new Intent(this,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemAllStores))) {					//Business
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			Intent intent=new Intent(this,MarketPlaceActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if(msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			msDrawerItem="";
			Intent intent=new Intent(this,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))  { 		//About Shoplocal
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			msDrawerItem="";
			Intent intent=new Intent(this,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemShare))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			showDialogTab();
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemContactUs))) {
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			msDrawerItem="";
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(this);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemFacebook))) {
			msDrawerItem="";
			Intent intent=new Intent(this,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemTwitter))) {
			msDrawerItem="";
			Intent intent=new Intent(this,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness))) { 		//Sell
			msDrawerItem="";
			if(mSessionManager.isLoggedIn()) {
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(this,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else{
				Intent intent=new Intent(mActivity,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		}else if( msDrawerItem.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard))) {
			msDrawerItem="";
			if(mSessionManager.isLoggedIn()) {
				try {

					long rowcount=mDbUtil.getMyPlaceCount();
					if(rowcount==0) {
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}

				}catch(Exception ex) {
					ex.printStackTrace();
				}

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(this,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
	}
	
	/*
	 * share this app
	 * 
	 */
	private void showDialogTab(){
		mDialogShare.show();
		mListViewShare.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && mbFacebook == true){
					if(mSessionManager.isLoggedInFacebook()){
						FacebookOperations.sendAppReuest(mActivity);
					}else{
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
						.setLink("http://shoplocal.co.in/download")
						.build();
						mUiHelper.trackPendingDialogCall(shareDialog.present());
					}
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && mbFacebook == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}
				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}
				dismissDialog();
			}
		});
	}
	void dismissDialog(){
		mDialogShare.dismiss();
	}

	protected void storeShareDialog(final String storeName,final String imageUrl,final String shareText)
	{
		mDialogShare.show();
		mListViewShare.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && mbFacebook == true){
					mbFaceBookShare=true;
					if(imageUrl.length()!=0){
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
						.setLink("http://shoplocal.co.in/download")
						.setName(storeName)
						.setPicture(Config.IMAGE_URL+imageUrl)
						.setDescription(shareText)
						.build();
						mUiHelper.trackPendingDialogCall(shareDialog.present());
					}else{
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
						.setLink("http://shoplocal.co.in/download")
						.setName(storeName)
						.setDescription(shareText)
						.build();
						mUiHelper.trackPendingDialogCall(shareDialog.present());
					}


					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && mbFacebook == false){
					mbFaceBookShare=false;
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					mbFaceBookShare=false;
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_SUBJECT, storeName);
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivityForResult(i,3);
				}

				dismissDialog();

			}
		});
	}

	protected void offerShareDialog(final String storeName,final String imageUrl,final String shareText)
	{
		mDialogShare.show();
		mListViewShare.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && mbFacebook == true){
					mbFaceBookShare=true;
					if(imageUrl.length()!=0){
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
						.setLink("http://shoplocal.co.in/download")
						.setName(storeName + " - " + "Offers")
						.setPicture(Config.IMAGE_URL+imageUrl)
						.setDescription(shareText)
						.build();
						mUiHelper.trackPendingDialogCall(shareDialog.present());
					}else{
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
						.setLink("http://shoplocal.co.in/download")
						.setName(storeName + " - " + "Offers")
						.setDescription(shareText)
						.build();
						mUiHelper.trackPendingDialogCall(shareDialog.present());
					}

					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && mbFacebook == false){
					mbFaceBookShare=false;
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}
				else if(!app.equalsIgnoreCase("facebook")){
					mbFaceBookShare=false;
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_SUBJECT,storeName);
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivityForResult(i, 3);
				}
				dismissDialog();
			}
		});
	}

	private void login() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Shoplocal");
		alert
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				msDrawerItem="";
				Intent intent=new Intent(mActivity,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});

		AlertDialog alertDialog3 = alert.create();
		alertDialog3.show();
	}
	void showToast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}
	protected boolean mbLocationFound = false;
	protected void getLocationFromPrefs() {
		SharedPreferences pref=this.getSharedPreferences("location",0);
		Config.LATITUDE=pref.getString("latitued", Config.LATITUDE);
		Config.LONGITUDE=pref.getString("longitude", Config.LONGITUDE);
		mbLocationFound=pref.getBoolean("isLocationFound",false);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	public void onClick(View v) {
		if(v.getId()==mButtonLogin.getId()){
			Intent intent=new Intent(mActivity,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
	}



	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(itemPosition==1){
			EventTracker.logEvent("Tab_Location", false);
			Intent intent=new Intent(mActivity, LocationActivity.class);
			startActivity(intent);
			finish();
		}
		return false;
	}
}