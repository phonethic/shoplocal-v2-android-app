package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import com.crittercism.app.Crittercism;
import com.phonethics.localnotification.LocalNotification;

public class StartUpActivity extends Activity {
	private DBUtil mDbUtil;
	private Context mContext;
	private String msArea;
	private boolean mbTourEnded=false;

	private final String PREF_NAME="TOUR_PREF";
	private final String TOUR_KEY="TOUR_VALUE";

	private LocalNotification mLocalNotification;

	private int miLaunchVia=0;

	Map<String,String> mEventMap=new HashMap<String, String>();

	private long mlSplashTimeOut=1500;
	private Typeface tf;

	private TextView mTxtShoplocal;

	private SharedPreferences mPref;
	private Editor mEditor;

	private NetworkCheck misNet;


	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_up);
		mContext=getApplicationContext();
		mDbUtil=DBUtil.getInstance(mContext);
		mLocalNotification=new LocalNotification(mContext);

		misNet=new NetworkCheck(mContext);



		tf=Typeface.createFromAsset(mContext.getAssets(), "fonts/GOTHIC_0.TTF");

		mTxtShoplocal=(TextView)findViewById(R.id.txtShoplocal);
		mTxtShoplocal.setTypeface(tf);

		//Adding Launch Time
		mPref=getSharedPreferences(Config.APP_START_TIME_KEY, MODE_PRIVATE);
		mEditor=mPref.edit();

		//Stores App launch time in preference for Sending it to Server.
		DateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mEditor.putString(Config.APP_START_TIME, sdf.format(new Date()));
		mEditor.commit();


		/*try{
//			BluetoothAdapter m_BluetoothAdapter = BluetoothAdapter.getDefaultAdapter(); 
//			String m_bluetoothAdd = m_BluetoothAdapter.getAddress();

			//Get the App Id from UrbanAirship for this device.
			Config.debug("StartUP", "StartUp Device Id "+PushManager.shared().getAPID());
			Config.DEVICE_ID=PushManager.shared().getAPID();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		if(misNet.isNetworkAvailable()){
			if(PushManager.shared().getAPID()!=null){
				Config.debug("StartUP", "StartUp Device Id called "+PushManager.shared().getAPID());
				AppLaunchPrefs.appLaunchApi(mContext);
				
			}else{
				Config.debug("StartUP", "StartUp Device Id called "+PushManager.shared().getAPID());
			}
			
		}*/
		startTime();
		try {
			Crittercism.initialize(mContext, getResources().getString(R.string.critism));
			try {
				Bundle bundle=getIntent().getExtras();
				if(bundle!=null) {
					miLaunchVia=bundle.getInt("requestCode",0);
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			
			msArea=mDbUtil.getActiveAreaPlace();

			try	{
				EventTracker.startLocalyticsSession(mContext);
				if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Edit_Store_alert))) {
					mEventMap.put("Type", "Edit_Store");
				}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Talk_Now_alert))) {
					mEventMap.put("Type", "Talk_Now");
				}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Login_alert))) {
					mEventMap.put("Type", "Customer_SignIn");
				}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Favourite_alert))) {
					mEventMap.put("Type", "Customer_Favourite");
				}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.Customer_Profile_alert))) {
					mEventMap.put("Type", "Customer_SpecialDate");
				}else if(miLaunchVia==Integer.parseInt(getResources().getString(R.string.App_Launch))) {
					mEventMap.put("Type", "App_NotLaunched");
				}

				if(mEventMap.size()!=0) {
					Log.i("Request Code ", "Request Code "+mEventMap.toString());
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			EventTracker.logEvent("App_Launches", false);

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {

					if(msArea.length()!=0) {
						Intent intent=new Intent(mContext, MarketPlaceActivity.class);
						intent.putExtra("opeDrawer", true);
						startActivity(intent);
						StartUpActivity.this.finish();
					} else {
						Intent intent=new Intent(mContext, LocationActivity.class);
						startActivity(intent);
						StartUpActivity.this.finish();
					}
				}
			}, mlSplashTimeOut);





		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	

	void startTime()
	{
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		SharedPreferences prefs=getSharedPreferences(Config.APP_START_TIME, 1);
		Editor edit=prefs.edit();
		edit.putString(Config.APP_START_TIME_KEY, df.format(new Date()));
		edit.commit();
	}


	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(mContext);
	}

	protected void onResume() {
		super.onResume();
		try {
			if(mEventMap.size()!=0) {
				EventTracker.logEvent("App_LaunchLocalNotification",mEventMap );
			}
			checkFirstTimeInstall();
			checkLastUsedMonth();
			checkLastUsedDate();
			checkLastUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	protected void onPause() {
		EventTracker.endLocalyticsSession(mContext);
		super.onPause();
	}

	protected void onStop() {
		EventTracker.endFlurrySession(mContext);
		super.onStop();
	}

	private void checkFirstTimeInstall() {
		try {
			final String PREFS_NAME = "MyPrefsFile";
			SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);

			if (settings.getBoolean("my_first_time", true)) {
				EventTracker.logEvent("App_Installs", false);
				settings.edit().putBoolean("my_first_time", false).commit(); 
			}
		}catch(Exception e) {
			//e.printStackTrace();
		}
	}

	private void checkLastUpdate() {
		SharedPreferences prefs =  mContext.getSharedPreferences("verCode", 1);
		SharedPreferences.Editor editor = prefs.edit();
		int versionNew = 0;
		try{
			PackageInfo pInfoNew = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNew = pInfoNew.versionCode;
		}catch(Exception e){

		}
		int tempVer = prefs.getInt("verCode", -1);
		if(tempVer == -1){ 
			try {
				PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				int version = pInfo.versionCode;
				editor.putInt("verCode", version);
				editor.commit();
			}catch(Exception e){
			}
		}

		if(versionNew != tempVer) {
			try	{
				if(mLocalNotification.isAppLaunchedFired()==true) {
					mLocalNotification.clearAppLaunchPrefs();
				}
				if(mLocalNotification.isLoginAlarmFired()==true) {
					mLocalNotification.clearLoginPrefs();
				}
				if(mLocalNotification.isPlaceAlarmFired()==true) {
					mLocalNotification.clearPlacePrefs();
				}
				if(mLocalNotification.isPostAlarmFired()==true) {
					mLocalNotification.clearPostPrefs();
				}
				if(mLocalNotification.isProfilAlarmFired()==true) {
					mLocalNotification.clearProfilePrefs();
				}
				if(mLocalNotification.isFavAlarmFired()==true) {
					mLocalNotification.clearLoginPrefs();
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
			EventTracker.logEvent("App_Upgrades", false);
			editor.putInt("verCode", versionNew);
			editor.commit();
		}
	}

	private void checkLastUsedDate() {
		try {
			String current_date="";
			String lastDate="";
			Date sysDate=new Date();

			SimpleDateFormat formatter = new SimpleDateFormat("MMMM d yyyy");
			SharedPreferences prefs =  mContext.getSharedPreferences("dateUsed", 1);

			SharedPreferences.Editor editor = prefs.edit();

			String tempDate = prefs.getString("dateUsed", "");

			if(tempDate.length()==0){
				current_date=formatter.format(sysDate);
				editor.putString("dateUsed", current_date);
				editor.commit();
				EventTracker.logEvent("App_DailyEngagedUsers", false);
			}else {
				lastDate=prefs.getString("dateUsed", "");	
				try {
					Date date=formatter.parse(lastDate);
					if(date.before(formatter.parse(formatter.format(sysDate)))) {
						current_date=formatter.format(sysDate);
						editor.putString("dateUsed", current_date);
						editor.commit();
						EventTracker.logEvent("App_DailyEngagedUsers", false);
						lastDate=prefs.getString("dateUsed", "");	
					}
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	private void checkLastUsedMonth(){
		try {
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH)+1;
			SharedPreferences prefs =  mContext.getSharedPreferences("monthUsed", 1);
			SharedPreferences.Editor editor = prefs.edit();

			int tempMonth = prefs.getInt("monthUsed", -1);
			if(tempMonth == -1) {
				editor.putInt("monthUsed", month);
				editor.commit();
				EventTracker.logEvent("App_MonthlyEngagedUsers", false);
			}
			if(month != tempMonth) {
				editor.putInt("monthUsed", month);
				editor.commit();
				EventTracker.logEvent("App_MonthlyEngagedUsers", false);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	private boolean getTourPrefs() {
		SharedPreferences prefs=mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
		return prefs.getBoolean(TOUR_KEY, false);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void finish() {
		super.finish();

	}

	//	
	//	 if(msArea.length()!=0) {
	//			Intent intent=new Intent(mContext, MarketPlaceActivity.class);
	//			intent.putExtra("opeDrawer", true);
	//			startActivity(intent);
	//			this.finish();
	//		    } else {
	//			mbTourEnded=getTourPrefs();
	//			if(!mbTourEnded) {
	//			    Intent intent=new Intent(mContext, TourActivity.class);
	//			    startActivity(intent);
	//			    this.finish();
	//			} else {
	//			    if(msArea.length()==0) {
	//				Intent intent=new Intent(mContext, LocationActivity.class);
	//				startActivity(intent);
	//				this.finish();
	//			    } else {
	//				Intent intent=new Intent(mContext, MarketPlaceActivity.class);
	//				intent.putExtra("opeDrawer", true);
	//				startActivity(intent);
	//				this.finish();
	//			    }
	//			}
	//		    }

}
