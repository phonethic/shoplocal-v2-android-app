package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.CallListDialog;
import com.phonethics.adapters.MarketPlaceSearchAdapter;

public class CategorySearch extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener,ListClickListener
{
	private Activity mContext;

	private PullToRefreshListView mCategList;
	private TextView mTxtCounter;

	private MarketPlaceSearchAdapter mMarketAdapter;

	private String msCallType="";
	private String msActiveAreaName="";
	private String msStoreName="";

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrPostTitle=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrTotalLike=new ArrayList<String>();

	private ArrayList<String> mArrMobileNo1=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo2=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo3=new ArrayList<String>();

	private ArrayList<String> mArrTelNo1=new ArrayList<String>();
	private ArrayList<String> mArrTelNo2=new ArrayList<String>();
	private ArrayList<String> mArrTelNo3=new ArrayList<String>();

	private ArrayList<String> mArrDistance= new ArrayList<String>();
	private ArrayList<String> mArrLogo= new ArrayList<String>();
	private ArrayList<String> mArrHasOffer= new ArrayList<String>();
	private ArrayList<String> mArrDescription= new ArrayList<String>();
	private ArrayList<String> mArrCall= new ArrayList<String>();

	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;

	private boolean mbIsRefreshing=false;

	int miTotalRecords;
	int miTotalPages;
	int miCurrentPage=0;
	int miItemPerPage=50;
	private int miTempPosition=0;

	private String msCategoryId="";

	private ProgressDialog mProgressDialog;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_search);
		hideDrawerLayout();
		setTitle("Category");
		mContext=this;
		mTxtCounter=(TextView)findViewById(R.id.txtCategSearchCounter);
		mCategList=(PullToRefreshListView)findViewById(R.id.listCategSearchResult);
		
		mProgressDialog=new ProgressDialog(mContext);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");
		
		msActiveAreaName=mDbUtil.getActiveArea();

		mMarketAdapter=new MarketPlaceSearchAdapter(mContext, 0, 0, mArrStoreId,mArrStoreName, mArrLogo, mArrHasOffer, 
				mArrPostTitle, mArrDescription, mArrDistance, mArrTotalLike,this); 
		mCategList.setAdapter(mMarketAdapter);

		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());

		Bundle bundle=getIntent().getExtras();

		if(bundle!=null)
		{
			msCategoryId=bundle.getString("category_id");
			getStores();
		}

		mCategList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				int tempPos=position-1;
				Intent intent=new Intent(mContext, StoreDetailsActivity.class);
				intent.putExtra("STORE_ID", mArrStoreId.get(tempPos));
				startActivity(intent);
			}
		});

		mCategList.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
			@Override
			public void onLastItemVisible() {
				if(mInternet.isNetworkAvailable()){
					if(miCurrentPage<miTotalPages){
						getStores();
					}
					else{
						debug("current page "+miCurrentPage+" total pages "+miTotalPages);
					}
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		mCategList.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mbIsRefreshing=true;
				if(mInternet.isNetworkAvailable()){
					miCurrentPage=0;
					getStores();
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

	}//onCreate ends here


	void getStores()
	{
		if(mInternet.isNetworkAvailable()){
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("area_id", Config.MY_AREA));
			nameValuePairs.add(new BasicNameValuePair("latitude", Config.LATITUDE));
			nameValuePairs.add(new BasicNameValuePair("longitude",Config.LONGITUDE ));
			nameValuePairs.add(new BasicNameValuePair("distance",Config.DISTANCE));
			nameValuePairs.add(new BasicNameValuePair("search", mEdtSearch.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("category_id",msCategoryId));
			nameValuePairs.add(new BasicNameValuePair("page", (miCurrentPage+1)+""));
			nameValuePairs.add(new BasicNameValuePair("count", miItemPerPage+""));
			String paramString=URLEncodedUtils.format(nameValuePairs, "utf-8");
			String sURL = Config.MARKET_PLACE_URL +paramString;
			new ShoplocalAsyncTask(this, "get", "customer", false,"stores").execute(sURL);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));	
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishTo();
		return true;
	}

	private void showProgressDialog(){
		mProgressDialog.show();
	}

	public void onBackPressed() {
		super.onBackPressed();
		finishTo();
	}

	void finishTo()
	{
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		if(tag.equalsIgnoreCase("stores"))
		{
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;
				if(status.equals("false")){
					return;
				}
				if(mbIsRefreshing==true){
					mbIsRefreshing=false;
					clearData();
					mCategList.onRefreshComplete();
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");
				if(dataObject == null) return;

				miCurrentPage++;
				miTotalRecords = dataObject.getInt("total_record");
				miTotalPages = dataObject.getInt("total_page");

				JSONArray jsonArray = dataObject.getJSONArray("record");
				if(jsonArray == null)  return;

				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject storeObject = jsonArray.getJSONObject(i);
					if(storeObject != null) {
						mArrStoreId.add(storeObject.getInt("id")+"");
						mArrStoreName.add(storeObject.getString("name"));
						mArrDescription.add(storeObject.getString("description"));
						mArrLogo.add(storeObject.getString("image_url"));
						mArrMobileNo1.add(storeObject.getString("mob_no1"));
						mArrMobileNo2.add(storeObject.getString("mob_no2"));
						mArrMobileNo3.add(storeObject.getString("mob_no3"));
						mArrTelNo1.add(storeObject.getString("tel_no1"));
						mArrTelNo2.add(storeObject.getString("tel_no2"));
						mArrTelNo3.add(storeObject.getString("tel_no3"));
						if(storeObject.has("distance")){
							mArrDistance.add(storeObject.getString("distance"));
							debug(storeObject.getString("distance"));
						}
						else{
							mArrDistance.add("0");
						}
						mArrTotalLike.add(storeObject.getInt("total_like")+"");
						mArrHasOffer.add(storeObject.getString("has_offer"));
						mArrPostTitle.add(storeObject.getString("title"));
					}
				}
				mMarketAdapter.notifyDataSetChanged();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}

	}

	void debug(String msg){
		Log.i("Category ", "Category "+msg);
	}

	@Override
	public void onError(int errorCode) {
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		showToast(getResources().getString(R.string.errorMessage));
	}

	void clearData()
	{
		mArrStoreId.clear();
		mArrStoreName.clear();
		mArrPostTitle.clear();
		mArrTotalLike.clear();
		mArrDistance.clear();
		mArrLogo.clear();
		mArrHasOffer.clear();
		mArrDescription.clear();

		miTotalPages=0;
		miTotalRecords=0;
		miCurrentPage=0;

	}


	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		miTempPosition=position;
		if(tag.equalsIgnoreCase("call")){
			mArrCall.clear();

			mCalldailog.setTitle(mArrStoreName.get(position));
			if(mArrMobileNo1.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo1.get(position).toString());
			}
			if(mArrMobileNo2.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo2.get(position).toString());
			}
			if(mArrMobileNo3.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo3.get(position).toString());
			}
			if(mArrTelNo1.get(position).length()>5){
				mArrCall.add(mArrTelNo1.get(position).toString());
			}
			if(mArrTelNo2.get(position).length()>5){
				mArrCall.add(mArrTelNo2.get(position).toString());
			}
			if(mArrTelNo3.get(position).length()>5){
				mArrCall.add(mArrTelNo3.get(position).toString());
			}
			mCallAdapter.notifyDataSetChanged();

			if(mArrCall.size()!=0){
				msStoreName=mArrStoreName.get(position);
				
				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("Source","MarketPlaceCategory");
				callClicked.put("storeName",msStoreName);
				debug(msStoreName);
				EventTracker.logEvent("StoreCall", callClicked);

				msCallType="StoreCall";
				callContactApi(mArrStoreId.get(position));
				mCalldailog.show();
			}else{
				showToast("No numbers to call");
			}
		}		
	}
	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {


			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("Source","MarketPlaceCategory");
			callClicked.put("storeName",msStoreName);
			debug(msStoreName);
			callClicked.put("Number",mArrCall.get(position));
			EventTracker.logEvent("StoreCallDone", callClicked);
			likePlace(mArrStoreId.get(miTempPosition));
			msCallType="StoreCallDone";
			callContactApi(mArrStoreId.get(miTempPosition));
			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();

		}

	}
	void callContactApi(String id)
	{
		try
		{
//			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);

					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put("place_id", id);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);

					debug(jsonObject.toString());

					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
//			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	void likePlace(String postId)
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", postId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlaceOnCall").execute(msUrl);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}

