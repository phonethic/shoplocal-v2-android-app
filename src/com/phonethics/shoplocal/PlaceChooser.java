package com.phonethics.shoplocal;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.StoreListModel;
import com.squareup.picasso.Picasso;

public class PlaceChooser extends ShoplocalActivity
{

	private ListView mStoreList;
	private ListView mTalkNowList;

	private EditText mEdtSearch;

	private Button mBtnProceed;

	private Activity mContext;

	private int miChoiceMode=0;
	private int miViewPost=-1;
	private int miAddStore=0;
	private int miManageGallery=0;
	private int miReport=-1;
	private int miSureShop=-1;
	private int miTextLength=0;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrStorDescription=new ArrayList<String>();
	private ArrayList<String> mArrStoreLogo=new ArrayList<String>();
	private ArrayList<String> mTempPlaceId=new ArrayList<String>();


	private StoreSearchListAdapter mAdapter;
	private CreateBroadCastListAdapter mTalkNowAdapter;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_chooser);
		hideDrawerLayout();
		setTitle("Choose Place");
		mContext=this;

		mStoreList=(ListView)findViewById(R.id.singlePlaceChooserList);
		mTalkNowList=(ListView)findViewById(R.id.MultiPlaceChooserList);

		mBtnProceed=(Button)findViewById(R.id.btnSelectStore);

		mEdtSearch = (EditText) findViewById(R.id.searchText);

		try
		{
			Bundle bundle=getIntent().getExtras();
			if(bundle!=null)
			{

				miChoiceMode=bundle.getInt("choiceMode");
				miViewPost=bundle.getInt("viewPost");
				miReport=bundle.getInt("report");
				miSureShop=bundle.getInt("sure_shop");
				miAddStore=bundle.getInt("AddStore");
				miManageGallery=bundle.getInt("manageGallery");
				
				if(miChoiceMode==1){
					mStoreList.setVisibility(View.VISIBLE);
					mTalkNowList.setVisibility(View.GONE);
					mBtnProceed.setVisibility(View.GONE);
				}
				//Broadcast
				else if(miChoiceMode==2){
					mStoreList.setVisibility(View.GONE);
					mTalkNowList.setVisibility(View.VISIBLE);
					mBtnProceed.setVisibility(View.VISIBLE);
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			mArrStoreId=mDbUtil.getAllPlacesIDs();
			mArrStoreName=mDbUtil.getAllPlacesNames();
			mArrStoreLogo=mDbUtil.getAllPlacesPhotoUrl();
			mArrStorDescription=mDbUtil.getAllPlacesDesc();
			mAdapter=new StoreSearchListAdapter(mContext, R.drawable.ic_launcher, R.drawable.ic_launcher, mArrStoreName, mArrStoreLogo,mArrStorDescription);
			mStoreList.setAdapter(mAdapter);

			ArrayList<StoreListModel> list=new ArrayList<StoreListModel>();
			ArrayList<StoreListModel> list_id=new ArrayList<StoreListModel>();
			ArrayList<StoreListModel> list_id_desc=new ArrayList<StoreListModel>();

			for(int i=0;i<mArrStoreId.size();i++)
			{
				list.add( get(mArrStoreName.get(i), false) );
				list_id.add( getID(mArrStoreName.get(i),mArrStoreId.get(i),false) );
				list_id_desc.add( getID(mArrStoreName.get(i),mArrStoreId.get(i),mArrStorDescription.get(i),false) );
			}

			mTalkNowAdapter=new CreateBroadCastListAdapter(mContext,0,0,list,list_id,mArrStoreLogo,list_id_desc);
			mTalkNowList.setAdapter(mTalkNowAdapter);
			// Searching in mode 1 
			if(miChoiceMode == 1){
				mEdtSearch.setOnEditorActionListener(new OnEditorActionListener() {
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						}
						return false;
					}
				});

				mEdtSearch.addTextChangedListener(new TextWatcher() {
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						final ArrayList<String> STOREID_SUB = new ArrayList<String>();
						final	ArrayList<String> STORE_NAME_SUB = new ArrayList<String>();
						final	ArrayList<String> PHOTO_SUB = new ArrayList<String>();
						final ArrayList<String> STORE_DESC_SUB = new ArrayList<String>();

						miTextLength = mEdtSearch.getText().length();

						for (int i = 0; i < mArrStoreName.size(); i++)
						{
							if (miTextLength <= mArrStoreName.get(i).length()){
								if (mEdtSearch.getText().toString().
										equalsIgnoreCase((String) mArrStoreName.get(i).subSequence(0, miTextLength))){
									STORE_NAME_SUB.add(mArrStoreName.get(i));
									STOREID_SUB.add(mArrStoreId.get(i));
									PHOTO_SUB.add(mArrStoreLogo.get(i));
									STORE_DESC_SUB.add(mArrStorDescription.get(i));	
								}
							}
						}
						mAdapter=new StoreSearchListAdapter(mContext, R.drawable.ic_launcher, R.drawable.ic_launcher, STORE_NAME_SUB, PHOTO_SUB,STORE_DESC_SUB);
						mStoreList.setAdapter(mAdapter);	
						
						mStoreList.setOnItemClickListener(new OnItemClickListener() {

							@Override 
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int position, long arg3) {
								if(miViewPost==2){
									Intent intent=new Intent(mContext, EditStore.class);
									intent.putExtra("STORE_NAME", STORE_NAME_SUB.get(position));
									intent.putExtra("STORE_LOGO", PHOTO_SUB.get(position));
									intent.putExtra("STORE_ID", STOREID_SUB.get(position));
									intent.putExtra("manageGallery",miManageGallery);
									/*			intent.putExtra("business_id", BUSINESS_REC_ID);*/
									startActivity(intent);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
									finish();
								}
								//View Posts
								else if(miViewPost==1){
									Intent intent=new Intent(mContext, ViewPost.class);
									intent.putExtra("storeName", STORE_NAME_SUB.get(position));
									intent.putExtra("STORE_ID", STOREID_SUB.get(position));
									startActivity(intent);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
									finish();
								}
								else if(miReport == 1){
									Intent intent=new Intent(mContext, Reports.class);
									intent.putExtra("STORE_ID", STOREID_SUB.get(position));
									startActivity(intent);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
									finish();
								}
								else if(miSureShop==1){
									Intent intent=new Intent(mContext,SureShop.class);
									intent.putExtra("SureShop",1);
									intent.putExtra("STORE_NAME", STORE_NAME_SUB.get(position));
									intent.putExtra("STORE_LOGO", PHOTO_SUB.get(position));
									intent.putExtra("STORE_ID", STOREID_SUB.get(position));
									startActivity(intent);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
									finish();
								}
								
							}
						});
					}
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
					}
					public void afterTextChanged(Editable s) {
					}
				});
			}
			else{
				mEdtSearch.setVisibility(View.GONE);
			}
			mBtnProceed.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					mTempPlaceId.clear();
					ArrayList<StoreListModel>temp=mTalkNowAdapter.TITLE	;
					for(int i=0;i<temp.size();i++)
					{
						StoreListModel model=temp.get(i);
						/*cadapter.notifyDataSetChanged();*/
						if(model.isSelected()){
							Log.i("PLACE ", "Slected Place "+mArrStoreName.get(i)+" ID "+mArrStoreId.get(i));
							mTempPlaceId.add(mArrStoreId.get(i));
						}
					}
					if(mTempPlaceId.size()!=0){
						Intent intent=new Intent(mContext, CreateBroadCast.class);
						intent.putStringArrayListExtra("SELECTED_STORE_ID",mTempPlaceId);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}
					else{
						showToast(getResources().getString(R.string.choosePlaceAlert));
					}
				}
			});

			mStoreList.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					storeListClick(position);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	void storeListClick(int position)
	{
		//Edit Store
		if(miViewPost==2){
			Intent intent=new Intent(mContext, EditStore.class);
			intent.putExtra("STORE_NAME", mArrStoreName.get(position));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(position));
			intent.putExtra("STORE_ID", mArrStoreId.get(position));
			intent.putExtra("manageGallery",miManageGallery);
			/*			intent.putExtra("business_id", BUSINESS_REC_ID);*/
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		//View Posts
		else if(miViewPost==1){
			Intent intent=new Intent(mContext, ViewPost.class);
			intent.putExtra("storeName", mArrStoreName.get(position));
			intent.putExtra("STORE_ID", mArrStoreId.get(position));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else if(miReport == 1){
			Intent intent=new Intent(mContext, Reports.class);
			intent.putExtra("STORE_ID", mArrStoreId.get(position));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else if(miSureShop==1){
			Intent intent=new Intent(mContext,SureShop.class);
			intent.putExtra("SureShop",1);
			intent.putExtra("STORE_NAME", mArrStoreName.get(position));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(position));
			intent.putExtra("STORE_ID", mArrStoreId.get(position));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
	
	}
	
	

	private StoreListModel get(String placename,boolean selected) {
		return new StoreListModel(placename,selected);
	}
	private StoreListModel getID(String placename,String id,boolean selected) {
		StoreListModel sm=new StoreListModel(placename, selected);
		sm.setId(id);
		return  sm;
	}

	private StoreListModel getID(String placename,String id,String placedesc,boolean selected) {
		StoreListModel sm=new StoreListModel(placename,placedesc,selected);
		sm.setId(id);
		return  sm;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishto();
		return true;
	}

	//Create New Post
	class CreateBroadCastListAdapter extends ArrayAdapter<StoreListModel>
	{
		ArrayList<StoreListModel> TITLE;
		ArrayList<StoreListModel> ID;
		ArrayList<StoreListModel>list_id_desc;
		ArrayList<String>distance,logo;
		Activity context;
		LayoutInflater inflate;

		public CreateBroadCastListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<StoreListModel> TITLE,ArrayList<StoreListModel> ID,ArrayList<String>logo,ArrayList<StoreListModel>list_id_desc) {
			super(context, resource, textViewResourceId, TITLE);

			this.TITLE=TITLE;
			this.logo=logo;
			this.context=context;
			this.ID=ID;
			this.list_id_desc=list_id_desc;
			inflate=context.getLayoutInflater();
		}

		public int getCount() {

			return TITLE.size();
		}



		public long getItemId(int position) {
			return position;
		}

		StoreListModel getselectedposition(int position) {
			return ((StoreListModel) getItem(position));
		}

		ArrayList<StoreListModel> getcheckedposition() {
			ArrayList<StoreListModel> checkedposition = new ArrayList<StoreListModel>();
			for (StoreListModel p : ID) {
				if (p.isSelected())
					checkedposition.add(p);
			}
			return checkedposition;
		}

		OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getselectedposition((Integer) buttonView.getTag()).selected = isChecked;
			}
		};



		public View getView(final int position, View convertView, ViewGroup parent) {
			if(convertView==null)
			{
				final ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_selectstore,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.placeCheckBox=(CheckBox)convertView.findViewById(R.id.placeCheckBox);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.placeCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {


					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

						StoreListModel element=(StoreListModel)holder.placeCheckBox.getTag();
						element.setSelected(buttonView.isChecked());

					}
				});
				convertView.setTag(holder);
				holder.placeCheckBox.setTag(TITLE.get(position));
			}
			else
			{
				((ViewHolder)convertView.getTag()).placeCheckBox.setTag(TITLE.get(position));
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.placeCheckBox.setChecked(TITLE.get(position).isSelected());


			if(logo.get(position).toString().length()!=0)

			{
				String photo_source=logo.get(position).toString().replaceAll(" ", "%20");
				try{
					Picasso.with(context).load(Config.IMAGE_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}

			hold.txtTitle.setText(TITLE.get(position).getPlacename());
			hold.txtSearchStoreDesc.setText(list_id_desc.get(position).getPlacedesc());
			return convertView;
		}
	}

	class ViewHolder
	{

		TextView txtTitle;
		ImageView imgStoreLogo;
		TextView txtStore;
		CheckBox placeCheckBox;
		TextView txtSearchStoreDesc;

	}

	void finishto()
	{
		Intent intent=new Intent(mContext, MerchantTalkHome.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

	public void onBackPressed() {
		finishto();
	}

	class StoreSearchListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> TITLE,distance,logo;
		Activity context;
		LayoutInflater inflate;
		ArrayList<String>STORE_DESC;
		public StoreSearchListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> TITLE,ArrayList<String>logo,ArrayList<String>STORE_DESC) {
			super(context, resource, textViewResourceId, TITLE);

			this.TITLE=TITLE;
			this.logo=logo;
			this.context=context;
			this.STORE_DESC=STORE_DESC;
			inflate=context.getLayoutInflater();
		}

		public int getCount() {
			return TITLE.size();
		}

		public String getItem(int position) {
			return TITLE.get(position);
		}

		public View getView(int position, View convertView, ViewGroup parent) {


			if(convertView==null)
			{
				StoreSearchViewHolder holder=new StoreSearchViewHolder();
				convertView=inflate.inflate(R.layout.layout_place_search_api,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.band=(TextView)convertView.findViewById(R.id.band);
				holder.txtSearchStoreDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				convertView.setTag(holder);

			}
			StoreSearchViewHolder hold=(StoreSearchViewHolder)convertView.getTag();

			hold.band.setVisibility(View.GONE);
			hold.txtSearchStoreDistance.setVisibility(View.GONE);
			if(logo.get(position).toString().length()!=0)
			{
				String photo_source=logo.get(position).toString().replaceAll(" ", "%20");
				try{
					Picasso.with(context).load(Config.IMAGE_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}
			String title = TITLE.get(position).trim();
			String seperator = "\\\\'";
			String []tempText = title.split(seperator);
			
			/*if (tip.contains(seperator)){*/
	        String b= "";
	        for (String c : tempText) {
	        	b +=c+"\'";
	        }
			hold.txtTitle.setText(b.substring(0, b.length()-1));
			hold.txtSearchStoreDesc.setText(STORE_DESC.get(position));

			return convertView;
		}

	}

	class StoreSearchViewHolder
	{

		TextView txtTitle;
		ImageView imgStoreLogo;
		TextView txtStore;
		TextView txtSearchStoreDesc;
		TextView band;
		TextView txtSearchStoreDistance;

	}

}