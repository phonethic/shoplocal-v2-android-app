package com.phonethics.shoplocal;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TourActivity extends Activity implements OnClickListener{

	Activity mContext;
	SessionManager mSession;

	TextView mTourSubTitle;
	TextView mTourSubTitle2;
	TextView mTxtlater;
	TextView mTxtMerchantSignUp;

	ImageView mStaticImage;
	ImageView mStaticLogo;

	Button mSignUpFb;
	Button mSignUpCustomer;

	LinearLayout mSignUpButtonLayout;
	Typeface mTf;

	Animation mFadeIn;
	Animation mFadeOut;

	NetworkCheck network;
	Runnable mRunnable_anim;

	int miCount =1;
	boolean mbRunnableFinished = false;

	ArrayList<String> mSubTitles=new ArrayList<String>();

	String PREF_NAME="TOUR_PREF";
	String TOUR_KEY="TOUR_VALUE";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour);

		mContext=this;
		mSession=new SessionManager(mContext);
		network=new NetworkCheck(mContext);
		mFadeIn=AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
		mFadeOut=AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
		mTf=Typeface.createFromAsset(mContext.getAssets(), "fonts/GOTHIC_0.TTF");

		mTourSubTitle=(TextView)findViewById(R.id.tourSubTitle);
		mTourSubTitle2=(TextView)findViewById(R.id.tourSubTitle2);
		mTxtMerchantSignUp=(TextView)findViewById(R.id.txtMerchantSignUp);
		mTxtlater=(TextView)findViewById(R.id.txtlater);

		mSignUpButtonLayout=(LinearLayout)findViewById(R.id.signUpButtonLayout);

		mStaticLogo=(ImageView)findViewById(R.id.staticLogo);
		mStaticImage=(ImageView)findViewById(R.id.staticImage);

		mSignUpCustomer=(Button)findViewById(R.id.signUpCustomer);
		mSignUpFb = (Button) findViewById(R.id.signUpFb);

		mSignUpFb.setTypeface(mTf);
		mSignUpCustomer.setTypeface(mTf);

		mFadeOut.setAnimationListener(new StaticImage());

		mSignUpCustomer.setText("Sign up with shoplocal");

		mTourSubTitle.setTypeface(mTf);
		mTourSubTitle2.setTypeface(mTf);
		mTxtlater.setTypeface(mTf);
		mSignUpCustomer.setTypeface(mTf);

		mSubTitles.add(getResources().getString(R.string.subTitle1));
		mSubTitles.add(getResources().getString(R.string.subTitle2));
		mSubTitles.add(getResources().getString(R.string.subTitle3));
		mSubTitles.add(getResources().getString(R.string.subTitle4));
		mSubTitles.add(getResources().getString(R.string.subTitle5));
		mSubTitles.add(getResources().getString(R.string.subTitle6));
		mSubTitles.add(getResources().getString(R.string.subTitle7));
		mSubTitles.add(getResources().getString(R.string.subTitle8));
		mSubTitles.add(getResources().getString(R.string.subTitle9));
		mSubTitles.add(getResources().getString(R.string.subTitle10));
		mSubTitles.add(getResources().getString(R.string.subTitle11));
		mSubTitles.add(getResources().getString(R.string.subTitle12));
		mSubTitles.add(getResources().getString(R.string.subTitle22));


		mTourSubTitle2.setText(getResources().getString(R.string.subTitle1));

		//First text animation
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new AccelerateInterpolator()); 
		fadeIn.setDuration(1000);

		mTourSubTitle2.startAnimation(fadeIn);
		try{
			fadeIn.setAnimationListener(new AnimationListener() {
				public void onAnimationStart(Animation animation) {
				}
				public void onAnimationRepeat(Animation animation) {
				}
				public void onAnimationEnd(Animation animation) {
					Animation fadeOut = new AlphaAnimation(1, 0);
					fadeOut.setInterpolator(new AccelerateInterpolator()); 
					fadeOut.setDuration(600);
					mTourSubTitle2.startAnimation(fadeOut);
				}
			});
			startImageAnimation(mTourSubTitle, mTourSubTitle2, mSubTitles);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		mSignUpCustomer.setOnClickListener(this);
		mTxtlater.setOnClickListener(this);
		mTxtMerchantSignUp.setOnClickListener(this);
		mSignUpFb.setOnClickListener(this);
		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
	}
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}
	void setTourPrefs(){
		SharedPreferences prefs=mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putBoolean(TOUR_KEY, true);
		editor.commit();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2){
			if(mSession.isLoggedInCustomer()){
				setTourPrefs();
				Intent intent=new Intent(mContext,LocationActivity.class);
				startActivity(intent);;
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();
			}
		}
		if(requestCode==4){
			if(mSession.isLoggedIn()){
				EventTracker.logEvent("Tour_SellSuccess", false);
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();
				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore){
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();
				}
				else{
					Intent intent=new Intent(mContext,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}

	void startImageAnimation(final TextView txt_1,final TextView txt_2,final ArrayList<String> subTitles)
	{
		final Animation fade_in_anim=AnimationUtils.loadAnimation(mContext, R.anim.word_fade_in);
		final Animation fade_out_anim=AnimationUtils.loadAnimation(mContext, R.anim.word_fade_out);
		final Handler handler_anim = new Handler();
		
		mRunnable_anim = new Runnable() {
			public void run() {
				try{
					
					txt_2.setText(subTitles.get(miCount));
					txt_2.startAnimation(fade_in_anim);

					fade_in_anim.setAnimationListener(new AnimationListener() {
						public void onAnimationStart(Animation animation) {
							txt_1.startAnimation(fade_out_anim);
						}
						public void onAnimationRepeat(Animation animation) {
						}
						public void onAnimationEnd(Animation animation) {
							txt_1.setText(subTitles.get(miCount));
							miCount++;
							if(miCount==subTitles.size()-1){
								mbRunnableFinished = true;
								handler_anim.removeCallbacks(mRunnable_anim);
								mStaticImage.startAnimation(mFadeOut);
								
								if(mbRunnableFinished){
									txt_2.setVisibility(View.GONE);
									Animation fadeOutManual = new AlphaAnimation(1, 0);
									fadeOutManual.setDuration(400);
									txt_1.startAnimation(fadeOutManual);
									
									fadeOutManual.setAnimationListener(new AnimationListener() {
										public void onAnimationStart(Animation animation) {
										}
										public void onAnimationRepeat(Animation animation) {
										}
										public void onAnimationEnd(Animation animation) {
											txt_1.setText("");
											txt_2.setVisibility(View.VISIBLE);
											txt_2.setText(subTitles.get(subTitles.size()-1));
											Animation fadeInManual = new AlphaAnimation(0, 1);
											fadeInManual.setDuration(800);
											txt_2.startAnimation(fadeInManual);
										}
									});
								}
							}
						}
					});
					handler_anim.postDelayed(mRunnable_anim, 300);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		handler_anim.postDelayed(mRunnable_anim,1600);
	}

	class StaticImage implements AnimationListener
	{
		public void onAnimationEnd(Animation animation) {
			mStaticImage.setVisibility(View.GONE);
			mSignUpButtonLayout.setVisibility(View.VISIBLE);
			mStaticLogo.setVisibility(View.VISIBLE);
			mTxtMerchantSignUp.setVisibility(View.VISIBLE);
			mTxtMerchantSignUp.startAnimation(mFadeIn);
			mStaticLogo.startAnimation(mFadeIn);
			mSignUpButtonLayout.startAnimation(mFadeIn);
			Animation fadeOutManual = new AlphaAnimation(1, 0);
			fadeOutManual.setDuration(800);
			fadeOutManual.setStartOffset(200);
			mTourSubTitle2.startAnimation(fadeOutManual);
			fadeOutManual.setFillAfter(true);
		}
		public void onAnimationRepeat(Animation animation) {
		}
		public void onAnimationStart(Animation animation) {
		}
	}

	@Override
	public void onClick(View v) {
		
		if(v.getId()==mSignUpCustomer.getId()){
			EventTracker.logEvent("Tour_SignUp", false);
			setTourPrefs();
			Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
			intent.putExtra("facebookLogin", false);
			intent.putExtra("isFromTour", true);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if(v.getId()==mTxtlater.getId()){
			EventTracker.logEvent("Tour_Later", false);
			setTourPrefs();
			Intent intent=new Intent(mContext,LocationActivity.class);
			startActivity(intent);;
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		if(v.getId()==mTxtMerchantSignUp.getId()){
			EventTracker.logEvent("Tour_Sell", false);
			setTourPrefs();
			Intent intent=new Intent(mContext,SplitLoginSignUp.class);
			startActivityForResult(intent, 4);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if(v.getId()==mSignUpFb.getId()){
			EventTracker.logEvent("Tour_LoginFB", false);
			setTourPrefs();
			Intent intent = new Intent(mContext, LoginSignUpCustomer.class);
			intent.putExtra("facebookLogin", true);
			startActivityForResult(intent,2);
		}

	}

}
