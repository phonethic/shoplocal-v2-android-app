package com.phonethics.shoplocal;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.widget.FacebookDialog;
import com.squareup.picasso.Picasso;



public class OffersBroadCastDetails extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener 
{
	private Dialog mCalldailog;
	private ListView mListCall;

	private Activity mContext;

	private LinearLayout mMerchantView;
	private LinearLayout mLikeShareLayout;

	private TextView mTxtCustomView;
	private TextView mTxtOfferTitle;
	private TextView mTxtOfferDescription;
	private TextView mTxtOfferShare;
	private TextView mTxtOfferLike;
	private TextView mTxtOfferView;
	private TextView mTxtOfferShareBtn;
	private TextView mTxtOfferValidTill;
	private TextView mTxtOfferPostedDate;

	private TableRow mTblOfferImageRow;

	private ImageView mImgOfferImage;
	private ImageView mImgOfferfav;
	private ImageView mImgOfferShare;
	private ImageView mImgVerticalSep;
	private ImageView mImageSeprator;

	private Button mBtnOfferShare;
	private Button mBtnOfferLike;
	private Button mBtnAddtoShoplocal;
	private Button mBtnBuy;
	private Button mBtnGotoStore;

	private View mViewOverlay;

	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msPostId="";
	private String msStoreId="";
	private String msStoreName="";
	private String msUserLike="";
	private String msImageLink="";
	private String msPostDescription="";
	private String msTotalLike="";
	private String msTotalShare="";
	private String msTotalView="";
	private String msIsOffer="";
	private String msOfferValidTill="";
	private String msOfferPostDate="";
	private String msOfferImageTitle="";
	private String msShareVia="Android";
	private String msActiveAreaName="";

	private boolean mbCalledFromCust;
	private boolean mbCalledFromStore;
	private boolean mbCalledFromOffers;
	private boolean mbAutoLike;
	private boolean mbGetLike;
	private boolean mbIspostLiked;

	private ArrayList<String>mArrCallnumbers=new ArrayList<String>();

	int PLACE_LIKE=1;
	int POST_LIKE=2;

	Map<String,String> eventMap=new HashMap<String, String>();
	Map<String,String> eventMapStoreCall=new HashMap<String, String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offers_broad_cast_details);
		hideDrawerLayout();

		mContext=this;

		msActiveAreaName=mDbUtil.getActiveArea();

		View actionBarView = getLayoutInflater().inflate(R.layout.actionbarlayoutforstoredetail, null);
		mTxtCustomView = (TextView) actionBarView.findViewById(R.id.textViewActionBarTitle);
		mActionBar.setCustomView(actionBarView);
		mActionBar.show();

		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);
		mMerchantView=(LinearLayout)findViewById(R.id.merchantView);
		mLikeShareLayout=(LinearLayout)findViewById(R.id.likeShareLayout);

		mTxtOfferTitle=(TextView)findViewById(R.id.txtBroadCastTitle);
		mTxtOfferDescription=(TextView)findViewById(R.id.txtBroadCastBody);
		mTxtOfferLike=(TextView)findViewById(R.id.txtPostFav);
		mTxtOfferShare=(TextView)findViewById(R.id.txtPostShare);
		mTxtOfferView=(TextView)findViewById(R.id.txtPostView);
		mTxtOfferShareBtn=(TextView)findViewById(R.id.shareTextButton);
		
		mTxtOfferValidTill=(TextView)findViewById(R.id.offerValidTill);
		mTxtOfferPostedDate=(TextView)findViewById(R.id.offerPosted);

		mTblOfferImageRow=(TableRow)findViewById(R.id.imageRow);

		mImgOfferImage=(ImageView)findViewById(R.id.imgStoreImage);
		mImgOfferfav=(ImageView)findViewById(R.id.postFav);
		mImgOfferShare=(ImageView)findViewById(R.id.sharePost);

		mBtnOfferShare=(Button)findViewById(R.id.shareButton);
		mBtnOfferLike=(Button)findViewById(R.id.likeButton);
		mBtnAddtoShoplocal=(Button)findViewById(R.id.addToShopLocal);
		mBtnBuy=(Button)findViewById(R.id.byNowButton);
		mBtnGotoStore=(Button)findViewById(R.id.gotoStore);

		mImgVerticalSep=(ImageView)findViewById(R.id.vertiSeprator);
		mImageSeprator=(ImageView)findViewById(R.id.imageSeprator);

		mViewOverlay=(View)findViewById(R.id.viewOverlay);

		mBtnBuy.setText("Call");
		mBtnBuy.setTypeface(tf);
		mBtnGotoStore.setTypeface(tf);

		mTxtOfferTitle.setTypeface(tf);
		mTxtOfferDescription.setTypeface(tf);

		mTxtOfferLike.setTypeface(tf);
		mTxtOfferShare.setTypeface(tf);
		mTxtOfferView.setTypeface(tf);
		
		mTxtOfferValidTill.setTypeface(tf,Typeface.BOLD);
		mTxtOfferPostedDate.setTypeface(tf);

		mTxtOfferLike.setTextColor(Color.parseColor("#c4c4c4"));
		mTxtOfferShare.setTextColor(Color.parseColor("#c4c4c4"));
		mTxtOfferView.setTextColor(Color.parseColor("#c4c4c4"));

		mImgOfferShare.setVisibility(View.VISIBLE);

		try {
			
			Bundle b=getIntent().getExtras();

			if(b!=null) {
				mTxtOfferTitle.setText(b.getString("title"));
				mTxtOfferDescription.setText(b.getString("body"));
				msImageLink=b.getString("photo_source");
				mbCalledFromCust=b.getBoolean("fromCustomer");
				mbCalledFromOffers=b.getBoolean("fromNews");
				msPostId=b.getString("POST_ID");
				msStoreId=b.getString("PLACE_ID");
				msUserLike=b.getString("USER_LIKE");
				msStoreName=b.getString("storeName");
				mbCalledFromStore=b.getBoolean("isFromStoreDetails",false);
				setTitle(msStoreName);

				try
				{
					//actionBar.setTitle(storeName);
					/** Set titles for action bar */
					mTxtCustomView.setText(msStoreName);

					/** Check if actionbar title lines is > 1 and reduce text size */
					mTxtCustomView.post(new Runnable() {


						public void run() {

							int lineCount    = mTxtCustomView.getLineCount();
							if(lineCount > 1){
								mTxtCustomView.setTextSize(15.0f);
							}
						}
					});
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				if(mbCalledFromCust==true && mbCalledFromOffers==false)
				{
					msMobNo1=b.getString("mob1");
					msMobNo2=b.getString("mob2");
					msMobNo3=b.getString("mob3");


					msTelNo1=b.getString("tel1");
					msTelNo2=b.getString("tel2");
					msTelNo3=b.getString("tel3");

					if(msMobNo1.length()>5)
					{
						mArrCallnumbers.add("+"+msMobNo1);
					}
					if(msMobNo2.length()>5)
					{
						mArrCallnumbers.add("+"+msMobNo2);
					}
					if(msMobNo3.length()>5)
					{
						mArrCallnumbers.add("+"+msMobNo3);
					}


					if(msTelNo1.length()>5)
					{
						mArrCallnumbers.add(msTelNo1);
					}
					if(msTelNo2.length()>5)
					{
						mArrCallnumbers.add(msTelNo2);
					}
					if(msTelNo3.length()>5)
					{
						mArrCallnumbers.add(msTelNo3);
					}


					mListCall.setAdapter(new CallListDialog(mContext, 0, mArrCallnumbers));
					mListCall.setOnItemClickListener(new OnItemClickListener() {
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							try
							{
								eventMap.put("Source", "OfferDetails");
								eventMap.put("AreaName",msActiveAreaName);
								EventTracker.logEvent("StoreCallDone",eventMap);

								Intent call = new Intent(android.content.Intent.ACTION_DIAL);
								call.setData(Uri.parse("tel:"+mArrCallnumbers.get(position)));
								startActivity(call);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								mCalldailog.dismiss();
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}
						}
					});


				}

				if(msImageLink.startsWith("notfound"))
				{
					Log.i("Intent Data", "Intent Data photo_source"+msImageLink);

					mImgOfferImage.setScaleType(ScaleType.FIT_CENTER);
				}

				if(msPostId!=null && msPostId.length()>0)
				{
					if(mInternet.isNetworkAvailable()){
						loadOfferDetails();
					}
					else{
						showToast(getString(R.string.noInternetConnection));
					}
				}
				mTxtOfferShareBtn.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						showDialog();
					}
				});

				mBtnAddtoShoplocal.setTypeface(tf);
				mBtnAddtoShoplocal.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						if(mSessionManager.isLoggedInCustomer())
						{
							//likePlace();
							likeStore();
						}
						else
						{
							loginAlert(getResources().getString(R.string.placeFavAlert),PLACE_LIKE);
						}
					}
				});

				mBtnOfferShare.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						showDialog();
					}
				});


				mBtnBuy.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						try
						{
							eventMapStoreCall.put("Source", "OfferDetails");
							eventMapStoreCall.put("AreaName", msActiveAreaName);
							EventTracker.logEvent("StoreCall",eventMapStoreCall);
							mCalldailog.show();
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});

				mBtnGotoStore.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if(mCalldailog.isShowing())
						{
							mCalldailog.dismiss();
						}
						EventTracker.logEvent("OfferDetails_GotoStore", false);
						Intent intent=new Intent(mContext, StoreDetailsActivity.class);
						intent.putExtra("STORE_ID", msStoreId);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}
				});



			}

			if(mbCalledFromCust==true && mbCalledFromOffers==false){
				mImgOfferfav.setVisibility(View.VISIBLE);
				mBtnOfferLike.setVisibility(View.VISIBLE);
				mTxtOfferShareBtn.setVisibility(View.GONE);
				mBtnOfferShare.setVisibility(View.VISIBLE);
				mBtnBuy.setVisibility(View.VISIBLE);
				mImgVerticalSep.setVisibility(View.GONE);
				mTxtOfferView.setVisibility(View.GONE);
				mTxtOfferLike.setVisibility(View.GONE);
				mTxtOfferShare.setVisibility(View.GONE);
				mMerchantView.setVisibility(View.GONE);
				mImageSeprator.setVisibility(View.GONE);
				if(mbCalledFromStore==false){
					mBtnGotoStore.setVisibility(View.VISIBLE);
				}
				else{
					mBtnGotoStore.setVisibility(View.GONE);
				}

			}else if(mbCalledFromOffers==true){
				mImgOfferfav.setVisibility(View.GONE);
				mBtnOfferLike.setVisibility(View.GONE);
				mTxtOfferShareBtn.setVisibility(View.GONE);
				mBtnOfferShare.setVisibility(View.GONE);
				mBtnBuy.setVisibility(View.GONE);
				mImgVerticalSep.setVisibility(View.GONE);
				mTxtOfferView.setVisibility(View.GONE);
				mTxtOfferLike.setVisibility(View.GONE);
				mTxtOfferShare.setVisibility(View.GONE);
				mMerchantView.setVisibility(View.GONE);
				mBtnGotoStore.setVisibility(View.GONE);
				mBtnAddtoShoplocal.setVisibility(View.GONE);
				mViewOverlay.setVisibility(View.GONE);
				mImageSeprator.setVisibility(View.GONE);
			}
			else{
				mImgOfferfav.setVisibility(View.GONE);
				mBtnOfferLike.setVisibility(View.GONE);
				mMerchantView.setVisibility(View.VISIBLE);
				mBtnOfferShare.setVisibility(View.GONE);
				mTxtOfferShareBtn.setVisibility(View.VISIBLE);
				mBtnBuy.setVisibility(View.GONE);
				mViewOverlay.setVisibility(View.GONE);
				mBtnAddtoShoplocal.setVisibility(View.GONE);
			}


			mBtnOfferLike.setOnClickListener(new OnClickListener() {


				public void onClick(View v) {
					try
					{
						if(mSessionManager.isLoggedInCustomer())
						{
							mbGetLike=true;
							mbIspostLiked=true;
							if(msUserLike.equalsIgnoreCase("0"))
							{
								Log.i("Liked","Liked");
								likeOffer();
								likeStore();
							}
							else if(msUserLike.equalsIgnoreCase("1"))
							{
								Log.i("Liked","Unliked");
								unlikeOffer();
							}
							else if(msUserLike.equalsIgnoreCase("-1"))
							{
								mbAutoLike=true;
								//loadBroadCast();
								loadOfferDetails();
							}
						}
						else
						{
							Log.i("Liked","Clicked else");
							mbGetLike=false;
							mbIspostLiked=false;
							loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
						}

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});

			if(msImageLink!=null && msImageLink.length()!=0 && !msImageLink.equalsIgnoreCase("notfound"))
			{
				msImageLink=msImageLink.replaceAll(" ", "%20");
				mImgOfferImage.setScaleType(ScaleType.FIT_XY);
				mImgOfferImage.setVisibility(View.VISIBLE);
				mTblOfferImageRow.setVisibility(View.VISIBLE);
				try{
					Picasso.with(mContext).load(Config.IMAGE_URL+msImageLink)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(mImgOfferImage);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}

				mImgOfferImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						EventTracker.logEvent("OfferDetails_ImageViewed", false);
						
						ArrayList<String>GALLERY_PHOTO_SOURCE=new ArrayList<String>();
						ArrayList<String>GALLERY_PHOTO_CAPTION=new ArrayList<String>();
						GALLERY_PHOTO_SOURCE.add("");
						GALLERY_PHOTO_CAPTION.add("");

						GALLERY_PHOTO_SOURCE.add(msImageLink);
						GALLERY_PHOTO_CAPTION.add(msOfferImageTitle);
						Intent intent=new Intent(mContext,StorePagerGallery.class);
						intent.putExtra("photo_source", GALLERY_PHOTO_SOURCE);
						intent.putExtra("isCaption",true);
						intent.putExtra("caption", GALLERY_PHOTO_CAPTION);
						intent.putExtra("position", 1);
						startActivity(intent);	
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
			}
			else{
				mImgOfferImage.setVisibility(View.GONE);
				mImgOfferImage.setScaleType(ScaleType.FIT_CENTER);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void loadOfferDetails()
	{
		if(mInternet.isNetworkAvailable()){
			String msPostUrl="";
			msPostUrl = Config.LOAD_ALL_POST_URL+"post_id="+msPostId;
			if(mSessionManager.isLoggedInCustomer()){
				msPostUrl= Config.LOAD_ALL_POST_URL+"post_id="+msPostId+"&user_id="+Config.CUST_USER_ID;
			}
			new ShoplocalAsyncTask(this, "get", "customer", false, "loadPosts").execute(msPostUrl);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}
	void likeOffer()
	{
		try {
			if(mInternet.isNetworkAvailable()){
				
				Map<String,String> eventLike=new HashMap<String, String>();
				eventLike.put("Source", "OfferDetails");
				eventLike.put("AreaName",msActiveAreaName);
				EventTracker.logEvent("Offer_Like", eventLike);
				
				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("post_id", msPostId);
				json.put("auth_id", Config.CUST_AUTH_ID);
				String msUrl=Config.POST_LIKE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"likePost").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	void likeStore()
	{
		try {
			if(mInternet.isNetworkAvailable()){
				EventTracker.logEvent("OfferDetails_StoreFavourited", false);
				
				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("place_id", msStoreId);
				json.put("auth_id", Config.CUST_AUTH_ID);
				String msUrl=Config.PLACE_LIKE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlace").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	void sharePost()
	{
		try {
			if(mInternet.isNetworkAvailable()){
				
				Map<String,String> eventShare=new HashMap<String, String>();
				eventShare.put("Source", "OfferDetails");
				eventShare.put("AreaName",msActiveAreaName);
				EventTracker.logEvent("Offer_Share", eventShare);
				
				JSONObject json = new JSONObject();
				json.put("post_id", msPostId);
				json.put("via", msShareVia);
				String msUrl=Config.POST_SHARE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"sharePost").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	void unlikeOffer()
	{
		if(mInternet.isNetworkAvailable()){
			String msUrl=Config.POST_LIKE_URL+"?post_id="+msPostId;
			new ShoplocalAsyncTask(this, "delete","customer",true,null,"unlikePost").execute(msUrl);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}
	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==PLACE_LIKE)
				{
					Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
					mContext.startActivityForResult(intent,4);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				if(like==POST_LIKE)
				{
					Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
					mContext.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
		return true;
	}



	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
	}

	@Override
	public void onAsyncStarted() {

	}
	void debug(String msg){
		Log.i("Offers ", "Offers "+msg);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {

		debug(sJson);

		if(tag.equalsIgnoreCase("likePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}
				if(status.equalsIgnoreCase("true")){
					msUserLike="1";
					String msg=jsonObject.getString("message");
					showToast(msg);
				}

			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("likePlace")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){

					try
					{
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(msStoreName);
						if(msImageLink.length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+msImageLink);
						}
						fbObj.setStoreDescription(msPostDescription);
						String encodedurl = URLEncoder.encode(msStoreName,"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+msStoreId);
						FacebookOperations.FavThisOnFb(mActivity, fbObj, getString(R.string.fb_store_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}

					
					String message=jsonObject.getString("message");
					showToast(message);
				}
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		if(tag.equalsIgnoreCase("unlikePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){
					msUserLike="0";
				}



			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("loadPosts")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;

				JSONObject dataObject = jsonObject.getJSONObject("data");

				if(dataObject == null) return;

				if(dataObject.has("total_like")){
					msTotalLike=dataObject.getString("total_like");
				}
				if(dataObject.has("total_share")){
					msTotalShare=dataObject.getString("total_share");
				}
				if(dataObject.has("total_view")){
					msTotalView=dataObject.getString("total_view");
				}
				if(dataObject.has("user_like")){
					msUserLike=dataObject.getString("user_like");
				}

				msIsOffer=dataObject.getString("is_offered");
				msOfferPostDate=dataObject.getString("date");
				msOfferValidTill=dataObject.getString("offer_date_time");
				msOfferImageTitle=dataObject.getString("image_title1");
				msPostDescription=dataObject.getString("description");

				mTxtOfferLike.setText(msTotalLike);
				mTxtOfferShare.setText(msTotalShare);
				mTxtOfferView.setText(msTotalView);
				
				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				DateFormat date_format=new SimpleDateFormat("hh:mm a");

				if(dataObject.getString("is_offered").equalsIgnoreCase("1")){
					setOfferValidTillDate(dataObject.getString("offer_date_time"));
				}
				setPostedDate(dataObject.getString("date"));
				
//				mBtnOfferLike.setText(msTotalLike);
//				mBtnOfferShare.setText(msTotalShare);

			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		if(tag.equalsIgnoreCase("sharePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;

				if(status.equalsIgnoreCase("true")){
					

					if(mbFaceBookShare==false)
					{
						try
						{
							FbObject fbObj=new FbObject();
							fbObj.setStoreTitle(msStoreName);
							if(msImageLink.length()==0){
								fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
							}else{
								fbObj.setStoreImage(Config.IMAGE_URL+msImageLink);
							}
							fbObj.setStoreDescription(msPostDescription);
							String encodedurl = URLEncoder.encode(msStoreName,"UTF-8");
							debug(Config.FACEBOOK_SHARE_URL+msSlug);
							fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+msStoreId);
							FacebookOperations.shareThisInfo(mActivity, fbObj, getString(R.string.fb_store_object));
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
					
					String msg=jsonObject.getString("message");
					showToast(msg);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	void setPostedDate(String sDate)
	{
		DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dt2=new SimpleDateFormat("MMM yyyy");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		DateFormat date_format=new SimpleDateFormat("hh:mm a");

		try {
			Date date_con = (Date) dt.parse(sDate);
			Date date=dateFormat.parse(sDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_con);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			mTxtOfferPostedDate.setText("Offer Created On: "+day+" "+dt2.format(date_con)+"   "+date_format.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	void setOfferValidTillDate(String sDate)
	{
		DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dt2=new SimpleDateFormat("MMM yyyy");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		DateFormat date_format=new SimpleDateFormat("hh:mm a");

		try {
			Date date_con = (Date) dt.parse(sDate);
			Date date=dateFormat.parse(sDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_con);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			mTxtOfferValidTill.setVisibility(View.VISIBLE);
			mTxtOfferValidTill.setText("Offer Valid Till: "+day+" "+dt2.format(date_con)+"   "+date_format.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
			
	}

	@Override
	public void onError(int errorCode) {
		showToast(getResources().getString(R.string.errorMessage));
	}

	void showDialog(){
		try
		{
			String shareBody= "";
			String validTill="";
			String contact="";
			String contact_lable="\nContact:";
			if(msIsOffer.length()!=0 && msIsOffer.equalsIgnoreCase("1")){
				try
				{
					DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
					DateFormat dt2=new SimpleDateFormat("MMM");
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

					Date date_con = (Date) dt.parse(msOfferValidTill.toString());
					Date date=dateFormat.parse(msOfferValidTill.toString());

					DateFormat date_format=new SimpleDateFormat("hh:mm a");
					Calendar cal = Calendar.getInstance();
					cal.setTime(date_con);
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH);
					int day = cal.get(Calendar.DAY_OF_MONTH);
					validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);

				}catch(Exception ex)
				{
					ex.printStackTrace();
					validTill="";
				}
			}
			else{
				validTill="";
			}
			try
			{
				if(mbCalledFromCust || mbCalledFromStore){
					if(msMobNo1.length()>0){
						contact="+"+msMobNo1;
					} 
					if(msMobNo2.length()>0){
						contact+=" +"+msMobNo2;
					}
					if(msMobNo3.length()>0){
						contact+=" +"+msMobNo3;
					}
					if(msTelNo1.length()>0){
						contact+=" "+msTelNo1;
					}
					if(msTelNo2.length()>0){
						contact+=" "+msTelNo2;
					}
					if(msTelNo3.length()>0){
						contact+=" "+msTelNo3;
					}
					if(contact.length()==0){
						contact_lable="";
					}
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			shareBody=msStoreName + " - " + "Offers" + " : " + "\n\n"+ mTxtOfferTitle.getText().toString() + "\n" + mTxtOfferDescription.getText().toString()+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
			offerShareDialog(msStoreName, msImageLink, shareBody);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			if(requestCode==2){
				if(mSessionManager.isLoggedInCustomer()){
					mbGetLike=true;
					mbIspostLiked=true;
					mbAutoLike=true;
					loadOfferDetails();
				}
			}
			if(requestCode == 3){
				msShareVia="Android";
				sharePost();
			}

			try
			{
				mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
					public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
						Log.e("Activity", String.format("Error: %s", error.toString()));
					}
					public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
						Log.i("Activity", "Success!");
						msShareVia="Android-Facebook";
						sharePost();
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}catch(NullPointerException ex)
		{
			ex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	class CallListDialog extends ArrayAdapter<String>
	{
		ArrayList<String> callNumbers;
		Activity context;
		LayoutInflater inflator;
		public CallListDialog(Activity context, int resource, ArrayList<String> callNumbers) {
			super(context, resource, callNumbers);

			this.context=context;
			inflator=context.getLayoutInflater();
			this.callNumbers=callNumbers;

		}
		@Override
		public int getCount() {
			return callNumbers.size();
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			try
			{
				if(convertView==null)
				{
					CallListViewHolder holder=new CallListViewHolder();
					convertView=inflator.inflate(R.layout.calllistlayout, null);
					holder.contactNo=(TextView)convertView.findViewById(R.id.contactNo);
					convertView.setTag(holder);
				}
				CallListViewHolder hold=(CallListViewHolder) convertView.getTag();
				hold.contactNo.setText(callNumbers.get(position));
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return convertView;
		}
	}

	class CallListViewHolder
	{
		TextView contactNo;
	}

}
 