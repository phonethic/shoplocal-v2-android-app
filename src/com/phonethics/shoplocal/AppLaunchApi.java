package com.phonethics.shoplocal;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;

public class AppLaunchApi extends IntentService{


	private SharedPreferences mPref;
	
	public AppLaunchApi(){
		super("AppLaunch");
	}
	
	public AppLaunchApi(String name) {
		super(name);
	}
	
	
	@Override
	protected void onHandleIntent(Intent intent) {
		try{
			
			BufferedReader bufferedReader=null;
			String resp;
			
			HttpParams params=new BasicHttpParams();
			
			int timeOut=30000;
			
			HttpConnectionParams.setConnectionTimeout(params, timeOut);
			
			HttpConnectionParams.setSoTimeout(params, timeOut);
			
			HttpClient httpClient=new DefaultHttpClient(params);
			
			HttpPost httpPost=new HttpPost(Config.APP_LAUNCH_URL);
			
			httpPost.addHeader(Config.API_HEADER, Config.API_HEADER_VALUE);
			httpPost.addHeader(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
			
			JSONObject json=new JSONObject();

			PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
			
			//Adding Launch Time
			mPref=getSharedPreferences(Config.APP_START_TIME_KEY, MODE_PRIVATE);
			
			String currentDate=mPref.getString(Config.APP_START_TIME, "");
			
			Config.debug("API_LAUNCH", "API_LAUNCH TIME "+currentDate);
			
			json.put("os_type", "android");
			json.put("os_version", android.os.Build.VERSION.SDK_INT);
			json.put("device", android.os.Build.MODEL);
			json.put("transactionTime", currentDate);
			json.put("app_version", pinfo.versionName);
			
			StringEntity se=new StringEntity(json.toString());
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			
			httpPost.setEntity(se);
			
			HttpResponse httpResponse=httpClient.execute(httpPost);
			
			bufferedReader=new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			StringBuffer sbf=new  StringBuffer("");
			String line="";
			String LineSeprator=System.getProperty("line.separator");
			while((line=bufferedReader.readLine())!=null){
				sbf.append(line+LineSeprator);
			}
			
			bufferedReader.close();
			resp=sbf.toString();
			
			Config.debug("API_LAUNCH", "Api Launch"+resp.toString()+" json "+json.toString());
			
		}catch(Exception ex){
			ex.printStackTrace();
			
		}
	}

}
