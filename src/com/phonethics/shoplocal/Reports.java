package com.phonethics.shoplocal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class Reports extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener
{
	private String msStoreId="";
	private String msStoreName="";
	private String msTotalLikes="";
	private String msTotalShares="";
	private String msTotalViews="";
	private String msTotalPeople="";
	//private String msTotalCallCount="";
	//private String msMonthlyCallCount="";

	private TextView mTotalPeople;
	private TextView mTotalViews;
	private TextView mTotalLikes;
	private TextView mTotalShares;

	private LinearLayout mReachLayout;
	private LinearLayout mVisitLayout;
	private LinearLayout mFavLayout;
	private LinearLayout mShareLayout;

	private ImageView mImageInfo;

	private Button mButtonPostReport;

	private ProgressBar mProgReports;

	private Activity mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reports);

		mContext=this;
		hideDrawerLayout();
		setTitle("Reports");
		
		mButtonPostReport=(Button)findViewById(R.id.buttonPostReport);

		mImageInfo=(ImageView)findViewById(R.id.reportInfo);

		mTotalPeople=(TextView)findViewById(R.id.totalPeople);
		mTotalViews=(TextView)findViewById(R.id.totalViews);
		mTotalLikes=(TextView)findViewById(R.id.totalLikes);
		mTotalShares=(TextView)findViewById(R.id.totalShares);

		mReachLayout=(LinearLayout)findViewById(R.id.reachLayout);
		mVisitLayout=(LinearLayout)findViewById(R.id.visitLayout);
		mFavLayout=(LinearLayout)findViewById(R.id.favLayout);
		mShareLayout=(LinearLayout)findViewById(R.id.shareLayout);

		mProgReports=(ProgressBar)findViewById(R.id.progReports);

		Bundle b = getIntent().getExtras();
		if(b!=null){
			msStoreId = b.getString("STORE_ID");
		}
		if(msStoreId.length()!=0){
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.PLACE_DETAIL_URL+"place_id="+msStoreId;
				
				new ShoplocalAsyncTask(this, "get","merchant", true, "storeDetails").execute(msUrl);

				msUrl=Config.PEOPLE_COUNT_URL+"place_id="+msStoreId;
				
				new ShoplocalAsyncTask(this, "get","merchant", true, "peopleCount").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}

		mImageInfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.reportsMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}
		});

		mReachLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showAlert(getResources().getString(R.string.customers_in_area));
			}
		});
		mVisitLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showAlert(getResources().getString(R.string.visits_to_store));
			}
		});
		mFavLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showAlert(getResources().getString(R.string.no_of_people_fav));
			}
		});
		mShareLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showAlert(getResources().getString(R.string.no_of_people_share));
			}
		});

		//getCallCount();

	}
	
	void getCallCount() {
		try{
			if(mInternet.isNetworkAvailable()){
				String URL = Config.PEOPLE_COUNT_URL/*CALL_COUNT_API*/+"place_id=" + msStoreId;
				new ShoplocalAsyncTask(this, "get", "merchant", true, "callCount").execute(URL);
			}
		}catch(Exception ex){
			
		}
	}
	
	
	
//	void getCallCount(){
//		try{
//			if(mInternet.isNetworkAvailable()){
//				String URL=Config.CALL_COUNT_API+msStoreId;
//				new ShoplocalAsyncTask(this, "get", "merchant", true, "callCount").execute(URL);
//			}
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//	}

	void showAlert(String msg){
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog3 = alertDialogBuilder3.create();
		alertDialog3.show();
	}
	
	@Override
	public void onAsyncStarted() {
		mProgReports.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		mProgReports.setVisibility(View.GONE);
		debug(sJson);
		if(tag.equalsIgnoreCase("storeDetails")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null){
					String message=jsonObject.getString("message");
					showToast(message);
					if(jsonObject.has("error_code")){
						String error_code=jsonObject.getString("error_code");
						if(error_code.equalsIgnoreCase("-116")){
							invalidAuthFinish();
						}
					}
					return;
				}

				JSONArray dataArray = jsonObject.getJSONArray("data");
				if(dataArray == null) return;

				int count = dataArray.length();
				for(int i=0;i<count;i++)
				{
					JSONObject dataObject=dataArray.getJSONObject(i);
					msStoreName=dataObject.getString("name");
					msTotalLikes=dataObject.getString("total_like");
					msTotalShares=dataObject.getString("total_share");
					msTotalViews=dataObject.getString("total_view");

				}

				mTotalLikes.setText(msTotalLikes);
				mTotalViews.setText(msTotalViews);
				mTotalShares.setText(msTotalShares);


				mButtonPostReport.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent=new Intent(mContext, ViewPost.class);
						intent.putExtra("storeName", msStoreName);
						intent.putExtra("STORE_ID", msStoreId);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}
				});

			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("peopleCount")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null){
					String message=jsonObject.getString("message");
					showToast(message);
					if(jsonObject.has("error_code")){
						String error_code=jsonObject.getString("error_code");
						if(error_code.equalsIgnoreCase("-116")){
							invalidAuthFinish();
						}
					}
					return;
				}

				JSONObject data = jsonObject.getJSONObject("data");
				if(data == null) return;

				msTotalPeople=data.getString("customer_count");
				mTotalPeople.setText(msTotalPeople);

			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("callCount")){
			try{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null){
					String message=jsonObject.getString("message");
					showToast(message);
					if(jsonObject.has("error_code")){
						String error_code=jsonObject.getString("error_code");
						if(error_code.equalsIgnoreCase("-116")){
							invalidAuthFinish();
						}
					}
					return;
				}
				
				JSONObject data = jsonObject.getJSONObject("data");
				if(data == null) return;
				//msTotalCallCount=data.getString("total_calls");
				//msMonthlyCallCount=data.getString("total_monthly_calls");
				
				//Config.debug("Reports", "Reports "+msTotalCallCount+" "+msMonthlyCallCount);
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	void invalidAuthFinish()
	{
		try
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
					try{
						mSessionManager.logoutUser();
						Intent intent=new Intent(mContext, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onError(int errorCode) {
		mProgReports.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}

	void debug(String msg){
		Log.i("Reports", "Reports "+msg);
	}

	public void onBackPressed() {
		finishTo();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishTo();
		return true;
	}

	void finishTo(){
		Intent intent=new Intent(mContext, MerchantTalkHome.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
}
//public class Reports extends SherlockActivity implements PeopleCountReceiver,GetAllStore{
//
//	GetPeopleCountReciever mGetCount;
//	GetAllStores_Of_Perticular_User_Receiver mGetStoreReceiver;
//
//	//Api Urls
//	String API_HEADER;
//	String API_VALUE;
//	String URL;
//	String GET_STORE_URL;
//	String GET_STORE;
//	String STORE_ID;
//	String msStoreId;
//	String msStoreName;
//	String USER_ID="";
//	String AUTH_ID="";
//	String KEY_USER_ID="user_id";
//	String KEY_AUTH_ID="auth_id";
//	
//	ImageView mReportInfo;
//
//	TextView mTotalLikes;
//	TextView mTotalViews;
//	TextView mTotalShares;
//	TextView mTotalPeople;
//	
//	Button mBtnPostReport;
//
//	ActionBar mActionBar;
//	Typeface mTf;
//	Activity mContext;
//	SessionManager mSession;
//	NetworkCheck mNetwork;
//
//
//	protected void onCreate(Bundle savedInstanceState) {
//		setTheme(R.style.Theme_City_custom);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_reports);
//
//		mContext=this;
//		
//		mSession=new SessionManager(mContext);
//		mNetwork=new NetworkCheck(mContext);
//		
//		mActionBar=getSupportActionBar();
//		mActionBar.setTitle("Reports");
//		mActionBar.setDisplayHomeAsUpEnabled(true);
//		mActionBar.show();
//
//		mTf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
//
//		try{
//			//Session Data
//			HashMap<String,String>user_details=mSession.getUserDetails();
//			USER_ID=user_details.get(KEY_USER_ID).toString();
//			AUTH_ID=user_details.get(KEY_AUTH_ID).toString();
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//
//		/*
//		 * API KEY
//		 */
//		API_HEADER=getResources().getString(R.string.api_header);
//		API_VALUE=getResources().getString(R.string.api_value);
//		
//		URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api)+getResources().getString(R.string.peopleCount);
//
//		GET_STORE=getResources().getString(R.string.allStores);
//		GET_STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
//
//		mTotalPeople = (TextView) findViewById(R.id.totalPeople);
//		mTotalViews = (TextView) findViewById(R.id.totalViews);
//		mTotalLikes = (TextView) findViewById(R.id.totalLikes);
//		mTotalShares= (TextView) findViewById(R.id.totalShares);
//
//		mBtnPostReport=(Button)findViewById(R.id.buttonPostReport);
//
//		mTotalPeople.setTypeface(mTf);
//		mTotalViews.setTypeface(mTf);
//		mTotalLikes.setTypeface(mTf);
//		mTotalShares.setTypeface(mTf);
//
//		mReportInfo=(ImageView)findViewById(R.id.reportInfo);
//
//		mGetCount = new GetPeopleCountReciever(new Handler());
//		mGetCount.setReceiver(this);
//
//		mGetStoreReceiver=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
//		mGetStoreReceiver.setReceiver(this);
//
//		mReportInfo.setOnClickListener(new OnClickListener() {
//			public void onClick(View arg0) {
//				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
//				alertDialogBuilder3.setTitle("Shoplocal");
//				alertDialogBuilder3
//				.setMessage(getResources().getString(R.string.reportsMessage))
//				.setIcon(R.drawable.ic_launcher)
//				.setCancelable(false)
//				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,int id) {
//						dialog.dismiss();
//					}
//				});
//				AlertDialog alertDialog3 = alertDialogBuilder3.create();
//				alertDialog3.show();
//			}
//		});
//
//		try {
//			Bundle b = getIntent().getExtras();
//			STORE_ID = b.getString("STORE_ID");
//			Log.d("STOREID","STOREID" + STORE_ID);
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		if(mNetwork.isNetworkAvailable()){
//			getTotalUsers();
//			getParticularrStoreDetails();
//		}
//		else{
//			showToast(getResources().getString(R.string.noInternetConnection));
//		}
//
//	}
//
//	// Method the total number of customers
//	private void getTotalUsers() {
//		Intent intent = new Intent(mContext, GetPeopleCountService.class);
//		intent.putExtra("getPeopleCount", mGetCount);
//		intent.putExtra("URL", URL);
//		intent.putExtra("api_header",API_HEADER);
//		intent.putExtra("api_header_value", API_VALUE);
//		intent.putExtra("store_id", STORE_ID);
//		intent.putExtra("user_id", USER_ID);
//		intent.putExtra("auth_id", AUTH_ID);
//		mContext.startService(intent);
//	}
//
//
//	//Call Service to get perticular store details
//	void getParticularrStoreDetails()
//	{
//		Intent intent=new Intent(mContext, GetPerticularStoreDetailService.class);
//		intent.putExtra("getStore",mGetStoreReceiver);
//		intent.putExtra("URL", GET_STORE_URL+GET_STORE);
//		intent.putExtra("api_header",API_HEADER);
//		intent.putExtra("api_header_value", API_VALUE);
//		intent.putExtra("store_id", STORE_ID);
//		intent.putExtra("user_id", USER_ID);
//		intent.putExtra("auth_id", AUTH_ID);
//		intent.putExtra("isMerchant", true);
//		mContext.startService(intent);
//	}
//
//
//
//
//	public void onReceiveCountApi(int resultCode, Bundle resultData) {
//		try
//		{
//			String profile_status = resultData.getString("prof_status");
//			String totalPeopleCount = resultData.getString("totalpeoplecount");
//			if(profile_status.equalsIgnoreCase("true")){
//				mTotalPeople.setText(totalPeopleCount);
//			}
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//
//	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
//		try
//		{
//			String TOTAL_LIKES = resultData.getString("TOTAL_LIKES");
//			String TOTAL_VIEWS =  resultData.getString("TOTAL_VIEWS");
//			String TOTAL_SHARE =  resultData.getString("TOTAL_SHARE");
//			String store_status=resultData.getString("store_status");
//
//			if(store_status.equalsIgnoreCase("true")){
//				mTotalLikes.setText(TOTAL_LIKES);
//				mTotalViews.setText(TOTAL_VIEWS);
//				mTotalShares.setText(TOTAL_SHARE);
//				msStoreId=resultData.getString("ID");
//				msStoreName=resultData.getString("SOTRE_NAME");
//
//				mBtnPostReport.setOnClickListener(new OnClickListener() {
//					public void onClick(View arg0) {
//						Intent intent=new Intent(mContext, ViewPost.class);
//						intent.putExtra("storeName", msStoreName);
//						intent.putExtra("STORE_ID", msStoreId);
//						startActivity(intent);
//						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//						finish();
//						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//					}
//				});
//			}
//			else if(store_status.equalsIgnoreCase("false")){
//				String store_msg=resultData.getString("MSG");
//				showToast(store_msg);
//				String error_code=resultData.getString("error_code");
//				if(error_code.equalsIgnoreCase("-116")){
//					invalidAuthFinish();
//				}
//			}
//			else if(store_status.equalsIgnoreCase("error")){
//				String store_msg=resultData.getString("MSG");
//				showToast(store_msg);
//			}
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//	}
//
//	void invalidAuthFinish()
//	{
//		try
//		{
//			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
//			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
//			alertDialogBuilder3
//			.setMessage(getResources().getString(R.string.invalidCredential))
//			.setIcon(R.drawable.ic_launcher)
//			.setCancelable(false)
//			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog,int id) {
//					dialog.dismiss();
//					try{
//						mSession.logoutUser();
//						Intent intent=new Intent(mContext, MerchantTalkHome.class);
//						startActivity(intent);
//						finish();
//						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
//					}catch(Exception ex){
//						ex.printStackTrace();
//					}
//				}
//			});
//			AlertDialog alertDialog3 = alertDialogBuilder3.create();
//			alertDialog3.show();
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//	}
//
//	void showToast(String text){
//		Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
//	}
//
//
//	public boolean onOptionsItemSelected(MenuItem item) {
//		finishTo();
//		return true;
//	}
//
//	public void onBackPressed() {
//		finishTo();
//	}
//
//	void finishTo(){
//		Intent intent=new Intent(mContext, MerchantTalkHome.class);
//		startActivity(intent);
//		this.finish();
//		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
//	}
//
//	protected void onStart(){
//		super.onStart();
//		EventTracker.startFlurrySession(getApplicationContext());
//	}
//
//	protected void onStop(){
//		EventTracker.endFlurrySession(getApplicationContext());	
//		super.onStop();
//	}
//
//	protected void onResume(){
//		super.onResume();
//		EventTracker.startLocalyticsSession(getApplicationContext());
//
//	}
//
//	protected void onPause(){
//		EventTracker.endLocalyticsSession(getApplicationContext());
//		super.onPause();
//	}
//}
