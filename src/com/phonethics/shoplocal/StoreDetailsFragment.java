package com.phonethics.shoplocal;

import java.util.HashMap;
import java.util.Map;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StoreDetailsFragment extends Fragment{


	String PAGE_ABOUT_STORE="About Store";
	String PAGE_ADDRESS="Address";
	String PAGE_POST="Post/Offers";
	String PAGE_IMAGE_GALLERY="Photo Gallery";
	String PAGE_REPORTS="REPORTS";

	String Page;
	public static final String EXTRA_MESSAGE = "PAGE";
	View view;

	//About Store
	ImageView pagerLogo;
	TextView pagerOpenCloseTime;
	TextView pagerOpenCloseDays;
	TextView pagerStoreDesc;
	ImageView pagerStoreCall;
	ImageView pagerStoreFav;
	ImageView pagerStoreShare;

	//PAGE_POST
	ListView pagerPostList;

	//Address
	TextView pagerAddress;
	ImageView pagerLocate;
	TextView pagerPhoneNumbers;
	TextView pagerEmail;
	ImageView pagerFacebook;
	ImageView pagerTwitter;
	ImageView pagerWebsite;
	//	ImageView storePagerMapImage;
	//	Button buttonTakeMeThere;
	//Pager Gallery
	GridView grid;

	Map<String, String> call= new HashMap<String, String>();
	Map<String, String> facebook = new HashMap<String, String>();
	Map<String, String> twitter = new HashMap<String, String>();;
	Map<String, String> website = new HashMap<String, String>();
	Map<String, String> email = new HashMap<String, String>();
	Map<String, String> viewmap = new HashMap<String, String>();
	Map<String, String> directions = new HashMap<String, String>();
	Map<String, String> viewoffer = new HashMap<String, String>();
	//Map<String, String> viewgallery = new HashMap<String, String>();

	//Call Dialog
	Dialog calldailog;
	ListView listCall;

	FragmentActionListener mFragmentListener;
	
	public StoreDetailsFragment()
	{
	}

	public static final StoreDetailsFragment newInstance(String Page)
	{
		StoreDetailsFragment f=new StoreDetailsFragment();
		Bundle bdl=new Bundle(1);
		bdl.putString(EXTRA_MESSAGE, Page);
		f.setArguments(bdl);
		return f;

	}
	
	public interface FragmentActionListener
	{
		public void onCallClicked();
		public void onStoreFavourited();
		public void onStoreShare();
		public void onGetDirectionCliked();
		public void onEmailClicked();
		public void onFacebookClicked();
		public void onTwitterClicked();
		public void onWebsiteClicked();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try{
			mFragmentListener=(FragmentActionListener)activity;
		}catch(ClassCastException ex){
			ex.printStackTrace();
		}
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		String page = getArguments().getString(EXTRA_MESSAGE);

		if(page.equalsIgnoreCase(PAGE_ABOUT_STORE))
		{
			view=inflater.inflate(R.layout.store_details_about, null);
			pagerStoreCall=(ImageView)view.findViewById(R.id.pagerStoreCall);
			pagerStoreFav=(ImageView)view.findViewById(R.id.pagerStoreFav);
			pagerStoreShare=(ImageView)view.findViewById(R.id.pagerStoreShare);

			pagerStoreCall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					
					mFragmentListener.onCallClicked();
				}
			});

			pagerStoreFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					mFragmentListener.onStoreFavourited();

				}
			});

			pagerStoreShare.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					mFragmentListener.onStoreShare();
				}
			});
		}

		if(page.equalsIgnoreCase(PAGE_ADDRESS))
		{
			view=inflater.inflate(R.layout.store_details_address, null);

			pagerAddress=(TextView)view.findViewById(R.id.pagerAddress);
			pagerLocate=(ImageView)view.findViewById(R.id.pagerLocate);
			pagerPhoneNumbers=(TextView)view.findViewById(R.id.pagerPhoneNumbers);
			pagerEmail=(TextView)view.findViewById(R.id.pagerEmail);
			pagerFacebook=(ImageView)view.findViewById(R.id.pagerFacebook);
			pagerTwitter=(ImageView)view.findViewById(R.id.pagerTwitter);
			pagerWebsite=(ImageView)view.findViewById(R.id.pagerWebsite);


			pagerPhoneNumbers.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					
					mFragmentListener.onCallClicked();
				}
			});


			pagerLocate.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					mFragmentListener.onGetDirectionCliked();
				}
			});

			pagerEmail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					mFragmentListener.onEmailClicked();
				}
			});

			pagerFacebook.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					mFragmentListener.onFacebookClicked();
				}
			});

			pagerTwitter.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					mFragmentListener.onTwitterClicked();
				}
			});

			pagerWebsite.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					mFragmentListener.onWebsiteClicked();
				}
			});


		}

		if(page.equalsIgnoreCase(PAGE_POST))
		{
			view=inflater.inflate(R.layout.store_details_posts, null);
			pagerPostList=(ListView)view.findViewById(R.id.pagerPostList);

		}

		if(page.equalsIgnoreCase(PAGE_IMAGE_GALLERY))
		{
			view=inflater.inflate(R.layout.store_details_photo_gallery, null);
			grid=(GridView)view.findViewById(R.id.pagerGalleryGrid);
		}

		return view;
	}

	void showToast(String text)
	{
		Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
	}



}
