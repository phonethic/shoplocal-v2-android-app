package com.phonethics.shoplocal;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class SignupMerchant extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener , DBListener
{
	private TextView mTxtMerchantMobNo;
	private TextView mTxtServerMessage;

	private Button mBtnResendSms;
	private Button mBtnNext;

	private static EditText mEdtVerificationCode;

	private ProgressBar mProgess;

	private String msMobileNo="";
	private String msServerMessage="";
	private String msVerifcationCode="";
	private String msPassword="";

	private Activity mContext;
	private ArrayList<String> mArrId=new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_merchant);
		hideDrawerLayout();

		mContext=this;

		mProgess=(ProgressBar)findViewById(R.id.merchantProgressBar);

		mTxtMerchantMobNo=(TextView)findViewById(R.id.txtMerchantMobNo);
		mTxtServerMessage=(TextView)findViewById(R.id.serverMessage);

		mBtnResendSms=(Button)findViewById(R.id.signUpResendButton);
		mBtnNext=(Button)findViewById(R.id.signUpNext);

		mEdtVerificationCode=(EditText)findViewById(R.id.shopVerificationCode);

		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			try
			{
				msMobileNo=b.getString("mobileno");
				msServerMessage=b.getString("server_message");
				String txtNum="Edit: +"+msMobileNo;
				mTxtMerchantMobNo.setText(Html.fromHtml("<u>"+txtNum+"</u>"));
				mTxtServerMessage.setText(msServerMessage);

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		mTxtMerchantMobNo.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				mContext.finish();
				mContext.overridePendingTransition(0,R.anim.shrink_fade_out_center);
			}
		});

		mBtnNext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				msVerifcationCode=mEdtVerificationCode.getText().toString();
				msPassword=mEdtVerificationCode.getText().toString();
				if(msVerifcationCode.length()!=0){
					setPassword();
				}
				else{
					showToast("Please enter verification code");
				}

			}
		});
		mBtnResendSms.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				registerMerchant();
			}
		});

	}

	void registerMerchant()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);

				JSONObject json = new JSONObject();
				json.put("mobile", msMobileNo);
				json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
						+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
				String msUrl=Config.MERCHANT_REGISTER_URL;
				new ShoplocalAsyncTask(this, "post", "merchant", false, json, "register").execute(msUrl);
			}catch(Exception ex){
				ex.printStackTrace();

			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void setPassword()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				JSONObject json = new JSONObject();
				json.put("mobile", msMobileNo);
				json.put("password", msPassword);
				json.put("verification_code", msVerifcationCode);
				String msUrl=Config.MERCHANT_SET_PASSWORD_URL;
				new ShoplocalAsyncTask(this, "put", "merchant", false,json,"setPassword").execute(msUrl);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void login()
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				JSONObject json = new JSONObject();
				json.put("mobile", msMobileNo);
				json.put("password", msPassword);
				String msUrl=Config.MERCHANT_LOGIN_URL;
				new ShoplocalAsyncTask(this, "post", "merchant", false,json,"login").execute(msUrl);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}

	}

	void getAllStores()
	{
		if(mInternet.isNetworkAvailable()){
			String msUrl=Config.MERCHANT_ALL_PLACES_URL;
			new ShoplocalAsyncTask(this, "get", "merchant", true, "allStores").execute(msUrl);;
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishTo();
		return true;
	}

	@Override
	public void onAsyncStarted() {
		mProgess.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mProgess.setVisibility(View.GONE);
		if(tag.equalsIgnoreCase("register")){

			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String error_code=jsonObject.getString("code");
				if(error_code.equalsIgnoreCase("-119")){ //New user
					EventTracker.logEvent("MSignUp_New", false);
					Log.i("Signup ", "MSignup success true");
				}
				else if(error_code.equalsIgnoreCase("-102")){ //Returning user
					EventTracker.logEvent("MSignUp_Return", false);
					Log.i("Signup ", "MSignup success false");
				}
			}catch(Exception  ex)
			{
				ex.printStackTrace();
			}

		}if(tag.equalsIgnoreCase("setPassword")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");

				if(status==null) return;

				if(status.equalsIgnoreCase("true")){
					EventTracker.logEvent("MSignUp_VerifyCodeDone", false);
					login();
				}
				else if(status.equalsIgnoreCase("false")){
					EventTracker.logEvent("MSignUp_ShoplocalLoginSuccess", false);
					showToast(msg);
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

		}if(tag.equalsIgnoreCase("login")){

			try
			{
				JSONObject jsonObject=new JSONObject(sJson);

				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");

				JSONObject jsonData=jsonObject.getJSONObject("data");
				if(status==null) return;

				if(status.equalsIgnoreCase("true")){
					EventTracker.logEvent("MSignUp_ShoplocalLoginSuccess", false);

					String id=jsonData.getString("id");
					String auth_code=jsonData.getString("auth_code");
					mSessionManager.createLoginSession(msMobileNo, msPassword, id, auth_code);
					getAllStores();
					/*try
					{
						Set<String> tags = new HashSet<String>(); 
						tags=PushManager.shared().getTags();
						tags.add("merchant");
						PushManager.shared().setTags(tags);
						
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}*/
				}
				else if(status.equalsIgnoreCase("false")){
					EventTracker.logEvent("MSignUp_ShoplocalLoginFailed", false);
					showToast(msg);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		if(tag.equalsIgnoreCase("allStores")){

			try
			{
				ArrayList<String>mArrLat=new ArrayList<String>();
				ArrayList<String>mArrLong=new ArrayList<String>();
				ArrayList<String>mArrPhoto=new ArrayList<String>();
				ArrayList<String>mArrStoreName=new ArrayList<String>();
				ArrayList<String>mArrDesc=new ArrayList<String>();

				JSONObject jsonObject=new JSONObject(sJson);

				String status=jsonObject.getString("success");

				if(status.equalsIgnoreCase("false")){
					
					Cursor c1=new ShoplocalCursorLoader(mContext, "Delete from MERCHANT_STORES").loadInBackground();
					
					if(c1!=null){
						debug("deleted previous entries"); 
						
						Intent intent=new Intent();
						intent.putExtra("isAddStore",true);
						setResult(4, intent);
						mContext.finish();
						return;
					}
					
					
				}

				if(status.equalsIgnoreCase("true")){

					JSONArray jsonArray = jsonObject.getJSONArray("data"); 
					if(jsonArray == null)  return;

					int count = jsonArray.length();
					ContentValues[] values = new ContentValues[count];

					for(int i=0;i<count;i++){
						JSONObject storeObject=jsonArray.getJSONObject(i);
						if(storeObject!=null){
							ContentValues value=new ContentValues();
							value.put(DBUtil.ID, storeObject.getInt("id"));
							mArrId.add(storeObject.getInt("id")+"");
							value.put("place_parent", storeObject.getInt("place_parent"));
							value.put("store_name", storeObject.getString("name"));
							mArrStoreName.add(storeObject.getString("name"));
							value.put("description", storeObject.getString("description"));
							mArrDesc.add(storeObject.getString("description"));
							value.put("building", storeObject.getString("building"));
							value.put("street", storeObject.getString("street"));
							value.put("landmark", storeObject.getString("landmark"));
							value.put("area", storeObject.getString("pincode"));
							value.put("pincode", storeObject.getString("pincode"));
							value.put("city", storeObject.getString("city"));
							value.put("state", storeObject.getString("state"));
							value.put("country", storeObject.getString("country"));
							value.put("latitude",storeObject.getString("latitude"));
							mArrLat.add(storeObject.getString("latitude"));
							value.put("longitude",storeObject.getString("longitude"));
							mArrLong.add(storeObject.getString("longitude"));
							value.put("tel_no_1", storeObject.getString("tel_no1"));
							value.put("tel_no_2", storeObject.getString("tel_no2"));
							value.put("tel_no_3", storeObject.getString("tel_no3"));
							value.put("mob_no_1", storeObject.getString("mob_no1"));
							value.put("mob_no_2", storeObject.getString("mob_no2"));
							value.put("mob_no_3", storeObject.getString("mob_no3"));
							value.put("logo", storeObject.getString("image_url"));
							mArrPhoto.add(storeObject.getString("image_url"));
							value.put("email_id", storeObject.getString("email"));
							value.put("website", storeObject.getString("website"));
							value.put("facebook", storeObject.getString("facebook_url"));
							value.put("twitter", storeObject.getString("twitter_url"));
							value.put("store_status", storeObject.getString("place_status"));
							value.put("published_state", storeObject.getString("published"));
							value.put("total_like", storeObject.getString("total_like"));
							value.put("total_share", storeObject.getString("total_share"));
							value.put("total_view", storeObject.getString("total_view"));

							values[i] = value;
						}
					}

					Cursor c1=new ShoplocalCursorLoader(mContext, "Delete from MERCHANT_STORES").loadInBackground();
					if(c1!=null){
						debug("deleted previous entries");
						DBUtil dbUtil = DBUtil.getInstance(mContext.getApplicationContext());
						dbUtil.replaceOrUpdate(this, DBUtil.TABLE_MERCHANT_STORES, values,"merchant-stores");
						mProgess.setVisibility(View.VISIBLE);
					}

					//Checking For Empty content
					//And Setting Alarm Based on the empty place details.
					try
					{
						mLocalNotification.checkPref();

						for(int i=0;i<mArrId.size();i++)
						{
							if(mArrLat.get(i).toString().length()==0 || mArrLong.get(i).toString().length()==0 || mArrPhoto.get(i).length()==0 || mArrDesc.get(i).length()==0)
							{
								Log.i("PREF ID ","PREF ID NAME "+mArrId.get(i)+" "+mArrStoreName.get(i));
								mLocalNotification.setPlacePref(mArrId.get(i),true, mLocalNotification.isPlaceAlarmFired());
								break;
							}
							else
							{
								Log.i("PREF ID ","PREF ID NAME "+mArrId.get(i)+" "+mArrStoreName.get(i));
								mLocalNotification.setPlacePref(mArrId.get(i),false, mLocalNotification.isPlaceAlarmFired());
							}
						}
						
						DBUtil dbUtil = DBUtil.getInstance(mContext.getApplicationContext());
						ArrayList<String> placesIds =  dbUtil.getAllPlacesIDs();
						for (String id : placesIds) {
							debug(tag + " " + id);
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}	
		}

	}

	@Override
	public void onCompleteInsertion(String tag) {
		mProgess.setVisibility(View.GONE);
		Intent intent=new Intent();
		if(mArrId.size()==0){
			intent.putExtra("isAddStore",true);	
		}else{
			intent.putExtra("isAddStore",false);
		}
		setResult(4, intent);
		mContext.finish();
	}


	void debug(String msg){
		Log.i("Merchant Signup", "Merchant Signup "+msg);
	}

	@Override
	public void onError(int errorCode) {
		mProgess.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}


	public  static class VerificationBroadCast extends BroadcastReceiver
	{
		public void onReceive(Context context, Intent intent) {
			try
			{
				String verification_code= intent.getStringExtra("verification_code");
				setVerificationCode(verification_code);
				Log.i("Verification", "" +verification_code);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}


	static void setVerificationCode(String verification_code)
	{
		try
		{
			//signup page details.
			Log.i("Verification Code : ", "Verification Code merchant : "+verification_code);
			mEdtVerificationCode.setText(verification_code);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	public void onBackPressed() {
		finishTo();
	}



	void finishTo()
	{
		Intent intent=new Intent();
		intent.putExtra("isAddStore",false);
		mContext.setResult(4, intent);
		mContext.finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
	}



}
//public class SignupMerchant extends SherlockFragmentActivity implements SetPasswordReceiver, LoginReceiver, GetAllStore, MerchantRegisterInterface {
//
//	ActionBar actionBar;
//
//	ArrayList<String>pages=new ArrayList<String>();
//	static Activity context;
//
//
//	static String API_HEADER;
//	static String API_VALUE;
//
//	String SET_PASSWORD_PATH;
//	String REGISTER_PATH;
//
//
//	String LOGIN_PATH;
//	String LOGIN_URL;
//
//	ProgressBar shopProgress;
//	//Session Manger Class
//	SessionManager session;
//
//	String mobileno="";
//
//	static String STORE_URL;
//	static String SOTRES_PATH;
//
//	static String USER_ID="";
//	static String AUTH_ID="";
//
//	Button buttonEdit;
//	TextView txtMerchantMobNo;
//	Button signUpResendButton;
//	Button signUpNext;
//	static EditText  shopVerificationCode;
//	TextView serverMessage;
//
//	NetworkCheck isnetConnected;
//
//	String REGISTER_URL;
//
//	String password;
//	String mobile_no;
//
//	//Set Merchant Password Receiver
//	public MerchantPasswordReceiver mPasswordRecevier;
//	//Login Recevier
//	public MerchantLoginReceiver mLoginRecevier;
//
//	//All Stores
//	GetAllStores_Of_Perticular_User_Receiver mAllStores;
//
//	String verification_code;
//
//	MerchantResultReceiver mRegister;
//
//	String server_message;
//
//	boolean isVerifyDone;
//
//
//	
//	protected void onCreate(Bundle savedInstanceState) {
//		setTheme(R.style.Theme_City_custom);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_signup_merchant);
//		context=this;
//
//		session=new SessionManager(context);
//
//		isnetConnected=new NetworkCheck(context);
//
//		actionBar=getSupportActionBar();
//		actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
//		actionBar.show();
//		actionBar.setDisplayHomeAsUpEnabled(true);
//
//		//		buttonEdit=(Button)findViewById(R.id.buttonEdit);
//		txtMerchantMobNo=(TextView)findViewById(R.id.txtMerchantMobNo);
//		signUpResendButton=(Button)findViewById(R.id.signUpResendButton);
//		signUpNext=(Button)findViewById(R.id.signUpNext);
//		shopVerificationCode=(EditText)findViewById(R.id.shopVerificationCode);
//		serverMessage=(TextView)findViewById(R.id.serverMessage);
//
//		mRegister=new MerchantResultReceiver(new Handler());
//		mRegister.setReceiver(this);
//
//		REGISTER_URL=getResources().getString(R.string.server_url) + getResources().getString(R.string.merchant_api);
//
//		//Defining URL's
//		LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.merchant_api);
//		LOGIN_PATH=getResources().getString(R.string.login);
//
//		//Store Path
//		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
//		SOTRES_PATH=getResources().getString(R.string.allStores);
//
//
//		REGISTER_PATH=getResources().getString(R.string.new_merchant);
//		SET_PASSWORD_PATH=getResources().getString(R.string.new_set_password);
//
//		//Api Headers
//		API_HEADER=getResources().getString(R.string.api_header);
//		API_VALUE=getResources().getString(R.string.api_value);
//
//
//		shopProgress=(ProgressBar)findViewById(R.id.merchantProgressBar);
//
//		mPasswordRecevier=new MerchantPasswordReceiver(new Handler());
//		mPasswordRecevier.setReceiver(this);
//
//		mLoginRecevier=new MerchantLoginReceiver(new Handler());
//		mLoginRecevier.setReceiver(this);
//
//		mAllStores=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
//		mAllStores.setReceiver(this);
//
//		Bundle b=getIntent().getExtras();
//
//		if(b!=null)
//		{
//			try
//			{
//				mobileno=b.getString("mobileno");
//				server_message=b.getString("server_message");
//				String txtNum="Edit: +"+mobileno;
//				txtMerchantMobNo.setText(Html.fromHtml("<u>"+txtNum+"</u>"));
//				serverMessage.setText(server_message);
//
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//		}
//
//
//		txtMerchantMobNo.setOnClickListener(new OnClickListener() {
//
//			
//			public void onClick(View arg0) {
//		
//				context.finish();
//				context.overridePendingTransition(0,R.anim.shrink_fade_out_center);
//			}
//		});
//
//		signUpNext.setOnClickListener(new OnClickListener() {
//
//			
//			public void onClick(View v) {
//		
//				verification_code=shopVerificationCode.getText().toString();
//				password=shopVerificationCode.getText().toString();
//				if(verification_code.length()!=0)
//				{
//					callSetPasswordService();	
//				}
//				else
//				{
//					showToast("Please enter verification code");
//				}
//
//			}
//		});
//		signUpResendButton.setOnClickListener(new OnClickListener() {
//
//			
//			public void onClick(View v) {
//		
//				callRegistrationApi();
//			}
//		});
//
//		isVerifyDone = false;
//
//	}//onCreate Ends Here
//
//	void callRegistrationApi()
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			//Calling Register Api to Register Merchant.
//			Intent intent=new Intent(context, MerchantRegister.class);
//			intent.putExtra("merchantRegister", mRegister);
//			intent.putExtra("URL", REGISTER_URL+REGISTER_PATH);
//			intent.putExtra("api_header",API_HEADER);
//			intent.putExtra("api_header_value", API_VALUE);
//			intent.putExtra("mobile_no",mobileno);
//			context.startService(intent);
//			shopProgress.setVisibility(View.VISIBLE);
//		}else
//		{
//			showToast(context.getResources().getString(R.string.noInternetConnection));
//		}
//	}
//
//	void loginService()
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			Intent intent=new Intent(context, MerchantLoginService.class);
//			intent.putExtra("merchantLogin", mLoginRecevier);
//			intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
//			intent.putExtra("api_header",API_HEADER);
//			intent.putExtra("api_header_value", API_VALUE);
//			intent.putExtra("contact_no", mobileno);
//			intent.putExtra("password", password);
//			context.startService(intent);
//			shopProgress.setVisibility(View.VISIBLE);
//
//		}
//		else
//		{
//			showToast(context.getResources().getString(R.string.noInternetConnection));
//		}
//	}
//	void callSetPasswordService()
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			Intent intent=new Intent(context, MerchantPasswordService.class);
//			intent.putExtra("URL", REGISTER_URL+SET_PASSWORD_PATH);
//			intent.putExtra("api_header",API_HEADER);
//			intent.putExtra("api_header_value", API_VALUE);
//			intent.putExtra("setPassword", mPasswordRecevier);
//			intent.putExtra("password", password);
//			intent.putExtra("mobile_no", mobileno);
//			/*if(ForgetPassword==1)
//			{*/
//			intent.putExtra("verification_code", verification_code);
//			intent.putExtra("isForgotPassword",true);
//			/*}*/
//			context.startService(intent);
//			shopProgress.setVisibility(View.VISIBLE);
//		}else
//		{
//			showToast(context.getResources().getString(R.string.noInternetConnection));
//		}
//	}
//	void loadAllStoreForUser()
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			Intent intent=new Intent(context, GetAllStores_Of_Perticular_User.class);
//			intent.putExtra("getAllStores",mAllStores);
//			intent.putExtra("URL", STORE_URL+SOTRES_PATH);
//			intent.putExtra("api_header",API_HEADER);
//			intent.putExtra("api_header_value", API_VALUE);
//			intent.putExtra("user_id", USER_ID);
//			intent.putExtra("auth_id", AUTH_ID);
//			context.startService(intent);
//			shopProgress.setVisibility(ViewGroup.VISIBLE);
//		}else
//		{
//			showToast(context.getResources().getString(R.string.noInternetConnection));
//		}
//		/*relBroadCastProgress.setVisibility(ViewGroup.VISIBLE);*/
//	}
//
//	void showToast(String text)
//	{
//		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
//	}
//
//
//	
//	public void onReceivePasswordResult(int resultCode, Bundle resultData) {
//
//		shopProgress.setVisibility(View.GONE);
//		String passstatus=resultData.getString("passstatus");
//		String passmsg=resultData.getString("passmsg");
//
//		String error_code=resultData.getString("error_code");
//
//		if(error_code.equalsIgnoreCase("-106")) //Setpassword success
//		{
//		}
//		else if(error_code.equalsIgnoreCase("-108")) //Setpassword failure
//		{
//
//			EventTracker.logEvent("MSignUp_VerifyCodeInvalid", false);
//		}
//
//		if(passstatus.equalsIgnoreCase("true"))
//		{
//			//showToast("Password set successfully!");
//			/*Intent intent=new Intent(context,ShopLocalMerchantInfo.class);
//			startActivity(intent);
//			context.finish();*/
//
//			EventTracker.logEvent("MSignUp_VerifyCodeDone", false);
//
//			loginService();
//
//
//		}
//		else
//		{
//			showToast(passmsg);
//			//				if(passmsg.startsWith("Your mobile number is not verified yet. Please press call or give a missed call on '01246758608'."))
//			//				{
//			//					viewMerchant.setCurrentItem(2, true);
//			//				}
//
//		}
//	}
//
//
//	
//	public void onReceiveLoginResult(int resultCode, Bundle resultData) {
//
//		try
//		{
//			shopProgress.setVisibility(View.GONE);
//			String auth_id=resultData.getString("login_authCode");
//			String loginstatus=resultData.getString("loginstatus");
//			String loginid=resultData.getString("loginid");
//
//			USER_ID=loginid;
//			AUTH_ID=auth_id;
//
//			if(loginstatus.equalsIgnoreCase("true"))
//			{
//				/*ForgetPassword=0;*/
//				EventTracker.logEvent("MSignUp_ShoplocalLoginSuccess", false);
//
//				session.createLoginSession(mobileno, password.toString(), USER_ID, AUTH_ID);
//				loadAllStoreForUser();
//
//				// registration completes
//
//				AppEventsLogger logger = AppEventsLogger.newLogger(this);
//
//				Bundle parameters = new Bundle();
//				parameters.putString("User Type", "Merchant");
//				parameters.putString("Login Type", "Mobile");
//
//				logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION,parameters);
//
//				try
//				{
//
//					Set<String> tags = new HashSet<String>(); 
//					tags=PushManager.shared().getTags();
//					tags.add("merchant");
//
//					PushManager.shared().setTags(tags);
//
//
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
//			}
//			else 
//			{
//				EventTracker.logEvent("MSignUp_ShoplocalLoginFailed", false);
//
//				Toast.makeText(context,auth_id, Toast.LENGTH_SHORT).show();
//			}
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//	
//	public void onBackPressed() {
//
//		finishTo();
//	}
//
//
//	
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		finishTo();
//		return true;
//	}
//
//	void finishTo()
//	{
//		Intent intent=new Intent();
//		intent.putExtra("isAddStore",false);
//		context.setResult(4, intent);
//		context.finish();
//		overridePendingTransition(0,R.anim.shrink_fade_out_center);
//		//		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
//
//	}
//
//	
//	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
//
//		shopProgress.setVisibility(View.GONE);
//		String Status=resultData.getString("SEARCH_STATUS");
//		try
//		{
//			if(Status.equalsIgnoreCase("true"))
//			{
//				Intent intent=new Intent();
//				intent.putExtra("isAddStore",false);
//				context.setResult(4, intent);
//				context.finish();
//
//			}
//			else
//			{
//				Intent intent=new Intent();
//				intent.putExtra("isAddStore",true);
//				context.setResult(4, intent);
//				context.finish();
//			}
//
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//	
//	public void onReceiverMerchantRegister(int resultCode, Bundle resultData) {
//
//		try
//		{
//			shopProgress.setVisibility(View.GONE);
//			String passstatus=resultData.getString("passstatus");
//			String passmsg=resultData.getString("passmsg");
//			String error_code=resultData.getString("error_code");
//
//			if(error_code.equalsIgnoreCase("-119")) //New user
//			{
//				EventTracker.logEvent("MSignUp_New", false);
//			}
//			else if(error_code.equalsIgnoreCase("-102")) //Returning user
//			{
//				EventTracker.logEvent("MSignUp_Return", false);
//			}
//
//			if(passstatus.equalsIgnoreCase("true"))
//			{
//				serverMessage.setText(passmsg);
//			}
//			else
//			{
//				showToast(passmsg);
//			}
//		}catch(Exception  ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//	public  static class VerificationBroadCast extends BroadcastReceiver
//	{
//
//
//		
//		public void onReceive(Context context, Intent intent) {
//	
//			String verification_code= intent.getStringExtra("verification_code");
//			setVerificationCode(verification_code);
//
//			Log.i("Verification", "" +verification_code);
//		}
//
//	}
//
//	static void setVerificationCode(String verification_code)
//	{
//		try
//		{
//		//signup page details.
//		Log.i("Verification Code : ", "Verification Code merchant : "+verification_code);
//		shopVerificationCode.setText(verification_code);
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//
//	}
//
//	
//	protected void onStart() {
//		super.onStart();
//		EventTracker.startFlurrySession(getApplicationContext());
//	}
//
//	
//	protected void onStop() {
//		EventTracker.endFlurrySession(getApplicationContext());	
//		super.onStop();
//	}
//
//	
//	protected void onResume() {
//		super.onResume();
//		EventTracker.startLocalyticsSession(getApplicationContext());
//		if(isVerifyDone == false){
//			EventTracker.logEvent("MSignUp_VerifyCode", false);
//			isVerifyDone = true;
//		}
//	}
//
//	
//	protected void onPause() {
//		EventTracker.endLocalyticsSession(getApplicationContext());
//		super.onPause();
//	}
//
//}
