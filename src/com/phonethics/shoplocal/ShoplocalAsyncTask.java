package com.phonethics.shoplocal;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
/**
 * 
 * @author manoj
 * ShoplocalAsyncTask class is responsible for making network call 
 * with get/post/put/delete request type.
 * returns json data to calling activity
 */
public class ShoplocalAsyncTask extends AsyncTask<String, Integer, Integer> {
	
	private ShoplocalAsyncTaskStatusListener mShoplocalAsyncTaskStatusListener;
	private String msRequestType="get";
	private String msURL;
	private String msJson;
	private String msUserType;
	private String msTag;
	private boolean mbIsCredentialNeeded;
	private JSONObject mPostData;

	/**
	 * 
	 * @param listener : Reference of Interface of calling activity. 
	 * @param requestType : Type of Request get/post/put/delete
	 * @param userType : Merchant/Customer
	 * @param isCredentialNeeded : Whether or not User Credentials needed.
	 * @param tag : Tag of calling api
	 */
	public ShoplocalAsyncTask(ShoplocalAsyncTaskStatusListener listener,String requestType,String userType,
			boolean isCredentialNeeded,String tag) {
		this.mShoplocalAsyncTaskStatusListener = listener;
		this.msRequestType=requestType;
		this.msUserType=userType;
		this.msTag=tag;
		this.mbIsCredentialNeeded=isCredentialNeeded;
	}

	/**
	 * 
	 * @param listener : Reference of Interface of calling activity. 
	 * @param requestType : Type of Request get/post/put/delete
	 * @param userType : Merchant/Customer
	 * @param isCredentialNeeded : Whether or not User Credentials needed.
	 * @param postData : Json data to be post.
	 * @param tag : Tag of calling api
	 */
	public ShoplocalAsyncTask(ShoplocalAsyncTaskStatusListener listener, String requestType,String userType,
			boolean isCredentialNeeded, JSONObject postData,String tag) {
		this.mShoplocalAsyncTaskStatusListener = listener;
		this.msTag=tag;
		this.msRequestType = requestType;
		this.mPostData=postData;
		this.msUserType=userType;
		this.mbIsCredentialNeeded=isCredentialNeeded;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mShoplocalAsyncTaskStatusListener.onAsyncStarted();
	}

	protected Integer doInBackground(String[] urls) {
		this.msURL = urls[0];
		debug("URL = " + msURL);
		int rc = 0 ;
		int timeOut=30000;
		
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams,timeOut);
		HttpConnectionParams.setSoTimeout(httpParams, timeOut);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

		DataInputStream dis = null;
		HttpResponse response = null;

		if(msRequestType.equals("get")) {
			
			HttpGet httpGet = new HttpGet(msURL);

			if(msUserType.equalsIgnoreCase("customer") && mbIsCredentialNeeded==true){
				httpGet.addHeader("user_id", Config.CUST_USER_ID);
				httpGet.addHeader("auth_id", Config.CUST_AUTH_ID);

				debug("USER ID "+ Config.CUST_USER_ID +" AUTH ID "+ Config.CUST_AUTH_ID);
			}
			
			if(msUserType.equalsIgnoreCase("merchant") && mbIsCredentialNeeded==true){
				httpGet.addHeader("user_id", Config.MERCHANT_USER_ID);
				httpGet.addHeader("auth_id", Config.MERCHANT_AUTH_ID);

				debug("Merchant USER ID "+ Config.MERCHANT_USER_ID +" AUTH ID "+ Config.MERCHANT_AUTH_ID);
			}
			httpGet.addHeader(Config.API_HEADER, Config.API_HEADER_VALUE);
			httpGet.addHeader(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
			try {
				response = httpClient.execute(httpGet);
			}catch(IOException e) {
				debug(e.toString());
				e.printStackTrace();
				return rc;
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

		if(msRequestType.equalsIgnoreCase("post")){
			HttpPost httpPost=new HttpPost(msURL);
			if(msUserType.equalsIgnoreCase("customer") && mbIsCredentialNeeded==true){
				httpPost.addHeader("user_id", Config.CUST_USER_ID);
				httpPost.addHeader("auth_id", Config.CUST_AUTH_ID);
				debug("USER ID "+ Config.CUST_USER_ID +" AUTH ID "+ Config.CUST_AUTH_ID);
			}
			if(msUserType.equalsIgnoreCase("merchant") && mbIsCredentialNeeded==true){
				httpPost.addHeader("user_id", Config.MERCHANT_USER_ID);
				httpPost.addHeader("auth_id", Config.MERCHANT_AUTH_ID);
			}
			httpPost.addHeader(Config.API_HEADER, Config.API_HEADER_VALUE);
			httpPost.addHeader(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
			StringEntity se = null;
			try {
				se = new StringEntity( mPostData.toString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return rc;
			}catch(Exception ex){
				ex.printStackTrace();
				return rc;
			}  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);
			// Execute HTTP Post Request
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				return rc;
			} catch (IOException e) {
				e.printStackTrace();
				return rc;
			}
			catch(Exception ex){
				ex.printStackTrace();
				return rc;
			}
		}

		if(msRequestType.equalsIgnoreCase("put")){

			HttpPost httpPost=new HttpPost(msURL);
			if(msUserType.equalsIgnoreCase("customer") && mbIsCredentialNeeded==true){
				httpPost.addHeader("user_id", Config.CUST_USER_ID);
				httpPost.addHeader("auth_id", Config.CUST_AUTH_ID);
				debug("USER ID "+ Config.CUST_USER_ID +" AUTH ID "+ Config.CUST_AUTH_ID);
			}
			if(msUserType.equalsIgnoreCase("merchant") && mbIsCredentialNeeded==true){
				httpPost.addHeader("user_id", Config.MERCHANT_USER_ID);
				httpPost.addHeader("auth_id", Config.MERCHANT_AUTH_ID);
			}
			httpPost.addHeader(Config.API_HEADER, Config.API_HEADER_VALUE);
			httpPost.addHeader("X-HTTP-Method-Override", "PUT");
			httpPost.addHeader(Config.DEVICE_ID_KEY, Config.DEVICE_ID);

			StringEntity se = null;
			try {
				se = new StringEntity(mPostData.toString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return rc;
			}
			catch(Exception ex){
				ex.printStackTrace();
				return rc;
			}

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);
			// Execute HTTP Post Request
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				return rc;
			} catch (IOException e) {
				e.printStackTrace();
				return rc;
			}
			catch(Exception ex){
				ex.printStackTrace();
				return rc;
			}
		}

		if(msRequestType.equalsIgnoreCase("delete")){

			HttpDelete httpDelete=new HttpDelete(msURL);
			if(msUserType.equalsIgnoreCase("customer") && mbIsCredentialNeeded==true){
				httpDelete.addHeader("user_id", Config.CUST_USER_ID);
				httpDelete.addHeader("auth_id", Config.CUST_AUTH_ID);

				debug("USER ID "+ Config.CUST_USER_ID +" AUTH ID "+ Config.CUST_AUTH_ID);
			}
			if(msUserType.equalsIgnoreCase("merchant") && mbIsCredentialNeeded==true){
				httpDelete.addHeader("user_id", Config.MERCHANT_USER_ID);
				httpDelete.addHeader("auth_id", Config.MERCHANT_AUTH_ID);
			}
			httpDelete.addHeader(Config.API_HEADER, Config.API_HEADER_VALUE);
			httpDelete.addHeader(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
			// Execute HTTP Post Request
			try {
				response = httpClient.execute(httpDelete);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				return rc;
			} catch (IOException e) {
				e.printStackTrace();
				return rc;
			}catch(Exception ex){
				ex.printStackTrace();
				return rc;
			}
		}


		try
		{
			//Getting status code from Response.
			rc = response.getStatusLine().getStatusCode();
			if(rc != 200) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
				return rc;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		HttpEntity entity = response.getEntity();

		if(entity != null) {
			try {
				dis = new DataInputStream(entity.getContent());
			}catch(Exception e) {
				debug(e.toString());
				e.printStackTrace();
				return 0;
			}

			if(dis == null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
				return 0;
			}

			if(entity.isChunked()) {
				byte[] ayBuffer = new byte[4096];
				try {
					int n = dis.read(ayBuffer);
					msJson="";
					while(n>0){
						msJson += new String(ayBuffer, 0, n);
						n = dis.read(ayBuffer);
					}
				}catch(Exception ioe) {
					debug(ioe.toString());
					ioe.printStackTrace();
				}
			}else {
				int len = (int) (entity.getContentLength());
				byte[] ayBuffer = new byte[len];
				int cyRead = 0;
				try{
					while(cyRead < len) {
						cyRead += dis.read(ayBuffer,cyRead,len-cyRead);
					}
				}catch(Exception e) {
					e.printStackTrace();
					httpClient.getConnectionManager().shutdown();
					httpClient = null;
					try {
						dis.close();
					}catch(Exception ee) {
						ee.printStackTrace();
					}finally {
						dis = null;
					}
				}
				msJson = new String(ayBuffer);
			}
			httpClient.getConnectionManager().shutdown();
			httpClient = null;
		}

		return new Integer(rc);
	}


	protected void onProgressUpdate(Integer ...progress) {

	}
	/**
	 * If Result Code Status is 200 then send Json data 
	 * with Url and Tag back to the calling activity
	 */
	protected void onPostExecute(Integer result) {
		debug("OnPostExecute");
		debug(result+"");
		if(result != 200) {
			mShoplocalAsyncTaskStatusListener.onError(result);
			return;
		}

		if(result == 200 && msJson != null) {
			//debug(msJson.toString());
			mShoplocalAsyncTaskStatusListener.onCompleted(msURL, msJson,msTag);
		}
	}

	private void debug(String s) {
		Log.v("ShoplocalAsyncTask " , "ShoplocalAsyncTask " + s);
	}


}