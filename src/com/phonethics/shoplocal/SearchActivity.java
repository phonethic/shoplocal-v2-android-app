package com.phonethics.shoplocal;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.CallListDialog;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.adapters.MarketPlaceSearchAdapter;
import com.phonethics.adapters.MyShoplocalAdapter;


public class SearchActivity extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener, ListClickListener, DBListener
{
	private PullToRefreshListView mListOffersSearch;
	private RelativeLayout mRelListResult;
	private LinearLayout mCounterLayout;

	private TextView mTxtCounter;
	private EditText mEdtSearch;
	private RelativeLayout mOfferLayout;
	private TextView mTxtWithOffer;
	private CheckBox mChkBox;


	private RelativeLayout mIsOfferLayout;
	private RelativeLayout mCategoryLayout;
	private RelativeLayout mSearchLayout;

	private ListView mListCategory;
	private PullToRefreshListView mMarketList;
	private PullToRefreshListView mOffersList;

	private ArrayList<String> mArrCategoryLable=new ArrayList<String>();
	private ArrayList<String> mArrCategoryId=new ArrayList<String>();

	private Activity mContext;

	private boolean mbIsOfferSearch;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo1=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo2=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo3=new ArrayList<String>();

	private ArrayList<String> mArrTelNo1=new ArrayList<String>();
	private ArrayList<String> mArrTelNo2=new ArrayList<String>();
	private ArrayList<String> mArrTelNo3=new ArrayList<String>();

	private ArrayList<String> mArrPostId=new ArrayList<String>();
	private ArrayList<String> mArrPostTitle=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrPostDesc=new ArrayList<String>();
	private ArrayList<String> mArrPostDate=new ArrayList<String>();
	private ArrayList<String> mArrPostImage=new ArrayList<String>();
	private ArrayList<String> mArrPostHasOffer=new ArrayList<String>();
	private ArrayList<String> mArrTotalLike=new ArrayList<String>();
	private ArrayList<String> mArrTotalShare=new ArrayList<String>();
	private ArrayList<String> mArrPostValidityDate= new ArrayList<String>();

	private ArrayList<String> mArrDistance= new ArrayList<String>();
	private ArrayList<String> mArrLogo= new ArrayList<String>();
	private ArrayList<String> mArrHasOffer= new ArrayList<String>();
	private ArrayList<String> mArrDescription= new ArrayList<String>();
	private ArrayList<String> mArrCall= new ArrayList<String>();

	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;

	private MyShoplocalAdapter mAdapter;
	private MarketPlaceSearchAdapter mMarketAdapter;
	private CategoriesAdapter mCategAdapter;

	private int miTotalRecords;
	private int miTotalPages;
	private int miCurrentPage=0;
	private int miItemPerPage=50;
	private int tempPosition=0;

	private boolean mbIsRefreshing=false;	

	private String msTempPostId="";
	private String msStoreId="";
	private String msPostId="";
	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msStoreName="";
	private String msImageLink="";
	private String msIsOffer="";
	private String msOfferValidTill="";
	private String msPostTitle="";
	private String msPostDescription="";
	private String msShareVia="";
	private String msCallType="";
	private String msActiveAreaName="";
	private String msDateTime;

	private ProgressDialog mProgressDialog;

	private Map<String, String> eventMap=new HashMap<String, String>();
	private Map<String,String> searchMap=new HashMap<String, String>();

	private String TAG="SearchActivity";

	private	SimpleDateFormat mFormat;

	private long mlDiffDays;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		mContext=this;

		msActiveAreaName=mDbUtil.getActiveArea();

		setTitle("Search");
		hideDrawerLayout();

		mFormat=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		mProgressDialog=new ProgressDialog(mContext);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");

		mChkBox=(CheckBox)findViewById(R.id.chkBox);
		mEdtSearch=(EditText)findViewById(R.id.edtSearch);
		mIsOfferLayout=(RelativeLayout)findViewById(R.id.offerLayout);
		mRelListResult=(RelativeLayout)findViewById(R.id.relListResult);
		mCategoryLayout=(RelativeLayout)findViewById(R.id.categoryLayout);

		mListCategory=(ListView)findViewById(R.id.listCategory);
		mMarketList=(PullToRefreshListView)findViewById(R.id.listMarketPlaceSearch);
		mOffersList=(PullToRefreshListView)findViewById(R.id.listOffersSearch);

		mTxtCounter=(TextView)findViewById(R.id.txtCounter);
		mTxtCounter.setVisibility(View.GONE);


		mCategoryLayout.setVisibility(View.VISIBLE);
		mIsOfferLayout.setVisibility(View.GONE);
		mRelListResult.setVisibility(View.GONE);

		Bundle b=getIntent().getExtras();

		if(b!=null){
			mbIsOfferSearch=b.getBoolean("isOfferSearch");
			if(mbIsOfferSearch==true){
				mOffersList.setVisibility(View.VISIBLE);
				mMarketList.setVisibility(View.GONE);
			}else{
				mOffersList.setVisibility(View.GONE);
				mMarketList.setVisibility(View.VISIBLE);
			}
		}

		mCategAdapter=new CategoriesAdapter(mContext, 0, 0, mArrCategoryLable);

		mAdapter=new MyShoplocalAdapter(mContext, 0, 0, mArrPostId, mArrPostTitle, mArrPostDesc, mArrStoreName, 
				mArrPostHasOffer, mArrPostImage, mArrPostDate, 
				mArrTotalLike, mArrTotalShare, this);
		mMarketAdapter=new MarketPlaceSearchAdapter(mContext, 0, 0,mArrStoreId,mArrStoreName, mArrLogo, mArrHasOffer, 
				mArrPostTitle, mArrDescription, mArrDistance, mArrTotalLike,this); 

		mOffersList.setAdapter(mAdapter);
		mMarketList.setAdapter(mMarketAdapter);
		mListCategory.setAdapter(mCategAdapter);
		mListCategory.setOnItemClickListener(new CategoryListClickListener());
		if(mDbUtil.getCategoryRowCount()==0)
		{
			loadCategories();	
		}else{
			try
			{
				SharedPreferences prefs=getSharedPreferences(Config.APP_CATEGORY, MODE_PRIVATE);
				msDateTime=prefs.getString(Config.APP_CATEGORY_KEY, mFormat.format(new Date()).toString());
				Date prevDate=mFormat.parse(msDateTime);
				Date currentDate=mFormat.parse(mFormat.format(new Date()));
				//in milliseconds
				long diff = currentDate.getTime() - prevDate.getTime();
				mlDiffDays = diff / (24 * 60 * 60 * 1000);
				if(mlDiffDays>=1){
					loadCategories();	
				}else{
					mArrCategoryLable.clear();
					mArrCategoryId.clear();
					mArrCategoryLable=mDbUtil.getData("CATEGORIES", "label");
					mArrCategoryId=mDbUtil.getData("CATEGORIES", "id");
					for(int i=0;i<mArrCategoryId.size();i++){
						Config.debug(TAG, TAG+" "+mArrCategoryLable.get(i)+" "+mArrCategoryId.get(i));
					}
					//			mCategAdapter.notifyDataSetChanged();
					mCategAdapter=new CategoriesAdapter(mContext, 0, 0, mArrCategoryLable);
					mListCategory.setAdapter(mCategAdapter);
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}


		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());

		//Add text change listener
		mEdtSearch.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//				mIsOfferLayout.setVisibility(View.VISIBLE);
			}
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			public void afterTextChanged(Editable s) {
				if(s.toString().length()==0){
					//Temporarily disabled
					if(!mCategoryLayout.isShown()){
						Animation animaSlidUp=AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
						mCategoryLayout.startAnimation(animaSlidUp);
						mCategoryLayout.setVisibility(View.VISIBLE);
						mRelListResult.setVisibility(View.GONE);

						//added 
						//clearSearchArray();

					}
					//					Animation animaSlidOut=AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
					//					mIsOfferLayout.startAnimation(animaSlidOut);
					//					mIsOfferLayout.setVisibility(View.GONE);
				}
			}
		});
		//Searching In API.
		mEdtSearch.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if(mInternet.isNetworkAvailable())
					{
						if(mEdtSearch.getText().toString().length()!=0)
						{
							/*		animaSlidUp.setFillAfter(true);*/
							Animation animaFadeOut=AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
							animaFadeOut.setFillAfter(true);

							//Temporarily disabled
							mCategoryLayout.startAnimation(animaFadeOut);
							if(mChkBox.isChecked())
							{
								mChkBox.setChecked(false);
							}
							clearData();
							if(mbIsOfferSearch==true){
								getOffers();
							}else{
								getStores();
							}
							try
							{
								//Hiding keyboard
								InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
							}catch(NullPointerException npe)
							{
								npe.printStackTrace();
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}

							mRelListResult.setVisibility(View.VISIBLE);

							animaFadeOut.setAnimationListener(new AnimationListener() {
								public void onAnimationStart(Animation animation) {
								}
								public void onAnimationRepeat(Animation animation) {
								}
								public void onAnimationEnd(Animation animation) {
									mCategoryLayout.setVisibility(View.GONE);
									mRelListResult.setVisibility(View.VISIBLE);
								}
							});
						}
					}
					else{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
					return true;
				}
				return false;
			}
		});

		mOffersList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				int tempPos=position-1;
				Intent intent=new Intent(mContext, OffersBroadCastDetails.class);
				intent.putExtra("storeName",mArrStoreName.get(tempPos));
				intent.putExtra("title",mArrPostTitle.get(tempPos));
				intent.putExtra("body",mArrPostDesc.get(tempPos));
				intent.putExtra("PLACE_ID",mArrStoreId.get(tempPos));
				intent.putExtra("POST_ID",mArrPostId.get(tempPos));
				intent.putExtra("fromCustomer", true);
				intent.putExtra("mob1",mArrMobileNo1.get(tempPos));
				intent.putExtra("mob2",mArrMobileNo2.get(tempPos));
				intent.putExtra("mob3",mArrMobileNo3.get(tempPos));
				intent.putExtra("tel1",mArrTelNo1.get(tempPos));
				intent.putExtra("tel2",mArrTelNo2.get(tempPos));
				intent.putExtra("tel3",mArrTelNo3.get(tempPos));
				intent.putExtra("photo_source",mArrPostImage.get(tempPos));
				intent.putExtra("USER_LIKE","-1");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		mOffersList.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				if(mInternet.isNetworkAvailable()){
					if(miCurrentPage<miTotalPages){
						getOffers();
					}
					else{
						debug("current page "+miCurrentPage+" total pages "+miTotalPages);
					}
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});
		mOffersList.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mbIsRefreshing=true;
				if(mInternet.isNetworkAvailable()){
					miCurrentPage=0;
					getOffers();
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		mMarketList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				int tempPos=position-1;
				Intent intent=new Intent(mContext, StoreDetailsActivity.class);
				intent.putExtra("STORE_ID", mArrStoreId.get(tempPos));
				startActivity(intent);
			}
		});

		mMarketList.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
			@Override
			public void onLastItemVisible() {
				if(mInternet.isNetworkAvailable()){
					if(miCurrentPage<miTotalPages){
						getStores();
					}
					else{
						debug("current page "+miCurrentPage+" total pages "+miTotalPages);
					}
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		mMarketList.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mbIsRefreshing=true;
				if(mInternet.isNetworkAvailable()){
					miCurrentPage=0;
					getStores();
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

	}//onCreate ends here

	private void showProgressDialog(){
		mProgressDialog.show();
	}

	void getOffers()
	{
		if(mInternet.isNetworkAvailable()){

			searchMap.put("Search_OffersCategory","All");
			searchMap.put("Search_Text",mEdtSearch.getText().toString());
			EventTracker.logEvent("Search_OffersCategory", searchMap);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("area_id", Config.MY_AREA));
			nameValuePairs.add(new BasicNameValuePair("search", mEdtSearch.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("page", (miCurrentPage+1)+""));
			nameValuePairs.add(new BasicNameValuePair("count", miItemPerPage+""));
			String paramString=URLEncodedUtils.format(nameValuePairs, "utf-8");
			//			String sURL = Config.OFFERS_STREAM_URL +"&area_id="+Config.MY_AREA +"&search="+mEdtSearch.getText().toString()+ "&page=" + (miCurrentPage+1) + "&count="+miItemPerPage;
			String sURL = Config.OFFERS_STREAM_URL +paramString;
			new ShoplocalAsyncTask(this, "get", "customer", false,"offers").execute(sURL);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));	
		}
	}

	void getStores()
	{
		if(mInternet.isNetworkAvailable()){

			searchMap.put("Category_Name","All");
			searchMap.put("Search_Text", mEdtSearch.getText().toString());
			EventTracker.logEvent("Search_StoreCategory", searchMap);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("area_id", Config.MY_AREA));
			nameValuePairs.add(new BasicNameValuePair("latitude", Config.LATITUDE));
			nameValuePairs.add(new BasicNameValuePair("longitude",Config.LONGITUDE ));
			nameValuePairs.add(new BasicNameValuePair("distance",Config.DISTANCE));
			nameValuePairs.add(new BasicNameValuePair("search", mEdtSearch.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("page", (miCurrentPage+1)+""));
			nameValuePairs.add(new BasicNameValuePair("count", miItemPerPage+""));
			String paramString=URLEncodedUtils.format(nameValuePairs, "utf-8");
			String sURL = Config.MARKET_PLACE_URL +paramString;
			new ShoplocalAsyncTask(this, "get", "customer", false,"stores").execute(sURL);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));	
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
		return true;
	}

	public void onBackPressed() {
		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
	}

	void loadCategories()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.CATEGORIES_URL;
				new ShoplocalAsyncTask(this, "get", "customer", false, "categories").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}
	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		if(tag.equalsIgnoreCase("categories")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");

				if(status==null || status.equalsIgnoreCase("false")) return;

				JSONArray jsonArray=jsonObject.getJSONArray("data");
				int count=jsonArray.length();

				for(int i=0;i<count;i++){
					JSONObject json=jsonArray.getJSONObject(i);
					mArrCategoryLable.add(json.getString("label"));
					mArrCategoryId.add(json.getString("id"));
				}

				ContentValues[] values = new ContentValues[count];
				for(int i=0;i<count;i++) {
					ContentValues value = new ContentValues();
					value.put("label", mArrCategoryLable.get(i));
					value.put("id", mArrCategoryId.get(i));
					values[i] = value;
				}

				DBUtil dbUtil = DBUtil.getInstance(mActivity.getApplicationContext());
				dbUtil.replaceOrUpdate(this,"CATEGORIES",values,"loadCateg"); 
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("offers")){
			try
			{


				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String msg=jsonObject.getString("message");
					showToast(msg);
					return;
				}

				if(mbIsRefreshing==true){
					mbIsRefreshing=false;
					clearData();
					mOffersList.onRefreshComplete();
				}

				if(status.equalsIgnoreCase("false")){
					String msg=jsonObject.getString("message");
					clearData();
					mTxtCounter.setText("");
					showToast(msg);
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");
				if(dataObject == null) return;

				miCurrentPage++;
				miTotalRecords = dataObject.getInt("total_record");
				miTotalPages = dataObject.getInt("total_page");

				JSONArray jsonArray = dataObject.getJSONArray("record");
				if(jsonArray == null)  return;

				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					if(offerObject != null) {
						mArrStoreId.add(offerObject.getInt("id")+"");
						mArrStoreName.add(offerObject.getString("name"));
						mArrMobileNo1.add(offerObject.getString("mob_no1"));
						mArrMobileNo2.add(offerObject.getString("mob_no2"));
						mArrMobileNo3.add(offerObject.getString("mob_no3"));
						mArrTelNo1.add(offerObject.getString("tel_no1"));
						mArrTelNo2.add(offerObject.getString("tel_no2"));
						mArrTelNo3.add(offerObject.getString("tel_no3"));
						mArrPostTitle.add(offerObject.getString("title"));
						mArrPostId.add(offerObject.getString("post_id"));
						mArrPostDesc.add(offerObject.getString("description"));
						mArrPostImage.add(offerObject.getString("image_url1"));
						mArrPostDate.add(offerObject.getString("date"));
						mArrPostValidityDate.add(offerObject.getString("offer_date_time"));
						mArrPostHasOffer.add(offerObject.getString("is_offered"));
						mArrTotalLike.add(offerObject.getString("total_like"));
						mArrTotalShare.add(offerObject.getString("total_share"));
					}
				}

				mTxtCounter.setText(mArrPostId.size()+"/"+miTotalRecords);
				mAdapter.notifyDataSetChanged();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("stores")){
			try
			{

				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String msg=jsonObject.getString("message");
					showToast(msg);
					return;
				}

				if(mbIsRefreshing==true){
					mbIsRefreshing=false;
					clearData();
					mMarketList.onRefreshComplete();
				}


				JSONObject dataObject = jsonObject.getJSONObject("data");
				if(dataObject == null) return;

				miCurrentPage++;
				miTotalRecords = dataObject.getInt("total_record");
				miTotalPages = dataObject.getInt("total_page");

				JSONArray jsonArray = dataObject.getJSONArray("record");
				if(jsonArray == null)  return;

				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject storeObject = jsonArray.getJSONObject(i);
					if(storeObject != null) {
						mArrStoreId.add(storeObject.getInt("id")+"");
						mArrStoreName.add(storeObject.getString("name"));
						mArrDescription.add(storeObject.getString("description"));
						mArrLogo.add(storeObject.getString("image_url"));
						mArrMobileNo1.add(storeObject.getString("mob_no1"));
						mArrMobileNo2.add(storeObject.getString("mob_no2"));
						mArrMobileNo3.add(storeObject.getString("mob_no3"));
						mArrTelNo1.add(storeObject.getString("tel_no1"));
						mArrTelNo2.add(storeObject.getString("tel_no2"));
						mArrTelNo3.add(storeObject.getString("tel_no3"));
						if(storeObject.has("distance")){
							mArrDistance.add(storeObject.getString("distance"));
							debug(storeObject.getString("distance"));
						}
						else{
							mArrDistance.add("0");
						}
						mArrTotalLike.add(storeObject.getInt("total_like")+"");
						mArrHasOffer.add(storeObject.getString("has_offer"));
						mArrPostTitle.add(storeObject.getString("title"));
					}
				}
				//				mTxtCounter.setText(mArrStoreId.size()+"/"+miTotalRecords);

				mMarketAdapter.notifyDataSetChanged();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("likePost")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){
					
					try
					{
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(mArrStoreName.get(tempPosition));
						if(mArrPostImage.get(tempPosition).length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+mArrPostImage.get(tempPosition));
						}
						fbObj.setStoreDescription(mArrPostDesc.get(tempPosition));
						String encodedurl = URLEncoder.encode(mArrStoreName.get(tempPosition),"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+mArrStoreId.get(tempPosition));
						FacebookOperations.FavThisOnFb(mActivity, fbObj, getString(R.string.fb_offer_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					
					String message=jsonObject.getString("message");
					showToast(message);
				}

			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("sharePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;

				if(status.equalsIgnoreCase("true")){

					if(mbFaceBookShare==false)
					{
						try
						{
							FbObject fbObj=new FbObject();
							fbObj.setStoreTitle(mArrStoreName.get(tempPosition));
							if(mArrPostImage.get(tempPosition).length()==0){
								fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
							}else{
								fbObj.setStoreImage(Config.IMAGE_URL+mArrPostImage.get(tempPosition));
							}
							fbObj.setStoreDescription(mArrPostDesc.get(tempPosition));
							String encodedurl = URLEncoder.encode(mArrStoreName.get(tempPosition),"UTF-8");
							debug(Config.FACEBOOK_SHARE_URL+msSlug);
							fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+mArrStoreId.get(tempPosition));
							FacebookOperations.shareThisInfo(mActivity, fbObj, getString(R.string.fb_store_object));
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	@Override
	public void onError(int errorCode) {
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		showToast(getResources().getString(R.string.errorMessage));
	}
	void debug(String msg){
		Log.d("Search", "Search "+msg);
	}

	private class CategoryListClickListener implements OnItemClickListener
	{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			Config.debug(TAG, TAG+" Clicked "+mArrCategoryId.get(position));
			if(mbIsOfferSearch==false){
				eventMap.put("Category_Name", mArrCategoryId.get(position));
				EventTracker.logEvent("Search_StoreCategory", eventMap);
				Intent intent=new Intent(mContext,CategorySearch.class);
				intent.putExtra("category_id", mArrCategoryId.get(position));
				intent.putExtra("offer", "");
				intent.putExtra("isCategory", true);
				startActivity(intent);
			}else{
				eventMap.put("Category_Name", mArrCategoryId.get(position));
				EventTracker.logEvent("Search_OffersCategory", eventMap);
				Intent intent=new Intent(mContext,NewsFeedCategory.class);
				intent.putExtra("category_id", mArrCategoryId.get(position));
				intent.putExtra("offer", "");
				intent.putExtra("isCategory", true);
				startActivity(intent);
			}
		}

	}


	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		this.msTempPostId=msPostId;
		tempPosition=position;
		if(tag.equalsIgnoreCase("like")){
			if(mSessionManager.isLoggedInCustomer()){
				likePost(msPostId);
			}else{
				loginAlert(getResources().getString(R.string.postFavAlert), 1);
			}
		}else if(tag.equalsIgnoreCase("share")){
			if(mInternet.isNetworkAvailable()){
				showShareDialog(position);
			}else{
				showToast(getString(R.string.noInternetConnection));
			}
		}else if(tag.equalsIgnoreCase("call")){

			mArrCall.clear();

			mCalldailog.setTitle(mArrStoreName.get(position));
			if(mArrMobileNo1.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo1.get(position).toString());
			}
			if(mArrMobileNo2.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo2.get(position).toString());
			}
			if(mArrMobileNo3.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo3.get(position).toString());
			}
			if(mArrTelNo1.get(position).length()>5){
				mArrCall.add(mArrTelNo1.get(position).toString());
			}
			if(mArrTelNo2.get(position).length()>5){
				mArrCall.add(mArrTelNo2.get(position).toString());
			}
			if(mArrTelNo3.get(position).length()>5){
				mArrCall.add(mArrTelNo3.get(position).toString());
			}
			mCallAdapter.notifyDataSetChanged();

			if(mArrCall.size()!=0){

				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("storeName",mArrStoreName.get(tempPosition));
				debug(mArrStoreName.get(tempPosition));
				msCallType="StoreCall";
				if(mbIsOfferSearch==true){
					callContactApi(msPostId,"post_id");

					callClicked.put("Source","SearchOffers");

				}else{
					callContactApi(mArrStoreId.get(position),"place_id");

					callClicked.put("Source","SearchMarketPlace");
				}
				mCalldailog.show();
				EventTracker.logEvent("StoreCall", callClicked);
			}else{
				showToast("No numbers to call");
			}
		}
	}

	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {

			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("storeName",mArrStoreName.get(tempPosition));
			callClicked.put("Number",mArrCall.get(position));
			debug(mArrStoreName.get(tempPosition));

			msCallType="StoreCallDone";
			if(mbIsOfferSearch==true){
				callContactApi(msTempPostId,"post_id");

				callClicked.put("Source","SearchOffers");
			}else{
				callContactApi(mArrStoreId.get(tempPosition),"place_id");

				callClicked.put("Source","SearchMarketPlace");
			}
				likePlace(mArrStoreId.get(tempPosition));
			EventTracker.logEvent("StoreCallDone", callClicked);

			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();


		}

	}
	void callContactApi(String id,String type)
	{
		try
		{
//			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);

					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put(type, id);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);

					debug(jsonObject.toString());

					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
//			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	void likePlace(String postId)
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", postId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlaceOnCall").execute(msUrl);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	void likePost(String msPostId)
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("post_id", msTempPostId);
				json.put("auth_id", Config.CUST_AUTH_ID);
				String msUrl=Config.POST_LIKE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"likePost").execute(msUrl);
			}else
			{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void showShareDialog(int position){
		try
		{
			msMobNo1=mArrMobileNo1.get(position);
			msMobNo2=mArrMobileNo2.get(position);
			msMobNo3=mArrMobileNo3.get(position);

			msTelNo1=mArrTelNo1.get(position);
			msTelNo2=mArrTelNo2.get(position);
			msTelNo3=mArrTelNo3.get(position);

			msIsOffer=mArrPostHasOffer.get(position);
			msOfferValidTill=mArrPostValidityDate.get(position);

			msPostTitle=mArrPostTitle.get(position);
			msPostDescription=mArrPostDesc.get(position);
			msStoreName=mArrStoreName.get(position);

			msImageLink=mArrPostImage.get(position);

			String shareBody= "";
			String validTill="";
			String contact="";
			String contact_lable="\nContact:";
			if(msIsOffer.length()!=0 && msIsOffer.equalsIgnoreCase("1")){
				try
				{
					DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
					DateFormat dt2=new SimpleDateFormat("MMM");
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

					Date date_con = (Date) dt.parse(msOfferValidTill.toString());
					Date date=dateFormat.parse(msOfferValidTill.toString());

					DateFormat date_format=new SimpleDateFormat("hh:mm a");
					Calendar cal = Calendar.getInstance();
					cal.setTime(date_con);
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH);
					int day = cal.get(Calendar.DAY_OF_MONTH);
					validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);

				}catch(Exception ex)
				{
					ex.printStackTrace();
					validTill="";
				}
			}
			else{
				validTill="";
			}
			try
			{
				if(msMobNo1.length()>0){
					contact="+"+msMobNo1;
				} 
				if(msMobNo2.length()>0){
					contact+=" +"+msMobNo2;
				}
				if(msMobNo3.length()>0){
					contact+=" +"+msMobNo3;
				}
				if(msTelNo1.length()>0){
					contact+=" "+msTelNo1;
				}
				if(msTelNo2.length()>0){
					contact+=" "+msTelNo2;
				}
				if(msTelNo3.length()>0){
					contact+=" "+msTelNo3;
				}
				if(contact.length()==0){
					contact_lable="";
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			shareBody=msStoreName + " - " + "Offers" + " : " + "\n\n"+msPostTitle + "\n" + 
					msPostDescription +validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
			offerShareDialog(msStoreName, msImageLink, shareBody);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==1)
				{
					Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
					mContext.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	void sharePost(String msPostId)
	{
		try
		{
			try {
				if(mInternet.isNetworkAvailable()){
					JSONObject json = new JSONObject();
					json.put("post_id", msTempPostId);
					json.put("via", msShareVia);
					String msUrl=Config.POST_SHARE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"sharePost").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void clearData()
	{
		mArrStoreId.clear();
		mArrStoreName.clear();
		mArrMobileNo1.clear();
		mArrMobileNo2.clear();
		mArrMobileNo3.clear();
		mArrTelNo1.clear();
		mArrTelNo2.clear();
		mArrTelNo3.clear();
		mArrPostTitle.clear();
		mArrPostId.clear();
		mArrPostDesc.clear();
		mArrPostImage.clear();
		mArrPostDate.clear();
		mArrPostHasOffer.clear();
		mArrTotalLike.clear();
		mArrTotalShare.clear();
		mArrPostValidityDate.clear();

		mArrDistance.clear();
		mArrLogo.clear();
		mArrHasOffer.clear();
		mArrTotalShare.clear();
		mArrDescription.clear();

		miTotalPages=0;
		miTotalRecords=0;
		miCurrentPage=0;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
		}
		if(requestCode==4)
		{
			if(mSessionManager.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();
				}
				else
				{
					Intent intent=new Intent(mContext,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
		if(requestCode == 3){
			//		VIA="Android";
			//	callShareApi(TEMP_POST_ID);
			msShareVia="Android";
			sharePost(msPostId);
		}

		try
		{
			mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {

				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}


				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					//				VIA="Android-Facebook";
					//				callShareApi(TEMP_POST_ID);
					msShareVia="Android-Facebook";
					sharePost(msPostId);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onCompleteInsertion(String tag) {
		mCategAdapter.notifyDataSetChanged();
		SharedPreferences prefs=getSharedPreferences(Config.APP_CATEGORY,MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putString(Config.APP_CATEGORY_KEY,mFormat.format(new Date()));
		editor.commit();
	}



}
//public class SearchActivity extends SherlockActivity implements SearchResult,GetCategory, OnCheckedChangeListener, Receiver, NewsFeed, FavouritePostInterface, ShareResultInterface {
//
//	Activity context;
//	ActionBar actionBar;
//
//	String STORE_URL;
//	String NEWS_FEED_SEARCH_URL;
//	String LOGIN_URL;
//	String API_HEADER;
//	String API_VALUE;
//	String STORE_CATEGORY;
//	String GET_SEARCH_URL;
//	String offer="";
//
//	//search layout
//	EditText edtSearch;
//	ProgressBar searchListProg;
//	Button btnSearchLoadMore;
//	ListView listCategory;
//
//	PullToRefreshListView listSearchResult;
//	PullToRefreshListView listNewsFeedSearch;
//
//
//	RelativeLayout searchListFilter;
//	RelativeLayout relListResult;
//	TextView txtSearchCounter;
//	TextView txtSearchCounterBack;
//	ImageView imgSearchButton;
//	ImageView imgSearchClose;
//
//
//	String TOTAL_SEARCH_PAGES="";
//	String TOTAL_SEARCH_RECORDS="";
//	String PHOTO_PARENT_URL;
//
//	//Search
//	ArrayList<String> Search_ID=new ArrayList<String>();
//	ArrayList<String> Search_SOTRE_NAME=new ArrayList<String>();	
//	ArrayList<String> Search_BUILDING=new ArrayList<String>();
//	ArrayList<String> Search_STREET=new ArrayList<String>();
//	ArrayList<String> Search_LANDMARK=new ArrayList<String>();
//	ArrayList<String> Search_AREA=new ArrayList<String>();
//	ArrayList<String> Search_CITY=new ArrayList<String>();
//	ArrayList<String> Search_STATE=new ArrayList<String>();
//	ArrayList<String> Search_COUNTRY=new ArrayList<String>();
//	ArrayList<String> Search_MOBILE_NO=new ArrayList<String>();
//	ArrayList<String> Search_PHOTO=new ArrayList<String>();
//	ArrayList<String> Search_DISTANCE=new ArrayList<String>();
//	ArrayList<String> Search_DESCRIPTION=new ArrayList<String>();
//	ArrayList<String>category=new ArrayList<String>();
//	ArrayList<String>category_id=new ArrayList<String>();
//
//	ArrayList<String> Search_HAS_OFFER=new ArrayList<String>();
//	ArrayList<String> Search_OFFER=new ArrayList<String>();
//
//	//Newsfeed
//	ArrayList<String> NEWS_FEED_ID=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_NAME=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_AREA_ID=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_MOBNO1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_MOBNO2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_MOBNO3=new ArrayList<String>();
//
//
//	ArrayList<String> NEWS_FEED_TELLNO1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_TELLNO2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_TELLNO3=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_LATITUDE=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_LONGITUDE=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_DISTANCE=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_POST_ID=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_POST_TYPE=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_POST_TITLE=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_DESCRIPTION=new ArrayList<String>();
//
//
//	ArrayList<String> NEWS_FEED_image_url1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_image_url2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_image_url3=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_thumb_url1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_thumb_url2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_thumb_url3=new ArrayList<String>();
//
//	ArrayList<String> NEWS_DATE=new ArrayList<String>();
//	ArrayList<String> NEWS_IS_OFFERED=new ArrayList<String>();
//	ArrayList<String> NEWS_OFFER_DATE_TIME=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_place_total_like=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_place_total_share=new ArrayList<String>();
//
//
//	int search_post_count=20;
//	int search_page=0;
//
//	boolean isCheckOffers=false;
//	boolean isSearchClicked=false;
//
//	SearchResultReceiver searchReceiver;
//	GetPlaceCategoryReceiver mStoreCategoryReceiver;
//	LocationResultReceiver mReceiver;
//	NewsFeedReceiver newsFeedReceiver;
//	NetworkCheck isnetConnected;
//
//	//Session Manger
//	SessionManager session;
//	boolean isSplitLogin=false;
//
//	CategoryListAdapter categAdapter;
//	RelativeLayout categoryLayout;
//
//	CheckBox chkBox;
//	int sposition=0;
//	String locality="Lokhandwala";
//	boolean isRefreshing=false;
//
//
//	String latitued="";
//	String longitude="";
//	String key="area_id";
//	String value;
//
//
//	boolean isLocationFound=false;
//
//
//
//	ListView drawerList;
//	String drawer_item="";
//	DrawerAdapter drawerAdapter;
//	boolean isGenericSearch=false;
//	boolean isNewsCategory=false;
//	String search_text;
//
//	//Database
//	DBUtil dButil;
//	NewsFeedAdapter adapter=null;
//	//Newsfeed
//	String USER_ID;
//	String AUTH_ID;
//	String VIA = "Android";
//	//User Id
//	String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";
//	//Auth Id
//	String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";
//	//Password
//	String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";
//	int POST_LIKE=2;
//	ShareResultReceiver shareServiceResult;
//	FavouritePostResultReceiver mFav;
//	String TEMP_POST_ID="";
//	String BROADCAST_URL;
//	String LIKE_API;
//	String SHARE_URL;
//
//	SearchApiListAdapter sadapter;
//
//	Map<String, String> eventMap;
//	Map<String,String> searchMap;
//
//	String areaName = "";
//
//	private UiLifecycleHelper uiHelper;
//
//	/* Custom dialog for share */
//	ArrayList<String> packageNames = new ArrayList<String>();
//	ArrayList<String> appName = new ArrayList<String>();
//	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
//	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
//	Dialog dialogShare;
//	ListView listViewShare;
//
//	/** Check if facebook is present in phone */
//	boolean isFacebookPresent = false;
//
//
//	protected void onCreate(Bundle savedInstanceState) {
//		setTheme(R.style.Theme_City_custom);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_search);
//
//		context=this;
//		session=new SessionManager(getApplicationContext());
//		isSplitLogin=session.isLoggedIn();
//		dButil=new DBUtil(context);
//
//		//facebook
//		uiHelper = new UiLifecycleHelper(context, null);
//		uiHelper.onCreate(savedInstanceState);
//
//		eventMap=new HashMap<String, String>();  
//		searchMap=new HashMap<String, String>();
//
//		EventTracker.startLocalyticsSession(context);
//
//
//
//		HashMap<String,String>user_details=session.getUserDetailsCustomer();
//		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
//		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
//
//		mFav=new FavouritePostResultReceiver(new Handler());
//		mFav.setReceiver(this);
//
//		shareServiceResult = new ShareResultReceiver(new Handler());
//		shareServiceResult.setReceiver(this);
//
//		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
//		LIKE_API=getResources().getString(R.string.like);
//
//		SHARE_URL = "/" + getResources().getString(R.string.share);
//
//		chkBox=(CheckBox)findViewById(R.id.chkBox);
//		chkBox.setOnCheckedChangeListener(this);
//		edtSearch=(EditText)findViewById(R.id.edtSearch);
//		btnSearchLoadMore=(Button)findViewById(R.id.btnSearchLoadMore);
//
//		listSearchResult=(PullToRefreshListView)findViewById(R.id.listSearchResult);
//		listNewsFeedSearch=(PullToRefreshListView)findViewById(R.id.listNewsFeedSearch);
//
//
//		categoryLayout=(RelativeLayout)findViewById(R.id.categoryLayout);
//		searchListFilter=(RelativeLayout)findViewById(R.id.searchListFilter);
//		relListResult=(RelativeLayout)findViewById(R.id.relListResult);
//		txtSearchCounter=(TextView)findViewById(R.id.txtSearchCounter);
//		//		txtSearchCounterBack=(TextView)findViewById(R.id.txtSearchCounterBack);
//		imgSearchButton=(ImageView)findViewById(R.id.imgSearchButton);
//		imgSearchClose=(ImageView)findViewById(R.id.imgSearchClose);
//		searchListProg=(ProgressBar)findViewById(R.id.searchListProg);
//
//		//Search List Categories
//
//		listCategory=(ListView)findViewById(R.id.listCategory);
//		isnetConnected=new NetworkCheck(context);
//
//		//SearchReceiver
//		searchReceiver=new SearchResultReceiver(new Handler());
//		searchReceiver.setReceiver(this);
//
//		mReceiver=new LocationResultReceiver(new Handler());
//		mReceiver.setReceiver(this);
//
//		newsFeedReceiver=new NewsFeedReceiver(new Handler());
//		newsFeedReceiver.setReceiver(this);
//
//		//Category Receiver
//		mStoreCategoryReceiver=new GetPlaceCategoryReceiver(new Handler());
//		mStoreCategoryReceiver.setReceiver(this);
//
//		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
//		NEWS_FEED_SEARCH_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
//		/*STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.dummy_search);*/
//		GET_SEARCH_URL=getResources().getString(R.string.searchapi);
//
//		//Store Category
//		STORE_CATEGORY=getResources().getString(R.string.place_category);
//
//		//Photo url
//		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);
//
//		//Api Key
//
//		API_HEADER=getResources().getString(R.string.api_header);
//		API_VALUE=getResources().getString(R.string.api_value);
//
//		actionBar=getSupportActionBar();
//		actionBar.setTitle("Categories");
//		actionBar.show();
//		actionBar.setHomeButtonEnabled(false);
//
//		sadapter=new SearchApiListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, Search_SOTRE_NAME, Search_PHOTO,Search_HAS_OFFER,Search_OFFER,Search_DESCRIPTION,Search_DISTANCE);
//		listSearchResult.setAdapter(sadapter);
//
//		adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
//				NEWS_FEED_place_total_like, NEWS_FEED_place_total_share,NEWS_FEED_MOBNO1,NEWS_FEED_MOBNO2,NEWS_FEED_MOBNO3,NEWS_FEED_TELLNO1,NEWS_FEED_TELLNO2,NEWS_FEED_TELLNO3,NEWS_OFFER_DATE_TIME);
//
//		listNewsFeedSearch.setAdapter(adapter);
//		try
//		{
//			Bundle b=getIntent().getExtras();
//			if(b!=null)
//			{
//				search_text=b.getString("search_text");
//				isGenericSearch=b.getBoolean("genericSearch",false);
//				isNewsCategory=b.getBoolean("isNewsCategory",false);
//				if(isNewsCategory)
//				{
//					listNewsFeedSearch.setVisibility(View.VISIBLE);
//					listSearchResult.setVisibility(View.GONE);
//					Log.i("VISIBILIETY", "VISIBILIETY News Vs Search isNewsCategory"+isNewsCategory+" "+listNewsFeedSearch.isShown()+" "+listSearchResult.isShown());
//				}
//				else
//				{
//					listNewsFeedSearch.setVisibility(View.GONE);
//					listSearchResult.setVisibility(View.VISIBLE);
//					Log.i("VISIBILIETY", "VISIBILIETY News Vs Search isNewsCategory"+isNewsCategory+" "+listNewsFeedSearch.isShown()+" "+listSearchResult.isShown());
//				}
//
//				if(isGenericSearch)
//				{
//					offer="1";
//					isCheckOffers=true;
//					chkBox.setChecked(true);
//					edtSearch.setText(search_text);
//					//Searching
//					if(isnetConnected.isNetworkAvailable())
//					{
//						if(edtSearch.getText().toString().length()!=0)
//						{
//							Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
//							animaSlidDown.setFillAfter(true);
//
//							//Temporarily disabled
//							searchListFilter.startAnimation(animaSlidDown);
//							categoryLayout.startAnimation(animaSlidDown);
//
//							isSearchClicked=true;
//							if(isSearchClicked==true)
//							{
//								clearSearchArray();
//							}
//							if(isNewsCategory)
//							{
//								clearArray();
//								loadNewsFeeds();
//							}
//							else
//							{
//								searchApi();
//							}
//							categoryLayout.setVisibility(View.GONE);
//							if(chkBox.isChecked())
//							{
//								chkBox.setChecked(false);
//								offer="";
//								isCheckOffers=false;
//							}
//
//							try
//							{
//								//Hiding keyboard
//								InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
//							}catch(NullPointerException npe)
//							{
//								npe.printStackTrace();
//							}catch(Exception ex)
//							{
//								ex.printStackTrace();
//							}
//							relListResult.setVisibility(View.VISIBLE);
//							animaSlidDown.setAnimationListener(new AnimationListener() {
//								public void onAnimationStart(Animation animation) {
//								}
//								public void onAnimationRepeat(Animation animation) {
//								}
//								public void onAnimationEnd(Animation animation) {
//									//Temporarily disabled
//									searchListFilter.setVisibility(View.GONE);
//									relListResult.setVisibility(View.VISIBLE);
//								}
//							});
//						}
//					}
//					else
//					{
//						Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
//					}
//
//				}
//				else
//				{
//					offer="";
//					isCheckOffers=false;
//					chkBox.setChecked(false);	
//				}
//			}
//
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//
//		try
//		{
//			value=dButil.getActiveAreaID();
//			if(value.length()!=0){
//				areaName = dButil.getAreaNameById(Integer.parseInt(value));
//			}
//			getLocationFromPrefs();
//
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//
//		category.add("Offers");
//		category_id.add("-1");
//
//
//
//		searchListFilter.setVisibility(View.VISIBLE);
//		relListResult.setVisibility(View.GONE);
//		categoryLayout.setVisibility(View.GONE);
//
//		categAdapter=new CategoryListAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher,category);
//		listCategory.setAdapter(categAdapter);
//
//		if(isnetConnected.isNetworkAvailable())
//		{
//			loadAllCategories();
//		}
//		else
//		{
//			MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
//
//		}
//
//		//NewsFeed Refresh Listener
//
//		listNewsFeedSearch.setOnRefreshListener(new OnRefreshListener<ListView>() {
//
//
//			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//
//				isRefreshing=true;
//				try
//				{
//					if(isnetConnected.isNetworkAvailable())
//					{
//						if(edtSearch.getText().toString().length()!=0)
//						{
//							search_page=0;
//							search_post_count=20;
//							loadNewsFeeds();
//						}
//					}else
//					{
//						Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
//
//					}
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
//			}
//		});
//
//		//Refresh Listener
//		listSearchResult.setOnRefreshListener(new OnRefreshListener<ListView>() {
//
//
//			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//
//				isRefreshing=true;
//				try
//				{
//					if(isnetConnected.isNetworkAvailable())
//					{
//						if(edtSearch.getText().toString().length()!=0)
//						{
//							search_page=0;
//							search_post_count=20;
//							searchApi();
//						}
//					}else
//					{
//						Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
//
//					}
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
//
//			}
//		});
//
//
//		listNewsFeedSearch.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
//
//
//			public void onLastItemVisible() {
//
//				try
//				{
//					if(isnetConnected.isNetworkAvailable())
//					{
//
//						if(NEWS_FEED_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
//						{
//							Log.i("ID SIZE","ID SIZE "+NEWS_FEED_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
//							btnSearchLoadMore.setVisibility(View.GONE);
//						}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
//						{
//							if(!searchListProg.isShown())
//							{
//
//								//								search_page++;
//								Log.i("Page", "Page "+search_page);
//								loadNewsFeeds();;
//							}
//							else
//							{
//								Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
//							}
//
//						}
//						else
//						{
//							Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
//							btnSearchLoadMore.setVisibility(View.GONE);
//						}
//					}
//					else
//					{
//						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
//
//					}
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
//			}
//		});
//		//On Last item visible
//
//		listSearchResult.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
//
//
//			public void onLastItemVisible() {
//
//				try
//				{
//					if(isnetConnected.isNetworkAvailable())
//					{
//
//						if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
//						{
//							Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
//							btnSearchLoadMore.setVisibility(View.GONE);
//						}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
//						{
//							if(!searchListProg.isShown())
//							{
//								Log.i("Page", "Page "+search_page);
//								if(latitued.equalsIgnoreCase("not found") && longitude.equalsIgnoreCase("not found"))
//								{
//									locationService();
//								}
//								else
//								{
//									searchApi();
//								}
//							}
//							else
//							{
//								Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
//							}
//
//						}
//						else
//						{
//							Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
//							btnSearchLoadMore.setVisibility(View.GONE);
//						}
//					}
//					else
//					{
//						Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
//
//					}
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
//			}
//		});
//
//		btnSearchLoadMore.setOnClickListener(new OnClickListener() {
//
//
//			public void onClick(View v) {
//
//				if(isnetConnected.isNetworkAvailable())
//				{
//
//					if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
//					{
//						Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
//						btnSearchLoadMore.setVisibility(View.GONE);
//					}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
//					{
//						if(!searchListProg.isShown())
//						{
//							search_page++;
//							Log.i("Page", "Page "+search_page);
//							searchApi();
//						}
//						else
//						{
//							Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
//						}
//
//					}
//					else
//					{
//						Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
//						btnSearchLoadMore.setVisibility(View.GONE);
//					}
//				}
//
//			}
//		});
//
//		//imgSearchButton click 
//		imgSearchButton.setOnClickListener(new OnClickListener() {
//
//
//			public void onClick(View v) {
//
//
//				//Animating EditText
//				Animation animation = new ScaleAnimation(0, 1, 1,1, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 1);
//				animation.setDuration(250);
//				edtSearch.setVisibility(View.VISIBLE);
//				edtSearch.startAnimation(animation);
//				animation.setAnimationListener(new AnimationListener() {
//
//
//					public void onAnimationStart(Animation animation) {
//						//Search Layout
//						Animation animaSlidUp=AnimationUtils.loadAnimation(context, R.anim.fade_in);
//						//Temporarily disabled 
//						searchListFilter.startAnimation(animaSlidUp);
//					}
//
//
//					public void onAnimationRepeat(Animation animation) {
//					}
//					public void onAnimationEnd(Animation animation) {
//						//Temporarily disabled 
//						searchListFilter.setVisibility(View.VISIBLE);
//					}
//				});
//			}
//		});
//
//		//Add text change listener
//		edtSearch.addTextChangedListener(new TextWatcher() {
//
//
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//				categoryLayout.setVisibility(View.VISIBLE);
//			}
//
//
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//
//
//			}
//
//
//			public void afterTextChanged(Editable s) {
//
//				if(s.toString().length()==0)
//				{
//					//Temporarily disabled
//					if(!searchListFilter.isShown())
//					{
//						Animation animaSlidUp=AnimationUtils.loadAnimation(context, R.anim.fade_in);
//						searchListFilter.startAnimation(animaSlidUp);
//						searchListFilter.setVisibility(View.VISIBLE);
//						relListResult.setVisibility(View.GONE);
//
//						//added 
//						clearSearchArray();
//
//					}
//					Animation animaSlidOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);
//					categoryLayout.startAnimation(animaSlidOut);
//					categoryLayout.setVisibility(View.GONE);
//				}
//			}
//		});
//		//Searching In API.
//		edtSearch.setOnEditorActionListener(new OnEditorActionListener() {
//
//
//			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//
//				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//					if(isnetConnected.isNetworkAvailable())
//					{
//						if(edtSearch.getText().toString().length()!=0)
//						{
//
//							/*		animaSlidUp.setFillAfter(true);*/
//							Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
//							animaSlidDown.setFillAfter(true);
//
//							//Temporarily disabled
//							searchListFilter.startAnimation(animaSlidDown);
//
//							categoryLayout.startAnimation(animaSlidDown);
//
//
//							isSearchClicked=true;
//							if(isSearchClicked==true)
//							{
//								clearSearchArray();
//							}
//
//							//							if(IS_SHOPLOCAL)
//							//							{
//							if(latitued.length()==0 && longitude.length()==0)
//							{
//								locationService();
//							}
//							else
//							{
//								if(isNewsCategory)
//								{
//									clearArray();
//									loadNewsFeeds();
//								}
//								else
//								{
//									searchApi();
//								}
//							}
//							categoryLayout.setVisibility(View.GONE);
//							if(chkBox.isChecked())
//							{
//								chkBox.setChecked(false);
//								offer="";
//								isCheckOffers=false;
//							}
//
//
//							try
//							{
//								//Hiding keyboard
//								InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
//							}catch(NullPointerException npe)
//							{
//								npe.printStackTrace();
//							}catch(Exception ex)
//							{
//								ex.printStackTrace();
//							}
//
//							relListResult.setVisibility(View.VISIBLE);
//
//							animaSlidDown.setAnimationListener(new AnimationListener() {
//
//
//								public void onAnimationStart(Animation animation) {
//								}
//								public void onAnimationRepeat(Animation animation) {
//								}
//								public void onAnimationEnd(Animation animation) {
//									//Temporarily disabled
//									searchListFilter.setVisibility(View.GONE);
//									relListResult.setVisibility(View.VISIBLE);
//								}
//							});
//						}
//					}
//					else
//					{
//						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
//
//					}
//
//					return true;
//				}
//				return false;
//			}
//		});
//
//		imgSearchClose.setOnClickListener(new OnClickListener() {
//
//
//			public void onClick(View v) {
//
//
//				//Animating Edit text
//				Animation animation = new ScaleAnimation(1, 0, 1,1, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 1);
//				animation.setDuration(250);
//				final Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
//				animaSlidDown.setFillAfter(true);
//
//				final Animation animFadeOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);
//
//				final Animation animFadeIn=AnimationUtils.loadAnimation(context, R.anim.fade_in);
//
//				edtSearch.startAnimation(animation);
//				animation.setAnimationListener(new AnimationListener() {
//					public void onAnimationStart(Animation animation) {
//						//Temporarily disabled
//						searchListFilter.startAnimation(animaSlidDown);
//					}
//					public void onAnimationRepeat(Animation animation) {
//					}
//					public void onAnimationEnd(Animation animation) {
//						imgSearchClose.setVisibility(View.GONE);
//						edtSearch.setVisibility(View.GONE);
//						searchListFilter.setVisibility(View.GONE);
//						relListResult.setVisibility(View.GONE);
//						imgSearchButton.setVisibility(View.VISIBLE);
//						try
//						{
//							//Hiding keyboard
//							InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//							imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
//						}catch(NullPointerException npe)
//						{
//							npe.printStackTrace();
//						}catch(Exception ex)
//						{
//							ex.printStackTrace();
//						}
//					}
//				});
//				animFadeOut.setAnimationListener(new AnimationListener() {
//					public void onAnimationStart(Animation animation) {
//					}
//					public void onAnimationRepeat(Animation animation) {
//					}
//					public void onAnimationEnd(Animation animation) {
//					}
//				});
//				clearSearchArray();
//			}
//		});
//
//
//		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
//		intentShareActivity.setType("text/plain");
//		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
//
//
//		final PackageManager pm = getPackageManager();
//		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
//		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
//				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
//		/** Anirudh facebook */
//		if(packageNames.size() == 0){
//			appName.add("Facebook");
//			packageNames.add("com.facebook");
//			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
//			for (ResolveInfo rInfo : list) {
//				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
//				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
//
//				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
//					isFacebookPresent = true;
//				}
//
//				if(app.equalsIgnoreCase("facebook") == false){
//					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
//					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
//					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
//				}
//
//			}
//			if(!isFacebookPresent)
//			{
//				appName.remove(0);
//				packageNames.remove(0);
//				appIcon.remove(0);
//			}
//
//
//		}
//
//		/** Anirudh- Custom share dialog */
//		dialogShare = new Dialog(SearchActivity.this);
//		dialogShare.setContentView(R.layout.sharedialog);
//		dialogShare.setTitle("Select an action");
//		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
//		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));
//	}//onCreate Ends Here
//
//
//
//
//	void clearSearchArray()
//	{
//		Search_ID.clear();
//		Search_SOTRE_NAME.clear();
//		Search_BUILDING.clear();
//		Search_STREET.clear();
//		Search_LANDMARK.clear();
//		Search_AREA.clear();
//		Search_CITY.clear();
//		Search_MOBILE_NO.clear();
//		Search_PHOTO.clear();
//		Search_DISTANCE.clear();
//		Search_DESCRIPTION.clear();
//		Search_HAS_OFFER.clear();
//		Search_OFFER.clear();
//		search_page=0;
//		search_post_count=20;
//
//	}
//
//
//	//call search api
//	void searchApi()
//	{
//
//		/** anirudh search text */
//		Log.i("STORE SEARCH", "OFFERS SEARCH  " +edtSearch.getText().toString());	
//		searchMap.put("Category_Name","All");
//		searchMap.put("Search_Text",edtSearch.getText().toString());
//		EventTracker.logEvent("Search_StoreCategory", searchMap);
//		Intent intent=new Intent(context, SearchService.class);
//		intent.putExtra("searchapi",searchReceiver);
//		intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
//		intent.putExtra("api_header",API_HEADER);
//		intent.putExtra("api_header_value", API_VALUE);
//		intent.putExtra("search_post_count",search_post_count);
//		intent.putExtra("search_page",search_page+1);
//		intent.putExtra("search", edtSearch.getText().toString());
//		if(!isLocationFound)
//		{
//			latitued="0";
//			longitude="0";
//			intent.putExtra("isCheckLocation", false);
//		}
//		else
//		{
//			intent.putExtra("isCheckLocation", true);
//		}
//		intent.putExtra("latitude", latitued);
//		intent.putExtra("longitude", longitude);
//
//		intent.putExtra("locality", locality);
//		intent.putExtra("offer", offer);
//		intent.putExtra("isCheckOffers", isCheckOffers);
//		intent.putExtra("key", key);
//		intent.putExtra("value", value);
//		context.startService(intent);
//		searchListProg.setVisibility(View.VISIBLE);
//	}
//
//	void loadNewsFeeds()
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			/** Anirudh search text */
//			Log.i("OFFERS SEARCH", "OFFERS SEARCH  " +edtSearch.getText().toString());
//			searchMap.put("Search_OffersCategory","All");
//			searchMap.put("Search_Text",edtSearch.getText().toString());
//			EventTracker.logEvent("Search_OffersCategory", searchMap);
//			Intent intent=new Intent(context, NewsFeedService.class);
//			intent.putExtra("newsFeed",newsFeedReceiver);
//			intent.putExtra("URL", NEWS_FEED_SEARCH_URL+"/"+GET_SEARCH_URL);
//			intent.putExtra("api_header",API_HEADER);
//			intent.putExtra("api_header_value", API_VALUE);
//			intent.putExtra("count",search_post_count);
//			intent.putExtra("page",search_page+1);
//			intent.putExtra("area_id",value);
//			intent.putExtra("offer", offer);
//			intent.putExtra("isSearch", true);
//			intent.putExtra("search", edtSearch.getText().toString());
//			intent.putExtra("isCheckOffers", isCheckOffers);
//			context.startService(intent);
//			searchListProg.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			showToast(getResources().getString(R.string.noInternetConnection));
//		}
//	}
//
//	void locationService()
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			//calling location service.
//			Intent intent=new Intent(context, LocationIntentService.class);
//			intent.putExtra("receiverTag", mReceiver);
//			startService(intent);
//			searchListProg.setVisibility(View.VISIBLE);
//		}else
//		{
//			MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
//
//		}
//	}
//
//
//	/*
//	 * Load All Categories
//	 */
//	void loadAllCategories()
//	{
//		Intent intent=new Intent(context,GetPlaceCategoryService .class);
//		intent.putExtra("category",mStoreCategoryReceiver);
//		intent.putExtra("URL", STORE_URL+STORE_CATEGORY);
//		intent.putExtra("api_header",API_HEADER);
//		intent.putExtra("api_header_value", API_VALUE);
//		intent.putExtra("IsAppend", false);
//		intent.putExtra("value", "shoplocal");
//		context.startService(intent);
//		searchListProg.setVisibility(View.VISIBLE);
//	}
//
//
//	public void onReciverSearchResult(int resultCode, Bundle resultData) {
//		//adding facebook analytics for search in Offer Stream
//		AppEventsLogger logger = AppEventsLogger.newLogger(this);
//
//		Bundle parameters = new Bundle();
//		parameters.putString("Area", areaName);
//		parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Store");
//		parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, edtSearch.getText().toString());
//
//
//		try
//		{
//			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");
//			searchListProg.setVisibility(View.GONE);
//			if(SEARCH_STATUS.equalsIgnoreCase("true"))
//			{
//				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "TRUE");
//				final ArrayList<String> TempID=resultData.getStringArrayList("ID");
//				final ArrayList<String> TempSOTRE_NAME=resultData.getStringArrayList("SOTRE_NAME");
//				ArrayList<String> TempBUILDING=resultData.getStringArrayList("BUILDING");
//				ArrayList<String> TempSTREET=resultData.getStringArrayList("STREET");
//				ArrayList<String> TempLANDMARK=resultData.getStringArrayList("LANDMARK");
//				final ArrayList<String> TempAREA=resultData.getStringArrayList("AREA");
//				final ArrayList<String> TempCITY=resultData.getStringArrayList("CITY");
//				final ArrayList<String> TempMOBILE_NO=resultData.getStringArrayList("MOBILE_NO");
//				final ArrayList<String> TempPHOTO=resultData.getStringArrayList("PHOTO");
//				ArrayList<String> TempDISTANCE=resultData.getStringArrayList("DISTANCE");
//				ArrayList<String> TempDescription=resultData.getStringArrayList("DESCRIPTION");
//				ArrayList<String> TempHasOffers=new ArrayList<String>();
//				ArrayList<String> TempOffers=new ArrayList<String>();
//
//
//				TempHasOffers.clear();
//				TempOffers.clear();
//
//				TempHasOffers=resultData.getStringArrayList("HAS_OFFERS");
//				TempOffers=resultData.getStringArrayList("OFFERS");
//
//				TOTAL_SEARCH_PAGES=resultData.getString("TOTAL_PAGES");
//				TOTAL_SEARCH_RECORDS=resultData.getString("TOTAL_RECORDS");
//
//				if(isRefreshing)
//				{
//					clearSearchArray();
//					isRefreshing=false;
//				}
//
//				for(int index=0;index<TempID.size();index++)
//				{
//					Search_ID.add(TempID.get(index));
//					Search_SOTRE_NAME.add(TempSOTRE_NAME.get(index));
//					Search_BUILDING.add(TempBUILDING.get(index));
//					Search_STREET.add(TempSTREET.get(index));
//					Search_LANDMARK.add(TempLANDMARK.get(index));
//					Search_AREA.add(TempAREA.get(index));
//					Search_CITY.add(TempCITY.get(index));
//					Search_MOBILE_NO.add(TempMOBILE_NO.get(index));
//					Search_PHOTO.add(TempPHOTO.get(index));
//					Search_DISTANCE.add(TempDISTANCE.get(index));
//					Search_DESCRIPTION.add(TempDescription.get(index));
//					Search_HAS_OFFER.add(TempHasOffers.get(index));
//					Search_OFFER.add(TempOffers.get(index));
//				}
//
//				search_page++;
//				//Get the first visible position of listview
//				sadapter.notifyDataSetChanged();
//
//				txtSearchCounter.setText(Search_ID.size()+"/"+TOTAL_SEARCH_RECORDS);
//				//Hide button if Total records are equal to Records fetched.
//				if(Integer.parseInt(TOTAL_SEARCH_RECORDS)==Search_ID.size())
//				{
//					btnSearchLoadMore.setVisibility(View.GONE);
//				}
//
//				listSearchResult.onRefreshComplete();
//				listSearchResult.setOnItemClickListener(new OnItemClickListener() {
//					public void onItemClick(AdapterView<?> arg0, View arg1,
//							int position, long arg3) {
//						if(!isRefreshing)
//						{
//							Intent intent=new Intent(context, StoreDetailsActivity.class);
//							intent.putExtra("STORE_ID", Search_ID.get(position-1));
//							intent.putExtra("STORE", Search_SOTRE_NAME.get(position-1));
//							intent.putExtra("PHOTO", Search_PHOTO.get(position-1));
//							intent.putExtra("AREA", Search_AREA.get(position-1));
//							intent.putExtra("CITY", Search_CITY.get(position-1));
//							intent.putExtra("MOBILE_NO", Search_MOBILE_NO.get(position-1));
//							startActivity(intent);
//							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//						}
//					}
//				});
//			}
//
//			if(!SEARCH_STATUS.equalsIgnoreCase("true"))
//			{
//				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "FALSE");
//
//				String message="";
//				message=resultData.getString("SEARCH_MESSAGE");
//				showToast(message);
//				search_page--;
//				searchListProg.setVisibility(View.GONE);
//				//				listSearchResult.onRefreshComplete();
//				if(search_page<0 || search_page==0)
//				{
//					search_page=1;
//					clearArray();
//				}
//
//			}
//			logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED,parameters);
//
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//			listSearchResult.onRefreshComplete();
//		}
//
//	}
//
//	//CategoryList Adapter
//	 class CategoryListAdapter extends ArrayAdapter<String>
//	{
//		ArrayList<String> category_name;
//		Activity context;
//		LayoutInflater inflate;
//		Typeface tf;
//		Animation anim_fromRight,anim_fromLeft;
//		String isFav="";
//
//		public CategoryListAdapter(Activity context, int resource,
//				int textViewResourceId, ArrayList<String> category_name) {
//			super(context, resource, textViewResourceId, category_name);
//
//			this.context=context;
//			this.category_name=category_name;
//
//			inflate=context.getLayoutInflater();
//
//			//getting font from asset directory.
//			tf=Typeface.createFromAsset(context.getAssets(), "fonts/DinDisplayProLight.otf");
//
//			anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
//			anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
//
//		}
//
//		public int getCount() {
//
//			return category_name.size();
//		}
//
//		public String getItem(int position) {
//
//			return category_name.get(position);
//		}
//
//		public View getView(final int position, View convertView, ViewGroup parent) {
//
//
//			//inflate a view and build ui for list.
//			if(convertView==null)
//			{
//				ViewHolder holder=new ViewHolder();
//				convertView=inflate.inflate(R.layout.categorylistlayout,null);
//				holder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);
//				holder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
//				holder.categoryLayout=(RelativeLayout)convertView.findViewById(R.id.categoryLayout);
//				convertView.setTag(holder);
//
//			}
//			final ViewHolder hold=(ViewHolder)convertView.getTag();
//			hold.txtCategory.setText(category_name.get(position));
//			hold.chkBox.setVisibility(View.GONE);
//
//			return convertView;
//		}
//
//		 class ViewHolder
//		{
//			ImageView imgStoreLogo;
//			TextView txtCategory;
//			CheckBox chkBox;
//			RelativeLayout categoryLayout;
//		}
//
//	}
//
//	//Search Api Adapter
//
//	class SearchApiListAdapter extends ArrayAdapter<String>
//	{
//		ArrayList<String> store_name,distance,logo,has_offer,offers;
//		ArrayList<String>Search_DESCRIPTION;
//		Activity context;
//		LayoutInflater inflate;
//		String photo;
//		Typeface tf;
//
//		public SearchApiListAdapter(Activity context, int resource,
//				int textViewResourceId, ArrayList<String> store_name,ArrayList<String>logo,ArrayList<String>has_offers,ArrayList<String>offers,ArrayList<String>Search_DESCRIPTION,ArrayList<String>distance) {
//			super(context, resource, textViewResourceId, store_name);
//
//			this.context=context;
//			this.store_name=store_name;
//			this.distance=distance;
//			this.logo=logo;
//			this.Search_DESCRIPTION=Search_DESCRIPTION;
//
//			this.offers=offers;
//			this.has_offer=has_offers;
//			inflate=context.getLayoutInflater();
//			//getting font from asset directory.
//			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
//
//		}
//
//		public int getCount() {
//			return store_name.size();
//		}
//		public String getItem(int position) {
//			return store_name.get(position);
//		}
//
//		public View getView(int position, View convertView, ViewGroup parent) {
//			if(convertView==null)
//			{
//				ViewHolder holder=new ViewHolder();
//				convertView=inflate.inflate(R.layout.layout_search_api,null);
//				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
//				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
//				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
//				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
//				holder.band=(TextView)convertView.findViewById(R.id.band);
//				holder.searchIsOffer=(ImageView)convertView.findViewById(R.id.searchIsOffer);
//				convertView.setTag(holder);
//
//			}
//			ViewHolder hold=(ViewHolder)convertView.getTag();
//
//			if(logo.get(position).toString().length()!=0)
//			{
//				photo=logo.get(position).replaceAll(" ", "%20");
//				try{
//					Picasso.with(context).load(PHOTO_PARENT_URL+photo)
//					.placeholder(R.drawable.ic_place_holder)
//					.error(R.drawable.ic_place_holder)
//					.into(hold.imgStoreLogo);
//				}
//				catch(IllegalArgumentException illegalArg){
//					illegalArg.printStackTrace();
//				}
//				catch(Exception e){ 
//					e.printStackTrace();
//				}
//			}
//			else
//			{
//				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
//			}
//			hold.txtStore.setText(store_name.get(position));
//			hold.txtSearchStoreDesc.setText(Search_DESCRIPTION.get(position));
//			hold.txtStore.setTypeface(tf, Typeface.BOLD);
//			hold.txtSearchStoreDesc.setTypeface(tf);
//			hold.txtDistance.setTypeface(tf);
//			hold.txtDistance.setVisibility(View.VISIBLE);
//
//
//			if(distance.get(position)!=null)
//			{
//				if(Double.parseDouble(distance.get(position))!=-1)
//				{
//					if(Double.parseDouble(distance.get(position))<1 && Double.parseDouble(distance.get(position))>0)
//					{
//						DecimalFormat format = new DecimalFormat("##.##");
//						String formatted = format.format(Double.parseDouble(distance.get(position))*100);
//						hold.txtDistance.setText(""+formatted+" m.");
//					}
//					else
//					{
//						DecimalFormat format = new DecimalFormat("##.##");
//						String formatted = format.format(Double.parseDouble(distance.get(position)));
//						if((Double.parseDouble(distance.get(position))*100)>8000)
//						{
//							hold.txtDistance.setVisibility(View.GONE);
//						}
//						else
//						{
//							hold.txtDistance.setVisibility(View.VISIBLE);
//							hold.txtDistance.setText(""+formatted+" km.");
//						}
//					}
//
//				}
//			}
//
//			hold.searchIsOffer.setVisibility(View.VISIBLE);
//			if(has_offer.get(position).toString().length()!=0 && has_offer.get(position).equalsIgnoreCase("1"))
//			{
//				hold.searchIsOffer.setVisibility(View.VISIBLE);
//			}
//			else
//			{
//				hold.searchIsOffer.setVisibility(View.GONE);
//			}
//
//			return convertView;
//		}
//
//		class ViewHolder
//		{
//			ImageView imgStoreLogo,searchIsOffer;;
//			TextView txtStore;
//			TextView txtSearchStoreDesc;
//			TextView txtDistance;
//			TextView band;
//		}
//
//
//	}
//
//
//	/*
//	 * (non-Javadoc)
//	 * @see com.phonethics.networkcall.GetPlaceCategoryReceiver.GetCategory#onReceiveStoreCategories(int, android.os.Bundle)
//	 */
//
//	public void onReceiveStoreCategories(int resultCode, Bundle resultData) {
//
//		try
//		{
//			String category_Status=resultData.getString("category_status");
//			ArrayList<String>CATEGORY_NAME=new ArrayList<String>();
//			ArrayList<String>CATEGORY_ID=new ArrayList<String>();
//
//			if(category_Status.equalsIgnoreCase("true"))
//			{
//				CATEGORY_NAME=resultData.getStringArrayList("CATEGORY_NAME");
//				CATEGORY_ID=resultData.getStringArrayList("CATEGORY_ID");
//				for(int i=0;i<CATEGORY_NAME.size();i++)
//				{
//					Log.i("CATEGORY_NAME", "CATEGORY_ID : "+CATEGORY_ID.get(i)+" CATEGORY_NAME "+CATEGORY_NAME.get(i));
//				}
//
//				category.clear();
//				category_id.clear();
//
//				category.add("Offers");
//				category_id.add("-1");
//				for(int i=0;i<CATEGORY_NAME.size();i++)
//				{
//					category.add(CATEGORY_NAME.get(i));
//					category_id.add(CATEGORY_ID.get(i));
//				}
//				categAdapter=new CategoryListAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher,category);
//				listCategory.setAdapter(categAdapter);
//				categAdapter.notifyDataSetChanged();
//				listCategory.setOnItemClickListener(new OnItemClickListener() {
//					public void onItemClick(AdapterView<?> arg0, View arg1,
//							int position, long arg3) {
//
//						if(!isNewsCategory)
//						{
//							/** Anirudh event tracker to be changed */
//							eventMap.put("Category_Name", category.get(position));
//							EventTracker.logEvent("Search_StoreCategory", eventMap);
//
//							if(position==0)
//							{
//								Intent intent=new Intent(context,CategorySearch.class);
//								intent.putExtra("category_id", "");
//								intent.putExtra("offer", "1");
//								intent.putExtra("isCategory", false);
//								startActivity(intent);
//							}
//							else if(!category_id.get(position).toString().equalsIgnoreCase("-1"))
//							{
//								Intent intent=new Intent(context,CategorySearch.class);
//								intent.putExtra("category_id", category_id.get(position));
//								Log.i("Categ", "Categ : "+category_id.get(position));
//								intent.putExtra("offer", "");
//								intent.putExtra("isCategory", true);
//								startActivity(intent);
//							}
//						}
//						else
//						{
//							eventMap.put("Category_Name", category.get(position));
//							EventTracker.logEvent("Search_OffersCategory", eventMap);
//							if(position==0)
//							{
//								Intent intent=new Intent(context,NewsFeedCategory.class);
//								intent.putExtra("category_id", "");
//								intent.putExtra("offer", "1");
//								intent.putExtra("isCategory", false);
//								startActivity(intent);
//							}
//							else if(!category_id.get(position).toString().equalsIgnoreCase("-1"))
//							{
//								Intent intent=new Intent(context,NewsFeedCategory.class);
//								intent.putExtra("category_id", category_id.get(position));
//								Log.i("Categ", "Categ : "+category_id.get(position));
//								intent.putExtra("offer", "");
//								intent.putExtra("isCategory", true);
//								startActivity(intent);
//							}
//						}
//					}
//				});
//
//			}
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//
//	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//		if(buttonView.isChecked())
//		{
//			offer="1";
//			isCheckOffers=true;
//		}
//		else
//		{
//			offer="";
//			isCheckOffers=false;
//		}
//	}
//
//
//	public void onReceiveResult(int resultCode, Bundle resultData) {
//		searchListProg.setVisibility(View.GONE);
//		String status=resultData.getString("status");
//		if(status.equalsIgnoreCase("set"))
//		{
//			try
//			{
//				latitued=resultData.getString("Lat");
//				longitude=resultData.getString("Longi");
//				if(latitued!=null || latitued.length()>0 && longitude!=null || longitude.length()>0)
//				{
//					Log.i("LAT", "LATI "+latitued);
//					Log.i("LONG", "LATI "+longitude);
//					searchApi();
//				}
//				Log.i("Location ", "LOCATION : "+latitued+" , "+longitude);
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//		}
//		else
//		{
//			showToast(getResources().getString(R.string.locationAlert));
//		}
//	}
//
//	void showToast(String text)
//	{
//		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
//	}
//
//	void getLocationFromPrefs()
//	{
//		SharedPreferences pref=context.getSharedPreferences("location",0);
//		latitued=pref.getString("latitued", "not found");
//		longitude=pref.getString("longitude", "not found");
//		isLocationFound=pref.getBoolean("isLocationFound",false);
//		Log.i("Location found","Location found "+isLocationFound+" "+latitued+","+longitude);
//	}
//	public boolean onCreateOptionsMenu(Menu menu) {
//		try
//		{
//			MenuItem extra=menu.add("close").setIcon(R.drawable.abs__ic_clear_disabled);
//			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//		return true;
//	}
//	public boolean onOptionsItemSelected(MenuItem item) {
//		if(item.getTitle().toString().equalsIgnoreCase("close"))
//		{
//			finish();
//			overridePendingTransition(0,R.anim.shrink_fade_out_center);
//		}
//		return true;
//	}
//
//	public void onBackPressed() {
//		finish();
//		overridePendingTransition(0,R.anim.shrink_fade_out_center);
//	}
//
//	void login()
//	{
//		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
//		alertDialogBuilder3.setTitle("Shoplocal");
//		alertDialogBuilder3
//		.setMessage(getResources().getString(R.string.loginDrawerMessage))
//		.setIcon(R.drawable.ic_launcher)
//		.setCancelable(false)
//		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog,int id) {
//				Intent intent=new Intent(context,LoginSignUpCustomer.class);
//				startActivityForResult(intent, 2);
//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//			}
//		})
//		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog,int id) {
//				dialog.dismiss();
//			}
//		})
//		;
//		AlertDialog alertDialog3 = alertDialogBuilder3.create();
//		alertDialog3.show();
//	}
//
//
//
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		if(requestCode==6)
//		{
//			VIA="Android";
//			callShareApi(TEMP_POST_ID);
//		}
//
//		try
//		{
//			uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
//
//				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
//					Log.e("Activity", String.format("Error: %s", error.toString()));
//				}
//
//				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
//					Log.i("Activity", "Success!");
//					VIA="Android-Facebook";
//					callShareApi(TEMP_POST_ID);
//				}
//			});
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//	public void onNewsFeedResult(int resultCode, Bundle resultData) {
//
//		searchListProg.setVisibility(View.GONE);
//		try
//		{
//
//			String search_status=resultData.getString("status");
//			String message=resultData.getString("message");
//			ArrayList<NewsFeedModel> newsFeed=resultData.getParcelableArrayList("posts");
//
//			listNewsFeedSearch.setVisibility(View.VISIBLE);
//			AppEventsLogger logger = AppEventsLogger.newLogger(this);
//
//			Bundle parameters = new Bundle();
//			parameters.putString("Area", areaName);
//			parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Offers");
//			parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, edtSearch.getText().toString());
//
//			if(search_status.equalsIgnoreCase("true"))
//			{
//
//				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "TRUE");
//				TOTAL_SEARCH_PAGES=resultData.getString("total_page");
//				TOTAL_SEARCH_RECORDS=resultData.getString("total_record");
//				Log.i("News FEED SIZE ", "News FEED SIZE "+newsFeed.size());
//				try
//				{
//					if(isRefreshing)
//					{
//						clearArray();
//						isRefreshing=false;
//					}
//
//					ArrayList<String> TEMP_NEWS_FEED_ID=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_NAME=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_AREA_ID=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_FEED_MOBNO1=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_MOBNO2=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_MOBNO3=new ArrayList<String>();
//
//
//					ArrayList<String> TEMP_NEWS_FEED_TELLNO1=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_TELLNO2=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_TELLNO3=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_FEED_LATITUDE=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_LONGITUDE=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_DISTANCE=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_FEED_POST_ID=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_POST_TYPE=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_FEED_POST_TITLE=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_DESCRIPTION=new ArrayList<String>();
//
//
//					ArrayList<String> TEMP_NEWS_FEED_image_url1=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_image_url2=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_image_url3=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_FEED_thumb_url1=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_thumb_url2=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_thumb_url3=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_DATE=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_IS_OFFERED=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_OFFER_DATE_TIME=new ArrayList<String>();
//
//					ArrayList<String> TEMP_NEWS_FEED_place_total_like=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_place_total_share=new ArrayList<String>();
//					ArrayList<String> TEMP_NEWS_FEED_place_verified=new ArrayList<String>();
//
//
//					TEMP_NEWS_FEED_ID.clear();
//					TEMP_NEWS_FEED_NAME.clear();
//					TEMP_NEWS_FEED_AREA_ID.clear();
//
//					TEMP_NEWS_FEED_MOBNO1.clear();
//					TEMP_NEWS_FEED_MOBNO2.clear();
//					TEMP_NEWS_FEED_MOBNO3.clear();
//
//
//					TEMP_NEWS_FEED_TELLNO1.clear();
//					TEMP_NEWS_FEED_TELLNO2.clear();
//					TEMP_NEWS_FEED_TELLNO3.clear();
//
//					TEMP_NEWS_FEED_LATITUDE.clear();
//					TEMP_NEWS_FEED_LONGITUDE.clear();
//					TEMP_NEWS_FEED_DISTANCE.clear();
//
//					TEMP_NEWS_FEED_POST_ID.clear();
//					TEMP_NEWS_FEED_POST_TYPE.clear();
//
//					TEMP_NEWS_FEED_POST_TITLE.clear();
//					TEMP_NEWS_FEED_DESCRIPTION.clear();
//
//
//					TEMP_NEWS_FEED_image_url1.clear();
//					TEMP_NEWS_FEED_image_url2.clear();
//					TEMP_NEWS_FEED_image_url3.clear();
//
//					TEMP_NEWS_FEED_thumb_url1.clear();
//					TEMP_NEWS_FEED_thumb_url2.clear();
//					TEMP_NEWS_FEED_thumb_url3.clear();
//
//					TEMP_NEWS_DATE.clear();
//					TEMP_NEWS_IS_OFFERED.clear();
//					TEMP_NEWS_OFFER_DATE_TIME.clear();
//
//					TEMP_NEWS_FEED_place_total_like.clear();
//					TEMP_NEWS_FEED_place_total_share.clear();
//
//					TEMP_NEWS_FEED_place_verified.clear();
//
//
//					for(int i=0;i<newsFeed.size();i++)
//					{
//						TEMP_NEWS_FEED_ID.add(newsFeed.get(i).getId());
//						TEMP_NEWS_FEED_NAME.add(newsFeed.get(i).getName());
//						TEMP_NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());
//
//						TEMP_NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
//						TEMP_NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
//						TEMP_NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());
//
//						TEMP_NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
//						TEMP_NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
//						TEMP_NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());
//
//						TEMP_NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
//						TEMP_NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
//						TEMP_NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());
//
//						TEMP_NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
//						TEMP_NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
//						TEMP_NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());
//
//						TEMP_NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());
//
//						TEMP_NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
//						TEMP_NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
//						TEMP_NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());
//
//						TEMP_NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
//						TEMP_NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
//						TEMP_NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());
//
//						TEMP_NEWS_DATE.add(newsFeed.get(i).getDate());
//						TEMP_NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());
//
//
//						TEMP_NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
//						TEMP_NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
//						TEMP_NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());
//
//						TEMP_NEWS_FEED_place_verified.add(newsFeed.get(i).getVerified());
//					}
//
//					for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
//					{
//						NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
//						NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
//						NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );
//
//						NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
//						NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
//						NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));
//
//						NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
//						NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
//						NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));
//
//						NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
//						NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
//						NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));
//
//						NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
//						NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
//						NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));
//
//						NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));
//
//						NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
//						NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
//						NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));
//
//						NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
//						NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
//						NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));
//
//						NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
//						NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));
//
//
//						NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
//						NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
//						NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));
//					}
//					adapter.notifyDataSetChanged();
//					search_page++;
//
//					Log.i("ADAPTER COUNT", "ADAPTER COUNT "+adapter.getCount());
//					Log.i("TOTAL PAGES AND RECORDS  ","TOTAL CURRENT PAGE, PAGES AND RECORDS : PAGE "+search_page +" TOTAL PAGES "+TOTAL_SEARCH_PAGES+" TOTAL_RECORD "+TOTAL_SEARCH_RECORDS);
//
//					txtSearchCounter.setText(NEWS_FEED_ID.size()+"/"+TOTAL_SEARCH_RECORDS);
//					listNewsFeedSearch.onRefreshComplete();
//					listNewsFeedSearch.setOnItemClickListener(new OnItemClickListener() {
//
//						public void onItemClick(AdapterView<?> arg0, View arg1,
//								int position, long arg3) {
//							if(!isRefreshing)
//							{
//								Intent intent=new Intent(context, OffersBroadCastDetails.class);
//								intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
//								intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
//								intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
//								intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
//								intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
//								intent.putExtra("fromCustomer", true);
//								intent.putExtra("isFromStoreDetails", false);
//								intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
//								intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
//								intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
//								intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
//								intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
//								intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
//								intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
//								intent.putExtra("USER_LIKE","-1");
//
//								context.startActivityForResult(intent, 5);
//								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//							}
//						}
//					});
//
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//
//				}
//
//			}
//			else
//			{
//				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "FALSE");
//				showToast(message);
//				listNewsFeedSearch.setVisibility(View.GONE);
//				listNewsFeedSearch.onRefreshComplete();
//				txtSearchCounter.setText("");
//			}
//			logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED,parameters);
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//			listNewsFeedSearch.setVisibility(View.GONE);
//			listNewsFeedSearch.onRefreshComplete();
//			txtSearchCounter.setText("");
//		}
//	}
//
//	// Newsfeed Adapter
//	class NewsFeedAdapter extends ArrayAdapter<String>
//	{
//
//		Activity context;
//
//		ArrayList<String> post_id;
//		ArrayList<String> news_title;
//		ArrayList<String> offer_title;
//		ArrayList<String> offer_desc;
//
//		ArrayList<String> offer_date;
//		ArrayList<String> is_offer;
//
//		ArrayList<String> photo_url1;
//		ArrayList<String> photo_url2;
//		ArrayList<String> photo_url3;
//
//
//
//		ArrayList<String>mob1;
//		ArrayList<String>mob2;
//		ArrayList<String>mob3;
//
//		ArrayList<String>tel1;
//		ArrayList<String>tel2;
//		ArrayList<String>tel3;
//
//		ArrayList<String>valid_till;
//
//
//
//		ArrayList<String> offer_total_like;
//		ArrayList<String> offer_total_total_share;
//		boolean [] isAnimEnd;   
//		LayoutInflater inflator;
//		Typeface tf;
//		
//		public NewsFeedAdapter(Activity context, int resource,
//				int textViewResourceId,  ArrayList<String>post_id,ArrayList<String>news_title,ArrayList<String> offer_title,
//				ArrayList<String> offer_desc,ArrayList<String> offer_date,
//				ArrayList<String> is_offer,ArrayList<String> photo_url1,
//				ArrayList<String> offer_total_like,ArrayList<String> offer_total_total_share,ArrayList<String>mob1,ArrayList<String>mob2,
//				ArrayList<String>mob3,ArrayList<String>tel1,ArrayList<String>tel2,ArrayList<String>tel3,ArrayList<String>valid_till) {
//			super(context, resource, textViewResourceId, offer_title);
//
//			this.context=context;
//			inflator=context.getLayoutInflater();
//
//			this.post_id=post_id;
//			this.news_title=news_title;
//			this.offer_title=offer_title;
//			this.offer_desc=offer_desc;
//
//			this.offer_date=offer_date;
//			this.is_offer=is_offer;
//
//			this.photo_url1=photo_url1;
//			this.offer_total_like=offer_total_like;
//
//			this.offer_total_total_share=offer_total_total_share;
//
//			this.mob1=mob1;
//			this.mob2=mob2;
//			this.mob3=mob3;
//
//			this.tel1=tel1;
//			this.tel2=tel2;
//			this.tel3=tel3;
//
//			this.valid_till=valid_till;
//
//			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
//		}
//
//		public int getCount() {
//			return offer_title.size();
//		}
//
//		public String getItem(int position) {
//			return offer_title.get(position);
//		}
//
//		public View getView(final int position, View convertView, ViewGroup parent) {
//
//			Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_in);
//			if(convertView==null)
//			{
//				ViewHolder holder=new ViewHolder();
//				convertView=inflator.inflate(R.layout.newsfeedrow,null);
//
//				holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
//				holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
//				holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);
//
//				holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
//				holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);
//
//				holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);
//				holder.newFeedband=(TextView)convertView.findViewById(R.id.newFeedband);
//
//				holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);
//				holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);
//
//				convertView.setTag(holder);
//			}
//			ViewHolder hold=(ViewHolder)convertView.getTag();
//			try
//			{
//				hold.newsFeedStoreName.setText(news_title.get(position));
//				hold.newsFeedOffersText.setText(offer_title.get(position));
//				hold.newsFeedOffersDesc.setText(offer_desc.get(position));
//
//				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
//				DateFormat dt2=new SimpleDateFormat("MMM");
//				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//
//				Date date_con = (Date) dt.parse(offer_date.get(position).toString());
//
//				Date date=dateFormat.parse(offer_date.get(position).toString());
//				DateFormat date_format=new SimpleDateFormat("hh:mm a");
//
//				Calendar cal = Calendar.getInstance();
//				cal.setTime(date_con);
//				int year = cal.get(Calendar.YEAR);
//				int month = cal.get(Calendar.MONTH);
//				int day = cal.get(Calendar.DAY_OF_MONTH);
//
//				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
//				hold.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));
//				hold.newsFeedTotalLike.setText(offer_total_like.get(position));
//
//
//				hold.newsFeedTotalShare.setText(offer_total_total_share.get(position));
//
//				hold.newsFeedTotalLike.setOnClickListener(new OnClickListener() {
//					public void onClick(View arg0) {
//						likePost(post_id.get(position));
//					}
//				});
//				hold.newsFeedTotalShare.setOnClickListener(new OnClickListener() {
//					public void onClick(View arg0) {
//						TEMP_POST_ID=post_id.get(position);
//						showDialog(position);
//					}
//				});
//
//
//				hold.newFeedband.setVisibility(View.GONE);
//				hold.newsIsOffer.setVisibility(View.VISIBLE);
//
//				hold.newsFeedStoreName.startAnimation(animation);
//				hold.newsFeedOffersText.startAnimation(animation);
//				hold.newsFeedOffersDesc.startAnimation(animation);
//				hold.newsFeedOffersDate.startAnimation(animation);
//				hold.newsFeedTotalLike.startAnimation(animation);
//				hold.newsFeedTotalShare.startAnimation(animation);
//
//
//				if(is_offer.get(position).toString().length()!=0 && is_offer.get(position).equalsIgnoreCase("1"))
//				{
//					hold.newsIsOffer.setVisibility(View.VISIBLE);
//				}
//				else
//				{
//					hold.newFeedband.setVisibility(View.GONE);
//					hold.newsIsOffer.setVisibility(View.INVISIBLE);
//				}
//
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//
//			try
//			{
//				hold.imgNewsFeedLogo.setVisibility(View.VISIBLE);
//				if(photo_url1.get(position).toString().length()==0)
//				{
//					hold.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
//					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_CENTER);
//				}
//				else
//				{
//					String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
//					try{
//						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
//						.placeholder(R.drawable.ic_place_holder)
//						.error(R.drawable.ic_place_holder)
//						.into(hold.imgNewsFeedLogo);
//					}
//					catch(IllegalArgumentException illegalArg){
//						illegalArg.printStackTrace();
//					}
//					catch(Exception e){ 
//						e.printStackTrace();
//					}
//					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_XY);
//					hold.imgNewsFeedLogo.startAnimation(animation);
//				}
//
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//			return convertView;
//		}
//
//		void showDialog(final int tempPos){
//			try
//			{
//				dialogShare.show();
//				listViewShare.setOnItemClickListener(new OnItemClickListener() {
//					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//							long arg3) {
//						String app = appName.get(position);
//						String shareBody= "";
//						String validTill="";
//						String contact="";
//						String contact_lable="\nContact:";
//						Log.i("Is Offer ","Is Offer valid_till "+valid_till.get(tempPos));
//						if(is_offer.get(tempPos).toString().length()!=0 && is_offer.get(tempPos).equalsIgnoreCase("1"))
//						{
//							try
//							{
//								DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
//								DateFormat dt2=new SimpleDateFormat("MMM");
//								DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//
//								Date date_con = (Date) dt.parse(valid_till.get(tempPos).toString());
//
//								Date date=dateFormat.parse(valid_till.get(tempPos).toString());
//								DateFormat date_format=new SimpleDateFormat("hh:mm a");
//
//								Calendar cal = Calendar.getInstance();
//								cal.setTime(date_con);
//								int year = cal.get(Calendar.YEAR);
//								int month = cal.get(Calendar.MONTH);
//								int day = cal.get(Calendar.DAY_OF_MONTH);
//
//								validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);
//
//							}catch(Exception ex)
//							{
//								ex.printStackTrace();
//								validTill="";
//							}
//						}
//						else
//						{
//							validTill="";
//						}
//
//						if(mob1.get(tempPos).toString().length()>0)
//						{
//							contact="+"+mob1.get(tempPos).toString();
//						} 
//						if(mob2.get(tempPos).toString().length()>0)
//						{
//							contact+=" +"+mob2.get(tempPos).toString();
//						}
//						if(mob3.get(tempPos).toString().length()>0)
//						{
//							contact+=" +"+mob3.get(tempPos).toString();
//						}
//						if(tel1.get(tempPos).toString().length()>0)
//						{
//							contact+=" "+tel1.get(tempPos).toString();
//						}
//						if(tel2.get(tempPos).toString().length()>0)
//						{
//							contact+=" "+tel1.get(tempPos).toString();
//						}
//						if(tel3.get(tempPos).toString().length()>0)
//						{
//							contact+=" "+tel1.get(tempPos).toString();
//						}
//						if(contact.length()==0)
//						{
//							contact_lable="";
//						}
//						shareBody=news_title.get(tempPos) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(tempPos) + "\n" + offer_desc.get(tempPos)+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
//						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){
//
//							if(photo_url1.get(tempPos).toString().length()!=0)
//							{
//								String photo_source=photo_url1.get(tempPos).toString().replaceAll(" ", "%20");
//								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
//								.setLink("http://shoplocal.co.in")
//								.setName(news_title.get(tempPos) + " - " + "Offers")
//								.setPicture(PHOTO_PARENT_URL+photo_source)
//								.setDescription(shareBody)
//								.build();
//								uiHelper.trackPendingDialogCall(shareDialog.present());
//								dismissDialog();
//							}
//							else
//							{
//								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
//								.setLink("http://shoplocal.co.in")
//								.setName(news_title.get(tempPos) + " - " + "Offers")
//								.setDescription(shareBody)
//								.build();
//								uiHelper.trackPendingDialogCall(shareDialog.present());
//								dismissDialog();
//							}
//						}
//						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
//							Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
//						}
//
//						else if(!app.equalsIgnoreCase("facebook")){
//							Intent i = new Intent(Intent.ACTION_SEND);
//							i.setPackage(packageNames.get(position));
//							i.setType("text/plain");
//							i.putExtra(Intent.EXTRA_SUBJECT, news_title.get(tempPos));
//							i.putExtra(Intent.EXTRA_TEXT, shareBody);
//							startActivityForResult(i,6);
//						}
//
//						dismissDialog();
//
//					}
//				});
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//		}
//
//		void dismissDialog(){
//			dialogShare.dismiss();
//		}
//	}
//
//	
//	 class ViewHolder
//	{
//		TextView newsFeedOffersDate;
//		TextView newsFeedOffersMonth;
//
//		TextView newsFeedOffersText;
//		TextView newsFeedOffersDesc;
//
//		TextView newsFeedTotalLike;
//		TextView newsFeedTotalShare;
//
//		ImageView imgNewsFeedLogo;
//
//		View newsFeedviewOverLay;
//
//		TextView newFeedband;
//
//		ImageView newsIsOffer;
//
//		TextView newsFeedStoreName;
//
//	}
//
//	void likePost(String POST_ID)
//	{
//		if(isnetConnected.isNetworkAvailable())
//		{
//			if(session.isLoggedInCustomer())
//			{
//				Intent intent=new Intent(context, FavouritePostService.class);
//				intent.putExtra("favouritePost",mFav);
//				intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
//				intent.putExtra("api_header",API_HEADER);
//				intent.putExtra("api_header_value", API_VALUE);
//				intent.putExtra("user_id", USER_ID);
//				intent.putExtra("auth_id", AUTH_ID);
//				intent.putExtra("post_id", POST_ID);
//				context.startService(intent);
//				searchListProg.setVisibility(View.VISIBLE);
//			}
//			else
//			{
//				loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
//			}
//		}
//		else
//		{
//			showToast(getResources().getString(R.string.noInternetConnection));
//		}
//	}
//	private void callShareApi(String POST_ID) {
//		Intent intent = new Intent(context, ShareService.class);
//		intent.putExtra("shareService", shareServiceResult);
//		intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
//		intent.putExtra("api_header",API_HEADER);
//		intent.putExtra("api_header_value", API_VALUE);
//		intent.putExtra("user_id", USER_ID);
//		intent.putExtra("auth_id", AUTH_ID);
//		intent.putExtra("post_id", POST_ID);
//		intent.putExtra("via", VIA);
//		context.startService(intent);
//	}
//
//	void loginAlert(String msg,final int like)
//	{
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//		alertDialogBuilder.setTitle("Discover");
//		alertDialogBuilder
//		.setMessage(msg)
//		.setIcon(R.drawable.ic_launcher)
//		.setCancelable(false)
//		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog,int id) {
//
//				dialog.dismiss();
//				if(like==POST_LIKE)
//				{
//					Intent intent=new Intent(context,LoginSignUpCustomer.class);
//					context.startActivityForResult(intent,2);
//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//				}
//
//			}
//		})
//		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog,int id) {
//				dialog.dismiss();
//			}
//		})
//		;
//
//		AlertDialog alertDialog = alertDialogBuilder.create();
//		alertDialog.show();
//	}
//
//	void clearArray()
//	{
//		NEWS_FEED_ID.clear();
//		NEWS_FEED_NAME.clear();
//		NEWS_FEED_AREA_ID.clear();
//
//		NEWS_FEED_MOBNO1.clear();
//		NEWS_FEED_MOBNO2.clear();
//		NEWS_FEED_MOBNO3.clear();
//
//
//		NEWS_FEED_TELLNO1.clear();
//		NEWS_FEED_TELLNO2.clear();
//		NEWS_FEED_TELLNO3.clear();
//
//		NEWS_FEED_LATITUDE.clear();
//		NEWS_FEED_LONGITUDE.clear();
//		NEWS_FEED_DISTANCE.clear();
//
//		NEWS_FEED_POST_ID.clear();
//		NEWS_FEED_POST_TYPE.clear();
//
//		NEWS_FEED_POST_TITLE.clear();
//		NEWS_FEED_DESCRIPTION.clear();
//
//
//		NEWS_FEED_image_url1.clear();
//		NEWS_FEED_image_url2.clear();
//		NEWS_FEED_image_url3.clear();
//
//		NEWS_FEED_thumb_url1.clear();
//		NEWS_FEED_thumb_url2.clear();
//		NEWS_FEED_thumb_url3.clear();
//
//		NEWS_DATE.clear();
//		NEWS_IS_OFFERED.clear();
//		NEWS_OFFER_DATE_TIME.clear();
//
//		NEWS_FEED_place_total_like.clear();
//		NEWS_FEED_place_total_share.clear();
//
//		search_page=0;
//		search_post_count=20;
//	}
//	public void postLikeReuslt(int resultCode, Bundle resultData) {
//		try
//		{
//			searchListProg.setVisibility(View.GONE);
//			String postLikestatus=resultData.getString("postLikestatus");
//			String postLikemsg=resultData.getString("postLikemsg");
//			if(postLikestatus.equalsIgnoreCase("true"))
//			{
//				showToast(postLikemsg);
//			}
//			else if(postLikestatus.equalsIgnoreCase("false"))
//			{
//				showToast(postLikemsg);
//				String error_code=resultData.getString("error_code");
//				if(error_code.equalsIgnoreCase("-221"))
//				{
//					session.logoutCustomer();
//				}
//
//			}
//			else if(postLikestatus.equalsIgnoreCase("error"))
//			{
//				showToast(postLikemsg);
//			}
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//
//
//	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
//		String status = resultData.getString("share_status");
//		String msg = resultData.getString("share_msg");
//		try {
//			if(status.equalsIgnoreCase("true")){
//				showToast(msg);
//			}
//			else{
//				showToast(msg);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//
//	protected void onStart() {
//		super.onStart();
//		EventTracker.startFlurrySession(getApplicationContext());
//	}
//
//
//	protected void onStop() {
//		EventTracker.endFlurrySession(getApplicationContext());	
//		super.onStop();
//	}
//
//
//	protected void onResume() {
//		super.onResume();
//		EventTracker.startLocalyticsSession(getApplicationContext());
//		uiHelper.onResume();
//	}
//
//
//	protected void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		uiHelper.onSaveInstanceState(outState);
//	}
//
//
//	protected void onPause() {
//		EventTracker.endLocalyticsSession(getApplicationContext());
//		super.onPause();
//		uiHelper.onPause();
//	}
//
//
//	public void onDestroy() {
//		super.onDestroy();
//		uiHelper.onDestroy();
//	}
//
//
//}
