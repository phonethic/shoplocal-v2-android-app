package com.phonethics.shoplocal;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.phonethics.adapters.CallListDialog;
import com.phonethics.adapters.MyShoplocalAdapter;

public class NewsFeedCategory extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener, ListClickListener
{

	private PullToRefreshListView mOffersList;
	
	private TextView mTxtCounter;
	
	private Activity mContext;

	private int miTotalRecords;
	private int miTotalPages;
	private int miCurrentPage=0;
	private int miItemPerPage=50;
	private int miTempPosition=0;
	
	private boolean mbIsRefreshing=false;	
	
	private String msCategoryId="";
	private String msTempPostId="";
	private String msStoreId="";
	private String msPostId="";
	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msStoreName="";
	private String msImageLink="";
	private String msIsOffer="";
	private String msOfferValidTill="";
	private String msPostTitle="";
	private String msPostDescription="";
	private String msShareVia="";
	private String msCallType="";
	private String msActiveAreaName="";

	private MyShoplocalAdapter mAdapter;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo1=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo2=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo3=new ArrayList<String>();

	private ArrayList<String> mArrTelNo1=new ArrayList<String>();
	private ArrayList<String> mArrTelNo2=new ArrayList<String>();
	private ArrayList<String> mArrTelNo3=new ArrayList<String>();

	private ArrayList<String> mArrPostId=new ArrayList<String>();
	private ArrayList<String> mArrPostTitle=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrPostDesc=new ArrayList<String>();
	private ArrayList<String> mArrPostDate=new ArrayList<String>();
	private ArrayList<String> mArrPostImage=new ArrayList<String>();
	private ArrayList<String> mArrPostHasOffer=new ArrayList<String>();
	private ArrayList<String> mArrTotalLike=new ArrayList<String>();
	private ArrayList<String> mArrTotalShare=new ArrayList<String>();
	private ArrayList<String> mArrPostValidityDate= new ArrayList<String>();
	private ArrayList<String> mArrCall= new ArrayList<String>();
	
	private ProgressDialog mProgressDialog;
	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_feed_category);
		hideDrawerLayout();
		setTitle("Offers Category");
		
		mContext=this;
		
		mProgressDialog=new ProgressDialog(mContext);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");
		
		mOffersList=(PullToRefreshListView)findViewById(R.id.listCategNewsFeedResult);
		mTxtCounter=(TextView)findViewById(R.id.txtCategNewsFeedCounter);
		mTxtCounter.setVisibility(View.GONE);
		Bundle bundle=getIntent().getExtras();
		
		msActiveAreaName=mDbUtil.getActiveArea();

		if(bundle!=null)
		{
			msCategoryId=bundle.getString("category_id");
			getOffers();
		}
		
		mAdapter=new MyShoplocalAdapter(mContext, 0, 0, mArrPostId, mArrPostTitle, mArrPostDesc, mArrStoreName, 
				mArrPostHasOffer, mArrPostImage, mArrPostDate, 
				mArrTotalLike, mArrTotalShare, this);
		mOffersList.setAdapter(mAdapter);
		
		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());
		
		mOffersList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				int tempPos=position-1;
				Intent intent=new Intent(mContext, OffersBroadCastDetails.class);
				intent.putExtra("storeName",mArrStoreName.get(tempPos));
				intent.putExtra("title",mArrPostTitle.get(tempPos));
				intent.putExtra("body",mArrPostDesc.get(tempPos));
				intent.putExtra("PLACE_ID",mArrStoreId.get(tempPos));
				intent.putExtra("POST_ID",mArrPostId.get(tempPos));
				intent.putExtra("fromCustomer", true);
				intent.putExtra("mob1",mArrMobileNo1.get(tempPos));
				intent.putExtra("mob2",mArrMobileNo2.get(tempPos));
				intent.putExtra("mob3",mArrMobileNo3.get(tempPos));
				intent.putExtra("tel1",mArrTelNo1.get(tempPos));
				intent.putExtra("tel2",mArrTelNo2.get(tempPos));
				intent.putExtra("tel3",mArrTelNo3.get(tempPos));
				intent.putExtra("photo_source",mArrPostImage.get(tempPos));
				intent.putExtra("USER_LIKE","-1");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		mOffersList.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				if(mInternet.isNetworkAvailable()){
					if(miCurrentPage<miTotalPages){
						getOffers();
					}
					else{
						debug("current page "+miCurrentPage+" total pages "+miTotalPages);
					}
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});
		mOffersList.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				mbIsRefreshing=true;
				if(mInternet.isNetworkAvailable()){
					miCurrentPage=0;
					getOffers();
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});
	}
	
	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			
			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("Source","OffersCategory");
			callClicked.put("storeName",mArrStoreName.get(miTempPosition));
			debug(mArrStoreName.get(miTempPosition));
			callClicked.put("Number",mArrCall.get(position));
			EventTracker.logEvent("StoreCallDone", callClicked);
			
			msCallType="StoreCallDone";
			callContactApi(msTempPostId);
			likePlace(mArrStoreId.get(miTempPosition));
			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();
			
		}
		
	}
	
	void callContactApi(String postId)
	{
		try
		{
//			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
					
					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put("post_id", postId);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);
					debug(jsonObject.toString());
					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
//			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	void likePlace(String postId)
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", postId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlaceOnCall").execute(msUrl);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	
	void debug(String msg)
	{
		Log.d("Offers Cate", "Offers Categ "+msg);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishTo();
		return true;
	}
	public void onBackPressed() {
		super.onBackPressed();
		finishTo();
	}

	void finishTo()
	{
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}
	
	void getOffers()
	{
		if(mInternet.isNetworkAvailable()){
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("area_id", Config.MY_AREA));
			nameValuePairs.add(new BasicNameValuePair("search", mEdtSearch.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("page", (miCurrentPage+1)+""));
			nameValuePairs.add(new BasicNameValuePair("category_id",msCategoryId));
			nameValuePairs.add(new BasicNameValuePair("count", miItemPerPage+""));
			String paramString=URLEncodedUtils.format(nameValuePairs, "utf-8");
			//			String sURL = Config.OFFERS_STREAM_URL +"&area_id="+Config.MY_AREA +"&search="+mEdtSearch.getText().toString()+ "&page=" + (miCurrentPage+1) + "&count="+miItemPerPage;
			String sURL = Config.OFFERS_STREAM_URL +paramString;
			new ShoplocalAsyncTask(this, "get", "customer", false,"offers").execute(sURL);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));	
		}
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}
	
	void clearData()
	{
		mArrStoreId.clear();
		mArrStoreName.clear();
		mArrMobileNo1.clear();
		mArrMobileNo2.clear();
		mArrMobileNo3.clear();
		mArrTelNo1.clear();
		mArrTelNo2.clear();
		mArrTelNo3.clear();
		mArrPostTitle.clear();
		mArrPostId.clear();
		mArrPostDesc.clear();
		mArrPostImage.clear();
		mArrPostDate.clear();
		mArrPostHasOffer.clear();
		mArrTotalLike.clear();
		mArrTotalShare.clear();
		mArrPostValidityDate.clear();

		mArrTotalShare.clear();

		miTotalPages=0;
		miTotalRecords=0;
		miCurrentPage=0;

	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		if(tag.equalsIgnoreCase("offers")){
			try
			{
				

				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equalsIgnoreCase("false")){
					String msg=jsonObject.getString("message");
					clearData();
					mTxtCounter.setText("");
					showToast(msg);
					return;
				}
				
				if(mbIsRefreshing==true){
					mbIsRefreshing=false;
					clearData();
					mOffersList.onRefreshComplete();
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");
				if(dataObject == null) return;

				miCurrentPage++;
				miTotalRecords = dataObject.getInt("total_record");
				miTotalPages = dataObject.getInt("total_page");

				JSONArray jsonArray = dataObject.getJSONArray("record");
				if(jsonArray == null)  return;

				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					if(offerObject != null) {
						mArrStoreId.add(offerObject.getInt("id")+"");
						mArrStoreName.add(offerObject.getString("name"));
						mArrMobileNo1.add(offerObject.getString("mob_no1"));
						mArrMobileNo2.add(offerObject.getString("mob_no2"));
						mArrMobileNo3.add(offerObject.getString("mob_no3"));
						mArrTelNo1.add(offerObject.getString("tel_no1"));
						mArrTelNo2.add(offerObject.getString("tel_no2"));
						mArrTelNo3.add(offerObject.getString("tel_no3"));
						mArrPostTitle.add(offerObject.getString("title"));
						mArrPostId.add(offerObject.getString("post_id"));
						mArrPostDesc.add(offerObject.getString("description"));
						mArrPostImage.add(offerObject.getString("image_url1"));
						mArrPostDate.add(offerObject.getString("date"));
						mArrPostValidityDate.add(offerObject.getString("offer_date_time"));
						mArrPostHasOffer.add(offerObject.getString("is_offered"));
						mArrTotalLike.add(offerObject.getString("total_like"));
						mArrTotalShare.add(offerObject.getString("total_share"));
					}
				}
				mAdapter.notifyDataSetChanged();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("likePost")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				
				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}


				if(status.equalsIgnoreCase("true")){
					
					try
					{
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(mArrStoreName.get(miTempPosition));
						if(mArrPostImage.get(miTempPosition).length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+mArrPostImage.get(miTempPosition));
						}
						fbObj.setStoreDescription(mArrPostDesc.get(miTempPosition));
						String encodedurl = URLEncoder.encode(mArrStoreName.get(miTempPosition),"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+mArrStoreId.get(miTempPosition));
						FacebookOperations.FavThisOnFb(mActivity, fbObj, getString(R.string.fb_offer_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					String message=jsonObject.getString("message");
					showToast(message);
				}

			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("sharePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;

				if(status.equalsIgnoreCase("true")){
					try
					{
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(mArrStoreName.get(miTempPosition));
						if(mArrPostImage.get(miTempPosition).length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+mArrPostImage.get(miTempPosition));
						}
						fbObj.setStoreDescription(mArrPostDesc.get(miTempPosition));
						String encodedurl = URLEncoder.encode(mArrStoreName.get(miTempPosition),"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+mArrStoreId.get(miTempPosition));
						FacebookOperations.shareThisInfo(mActivity, fbObj, getString(R.string.fb_store_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	private void showProgressDialog(){
	
		mProgressDialog.show();
	}

	@Override
	public void onError(int errorCode) {
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		showToast(getResources().getString(R.string.errorMessage));
		
	}

	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		this.msTempPostId=msPostId;
		miTempPosition=position;
		if(tag.equalsIgnoreCase("like")){
			if(mSessionManager.isLoggedInCustomer()){
				likePost(msPostId);
			}else{
				loginAlert(getResources().getString(R.string.postFavAlert), 1);
			}
		}else if(tag.equalsIgnoreCase("share")){
			if(mInternet.isNetworkAvailable()){
			showShareDialog(position);
			}else{
				showToast(getString(R.string.noInternetConnection));
			}
		}else if(tag.equalsIgnoreCase("call")){
			mArrCall.clear();
			mCalldailog.setTitle(mArrStoreName.get(position));
			if(mArrMobileNo1.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo1.get(position).toString());
			}
			if(mArrMobileNo2.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo2.get(position).toString());
			}
			if(mArrMobileNo3.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo3.get(position).toString());
			}
			if(mArrTelNo1.get(position).length()>5){
				mArrCall.add(mArrTelNo1.get(position).toString());
			}
			if(mArrTelNo2.get(position).length()>5){
				mArrCall.add(mArrTelNo2.get(position).toString());
			}
			if(mArrTelNo3.get(position).length()>5){
				mArrCall.add(mArrTelNo3.get(position).toString());
			}
			mCallAdapter.notifyDataSetChanged();
			
			if(mArrCall.size()!=0){
				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("Source","OffersCategory");
				callClicked.put("storeName",mArrStoreName.get(miTempPosition));
				debug(mArrStoreName.get(miTempPosition));
				EventTracker.logEvent("StoreCall", callClicked);
				
				msCallType="StoreCall";
				callContactApi(msPostId);
				mCalldailog.show();
			}else{
				showToast("No numbers to call");
			}
		}
		
		
	}
	
	void likePost(String msPostId)
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("post_id", msTempPostId);
				json.put("auth_id", Config.CUST_AUTH_ID);
				String msUrl=Config.POST_LIKE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"likePost").execute(msUrl);
			}else
			{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void showShareDialog(int position){
		try
		{
			msMobNo1=mArrMobileNo1.get(position);
			msMobNo2=mArrMobileNo2.get(position);
			msMobNo3=mArrMobileNo3.get(position);

			msTelNo1=mArrTelNo1.get(position);
			msTelNo2=mArrTelNo2.get(position);
			msTelNo3=mArrTelNo3.get(position);

			msIsOffer=mArrPostHasOffer.get(position);
			msOfferValidTill=mArrPostValidityDate.get(position);

			msPostTitle=mArrPostTitle.get(position);
			msPostDescription=mArrPostDesc.get(position);
			msStoreName=mArrStoreName.get(position);

			msImageLink=mArrPostImage.get(position);

			String shareBody= "";
			String validTill="";
			String contact="";
			String contact_lable="\nContact:";
			if(msIsOffer.length()!=0 && msIsOffer.equalsIgnoreCase("1")){
				try
				{
					DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
					DateFormat dt2=new SimpleDateFormat("MMM");
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

					Date date_con = (Date) dt.parse(msOfferValidTill.toString());
					Date date=dateFormat.parse(msOfferValidTill.toString());

					DateFormat date_format=new SimpleDateFormat("hh:mm a");
					Calendar cal = Calendar.getInstance();
					cal.setTime(date_con);
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH);
					int day = cal.get(Calendar.DAY_OF_MONTH);
					validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);

				}catch(Exception ex)
				{
					ex.printStackTrace();
					validTill="";
				}
			}
			else{
				validTill="";
			}
			try
			{
				if(msMobNo1.length()>0){
					contact="+"+msMobNo1;
				} 
				if(msMobNo2.length()>0){
					contact+=" +"+msMobNo2;
				}
				if(msMobNo3.length()>0){
					contact+=" +"+msMobNo3;
				}
				if(msTelNo1.length()>0){
					contact+=" "+msTelNo1;
				}
				if(msTelNo2.length()>0){
					contact+=" "+msTelNo2;
				}
				if(msTelNo3.length()>0){
					contact+=" "+msTelNo3;
				}
				if(contact.length()==0){
					contact_lable="";
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			shareBody=msStoreName + " - " + "Offers" + " : " + "\n\n"+msPostTitle + "\n" + 
					msPostDescription +validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
			offerShareDialog(msStoreName, msImageLink, shareBody);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==1)
				{
					Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
					mContext.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	void sharePost(String msPostId)
	{
		try
		{
			try {
				if(mInternet.isNetworkAvailable()){
					JSONObject json = new JSONObject();
					json.put("post_id", msTempPostId);
					json.put("via", msShareVia);
					String msUrl=Config.POST_SHARE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"sharePost").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
		}
		if(requestCode==4)
		{
			if(mSessionManager.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();
				}
				else
				{
					Intent intent=new Intent(mContext,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
		if(requestCode == 3){
			//		VIA="Android";
			//	callShareApi(TEMP_POST_ID);
			msShareVia="Android";
			sharePost(msPostId);
		}

		try
		{
			mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {

				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}


				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					//				VIA="Android-Facebook";
					//				callShareApi(TEMP_POST_ID);
					msShareVia="Android-Facebook";
					sharePost(msPostId);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
//public class NewsFeedCategory extends SherlockActivity implements NewsFeed, FavouritePostInterface, ShareResultInterface {
//
//
//
//	ActionBar actionBar;
//
//	//URL'S
//	static String STORE_URL;
//	static String GET_SEARCH_URL;
//	String PHOTO_PARENT_URL;
//
//	Activity context;
//
//	//Api header 
//	static String API_HEADER;
//	//Api header value
//	static String API_VALUE;
//
//	NewsFeedReceiver mReceiver;
//
//
//	//Network 
//	NetworkCheck network;
//
///*	ImageLoader imageLoaderList;*/
//
//	//Listview
//	PullToRefreshListView listCategNewsFeedResult;
//	TextView txtCategNewsFeedCounter;
//	ProgressBar newsFeedCategListProg;
//
//
//	ArrayList<String> NEWS_FEED_ID=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_NAME=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_AREA_ID=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_MOBNO1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_MOBNO2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_MOBNO3=new ArrayList<String>();
//
//
//	ArrayList<String> NEWS_FEED_TELLNO1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_TELLNO2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_TELLNO3=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_LATITUDE=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_LONGITUDE=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_DISTANCE=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_POST_ID=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_POST_TYPE=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_POST_TITLE=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_DESCRIPTION=new ArrayList<String>();
//
//
//	ArrayList<String> NEWS_FEED_image_url1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_image_url2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_image_url3=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_thumb_url1=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_thumb_url2=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_thumb_url3=new ArrayList<String>();
//
//	ArrayList<String> NEWS_DATE=new ArrayList<String>();
//	ArrayList<String> NEWS_IS_OFFERED=new ArrayList<String>();
//	ArrayList<String> NEWS_OFFER_DATE_TIME=new ArrayList<String>();
//
//	ArrayList<String> NEWS_FEED_place_total_like=new ArrayList<String>();
//	ArrayList<String> NEWS_FEED_place_total_share=new ArrayList<String>();
//
//
//	int post_count=20;
//	int page=0;
//
//
//	boolean isRefreshing=false;
//
//	NewsFeedAdapter adapter=null;
//
//	String TOTAL_PAGES="";
//	String TOTAL_RECORDS="";
//
//	String offer="";
//	String category_id="";
//
//	boolean isCheckOffers=false;
//	boolean isCategory=true;
//
//	DBUtil dbutil;
//	String value="";
//
//	static String BROADCAST_URL;
//	static String LIKE_API;
//	static String SHARE_URL;
//
//	static String USER_ID;
//	static String AUTH_ID;
//
//	String VIA = "Android";
//
//	//User Id
//	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";
//
//	//Auth Id
//	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";
//
//	//Password
//	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";
//
//	int POST_LIKE=2;
//
//	ShareResultReceiver shareServiceResult;
//	FavouritePostResultReceiver mFav;
//
//	SessionManager session;
//	String TEMP_POST_ID="";
//
//	private UiLifecycleHelper uiHelper;
//	
//	/* Custom dialog for share */
//	ArrayList<String> packageNames = new ArrayList<String>();
//	ArrayList<String> appName = new ArrayList<String>();
//	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
//	//String shareText = "Hea, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
//	Dialog dialogShare;
//	ListView listViewShare;
//	
//	/** Check if facebook is present in phone */
//	boolean isFacebookPresent = false;
//
//	
//	protected void onCreate(Bundle savedInstanceState) {
//		setTheme(R.style.Theme_City_custom);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_news_feed_category);
//
//		actionBar=getSupportActionBar();
//		actionBar.setTitle(getString(R.string.actionBarTitle));
//		actionBar.setDisplayHomeAsUpEnabled(true);
//		actionBar.show();
//
//		listCategNewsFeedResult=(PullToRefreshListView)findViewById(R.id.listCategNewsFeedResult);
//		txtCategNewsFeedCounter=(TextView)findViewById(R.id.txtCategNewsFeedCounter);
//		newsFeedCategListProg=(ProgressBar)findViewById(R.id.newsFeedCategListProg);
//
//		context=this;
//		network=new NetworkCheck(context);
//		dbutil=new DBUtil(context);
//		session=new SessionManager(context);
//
//		//facebook
//		uiHelper = new UiLifecycleHelper(context, null);
//		uiHelper.onCreate(savedInstanceState);
//
//
//
//	/*	imageLoaderList=new ImageLoader(context);*/
//
//		mReceiver=new NewsFeedReceiver(new Handler());
//		mReceiver.setReceiver(this);
//
//		//Urls
//		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
//		GET_SEARCH_URL=getResources().getString(R.string.searchapi);
//
//		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);
//
//		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
//		LIKE_API=getResources().getString(R.string.like);
//
//		SHARE_URL = "/" + getResources().getString(R.string.share);
//
//
//		//API key
//		API_HEADER=getResources().getString(R.string.api_header);
//		API_VALUE=getResources().getString(R.string.api_value);
//
//		HashMap<String,String>user_details=session.getUserDetailsCustomer();
//		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
//		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
//
//		mFav=new FavouritePostResultReceiver(new Handler());
//		mFav.setReceiver(this);
//
//		shareServiceResult = new ShareResultReceiver(new Handler());
//		shareServiceResult.setReceiver(this);
//
//
//
//		Bundle bundle=getIntent().getExtras();
//		if(bundle!=null)
//		{
//			value=dbutil.getActiveAreaID();
//			category_id=bundle.getString("category_id");
//			offer=bundle.getString("offer");
//			isCategory=bundle.getBoolean("isCategory");
//			if(!isCategory){
//				isCheckOffers=true;
//			}
//
//			loadNewsFeeds();
//		}
//
//		adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
//				NEWS_FEED_place_total_like, NEWS_FEED_place_total_share,NEWS_FEED_MOBNO1,NEWS_FEED_MOBNO2,NEWS_FEED_MOBNO3,NEWS_FEED_TELLNO1,NEWS_FEED_TELLNO2,NEWS_FEED_TELLNO3,NEWS_OFFER_DATE_TIME);
//
//		listCategNewsFeedResult.setAdapter(adapter);
//		//Pull to refresh 
//
//		listCategNewsFeedResult.setOnRefreshListener(new OnRefreshListener<ListView>() {
//
//			
//			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//
//				try
//				{
//					if(network.isNetworkAvailable())
//					{
//						if(!newsFeedCategListProg.isShown())
//						{
//							isRefreshing=true;
//							page=0;
//							loadNewsFeeds();
//						}
//					}
//					else
//					{
//						showToast(getResources().getString(R.string.noInternetConnection));
//					}
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
//			}
//		});
//
//		listCategNewsFeedResult.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
//
//			
//			public void onLastItemVisible() {
//
//				if(network.isNetworkAvailable())
//				{
//
//					if(NEWS_FEED_ID.size()==Integer.parseInt(TOTAL_RECORDS))
//					{
//
//					}
//					else if(page<Integer.parseInt(TOTAL_PAGES))
//					{
//
//						if(!newsFeedCategListProg.isShown())
//						{
//							//									page++;
//							Log.i("Page", "Page "+page);
//							//Load Stores from Internet
//							loadNewsFeeds();
//
//							Log.i("ID SIZE","NEWS FEED ID SIZE  "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
//						}
//						else
//						{
//							showToast(getResources().getString(R.string.loadingAlert));
//
//						}
//
//					}
//					else
//					{
//						Log.i("ID SIZE","NEWS FEED ID SIZE ELSE "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
//						newsFeedCategListProg.setVisibility(View.GONE);
//					}
//				}
//				else
//				{
//					listCategNewsFeedResult.onRefreshComplete();
//				}
//
//
//			}
//		});
//		
//		
//		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
//		intentShareActivity.setType("text/plain");
//		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
//		
//		
//		final PackageManager pm = getPackageManager();
//		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
//		
//		
//		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
//		        pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
//		
//		
//		/** Anirudh facebook */
//		if(packageNames.size() == 0){
//			appName.add("Facebook");
//			packageNames.add("com.facebook");
//			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
//			for (ResolveInfo rInfo : list) {
//				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
//				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
//				
//				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
//					isFacebookPresent = true;
//				}
//				
//				if(app.equalsIgnoreCase("facebook") == false){
//					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
//					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
//					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
//				}
//				
//			}
//			if(!isFacebookPresent)
//			{
//				appName.remove(0);
//				packageNames.remove(0);
//				appIcon.remove(0);
//			}
//		}
//		
//		
//		/** ANirudh custom share dialog */
//		dialogShare = new Dialog(NewsFeedCategory.this);
//		dialogShare.setContentView(R.layout.sharedialog);
//		dialogShare.setTitle("Select an action");
//		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
//		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));
//
//	    
//
//
//
//	}//onCreate Ends here
//
//
//
//	
//	public boolean onOptionsItemSelected(MenuItem item) {
//		
//		this.finish();
//		return true;
//	}
//
//
//
//
//	
//	public void onBackPressed() {
//		
//		this.finish();
//	}
//
//
//
//	void loadNewsFeeds()
//	{
//		if(network.isNetworkAvailable())
//		{
//			Intent intent=new Intent(context, NewsFeedService.class);
//			intent.putExtra("newsFeed",mReceiver);
//			intent.putExtra("URL", STORE_URL+"/"+GET_SEARCH_URL);
//			intent.putExtra("api_header",API_HEADER);
//			intent.putExtra("api_header_value", API_VALUE);
//			intent.putExtra("count",post_count);
//			intent.putExtra("page",page+1);
//			intent.putExtra("area_id",value);
//			intent.putExtra("isCategory",isCategory);
//			intent.putExtra("category_id",category_id);
//			intent.putExtra("offer", offer);
//			intent.putExtra("isCheckOffers", isCheckOffers);
//			startService(intent);
//			newsFeedCategListProg.setVisibility(View.VISIBLE);
//		}
//		else
//		{
//			showToast(getResources().getString(R.string.noInternetConnection));
//		}
//	}
//
//	
//	public void onNewsFeedResult(int resultCode, Bundle resultData) {
//		
//		newsFeedCategListProg.setVisibility(View.GONE);
//
//		try
//		{
//
//			String search_status=resultData.getString("status");
//			String message=resultData.getString("message");
//			ArrayList<NewsFeedModel> newsFeed=resultData.getParcelableArrayList("posts");
//
//
//
//
//
//			if(search_status.equalsIgnoreCase("true"))
//			{
//				TOTAL_PAGES=resultData.getString("total_page");
//				TOTAL_RECORDS=resultData.getString("total_record");
//
//				Log.i("News FEED SIZE ", "News FEED SIZE "+newsFeed.size());
//
//				try
//				{
//
//					if(isRefreshing)
//					{
//						clearArray();
//						isRefreshing=false;
//					}
//
//					for(int i=0;i<newsFeed.size();i++)
//					{
//						NEWS_FEED_ID.add(newsFeed.get(i).getId());
//						NEWS_FEED_NAME.add(newsFeed.get(i).getName());
//						NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());
//
//						NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
//						NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
//						NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());
//
//						NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
//						NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
//						NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());
//
//						NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
//						NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
//						NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());
//
//						NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
//						NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
//						NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());
//
//						NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());
//
//						NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
//						NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
//						NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());
//
//						NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
//						NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
//						NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());
//
//						NEWS_DATE.add(newsFeed.get(i).getDate());
//						NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());
//
//
//						NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
//						NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
//						NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());
//					}
//
//					//					for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
//					//					{
//					//						NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
//					//						NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
//					//						NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );
//					//
//					//						NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
//					//						NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
//					//						NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));
//					//
//					//						NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
//					//						NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
//					//						NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));
//					//
//					//						NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
//					//						NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
//					//						NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));
//					//
//					//						NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
//					//						NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
//					//						NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));
//					//
//					//						NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));
//					//
//					//						NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
//					//						NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
//					//						NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));
//					//
//					//						NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
//					//						NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
//					//						NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));
//					//
//					//						NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
//					//						NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));
//					//
//					//
//					//						NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
//					//						NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
//					//						NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));
//					//					}
//
//
//					//Get the first visible position of listview
//					//final int old_pos = listCategNewsFeedResult.getRefreshableView().getFirstVisiblePosition();
//
//					page++;
//					//					adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
//					//							NEWS_FEED_place_total_like, NEWS_FEED_place_total_share);
//					//
//					//					listCategNewsFeedResult.setAdapter(adapter);
//
//					adapter.notifyDataSetChanged();
//
//					//Restore visible position
//					//					listCategNewsFeedResult.post(new Runnable() {
//					//
//					//						
//					//						public void run() {
//					//			
//					//							listCategNewsFeedResult.getRefreshableView().setSelection(old_pos);
//					//						}
//					//					});
//
//					Log.i("TOTAL PAGES AND RECORDS  ","TOTAL CURRENT PAGE, PAGES AND RECORDS : PAGE "+page +" TOTAL PAGES "+TOTAL_PAGES+" TOTAL_RECORD "+TOTAL_RECORDS);
//
//					txtCategNewsFeedCounter.setText(NEWS_FEED_ID.size()+"/"+TOTAL_RECORDS);
//
//					listCategNewsFeedResult.onRefreshComplete();
//					listCategNewsFeedResult.setOnItemClickListener(new OnItemClickListener() {
//
//
//						
//						public void onItemClick(AdapterView<?> arg0, View arg1,
//								int position, long arg3) {
//			
//							if(!isRefreshing)
//							{
//								Intent intent=new Intent(context, OffersBroadCastDetails.class);
//								intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
//								intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
//								intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
//								intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
//								intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
//								intent.putExtra("fromCustomer", true);
//								intent.putExtra("isFromStoreDetails", false);
//								intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
//								intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
//								intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
//								intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
//								intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
//								intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
//								intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
//								//								if(POST_USER_LIKE.size()==0)
//								//								{
//								intent.putExtra("USER_LIKE","-1");
//								//								}else
//								//								{
//								//									intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
//								//
//								//								}
//
//								context.startActivityForResult(intent, 5);
//								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//							}
//						}
//					});
//
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//
//				}
//
//
//			}
//			else
//			{
//				showToast(message);
//
//			}
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}
//
//	void showToast(String text)
//	{
//		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
//	}
//
//
//	class NewsFeedAdapter extends ArrayAdapter<String>
//	{
//
//		Activity context;
//
//		ArrayList<String> post_id;
//		ArrayList<String> news_title;
//		ArrayList<String> offer_title;
//		ArrayList<String> offer_desc;
//
//		ArrayList<String> offer_date;
//		ArrayList<String> is_offer;
//
//		ArrayList<String> photo_url1;
//		ArrayList<String> photo_url2;
//		ArrayList<String> photo_url3;
//
//
//
//		ArrayList<String>mob1;
//		ArrayList<String>mob2;
//		ArrayList<String>mob3;
//		
//		ArrayList<String>tel1;
//		ArrayList<String>tel2;
//		ArrayList<String>tel3;
//		
//		ArrayList<String>valid_till;
//
//
//
//		ArrayList<String> offer_total_like;
//		ArrayList<String> offer_total_total_share;
//
//		boolean [] isAnimEnd;   
//
//		LayoutInflater inflator;
//
//		Typeface tf;
//
//
//		public NewsFeedAdapter(Activity context, int resource,
//				int textViewResourceId,  ArrayList<String>post_id,ArrayList<String>news_title,ArrayList<String> offer_title,
//				ArrayList<String> offer_desc,ArrayList<String> offer_date,
//				ArrayList<String> is_offer,ArrayList<String> photo_url1,
//				ArrayList<String> offer_total_like,ArrayList<String> offer_total_total_share,ArrayList<String>mob1,ArrayList<String>mob2,
//				ArrayList<String>mob3,ArrayList<String>tel1,ArrayList<String>tel2,ArrayList<String>tel3,ArrayList<String>valid_till) {
//			super(context, resource, textViewResourceId, offer_title);
//			
//			this.context=context;
//			inflator=context.getLayoutInflater();
//
//			this.post_id=post_id;
//			this.news_title=news_title;
//			this.offer_title=offer_title;
//			this.offer_desc=offer_desc;
//
//			this.offer_date=offer_date;
//			this.is_offer=is_offer;
//
//			this.photo_url1=photo_url1;
//			this.offer_total_like=offer_total_like;
//
//			this.offer_total_total_share=offer_total_total_share;
//			
//			this.mob1=mob1;
//			this.mob2=mob2;
//			this.mob3=mob3;
//			
//			this.tel1=tel1;
//			this.tel2=tel2;
//			this.tel3=tel3;
//			
//			this.valid_till=valid_till;
//
//
//			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_title.size()+" "+offer_desc.size()+" "+offer_date.size()+" "+is_offer.size()+" "+photo_url1.size());
//			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_total_like.size()+" "+offer_total_total_share.size());
//
//			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
//
//
//		}
//
//		
//		public int getCount() {
//			
//			return offer_title.size();
//		}
//
//		
//		public String getItem(int position) {
//			
//			return offer_title.get(position);
//		}
//
//		
//		public View getView(final int position, View convertView, ViewGroup parent) {
//			
//			Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_in);
//			if(convertView==null)
//			{
//				ViewHolder holder=new ViewHolder();
//				convertView=inflator.inflate(R.layout.newsfeedrow,null);
//
//				holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
//				//				holder.newsFeedOffersMonth=(TextView)convertView.findViewById(R.id.newsFeedOffersMonth);
//				//
//				//				holder.newsFeedviewOverLay=(View)convertView.findViewById(R.id.newsFeedviewOverLay);
//
//				holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
//				holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);
//
//				holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
//				holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);
//
//				holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);
//
//				holder.newFeedband=(TextView)convertView.findViewById(R.id.newFeedband);
//
//				holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);
//
//				holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);
//
//				holder.newsFeedOffersText.setTypeface(tf);
//				holder.newsFeedOffersDesc.setTypeface(tf);
//				holder.newsFeedTotalLike.setTypeface(tf);
//
//				holder.newsFeedTotalShare.setTypeface(tf);
//				holder.newFeedband.setTypeface(tf);
//
//				holder.newsFeedStoreName.setTypeface(tf);
//
//
//				convertView.setTag(holder);
//			}
//
//			ViewHolder hold=(ViewHolder)convertView.getTag();
//
//
//
//			try
//			{
//				hold.newsFeedStoreName.setText(news_title.get(position));
//				hold.newsFeedOffersText.setText(offer_title.get(position));
//				hold.newsFeedOffersDesc.setText(offer_desc.get(position));
//
//				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
//				DateFormat dt2=new SimpleDateFormat("MMM");
//				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//
//				Date date_con = (Date) dt.parse(offer_date.get(position).toString());
//
//				Date date=dateFormat.parse(offer_date.get(position).toString());
//				DateFormat date_format=new SimpleDateFormat("hh:mm a");
//
//				Calendar cal = Calendar.getInstance();
//				cal.setTime(date_con);
//				int year = cal.get(Calendar.YEAR);
//				int month = cal.get(Calendar.MONTH);
//				int day = cal.get(Calendar.DAY_OF_MONTH);
//
//				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
//				hold.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));
//
//				//				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
//				//				DateFormat dt2=new SimpleDateFormat("MMM");
//				//				Date date_con = (Date) dt.parse(offer_date.get(position).toString());
//				//				Calendar cal = Calendar.getInstance();
//				//				cal.setTime(date_con);
//				//				int year = cal.get(Calendar.YEAR);
//				//				int month = cal.get(Calendar.MONTH);
//				//				int day = cal.get(Calendar.DAY_OF_MONTH);
//				//
//				//				//	Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
//				//				hold.newsFeedOffersDate.setText(day+"");
//				//				hold.newsFeedOffersMonth.setText(dt2.format(date_con)+"");
//
//
//				// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
//				// {
//				// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
//				// }
//				// else
//				// {
//				// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
//				// }
//				hold.newsFeedTotalLike.setText(offer_total_like.get(position));
//
//
//				hold.newsFeedTotalShare.setText(offer_total_total_share.get(position));
//
//				hold.newsFeedTotalLike.setOnClickListener(new OnClickListener() {
//
//					
//					public void onClick(View arg0) {
//		
//						//							showToast("Clicked On "+news_title.get(position));
//						likePost(post_id.get(position));
//					}
//				});
//				hold.newsFeedTotalShare.setOnClickListener(new OnClickListener() {
//					
//					public void onClick(View arg0) {
//		
//						TEMP_POST_ID=post_id.get(position);
//
//						/*
//						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//						alertDialogBuilder.setTitle("Share");
//						alertDialogBuilder
//						.setMessage("Share using")
//						.setIcon(R.drawable.ic_launcher)
//						.setCancelable(true)
//						.setPositiveButton("Other",new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,int id) {
//
//								dialog.dismiss();
//							
//								List<Intent> targetedShareIntents = new ArrayList<Intent>();
//								Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//								sharingIntent.setType("text/plain");
//								String shareBody = news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position);
//
//								PackageManager pm = context.getPackageManager();
//								List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);
//
//								for(final ResolveInfo app : activityList) {
//
//									String packageName = app.activityInfo.packageName;
//									Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
//									targetedShareIntent.setType("text/plain");
//
//									if(TextUtils.equals(packageName, "com.facebook.katana")){
//										
//									} else {
//										targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//										targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, news_title.get(position) + " - " + "Offers");
//										targetedShareIntent.setPackage(packageName);
//										targetedShareIntents.add(targetedShareIntent);
//									}
//
//								}
//
//								Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
//								chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
//								startActivityForResult(chooserIntent,6);
//
//							}
//						})
//						.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,int id) {
//								dialog.dismiss();
//								
//								if(photo_url1.get(position).toString().length()!=0)
//								{
//									String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
//
//									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
//									.setLink("http://shoplocal.co.in")
//									.setName(news_title.get(position) + " - " + "Offers")
//									.setPicture(PHOTO_PARENT_URL+photo_source)
//									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
//									.build();
//									uiHelper.trackPendingDialogCall(shareDialog.present());
//								}
//								else
//								{
//									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
//									.setLink("http://shoplocal.co.in")
//									.setName(news_title.get(position) + " - " + "Offers")
//									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
//									.build();
//									uiHelper.trackPendingDialogCall(shareDialog.present());
//								}
//							}
//						})
//						;
//
//						AlertDialog alertDialog = alertDialogBuilder.create();
//
//						alertDialog.show();*/
//						
//					
//						showDialog(position);
//
//
//					}});
//
//
//				hold.newFeedband.setVisibility(View.GONE);
//				hold.newsIsOffer.setVisibility(View.VISIBLE);
//
//				hold.newsFeedStoreName.startAnimation(animation);
//				hold.newsFeedOffersText.startAnimation(animation);
//				hold.newsFeedOffersDesc.startAnimation(animation);
//				hold.newsFeedOffersDate.startAnimation(animation);
//				hold.newsFeedTotalLike.startAnimation(animation);
//				hold.newsFeedTotalShare.startAnimation(animation);
//
//				if(is_offer.get(position).toString().length()!=0 && is_offer.get(position).equalsIgnoreCase("1"))
//				{
//					hold.newsIsOffer.setVisibility(View.VISIBLE);
//
//					//hold.newFeedband.setText("Specail Offer : "+is_offer.get(position));
//					/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
//					hold.txtStore.setTextColor(Color.WHITE);*/
//
//				}
//				else
//				{
//					//hold.txtBackOffers.setText("offers");
//					hold.newFeedband.setVisibility(View.GONE);
//					hold.newsIsOffer.setVisibility(View.INVISIBLE);
//					/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
//					hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
//					hold.imgNewsFeedLogo.startAnimation(animation);
//				}
//
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//
//			try
//			{
//				hold.imgNewsFeedLogo.setVisibility(View.VISIBLE);
//				if(photo_url1.get(position).toString().length()==0)
//				{
//					//Log.i("LOGO", "LOGO "+logo.get(position));
//					//					hold.imgNewsFeedLogo.setVisibility(View.GONE);
//					hold.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
//					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_CENTER);
//					//					imageLoaderList.DisplayImage("",hold.imgNewsFeedLogo);
//					//					hold.newsFeedOffersDate.setTextColor(Color.argb(255, 255, 255, 255));
//					//					hold.newsFeedviewOverLay.setVisibility(View.INVISIBLE);
//				}
//				else
//				{
//					String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
//					//					hold.newsFeedOffersDate.setTextColor(Color.argb(200, 255, 255, 255));
//					//					hold.newsFeedviewOverLay.setVisibility(View.VISIBLE);
//					/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgNewsFeedLogo);*/
//					try{
//						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
//						.placeholder(R.drawable.ic_place_holder)
//						.error(R.drawable.ic_place_holder)
//						.into(hold.imgNewsFeedLogo);
//					}
//					catch(IllegalArgumentException illegalArg){
//						illegalArg.printStackTrace();
//					}
//					catch(Exception e){ 
//						e.printStackTrace();
//					}
//					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_XY);
//					hold.imgNewsFeedLogo.startAnimation(animation);
//				}
//
//				//				Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//				//				convertView.startAnimation(animation);
//				//				lastPosition = position;
//
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//
//
//
//			return convertView;
//		}
//		
//		void showDialog(final int tempPos){
//			try
//			{
//				dialogShare.show();
//
//				listViewShare.setOnItemClickListener(new OnItemClickListener() {
//
//					
//					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//							long arg3) {
//
//						String app = appName.get(position);
//						
//						
//						
//						String shareBody= "";
//						
//						String validTill="";
//						
//						String contact="";
//						
//						String contact_lable="\nContact:";
//						
//						Log.i("Is Offer ","Is Offer valid_till "+valid_till.get(tempPos));
//						
//						if(is_offer.get(tempPos).toString().length()!=0 && is_offer.get(tempPos).equalsIgnoreCase("1"))
//						{
//							try
//							{
//								DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
//								DateFormat dt2=new SimpleDateFormat("MMM");
//								DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//
//								Date date_con = (Date) dt.parse(valid_till.get(tempPos).toString());
//
//								Date date=dateFormat.parse(valid_till.get(tempPos).toString());
//								DateFormat date_format=new SimpleDateFormat("hh:mm a");
//
//								Calendar cal = Calendar.getInstance();
//								cal.setTime(date_con);
//								int year = cal.get(Calendar.YEAR);
//								int month = cal.get(Calendar.MONTH);
//								int day = cal.get(Calendar.DAY_OF_MONTH);
//								
//								validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);
//								
//							}catch(Exception ex)
//							{
//								ex.printStackTrace();
//								validTill="";
//							}
//							/*validTill="\nValid Till:"+valid_till.get(tempPos);*/
//						}
//						else
//						{
//							validTill="";
//						}
//						
//						if(mob1.get(tempPos).toString().length()>0)
//						{
//							contact="+"+mob1.get(tempPos).toString();
//						} 
//						if(mob2.get(tempPos).toString().length()>0)
//						{
//							contact+=" +"+mob2.get(tempPos).toString();
//						}
//						if(mob3.get(tempPos).toString().length()>0)
//						{
//							contact+=" +"+mob3.get(tempPos).toString();
//						}
//						if(tel1.get(tempPos).toString().length()>0)
//						{
//							contact+=" "+tel1.get(tempPos).toString();
//						}
//						if(tel2.get(tempPos).toString().length()>0)
//						{
//							contact+=" "+tel1.get(tempPos).toString();
//						}
//						if(tel3.get(tempPos).toString().length()>0)
//						{
//							contact+=" "+tel1.get(tempPos).toString();
//						}
//						
//						if(contact.length()==0)
//						{
//							contact_lable="";
//						}
//						
//						
//						shareBody=news_title.get(tempPos) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(tempPos) + "\n" + offer_desc.get(tempPos)+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
//						
//						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){
//
//							if(photo_url1.get(tempPos).toString().length()!=0)
//							{
//								String photo_source=photo_url1.get(tempPos).toString().replaceAll(" ", "%20");
//
//								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
//								.setLink("http://shoplocal.co.in")
//								.setName(news_title.get(tempPos) + " - " + "Offers")
//								.setPicture(PHOTO_PARENT_URL+photo_source)
//								.setDescription(shareBody)
//								.build();
//								uiHelper.trackPendingDialogCall(shareDialog.present());
//								dismissDialog();
//							}
//							else
//							{
//								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
//								.setLink("http://shoplocal.co.in")
//								.setName(news_title.get(tempPos) + " - " + "Offers")
//								.setDescription(shareBody)
//								.build();
//								uiHelper.trackPendingDialogCall(shareDialog.present());
//								dismissDialog();
//							}
//						}
//						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
//							Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
//						}
//
//						else if(!app.equalsIgnoreCase("facebook")){
//							Intent i = new Intent(Intent.ACTION_SEND);
//							i.setPackage(packageNames.get(position));
//							i.setType("text/plain");
//							i.putExtra(Intent.EXTRA_SUBJECT, news_title.get(tempPos));
//							i.putExtra(Intent.EXTRA_TEXT, shareBody);
//							startActivityForResult(i,6);
//						}
//
//						dismissDialog();
//
//					}
//				});
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//		}
//		
//		void dismissDialog(){
//			dialogShare.dismiss();
//		}
//
//
//
//	}
//
//	static class ViewHolder
//	{
//		TextView newsFeedOffersDate;
//		TextView newsFeedOffersMonth;
//
//		TextView newsFeedOffersText;
//		TextView newsFeedOffersDesc;
//
//		TextView newsFeedTotalLike;
//		TextView newsFeedTotalShare;
//
//		ImageView imgNewsFeedLogo;
//
//		View newsFeedviewOverLay;
//
//		TextView newFeedband;
//
//		ImageView newsIsOffer;
//
//		TextView newsFeedStoreName;
//
//	}
//
//	void likePost(String POST_ID)
//	{
//		if(network.isNetworkAvailable())
//		{
//			if(session.isLoggedInCustomer())
//			{
//				Intent intent=new Intent(context, FavouritePostService.class);
//				intent.putExtra("favouritePost",mFav);
//				intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
//				intent.putExtra("api_header",API_HEADER);
//				intent.putExtra("api_header_value", API_VALUE);
//				intent.putExtra("user_id", USER_ID);
//				intent.putExtra("auth_id", AUTH_ID);
//				intent.putExtra("post_id", POST_ID);
//				context.startService(intent);
//				newsFeedCategListProg.setVisibility(View.VISIBLE);
//			}
//			else
//			{
//				loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
//			}
//		}
//		else
//		{
//			showToast(getResources().getString(R.string.noInternetConnection));
//		}
//	}
//	private void callShareApi(String POST_ID) {
//		
//
//		Intent intent = new Intent(context, ShareService.class);
//		intent.putExtra("shareService", shareServiceResult);
//		intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
//		intent.putExtra("api_header",API_HEADER);
//		intent.putExtra("api_header_value", API_VALUE);
//		intent.putExtra("user_id", USER_ID);
//		intent.putExtra("auth_id", AUTH_ID);
//		intent.putExtra("post_id", POST_ID);
//		intent.putExtra("via", VIA);
//		context.startService(intent);
//
//
//	}
//
//	void loginAlert(String msg,final int like)
//	{
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//		alertDialogBuilder.setTitle("Discover");
//		alertDialogBuilder
//		.setMessage(msg)
//		.setIcon(R.drawable.ic_launcher)
//		.setCancelable(false)
//		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog,int id) {
//
//				dialog.dismiss();
//				if(like==POST_LIKE)
//				{
//					Intent intent=new Intent(context,LoginSignUpCustomer.class);
//					context.startActivityForResult(intent,2);
//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
//				}
//
//			}
//		})
//		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog,int id) {
//				dialog.dismiss();
//			}
//		})
//		;
//
//		AlertDialog alertDialog = alertDialogBuilder.create();
//
//		alertDialog.show();
//	}
//
//	void clearArray()
//	{
//		NEWS_FEED_ID.clear();
//		NEWS_FEED_NAME.clear();
//		NEWS_FEED_AREA_ID.clear();
//
//		NEWS_FEED_MOBNO1.clear();
//		NEWS_FEED_MOBNO2.clear();
//		NEWS_FEED_MOBNO3.clear();
//
//
//		NEWS_FEED_TELLNO1.clear();
//		NEWS_FEED_TELLNO2.clear();
//		NEWS_FEED_TELLNO3.clear();
//
//		NEWS_FEED_LATITUDE.clear();
//		NEWS_FEED_LONGITUDE.clear();
//		NEWS_FEED_DISTANCE.clear();
//
//		NEWS_FEED_POST_ID.clear();
//		NEWS_FEED_POST_TYPE.clear();
//
//		NEWS_FEED_POST_TITLE.clear();
//		NEWS_FEED_DESCRIPTION.clear();
//
//
//		NEWS_FEED_image_url1.clear();
//		NEWS_FEED_image_url2.clear();
//		NEWS_FEED_image_url3.clear();
//
//		NEWS_FEED_thumb_url1.clear();
//		NEWS_FEED_thumb_url2.clear();
//		NEWS_FEED_thumb_url3.clear();
//
//		NEWS_DATE.clear();
//		NEWS_IS_OFFERED.clear();
//		NEWS_OFFER_DATE_TIME.clear();
//
//		NEWS_FEED_place_total_like.clear();
//		NEWS_FEED_place_total_share.clear();
//
//		page=0;
//		post_count=20;
//
//
//	}
//
//
//
//	
//	public void postLikeReuslt(int resultCode, Bundle resultData) {
//		
//		newsFeedCategListProg.setVisibility(View.GONE);
//		String postLikestatus=resultData.getString("postLikestatus");
//		String postLikemsg=resultData.getString("postLikemsg");
//		if(postLikestatus.equalsIgnoreCase("true"))
//		{
//			showToast(postLikemsg);
//			try
//			{
//
//			}catch(Exception ex)
//			{
//				ex.printStackTrace();
//			}
//		}
//		else
//		{
//			showToast(postLikemsg);
//			//			if(postLikemsg.startsWith(getResources().getString(R.string.invalidAuth)))
//			//			{
//			//				session.logoutCustomer();
//			//			}
//			String error_code=resultData.getString("error_code");
//			if(error_code.equalsIgnoreCase("-221"))
//			{
//				session.logoutCustomer();
//			}
//			else if(postLikemsg.startsWith(getResources().getString(R.string.connectionTimedOut)))
//			{
//
//			}
//			else
//			{
//
//			}
//
//		}
//	}
//
//
//	
//	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
//		
//		String status = resultData.getString("share_status");
//
//		String msg = resultData.getString("share_msg");
//		try {
//
//			if(status.equalsIgnoreCase("true")){
//
//				showToast(msg);
//			}
//			else{
//
//				showToast(msg);
//			}
//
//		} catch (Exception e) {
//			
//
//			e.printStackTrace();
//		}
//	}
//
//	
//	protected void onActivityResult(final int requestCode, final int resultCode, Intent data) {
//		
//		super.onActivityResult(requestCode, resultCode, data);
//		if(session.isLoggedInCustomer())
//		{
//			HashMap<String,String>user_details=session.getUserDetailsCustomer();
//			USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
//			AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
//		}
//		if(requestCode==6)
//		{
//			VIA="Android";
//			callShareApi(TEMP_POST_ID);
//		}
//
//		try
//		{
//			uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
//				
//				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
//					Log.e("Activity", String.format("Error: %s", error.toString()));
//				}
//
//				
//				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
//					Log.i("Activity", "Success!");
//					VIA="Android-Facebook";
//					callShareApi(TEMP_POST_ID);
//				}
//			});
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//
//	}
//
//
//	
//	protected void onResume() {
//		super.onResume();
//		uiHelper.onResume();
//	}
//
//	
//	protected void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		uiHelper.onSaveInstanceState(outState);
//	}
//
//	
//	protected void onPause() {
//		super.onPause();
//		uiHelper.onPause();
//	}
//
//	
//	public void onDestroy() {
//		super.onDestroy();
//		uiHelper.onDestroy();
//	}
//
//
//
//}
