package com.phonethics.shoplocal;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class AboutUs extends ShoplocalActivity
{

	private Activity mContext;

	private TextView mTxtAboutTabLable;
	private TextView mTxtVersion;
	private TextView mTxtShoplocalWeb;
	private TextView mTxtInfoLable;
	private TextView mTxtPrivacy;
	private TextView mTxtTerms;
	private TextView mTxtFaq;
	private Map<String, String> about= new HashMap<String, String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		mContext=this;
		hideDrawerLayout();
		setTitle(getResources().getString(R.string.itemAboutShoplocal));
		mTxtAboutTabLable=(TextView)findViewById(R.id.aboutAppLable);
		mTxtVersion=(TextView)findViewById(R.id.versionLable);
		mTxtShoplocalWeb=(TextView)findViewById(R.id.textViewShoplocal);
		mTxtInfoLable=(TextView)findViewById(R.id.infoLable);
		mTxtPrivacy=(TextView)findViewById(R.id.pref_PrivacyPolicy);
		mTxtTerms=(TextView)findViewById(R.id.pref_Terms);
		mTxtFaq=(TextView)findViewById(R.id.pref_Faq);

		mTxtAboutTabLable.setTypeface(tf);
		mTxtVersion.setTypeface(tf);
		mTxtShoplocalWeb.setTypeface(tf);
		mTxtInfoLable.setTypeface(tf);
		mTxtPrivacy.setTypeface(tf);
		mTxtTerms.setTypeface(tf);
		mTxtFaq.setTypeface(tf);
		


		try
		{
			PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
			mTxtVersion.setText("Version "+"("+pinfo.versionName+")");
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mTxtPrivacy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mContext,AboutWebView.class);
				intent.putExtra("url", "PrivacyPolicy");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				about.put("Source","Policy");
				EventTracker.logEvent("AboutShoplocal", about);
			}
		});

		mTxtTerms.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mContext,AboutWebView.class);
				intent.putExtra("url", "Terms");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				about.put("Source","T&C");
				EventTracker.logEvent("AboutShoplocal", about);
			}
		});

		mTxtFaq.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mContext,AboutWebView.class);
				intent.putExtra("url", "Faq");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				about.put("Source","FAQ");
				EventTracker.logEvent("AboutShoplocal", about);
			}
		});

		mTxtShoplocalWeb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mContext,AboutWebView.class);
				intent.putExtra("url", "Web");
				intent.putExtra("Web", "http://shoplocal.co.in/");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				about.put("Source","Website");
				EventTracker.logEvent("AboutShoplocal", about);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		this.finish();
		return true;
	}

}