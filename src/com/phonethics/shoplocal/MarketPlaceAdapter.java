package com.phonethics.shoplocal;

import java.text.DecimalFormat;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * 
 * @author manoj
 * MarketPlaceAdapter class is responsible for Pulling data from DB/Internet 
 * showing it in ListView.
 */
public class MarketPlaceAdapter extends ShoplocalAdapter implements DBListener {
	int ITEMS_PER_PAGE = 50;
	private int mPage = 0;
	private PullToRefreshListView mList;
	private TextView mTxtCounter;
	private boolean mbRefreshing = false;
	private Typeface  tf = null;
	private ProgressDialog mProgressDialog;
	private NetworkCheck mInernet;
	private ListClickListener mListener;
	
	/**
	 * MarketPlaceAdapter class is responsible for Pulling data from DB/Internet 
	 * showing it in ListView.
	 * @param activity : context of the Activity
	 * @param list : ListView to plug data into
	 * @param txtCounter : To show Item count (Currently Hidden)
	 * @param mListener : ListClick Listener
	 */
	public MarketPlaceAdapter(FragmentActivity activity, PullToRefreshListView list,TextView txtCounter,ListClickListener mListener) {
		super(activity,R.layout.layout_marketplace);
		debug("Constructor 1");
		mList = list;
		this.mTxtCounter=txtCounter;
		mInernet=new NetworkCheck(activity);
		tf=Typeface.createFromAsset(mActivity.getAssets(), "fonts/GOTHIC_0.TTF");
		this.mListener=mListener;
		mProgressDialog=new ProgressDialog(mActivity);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");
	}

	public void setmPage(int mPage) {
		this.mPage = mPage;
	}

	public boolean isMbRefreshing() {
		return mbRefreshing;
	}

	public void setMbRefreshing(boolean mbRefreshing) {
		this.mbRefreshing = mbRefreshing;
	}

	/**
	 * Query data from Database.
	 */
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		debug("onCreateLoader 2");
		String sQuery = "Select S._id,S."+DBUtil.ID+",S.logo,S.total_like,S.distance,S.has_offer,S.store_name,S.description,"
				+ "S.mob_no_1,S.mob_no_2,S.mob_no_3,S.tel_no_1,S.tel_no_2,S.tel_no_3,R.total_record,R.total_page,R.current_page"
				+" from ALL_STORES as S INNER JOIN ALL_STORES_RECORDS as R on S.area_id=R.area_id where S.area_id="+Config.MY_AREA
				+" and R.area_id="+Config.MY_AREA;
		return new ShoplocalCursorLoader(mActivity,sQuery);
	}

	/**
	 * Check if the Cursor has data or not. If not fetch data from Internet.
	 * If not notify listview to load data from cursor.
	 */
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor)  {
		debug("onFinish");
		try
		{
			if(mProgressDialog!=null){
				mProgressDialog.dismiss();
			}
			if(cursor == null || cursor.getCount() == 0) {
				String sURL = Config.MARKET_PLACE_URL + "latitude="+Config.LATITUDE + "&longitude=" + Config.LONGITUDE 
						+ "&distance="+Config.DISTANCE +"&area_id="+Config.MY_AREA 
						+ "&page=" + (mPage+1) + "&count="+ITEMS_PER_PAGE;
				if(mInernet.isNetworkAvailable()){
					getData(sURL,"",false,"MarktePlace");
				}else{
					showToast(mActivity.getResources().getString(R.string.noInternetConnection));
				}
				return;
			}
			changeCursor(cursor);
			mCursor = cursor;
			mCount = cursor.getCount();
			debug("total = "+mCount);
//			cursor.moveToNext();
//			int index=cursor.getColumnIndex("total_record");
//			mTxtCounter.setText(mCount+"/"+cursor.getString(index));
//			debug("total = " + mCount);
			notifyDataSetChanged();
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	void showToast(String msg){
		Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Plug in data to ListView from cursor.
	 */
	public void bindView(View convertView, Context context, Cursor cursor) {
		try
		{
			if(convertView == null) {
				convertView=LayoutInflater.from(mActivity).inflate(R.layout.layout_marketplace,null);
			}
			ViewHolder holder = (ViewHolder) convertView.getTag();

			if(holder == null) {
				holder=new ViewHolder();
				holder.imgPlaceLogo=(ImageView)convertView.findViewById(R.id.imgPlaceLogo);
				holder.txtStoreName=(TextView)convertView.findViewById(R.id.txtStoreName);
				holder.txtStoreDistance=(TextView)convertView.findViewById(R.id.txtStoreDistance);
				holder.txtStoreDescription=(TextView)convertView.findViewById(R.id.txtStoreDescription);
				holder.txtStoreLikeCount=(TextView)convertView.findViewById(R.id.txtStoreLikeCount);
				holder.imgOfferBand=(ImageView)convertView.findViewById(R.id.imgOfferBand);

				holder.txtStoreName.setTypeface(tf, Typeface.BOLD);
				holder.txtStoreDescription.setTypeface(tf);
				holder.txtStoreDistance.setTypeface(tf);

				convertView.setTag(holder);
			}

			int index = -1;

			int likes = 0;
			index = cursor.getColumnIndex("total_like");
			if(index > 0) {
				likes = cursor.getInt(index);	    
			}
			if(likes > 0) {
				holder.txtStoreLikeCount.setVisibility(View.VISIBLE);
				holder.txtStoreLikeCount.setText(""+likes);
			}else{
				holder.txtStoreLikeCount.setText("0");
			}

			index = cursor.getColumnIndex("distance");

			if(index > 0) {
				String sDistance = cursor.getString(index);
				holder.txtStoreDistance.setVisibility(View.VISIBLE);
				if(sDistance != null) {
					double distance = Double.parseDouble(sDistance);

					if(distance < 1 && distance >0) {
						DecimalFormat format = new DecimalFormat("##.###");
						String formatted = format.format(distance*100);
						holder.txtStoreDistance.setText(""+formatted+" m.");
					} else {
						DecimalFormat format = new DecimalFormat("##.###");
						String formatted = format.format(distance);
						if((distance*100)>8000) {
							holder.txtStoreDistance.setVisibility(View.GONE);
						} else {
							holder.txtStoreDistance.setVisibility(View.VISIBLE);
							holder.txtStoreDistance.setText(""+formatted+" km.");
						}
					}
				}
			}

			index = cursor.getColumnIndex("has_offer");
			if(index > 0) {
				int has_offer = cursor.getInt(index);
				if(has_offer == 1) {
					holder.imgOfferBand.setVisibility(View.VISIBLE);
				}else {
					holder.imgOfferBand.setVisibility(View.GONE);		    
				}
			}else {
				holder.imgOfferBand.setVisibility(View.GONE);		    
			}


			index = cursor.getColumnIndex("store_name");
			if(index > 0) {
				String sName = cursor.getString(index).trim();
				String seperator = "\\\\'";
				String []tempText = sName.split(seperator);
				
				/*if (tip.contains(seperator)){*/
		        String b= "";
		        for (String c : tempText) {
		        	b +=c+"\'";
		        }
		        b = b.substring(0, b.length()-1);
				holder.txtStoreName.setText(/*sName*/b.toUpperCase(Locale.US));
			}else {
				holder.txtStoreName.setVisibility(View.GONE);
			}

			index = cursor.getColumnIndex("description");
			if(index > 0) {
				String sDescription = cursor.getString(index);
				holder.txtStoreDescription.setText(sDescription);
			}else {
				holder.txtStoreDescription.setVisibility(View.GONE);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	
	@Override
	public View getView(final int position, View view, ViewGroup viewGroup) {
		View tmpView=super.getView(position, view, viewGroup);
		try
		{
		ImageView imgCall=(ImageView)tmpView.findViewById(R.id.imgPlaceLogo);
		imgCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showCall(position);
			}
		});
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return tmpView;
	}
	
	void showCall(int position){
		mListener.listItemClick(position, "", "call");
	}

	/**
	 * When network call completes successfully onComplete(..) gets triggered.
	 * Parse data/insert into database.
	 */
	public void onCompleted(String sURL, String sJson,String tag) {
		debug(sURL);
		debug(sJson);
		try {
			if(mProgressDialog!=null){
				mProgressDialog.dismiss();
			}
			

			JSONObject jsonObject = new JSONObject(sJson);
			String status = jsonObject.getString("success");
			if(status == null) return;

			if(status.equals("false")){

				return;
			}

			if(isMbRefreshing()==true){
				Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from ALL_STORES_RECORDS where area_id="+Config.MY_AREA).loadInBackground();
				Cursor c=new ShoplocalCursorLoader(mActivity, "Delete from ALL_STORES where area_id="+Config.MY_AREA).loadInBackground();
				if(c!=null && c1!=null){
					debug("Deleted "+c.getCount());
					setMbRefreshing(false);
					mList.onRefreshComplete();
				}
			}

			JSONObject dataObject = jsonObject.getJSONObject("data");
			if(dataObject == null) return;

			int records = dataObject.getInt("total_record");
			int page = dataObject.getInt("total_page");

			JSONArray jsonArray = dataObject.getJSONArray("record");
			if(jsonArray == null)  return;

			int count = jsonArray.length();
			ContentValues[] values = new ContentValues[count];

			for(int i=0;i<count;i++) {
				JSONObject storeObject = jsonArray.getJSONObject(i);
				if(storeObject != null) {
					ContentValues value = new ContentValues();
					value.put("id",storeObject.getInt("id"));
					value.put("place_parent",storeObject.getInt("place_parent"));
					value.put("store_name",storeObject.getString("name"));
					value.put("description",storeObject.getString("description"));
					value.put("building",storeObject.getString("building"));
					value.put("street",storeObject.getString("street"));
					value.put("landmark",storeObject.getString("landmark"));
					value.put("area_id",storeObject.getInt("area_id"));
					value.put("area",storeObject.getString("area"));
					value.put("city",storeObject.getString("city"));

					value.put("tel_no_1",storeObject.getString("tel_no1"));
					value.put("tel_no_2",storeObject.getString("tel_no2"));
					value.put("tel_no_3",storeObject.getString("tel_no3"));

					value.put("mob_no_1",storeObject.getString("mob_no1"));
					value.put("mob_no_2",storeObject.getString("mob_no2"));
					value.put("mob_no_3",storeObject.getString("mob_no3"));

					value.put("logo",storeObject.getString("image_url"));
					value.put("email_id",storeObject.getString("email"));
					value.put("website",storeObject.getString("website"));	

					if(storeObject.has("distance")){
						value.put("distance",storeObject.getString("distance"));
						debug(storeObject.getString("distance"));
					}
					else{
						value.put("distance","0");
					}

					value.put("total_like",storeObject.getInt("total_like"));
					value.put("has_offer",storeObject.getString("has_offer"));
					value.put("title",storeObject.getString("title"));
					values[i] = value;
				}
			}

			//Inserting In RECORD_STATUS
			ContentValues [] contentValues=new ContentValues[1];
			ContentValues value=new ContentValues();
			value.put("area_id",Config.MY_AREA);
			value.put("current_page",mPage+1);
			value.put("total_record",records);
			value.put("total_page",page);
			contentValues[0]=value;

			DBUtil dbUtil = DBUtil.getInstance(mActivity.getApplicationContext());
			dbUtil.replaceOrUpdate("ALL_STORES_RECORDS", contentValues);
			dbUtil.replaceOrUpdate(this,"all_stores",values,"stores");

			//mTxtCounter.setText(mCount+"/"+records);
		}catch(Exception e) {
			e.printStackTrace();
			debug("Exception = " + e.toString());
		}

	}

	private class ViewHolder {
		ImageView imgPlaceLogo;
		ImageView imgOfferBand;
		TextView txtStoreName;
		TextView txtStoreDescription;
		TextView txtStoreDistance;
		TextView txtStoreLikeCount;
	}

	private void debug(String s) {
		Log.v("MarketPlaceAdapter " , "MarketPlaceAdapter " + s);
	}


	@Override
	public void onCompleteInsertion(String tag) {
		debug("Database callback");
//		mActivity.getSupportLoaderManager().restartLoader(R.id.MARKET_LOADER,null,this);
		mActivity.getSupportLoaderManager().getLoader(R.id.MARKET_LOADER).forceLoad();
	}

	private void showProgressDialog(){
		mProgressDialog.show();
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}

	@Override
	public void onError(int errorCode) {
		if(mProgressDialog!=null){
			mProgressDialog.dismiss();

			if(isMbRefreshing()==true){
				mList.onRefreshComplete();
				setMbRefreshing(false);
			}
			Toast.makeText(mActivity, "Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
		}
	}

}

//index = cursor.getColumnIndex("logo");
//if(index >= 0) {
//	sLogo = cursor.getString(index);
//}
//if(sLogo == null) {
//	holder.imgPlaceLogo.setImageResource(R.drawable.ic_place_holder);
//}else {
//	String sImageURL = sLogo.replaceAll(" ", "%20");
//
//	try{
//		Picasso.with(mActivity).load(Config.IMAGE_URL+sImageURL)
//		.placeholder(R.drawable.ic_place_holder)
//		.error(R.drawable.ic_place_holder)
//		.into(holder.imgPlaceLogo);
//	}
//	catch(IllegalArgumentException illegalArg){
//		illegalArg.printStackTrace();
//	}
//	catch(Exception e){ 
//		e.printStackTrace();
//	}
//}