package com.phonethics.shoplocal;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DBUtil extends SQLiteOpenHelper {
	private static Context mContext;
	private static String DATABASE_NAME = "Shoplocal.db";
	private static int 	DATABASE_VERSOIN=13;
	//	private  DbHelper 			dbHelper;
	private static SQLiteDatabase mDatabase;
	private static DBUtil mInstance = null;

	private DBListener mDbListener;

	//Tables
	private String TABLE_AREAS 						="AREAS";
	public final static String TABLE_MERCHANT_STORES	 		="MERCHANT_STORES";
	private String TABLE_MARKET_PLACE	 			="ALL_STORES";  
	private String TABLE_FAVOURITE_STORES 			="FAVOURITE_STORES";
	private String TABLE_OFFERS			 			="OFFERS";
	private String TABLE_CATEGORIES					="CATEGORIES";	
	private String TABLE_DATE_CATEGORIES 			="DATE_CATEGORIES";
	private String TABLE_MARKET_PLACE_RECORDS		="ALL_STORES_RECORDS";			
	private String TABLE_OFFERS_RECORDS				="OFFERS_RECORDS";	
	private String TABLE_FAVOURITE_RECORDS			="FAVOURITE_RECORDS";	

	private String AUTO_ID		="_id";
	public static final	String ID			="id";
	private String CATEGORY_NAME="label";
	private String PARENT_CATEGORY="parent_category";
	private String PLACE_PARENT	="place_parent";
	private String STORE_NAME	="store_name";
	private String DESCRIPTION	="description";

	private String BUILDING		="building";
	private String STREET		="street";
	private String LANDMARK		="landmark";
	private String AREA_ID		="area_id";
	private String AREA			="area";
	private String SLUG			="slug";
	private String CITY			="city";
	private String MOB_NO_1		="mob_no_1";
	private String MOB_NO_2		="mob_no_2";
	private String MOB_NO_3		="mob_no_3";

	private String TEL_NO_1		="tel_no_1";
	private String TEL_NO_2		="tel_no_2";
	private String TEL_NO_3		="tel_no_3";


	private String LOGO				="logo";
	private String DISTANCE			="distance";
	private String TOTAL_PAGES		="total_page";
	private String TOTAL_RECORDS	="total_record";
	private String CURRENT_PAGE		="current_page";
	private String TOTAL_LIKE		="total_like";
	private String VERIFIED			="verified";
	private String HAS_OFFER		="has_offer";
	private String OFFER_TITLE		="title";
	private String LOCALITY			="locality";
	private String LATITUDE     	="latitude";
	private String LONGITUDE		="longitude";
	private String PINCODE			="pincode";
	private String COUNTRY			="country";
	private String COUNTRY_CODE		="country_code";
	private String ISO_CODE			="iso_code";
	private String PUBLISHED_STATE	="published_state";
	private String ACTIVE_AREA		="active_area";
	private String STATE			="state";
	private String EMAIL			="email_id";
	private String WEBSITE			="website";
	private String FACEBOOK			="facebook";
	private String TWITTER			="twitter";
	private String TOTAL_SHARE		="total_share";
	private String TOTAL_VIEW		="total_view";
	private String POST_ID			="post_id";
	private String POST_TYPE		="post_type";
	private String IMAGE_URL_1		="image_url_1";
	private String IMAGE_URL_2		="image_url_2";
	private String IMAGE_URL_3		="image_url_3";
	private String THUMB_URL_1		="thumb_url_1";
	private String THUMB_URL_2		="thumb_url_2";
	private String THUMB_URL_3		="thumb_url_3";
	private String DATE				="post_date";
	private String OFFER_DATE_TIME 	="offer_date_time";
	private String STORE_STATUS		="store_status";
	private String OFFER_DESCRIPTION="offer_description";
	private String DATE_NAME	 	= "date_name";
	private String MERCHANT_COUNT	="merchant_count";


	public static  DBUtil getInstance(Context context) {
		if(mInstance == null && context != null) {
			mInstance = new DBUtil(context);
			mContext = context;
			mDatabase = mInstance.getWritableDatabase();
			Log.i("DButil","Database is opening ");
		}
		if(mDatabase!=null && !mDatabase.isOpen())
		{
			mDatabase=mInstance.getWritableDatabase();
		}
		mInstance.debug("Database is open "+mDatabase.isOpen());
		Log.i("DBUtil", "Database instances open "+mInstance+" "+mDatabase +"Context "+context);
		return mInstance;
	}

	public void close() {
		if(mDatabase != null) {
			mDatabase.close();
		}
		mInstance = null;
		mDatabase = null;
		Log.i("DBUtil", "Database instances close "+mInstance+" "+mDatabase+" ");
	}

	public Cursor rawQuery(String sQuery,String[] selectionArgs) {
		if(mDatabase == null) {
			mDatabase = getWritableDatabase();
		}
		debug("Query "+sQuery);
		return mDatabase.rawQuery(sQuery,selectionArgs);
	}

	public boolean replaceOrUpdate(DBListener dbListener,final String sTable, ContentValues[] contentValues,String tag) {
		this.mDbListener=dbListener;

		if(mDatabase == null) {
			//			mDatabase = getWritableDatabase();
			openDB();
		}
		mDatabase.beginTransaction();
		try {
			int count = contentValues.length;
			debug(count+"");
			for (int i=0; i<count;i++) {
				ContentValues value = contentValues[i];
				long id = mDatabase.replaceOrThrow(sTable,null,value);
				debug(tag+" Insert id = " + id);
			}
			debug(sTable);
			mDatabase.setTransactionSuccessful();
			mDbListener.onCompleteInsertion(tag);
		}catch (Exception e) {
			Log.v("Exception = " , " " + e.toString());
			e.printStackTrace();
		}finally {
			mDatabase.endTransaction();
		}
		return true;
	}
	
	public ArrayList<String> getAutoIds() {
		if (mDatabase == null)
			mDatabase = getWritableDatabase();
		
		ArrayList<String> autoIds = new ArrayList<String>();
		Cursor cursor = mDatabase.rawQuery("SELECT " + AUTO_ID + " FROM " + TABLE_MERCHANT_STORES	, null);
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			autoIds.add(cursor.getString(0));
		}
		cursor.close();
		return autoIds;
	}
	
//mDatabase.insert(sTable, null, value);//
	public boolean replaceOrUpdate(final String sTable, ContentValues[] contentValues) {

		if(mDatabase == null) {
			mDatabase = getWritableDatabase();
		}
		mDatabase.beginTransaction();
		try {
			int count = contentValues.length;
			for (int i=0; i<count;i++) {
				ContentValues value = contentValues[i];
				long id = mDatabase.replaceOrThrow(sTable,null,value);
				debug("Insert id = " + id);
			}
			mDatabase.setTransactionSuccessful();
		}catch (Exception e) {
			Log.v("Exception = " , " " + e.toString());
		}finally {
			mDatabase.endTransaction();
		}
		return true;
	}

	private void debug(String s) {
		Log.v("DBUtil", "DBUtil " + s);
	}


	public String getTABLE_AREAS() {
		return TABLE_AREAS;
	}

	public String getTABLE_MERCHANT_STORES() {
		return TABLE_MERCHANT_STORES;
	}

	public String getTABLE_MARKET_PLACE() {
		return TABLE_MARKET_PLACE;
	}

	public String getTABLE_FAVOURITE_STORES() {
		return TABLE_FAVOURITE_STORES;
	}

	public String getTABLE_OFFERS() {
		return TABLE_OFFERS;
	}

	public String getTABLE_DATE_CATEGORIES() {
		return TABLE_DATE_CATEGORIES;
	}

	public String getAUTO_ID() {
		return AUTO_ID;
	}

	public String getID() {
		return ID;
	}

	public String getPLACE_PARENT() {
		return PLACE_PARENT;
	}

	public String getSTORE_NAME() {
		return STORE_NAME;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public String getBUILDING() {
		return BUILDING;
	}

	public String getSTREET() {
		return STREET;
	}

	public String getLANDMARK() {
		return LANDMARK;
	}

	public String getAREA_ID() {
		return AREA_ID;
	}

	public String getAREA() {
		return AREA;
	}

	public String getCITY() {
		return CITY;
	}

	public String getMOB_NO_1() {
		return MOB_NO_1;
	}

	public String getMOB_NO_2() {
		return MOB_NO_2;
	}

	public String getMOB_NO_3() {
		return MOB_NO_3;
	}

	public String getTEL_NO_1() {
		return TEL_NO_1;
	}

	public String getTEL_NO_2() {
		return TEL_NO_2;
	}

	public String getTEL_NO_3() {
		return TEL_NO_3;
	}

	public String getLOGO() {
		return LOGO;
	}

	public String getDISTANCE() {
		return DISTANCE;
	}

	public String getTOTAL_PAGES() {
		return TOTAL_PAGES;
	}

	public String getTOTAL_RECORDS() {
		return TOTAL_RECORDS;
	}

	public String getCURRENT_PAGE() {
		return CURRENT_PAGE;
	}

	public String getTOTAL_LIKE() {
		return TOTAL_LIKE;
	}

	public String getVERIFIED() {
		return VERIFIED;
	}

	public String getHAS_OFFER() {
		return HAS_OFFER;
	}

	public String getOFFER_TITLE() {
		return OFFER_TITLE;
	}

	public String getLOCALITY() {
		return LOCALITY;
	}

	public String getLATITUDE() {
		return LATITUDE;
	}

	public String getLONGITUDE() {
		return LONGITUDE;
	}

	public String getPINCODE() {
		return PINCODE;
	}

	public String getCOUNTRY() {
		return COUNTRY;
	}

	public String getCOUNTRY_CODE() {
		return COUNTRY_CODE;
	}

	public String getISO_CODE() {
		return ISO_CODE;
	}

	public String getPUBLISHED_STATE() {
		return PUBLISHED_STATE;
	}

	public String getACTIVE_AREA() {
		return ACTIVE_AREA;
	}

	public String getSTATE() {
		return STATE;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public String getWEBSITE() {
		return WEBSITE;
	}

	public String getFACEBOOK() {
		return FACEBOOK;
	}

	public String getTWITTER() {
		return TWITTER;
	}

	public String getTOTAL_SHARE() {
		return TOTAL_SHARE;
	}

	public String getTOTAL_VIEW() {
		return TOTAL_VIEW;
	}

	public String getPOST_ID() {
		return POST_ID;
	}

	public String getPOST_TYPE() {
		return POST_TYPE;
	}

	public String getIMAGE_URL_1() {
		return IMAGE_URL_1;
	}

	public String getIMAGE_URL_2() {
		return IMAGE_URL_2;
	}

	public String getIMAGE_URL_3() {
		return IMAGE_URL_3;
	}

	public String getTHUMB_URL_1() {
		return THUMB_URL_1;
	}

	public String getTHUMB_URL_2() {
		return THUMB_URL_2;
	}

	public String getTHUMB_URL_3() {
		return THUMB_URL_3;
	}

	public String getDATE() {
		return DATE;
	}

	public String getOFFER_DATE_TIME() {
		return OFFER_DATE_TIME;
	}

	public String getSTORE_STATUS() {
		return STORE_STATUS;
	}

	public String getOFFER_DESCRIPTION() {
		return OFFER_DESCRIPTION;
	}

	public String getDATE_NAME() {
		return DATE_NAME;
	}

	public DBUtil(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSOIN);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		//TABLE All STores RECORDS 
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_MARKET_PLACE_RECORDS+" ("+
				AREA_ID+" INTEGER PRIMARY KEY, "+
				CURRENT_PAGE+" INTEGER, "+
				TOTAL_RECORDS+" INTEGER, "+
				TOTAL_PAGES+" INTEGER );");

		//TABLE All STores RECORDS 
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_CATEGORIES+" ("+
				ID+" INTEGER PRIMARY KEY, "+
				CATEGORY_NAME+" TEXT, "+
				PARENT_CATEGORY+" INTEGER );");

		//TABLE Offers RECORDS 
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_OFFERS_RECORDS+" ("+
				AREA_ID+" INTEGER PRIMARY KEY, "+
				CURRENT_PAGE+" INTEGER, "+
				TOTAL_RECORDS+" INTEGER, "+
				TOTAL_PAGES+" INTEGER );");

		//TABLE Favourites RECORDS 
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_FAVOURITE_RECORDS+" ("+
				AREA_ID+" INTEGER PRIMARY KEY, "+
				CURRENT_PAGE+" INTEGER, "+
				TOTAL_RECORDS+" INTEGER, "+
				TOTAL_PAGES+" INTEGER );");



		//Favourite Store
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_FAVOURITE_STORES +" ("+
				AUTO_ID  +" INTEGER PRIMARY KEY AUTOINCREMENT , "+
				ID		+" INTEGER NOT NULL UNIQUE, "+
				PLACE_PARENT +" INTEGER,  "+
				STORE_NAME 	+" TEXT, "+
				DESCRIPTION 	+" TEXT, "+
				BUILDING 	+" TEXT, "+
				STREET 		+" TEXT, "+
				LANDMARK 		+" TEXT, "+
				AREA_ID 		+" INTEGER, "+
				AREA 	+" TEXT, "+
				CITY 	+" TEXT, "+
				MOB_NO_1 	+" TEXT, "+
				MOB_NO_2 	+" TEXT, "+
				MOB_NO_3 	+" TEXT, "+
				TEL_NO_1 	+" TEXT, "+
				TEL_NO_2 	+" TEXT, "+
				TEL_NO_3 	+" TEXT, "+
				LOGO 	+" TEXT, "+
				DISTANCE 	+" TEXT, "+
				EMAIL +" TEXT, "+
				WEBSITE +" TEXT, "+
				TOTAL_LIKE +" INTEGER, "+
				VERIFIED +" TEXT); ");	

		//Market Place
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_MARKET_PLACE +" ("+
				AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
				ID		+" INTEGER NOT NULL UNIQUE, "+
				PLACE_PARENT+" INTEGER,  "+
				STORE_NAME 	+" TEXT, "+
				DESCRIPTION +" TEXT, "+
				BUILDING 	+" TEXT, "+
				STREET 		+" TEXT, "+
				LANDMARK 	+" TEXT, "+
				AREA_ID		+" INTEGER,"+
				AREA 		+" TEXT, "+
				CITY 		+" TEXT, "+
				MOB_NO_1 	+" TEXT, "+
				MOB_NO_2 	+" TEXT, "+
				MOB_NO_3 	+" TEXT, "+
				TEL_NO_1 	+" TEXT, "+
				TEL_NO_2 	+" TEXT, "+
				TEL_NO_3 	+" TEXT, "+
				LOGO 		+" TEXT, "+
				DISTANCE 	+" TEXT, "+
				HAS_OFFER 	+" TEXT, "+
				EMAIL +" TEXT, "+
				WEBSITE +" TEXT, "+
				OFFER_TITLE +" TEXT, "+
				TOTAL_LIKE  +" INTEGER,"+
				VERIFIED 	+" TEXT);");	



		//Merchants Store
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_MERCHANT_STORES +" ("+
				AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
				ID		+" INTEGER NOT NULL , "+ /*UNIQUE*/
				PLACE_PARENT +" INTEGER,  "+
				STORE_NAME 	+" TEXT, "+
				DESCRIPTION 	+" TEXT, "+
				BUILDING 	+" TEXT, "+
				STREET 		+" TEXT, "+
				LANDMARK 		+" TEXT, "+
				AREA 	+" TEXT, "+
				PINCODE 	+" TEXT, "+
				CITY 	+" TEXT, "+
				STATE 	+" TEXT, "+
				COUNTRY 	+" TEXT, "+
				LATITUDE 	+" TEXT, "+
				LONGITUDE 	+" TEXT, "+
				TEL_NO_1 	+" TEXT, "+
				TEL_NO_2 +" TEXT, "+
				TEL_NO_3 +" TEXT, "+
				MOB_NO_1 	+" TEXT, "+
				MOB_NO_2 +" TEXT, "+
				MOB_NO_3 +" TEXT, "+
				LOGO 	+" TEXT, "+
				EMAIL +" TEXT, "+
				WEBSITE +" TEXT, "+
				FACEBOOK 	+" TEXT, "+
				TWITTER +" TEXT, "+
				STORE_STATUS +" TEXT, "+
				PUBLISHED_STATE 	+" TEXT, "+
				TOTAL_LIKE +" TEXT, "+
				TOTAL_SHARE +" TEXT, "+
				TOTAL_VIEW +" TEXT);");

		//Offers Database
		db.execSQL("CREATE TABLE IF NOT EXISTS "+
				TABLE_OFFERS +" ("+
				AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
				ID		+" INTEGER , "+
				AREA_ID	+" INTEGER , "+
				STORE_NAME +" TEXT,  "+
				MOB_NO_1 	+" TEXT, "+
				MOB_NO_2 +" TEXT, "+
				MOB_NO_3 +" TEXT, "+
				TEL_NO_1 	+" TEXT, "+
				TEL_NO_2 +" TEXT, "+
				TEL_NO_3 +" TEXT, "+
				LATITUDE 	+" TEXT, "+
				LONGITUDE 	+" TEXT, "+
				DISTANCE 	+" TEXT, "+
				POST_ID 	+" INTEGER NOT NULL UNIQUE, "+
				POST_TYPE 	+" TEXT, "+
				OFFER_TITLE 	+" TEXT, "+
				DESCRIPTION 	+" TEXT, "+
				IMAGE_URL_1 	+" TEXT, "+
				IMAGE_URL_2 	+" TEXT, "+
				IMAGE_URL_3 	+" TEXT, "+
				THUMB_URL_1 	+" TEXT, "+
				THUMB_URL_2 	+" TEXT, "+
				THUMB_URL_3 	+" TEXT, "+
				DATE 				+" TEXT, "+
				HAS_OFFER 		+" TEXT, "+
				OFFER_DATE_TIME 	+" TEXT, "+
				TOTAL_LIKE 	+" TEXT, "+
				VERIFIED	+" TEXT, "+
				TOTAL_SHARE +" TEXT);");	


		//Area
		db.execSQL("CREATE TABLE IF NOT EXISTS " +
				TABLE_AREAS +" ("+
				AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
				AREA_ID + " INTEGER NOT NULL , " +/*UNIQUE*/
				AREA + " TEXT, " +
				SLUG + " TEXT, " +
				LOCALITY + " TEXT, " +
				LATITUDE + " TEXT, " +
				LONGITUDE + " TEXT, " +
				PINCODE + " TEXT, " +
				CITY + " TEXT, " +
				STATE + " TEXT, " +
				COUNTRY + " TEXT, " +
				COUNTRY_CODE + " TEXT, " +
				ISO_CODE + " TEXT, " +
				ID + " TEXT, " +
				MERCHANT_COUNT + " TEXT, "+
				PUBLISHED_STATE + " INTEGER, " +
				ACTIVE_AREA + " INTEGER);");

		//Date Categories
		db.execSQL("CREATE TABLE IF NOT EXISTS " +
				TABLE_DATE_CATEGORIES + " (" +
				AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
				ID + " TEXT, " +
				DATE_NAME + " TEXT);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i("Database", "DATABASE UPDATED to "+DATABASE_VERSOIN);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_MARKET_PLACE_RECORDS);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_OFFERS_RECORDS);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_FAVOURITE_RECORDS);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_MARKET_PLACE);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_FAVOURITE_STORES);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_AREAS);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_DATE_CATEGORIES);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_MERCHANT_STORES);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_OFFERS);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_CATEGORIES);
		onCreate(db);
	}

	public  DBUtil open() throws SQLException
	{
		mDatabase=getWritableDatabase();
		return this;

	}

	public void closeDb()
	{
		if(mDatabase.isOpen())
		{
			Log.i("DB STATE ", "DB STATE "+mDatabase.isOpen());
			mDatabase.close();
			Log.i("DB STATE ", "DB STATE "+mDatabase.isOpen());
		}
	}


	public ArrayList<String> getData(String tableName,String columnName,int areaId)
	{
		ArrayList<String> data=new ArrayList<String>();
		try
		{
			String query="SELECT "+columnName+" FROM "+tableName+" WHERE "+AREA_ID+" = "+areaId;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				data.add(cursor.getString(cursor.getColumnIndex(columnName)));
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return data;
	}

	public ArrayList<String> getData(String tableName,String columnName)
	{
		ArrayList<String> data=new ArrayList<String>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}
			String query="SELECT "+columnName+" FROM "+tableName;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				data.add(cursor.getString(cursor.getColumnIndex(columnName)));
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return data;
	}

	public long create_geotext(int geo_txt_id,int place_parent,String geo_text_search_storename,String description,
			String geo_text_building,String geo_text_street,String geo_text_landmark,int area_id,String geo_text_area,String geo_text_city,
			String geo_text_mobno1,String geo_text_mobno2,String geo_text_mobno3,String geo_text_telno1,String geo_text_telno2,String geo_text_telno3,String geo_text_photo,String distance,int total_pages,int total_record,int current_page,String fav,String hasoffer,String offertite,int total_like,String verified)
	{
		ContentValues cv=new ContentValues();
		cv.put(ID, geo_txt_id);
		cv.put(PLACE_PARENT, place_parent);
		cv.put(STORE_NAME, geo_text_search_storename);
		cv.put(DESCRIPTION, description);
		cv.put(BUILDING, geo_text_building);
		cv.put(STREET, geo_text_street);
		cv.put(LANDMARK, geo_text_landmark);
		cv.put(AREA_ID, area_id);
		cv.put(AREA, geo_text_area);
		cv.put(CITY, geo_text_city);
		cv.put(MOB_NO_1, geo_text_mobno1);
		cv.put(MOB_NO_2, geo_text_mobno2);
		cv.put(MOB_NO_3, geo_text_mobno3);
		cv.put(TEL_NO_1, geo_text_telno1);
		cv.put(TEL_NO_2, geo_text_telno2);
		cv.put(TEL_NO_3, geo_text_telno3);
		cv.put(LOGO, geo_text_photo);
		cv.put(DISTANCE, distance);
		cv.put(TOTAL_PAGES, total_pages);
		cv.put(TOTAL_RECORDS, total_record);
		cv.put(HAS_OFFER, hasoffer);
		cv.put(OFFER_TITLE, offertite);
		cv.put(CURRENT_PAGE, current_page);
		cv.put(TOTAL_LIKE, total_like);
		cv.put(VERIFIED, verified);
		return mDatabase.insertWithOnConflict(TABLE_MARKET_PLACE, null, cv,SQLiteDatabase.CONFLICT_REPLACE);
	}

	//***************************** Favourite Table ****************************//
	public long create_geotextFav(int geo_txt_id,int place_parent,String geo_text_search_storename,String description,
			String geo_text_building,String geo_text_street,String geo_text_landmark,int area_id,String geo_text_area,String geo_text_city,
			String geo_text_mobno1,String geo_text_mobno2,String geo_text_mobno3,String geo_text_telno1,String geo_text_telno2,String geo_text_telno3,String geo_text_photo,String distance,int total_pages,int total_record,int current_page,String fav,String hasoffer,String offertite,int total_like,String verified)
	{
		ContentValues cv=new ContentValues();
		cv.put(ID, geo_txt_id);
		cv.put(PLACE_PARENT, place_parent);
		cv.put(STORE_NAME, geo_text_search_storename);
		cv.put(DESCRIPTION, description);
		cv.put(BUILDING, geo_text_building);
		cv.put(STREET, geo_text_street);
		cv.put(LANDMARK, geo_text_landmark);
		cv.put(AREA_ID, area_id);
		cv.put(AREA, geo_text_area);
		cv.put(CITY, geo_text_city);
		cv.put(MOB_NO_1, geo_text_mobno1);
		cv.put(MOB_NO_2, geo_text_mobno1);
		cv.put(MOB_NO_3, geo_text_mobno1);
		cv.put(TEL_NO_1, geo_text_mobno1);
		cv.put(MOB_NO_2, geo_text_mobno1);
		cv.put(TEL_NO_3, geo_text_mobno1);
		cv.put(LOGO, geo_text_photo);
		cv.put(DISTANCE, distance);
		cv.put(TOTAL_PAGES, total_pages);
		cv.put(TOTAL_RECORDS, total_record);
		cv.put(CURRENT_PAGE, current_page);
		cv.put(TOTAL_LIKE, total_like);
		cv.put(VERIFIED, verified);
		return mDatabase.insertWithOnConflict(TABLE_FAVOURITE_STORES, null, cv,SQLiteDatabase.CONFLICT_REPLACE);
	}


	/*
	 * Merchants Store
	 *
	 */
	public long create_place_chooser(int PLACE_KEY_PLACE_ID,int PLACE_KEY_PLACE_PARENT,String PLACE_KEY_PLACE_NAME,String PLACE_KEY_PLACE_DESCRIPTION,String PLACE_KEY_PLACE_BUILDING,
			String PLACE_KEY_PLACE_STREET,String PLACE_KEY_PLACE_LANDMARK,String PLACE_KEY_PLACE_AREA,String PLACE_KEY_PLACE_PINCODE,String PLACE_KEY_PLACE_CITY,String PLACE_KEY_PLACE_STATE,String PLACE_KEY_PLACE_COUNTRY,
			String PLACE_KEY_PLACE_LATITUDE,String PLACE_KEY_PLACE_LONGITUDE,String PLACE_KEY_PLACE_TELLNO1,String PLACE_KEY_PLACE_TELLNO2,String PLACE_KEY_PLACE_TELLNO3,String PLACE_KEY_PLACE_MOBNO1,String PLACE_KEY_PLACE_MOBNO2,
			String PLACE_KEY_PLACE_MOBNO3,String PLACE_KEY_PLACE_image_url,String PLACE_KEY_PLACE_email,String PLACE_KEY_PLACE_website,
			String PLACE_KEY_PLACE_facebook,String PLACE_KEY_PLACE_twitter,String PLACE_KEY_PLACE_place_status,String PLACE_KEY_PLACE_place_published,
			String PLACE_KEY_PLACE_place_total_like,String PLACE_KEY_PLACE_place_total_share,String PLACE_KEY_PLACE_place_total_view)
	{
		ContentValues cv=new ContentValues();
		cv.put(ID, PLACE_KEY_PLACE_ID);
		cv.put(PLACE_PARENT, PLACE_KEY_PLACE_PARENT);
		cv.put(STORE_NAME, PLACE_KEY_PLACE_NAME);
		cv.put(DESCRIPTION, PLACE_KEY_PLACE_DESCRIPTION);
		cv.put(BUILDING, PLACE_KEY_PLACE_BUILDING);
		cv.put(STREET, PLACE_KEY_PLACE_STREET);
		cv.put(LANDMARK, PLACE_KEY_PLACE_LANDMARK);
		cv.put(AREA, PLACE_KEY_PLACE_AREA);
		cv.put(PINCODE, PLACE_KEY_PLACE_PINCODE);
		cv.put(CITY, PLACE_KEY_PLACE_CITY);
		cv.put(STATE, PLACE_KEY_PLACE_STATE);
		cv.put(COUNTRY, PLACE_KEY_PLACE_COUNTRY);
		cv.put(LATITUDE, PLACE_KEY_PLACE_LATITUDE);
		cv.put(LONGITUDE, PLACE_KEY_PLACE_LONGITUDE);
		cv.put(TEL_NO_1, PLACE_KEY_PLACE_TELLNO1);
		cv.put(TEL_NO_2, PLACE_KEY_PLACE_TELLNO2);
		cv.put(TEL_NO_3, PLACE_KEY_PLACE_TELLNO3);
		cv.put(MOB_NO_1, PLACE_KEY_PLACE_MOBNO1);
		cv.put(MOB_NO_2, PLACE_KEY_PLACE_MOBNO2);
		cv.put(MOB_NO_3, PLACE_KEY_PLACE_MOBNO3);
		cv.put(LOGO, PLACE_KEY_PLACE_image_url);
		cv.put(EMAIL, PLACE_KEY_PLACE_email);
		cv.put(WEBSITE, PLACE_KEY_PLACE_website);
		cv.put(FACEBOOK, PLACE_KEY_PLACE_facebook);
		cv.put(TWITTER, PLACE_KEY_PLACE_twitter);
		cv.put(STORE_STATUS, PLACE_KEY_PLACE_place_status);
		cv.put(PUBLISHED_STATE, PLACE_KEY_PLACE_place_published);
		cv.put(TOTAL_LIKE, PLACE_KEY_PLACE_place_total_like);
		cv.put(TOTAL_SHARE, PLACE_KEY_PLACE_place_total_share);
		cv.put(TOTAL_VIEW, PLACE_KEY_PLACE_place_total_view);
		return mDatabase.insert(TABLE_MERCHANT_STORES, null, cv);

	}

	//Row count
	public long getMyPlaceCount()
	{
		String rawQuery="SELECT COUNT("+ID+") AS ROW_COUNT FROM "+TABLE_MERCHANT_STORES ;
		long rowcount=0;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}
		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return rowcount;
	}



	//News Feed.
	public long create_newsFeed(int place_id,int area_id,String news_place_name,String news_place_mobno1,String news_place_mobno2,String news_place_mobno3,
			String news_place_tellno1,String news_place_tellno2,String news_place_tellno3,String news_place_latitude,
			String news_place_longitude,String distance,int post_id,String type,String news_place_title,String news_place_description,
			String news_place_image_url1,String news_place_image_url2,String news_place_image_url3,String news_place_thumb_url1,String news_place_thumb_url2,
			String news_place_thumb_url3,String news_place_date,String news_place_is_offered,String news_place_offer_date_time,String news_place_total_like,
			String news_place_total_share,int news_place_total_page,int news_place_total_record,int current_page,String verified)
	{

		ContentValues cv=new ContentValues();
		cv.put(ID, place_id);
		cv.put(AREA_ID, area_id);
		cv.put(STORE_NAME, news_place_name);
		cv.put(MOB_NO_1, news_place_mobno1);
		cv.put(MOB_NO_2, news_place_mobno2);
		cv.put(MOB_NO_3, news_place_mobno3);
		cv.put(TEL_NO_1, news_place_tellno1);
		cv.put(TEL_NO_2, news_place_tellno2);
		cv.put(TEL_NO_3, news_place_tellno3);
		cv.put(LATITUDE, news_place_latitude);
		cv.put(LONGITUDE, news_place_longitude);
		cv.put(DISTANCE, distance);
		cv.put(POST_ID, post_id);
		cv.put(POST_TYPE, type);
		cv.put(OFFER_TITLE, news_place_title);
		cv.put(DESCRIPTION, news_place_description);
		cv.put(IMAGE_URL_1, news_place_image_url1);
		cv.put(IMAGE_URL_2, news_place_image_url2);
		cv.put(IMAGE_URL_3, news_place_image_url3);
		cv.put(THUMB_URL_1, news_place_thumb_url1);
		cv.put(THUMB_URL_2, news_place_thumb_url2);
		cv.put(THUMB_URL_3, news_place_thumb_url3);
		cv.put(DATE, news_place_date);
		cv.put(HAS_OFFER, news_place_is_offered);
		cv.put(OFFER_DATE_TIME, news_place_offer_date_time);
		cv.put(TOTAL_LIKE, news_place_total_like);
		cv.put(TOTAL_SHARE, news_place_total_share);
		cv.put(VERIFIED, verified);
		cv.put(TOTAL_PAGES, news_place_total_page);
		cv.put(TOTAL_RECORDS, news_place_total_record);
		cv.put(CURRENT_PAGE,current_page);
		return	mDatabase.insertWithOnConflict(TABLE_OFFERS, null, cv,SQLiteDatabase.CONFLICT_REPLACE);
	}


	//	Place names
	public ArrayList<String> getAllPlacesNames()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}
			String query="SELECT "+STORE_NAME+" FROM "+TABLE_MERCHANT_STORES;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(STORE_NAME)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	//	Place Desc
	public ArrayList<String> getAllPlacesDesc()
	{
		ArrayList<String> desc = new ArrayList<String>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}
			String query="SELECT "+DESCRIPTION+" FROM "+TABLE_MERCHANT_STORES;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				desc.add(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return desc;

	}

	private static void showToast(String msg) {
		Toast.makeText(mContext	, msg, Toast.LENGTH_SHORT).show();
	}
	//	Place ID
	public ArrayList<String> getAllPlacesIDs()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}
			String query="SELECT "+DBUtil.ID+" FROM "+TABLE_MERCHANT_STORES;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()) {
				ids.add(cursor.getString(cursor.getColumnIndex(ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}

	//	Place Photourl
	public ArrayList<String> getAllPlacesPhotoUrl()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}
			String query="SELECT "+LOGO+" FROM "+TABLE_MERCHANT_STORES;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(LOGO)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}


	// News Feed
	public ArrayList<String> getAllNewsFeedsID(int area_id)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+POST_ID+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(POST_ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}
	// New Feed Store ID
	public ArrayList<String> getAllNewsFeedsStoreID(int area_id)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+ID+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}

	// New Feed Store Name
	public ArrayList<String> getAllNewsFeedsStoreName(int area_id)
	{
		ArrayList<String> name = new ArrayList<String>();
		try
		{

			String query="SELECT "+STORE_NAME+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				name.add(cursor.getString(cursor.getColumnIndex(STORE_NAME)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return name;

	}

	// New Feed Store Area
	public ArrayList<String> getAllNewsFeedsStoreArea(int area_id)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA_ID+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getString(cursor.getColumnIndex(AREA_ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}

	// New Feed Store Lat
	public ArrayList<String> getAllNewsFeedsLat(int area_id)
	{
		ArrayList<String> lat = new ArrayList<String>();
		try
		{

			String query="SELECT "+LATITUDE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				lat.add(cursor.getString(cursor.getColumnIndex(LATITUDE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return lat;
	}

	// New Feed Store Long
	public ArrayList<String> getAllNewsFeedsLong(int area_id)
	{
		ArrayList<String> longitude = new ArrayList<String>();
		try
		{

			String query="SELECT "+LONGITUDE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				longitude.add(cursor.getString(cursor.getColumnIndex(LONGITUDE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return longitude;
	}

	// New Feed Store Distance
	public ArrayList<String> getAllNewsFeedsDistance(int area_id)
	{
		ArrayList<String> distance = new ArrayList<String>();
		try
		{

			String query="SELECT "+DISTANCE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				distance.add(cursor.getString(cursor.getColumnIndex(DISTANCE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return distance;
	}

	// New Feed Store Types
	public ArrayList<String> getAllNewsFeedsTypes(int area_id)
	{
		ArrayList<String> types = new ArrayList<String>();
		try
		{

			String query="SELECT "+POST_TYPE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				types.add(cursor.getString(cursor.getColumnIndex(POST_TYPE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return types;

	}

	//All Titles of News Feed
	public ArrayList<String> getAllNewsFeedsTitles(int area_id)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+OFFER_TITLE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(OFFER_TITLE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	//All Description of News Feed
	public ArrayList<String> getAllNewsFeedsDescs(int area_id)
	{
		ArrayList<String> desc = new ArrayList<String>();
		try
		{

			String query="SELECT "+DESCRIPTION+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				desc.add(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return desc;
	}

	//All is_Offer of News Feed
	public ArrayList<String> getAllNewsFeedsIsOffer(int area_id)
	{
		ArrayList<String> isOffered = new ArrayList<String>();
		try
		{

			String query="SELECT "+HAS_OFFER+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				isOffered.add(cursor.getString(cursor.getColumnIndex(HAS_OFFER)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return isOffered;

	}

	//All OFFER_DATE_TIME of News Feed
	public ArrayList<String> getAllNewsFeedsOfferDate(int area_id)
	{
		ArrayList<String> offer_date_Time = new ArrayList<String>();
		try
		{

			String query="SELECT "+OFFER_DATE_TIME+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				offer_date_Time.add(cursor.getString(cursor.getColumnIndex(OFFER_DATE_TIME)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return offer_date_Time;

	}

	//All DATE of News Feed
	public ArrayList<String> getAllNewsFeedsDATE(int area_id)
	{
		ArrayList<String> date = new ArrayList<String>();
		try
		{

			String query="SELECT "+DATE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				date.add(cursor.getString(cursor.getColumnIndex(DATE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return date;

	}

	//All PhotoUrl of News Feed
	public ArrayList<String> getAllNewsFeedsPhotoUrl1(int area_id)
	{
		ArrayList<String> photo1 = new ArrayList<String>();
		try
		{

			String query="SELECT "+IMAGE_URL_1+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo1.add(cursor.getString(cursor.getColumnIndex(IMAGE_URL_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo1;

	}

	//All PhotoUrl2 of News Feed
	public ArrayList<String> getAllNewsFeedsPhotoUrl2(int area_id)
	{
		ArrayList<String> photo2 = new ArrayList<String>();
		try
		{

			String query="SELECT "+IMAGE_URL_2+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo2.add(cursor.getString(cursor.getColumnIndex(IMAGE_URL_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo2;

	}

	//All PhotoUrl3 of News Feed
	public ArrayList<String> getAllNewsFeedsPhotoUrl3(int area_id)
	{
		ArrayList<String> photo3 = new ArrayList<String>();
		try
		{


			String query="SELECT "+IMAGE_URL_3+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo3.add(cursor.getString(cursor.getColumnIndex(IMAGE_URL_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo3;

	}

	//All Thumbs1 of News Feed
	public ArrayList<String> getAllNewsFeedsThumbs1(int area_id)
	{
		ArrayList<String> thumbs1 = new ArrayList<String>();
		try
		{

			String query="SELECT "+THUMB_URL_1+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				thumbs1.add(cursor.getString(cursor.getColumnIndex(THUMB_URL_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return thumbs1;

	}

	//All Thumbs2 of News Feed
	public ArrayList<String> getAllNewsFeedsThumbs2(int area_id)
	{
		ArrayList<String> thumbs2 = new ArrayList<String>();
		try
		{


			String query="SELECT "+THUMB_URL_2+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				thumbs2.add(cursor.getString(cursor.getColumnIndex(THUMB_URL_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return thumbs2;

	}

	//All Thumbs3 of News Feed
	public ArrayList<String> getAllNewsFeedsThumbs3(int area_id)
	{
		ArrayList<String> thumbs3 = new ArrayList<String>();
		try
		{


			String query="SELECT "+THUMB_URL_3+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				thumbs3.add(cursor.getString(cursor.getColumnIndex(THUMB_URL_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return thumbs3;

	}

	//All Total_like of News Feed
	public ArrayList<String> getAllNewsFeedTotalLike(int area_id)
	{
		ArrayList<String> total_like = new ArrayList<String>();
		try
		{


			String query="SELECT "+TOTAL_LIKE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_like.add(cursor.getString(cursor.getColumnIndex(TOTAL_LIKE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_like;

	}

	//All Total_share of News Feed
	public ArrayList<String> getAllNewsFeedTotalShare(int area_id)
	{
		ArrayList<String> total_share = new ArrayList<String>();
		try
		{


			String query="SELECT "+TOTAL_SHARE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_share.add(cursor.getString(cursor.getColumnIndex(TOTAL_SHARE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_share;

	}

	//All Mob1 of News Feed
	public ArrayList<String> getAllNewsFeedMob1(int area_id)
	{
		ArrayList<String> mobno1 = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_1+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobno1.add(cursor.getString(cursor.getColumnIndex(MOB_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobno1;

	}

	//All Mob2 of News Feed
	public ArrayList<String> getAllNewsFeedMob2(int area_id)
	{
		ArrayList<String> mobno1 = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_2+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobno1.add(cursor.getString(cursor.getColumnIndex(MOB_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobno1;

	}

	//All Mob2 of News Feed
	public ArrayList<String> getAllNewsFeedMob3(int area_id)
	{
		ArrayList<String> mobno1 = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_3+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobno1.add(cursor.getString(cursor.getColumnIndex(MOB_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobno1;

	}

	//All tell1 of News Feed
	public ArrayList<String> getAllNewsFeedTell1(int area_id)
	{
		ArrayList<String> telno1 = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_1+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				telno1.add(cursor.getString(cursor.getColumnIndex(TEL_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return telno1;

	}

	//All tell2 of News Feed
	public ArrayList<String> getAllNewsFeedTell2(int area_id)
	{
		ArrayList<String> telno2 = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_2+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				telno2.add(cursor.getString(cursor.getColumnIndex(TEL_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return telno2;

	}

	//All tell3 of News Feed
	public ArrayList<String> getAllNewsFeedTell3(int area_id)
	{
		ArrayList<String> telno3 = new ArrayList<String>();
		try
		{


			String query="SELECT "+TEL_NO_3+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				telno3.add(cursor.getString(cursor.getColumnIndex(TEL_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return telno3;

	}

	//All total_record of News Feed
	public Integer getAllNewsFeedTotalRecord(int area_id)
	{
		int total_record = 0;
		try
		{


			String query="SELECT "+TOTAL_RECORDS+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_record=Integer.parseInt(cursor.getString(cursor.getColumnIndex(TOTAL_RECORDS)));

			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_record;

	}

	//All total_page of News Feed
	public Integer getAllNewsFeedTotalPages(int area_id)
	{
		int total_page=0;
		try
		{

			String query="SELECT "+TOTAL_PAGES+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_page=Integer.parseInt(cursor.getString(cursor.getColumnIndex(TOTAL_PAGES)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_page;

	}

	//All current page of News Feed
	public Integer getAllNewsFeedCurrentPages(int area_id)
	{
		int current_page=0;
		try
		{

			String query="SELECT "+CURRENT_PAGE+" FROM "+TABLE_OFFERS+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				current_page=Integer.parseInt(cursor.getString(cursor.getColumnIndex(CURRENT_PAGE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return current_page;

	}


	//News feed row count
	public long getNewsFeedRowCount(int area_id)
	{
		String rawQuery="SELECT COUNT("+POST_ID+") AS ROW_COUNT FROM "+TABLE_OFFERS +" WHERE "+AREA_ID+" = "+area_id;
		long rowcount=0;

		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return rowcount;

	}

	//Update Current Page Status for News Feed

	public void updateNewsFeedsCurrentPage(int page,int area_id)
	{
		try
		{

			ContentValues cv=new ContentValues();
			cv.put(CURRENT_PAGE, page);
			mDatabase.update(TABLE_OFFERS, cv,AREA_ID+"="+area_id, null);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void updateNewsFeedsTotalPage(int page,int area_id)
	{
		try
		{

			ContentValues cv=new ContentValues();
			cv.put(TOTAL_PAGES, page);
			mDatabase.update(TABLE_OFFERS, cv,AREA_ID+"="+area_id, null);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	public void updateNewsFeedsTotalRecord(int page,int area_id)
	{
		try
		{

			ContentValues cv=new ContentValues();
			cv.put(TOTAL_RECORDS, page);
			mDatabase.update(TABLE_OFFERS, cv,AREA_ID+"="+area_id, null);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	// Delete all record by Area Id in NewsFeed Table
	void deleteNewsFeedByArea(int area_id)
	{
		try
		{


			mDatabase.delete(TABLE_OFFERS, AREA_ID+" = "+area_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}

	}




	/*
	 * Getting total records from Geo-text search
	 */

	public Integer getTotalRecords(int place_parent)
	{
		int total_records = 0;
		try
		{

			String query="SELECT "+TOTAL_RECORDS+" FROM "+TABLE_MARKET_PLACE+" WHERE "+PLACE_PARENT+" = "+place_parent;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_records=cursor.getInt(cursor.getColumnIndex(TOTAL_RECORDS));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_records;

	}

	/*
	 * Getting total no of pages from Geo-text search
	 */

	public Integer getTotalPages(int place_parent)
	{
		int total_pages = 0;
		try
		{

			String query="SELECT "+TOTAL_PAGES+" FROM "+TABLE_MARKET_PLACE+" WHERE "+PLACE_PARENT+" = "+place_parent;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_pages=cursor.getInt(cursor.getColumnIndex(TOTAL_PAGES));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_pages;

	}

	/*
	 * Getting Curren tpage from Geo-text search
	 */
	public Integer getCurentPage(int place_parent)
	{
		int current_page = 0;
		try
		{


			String query="SELECT "+CURRENT_PAGE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+PLACE_PARENT+" = "+place_parent;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				current_page=cursor.getInt(cursor.getColumnIndex(CURRENT_PAGE));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return current_page;

	}

	public ArrayList<Integer> getAllCurrentPages()
	{
		ArrayList<Integer> currentPage = new ArrayList<Integer>();
		try
		{

			String query="SELECT "+CURRENT_PAGE+" FROM "+TABLE_MARKET_PLACE;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				currentPage.add(cursor.getInt(cursor.getColumnIndex(CURRENT_PAGE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return currentPage;

	}

	public void updateCurrentPage(int page)
	{
		try
		{

			ContentValues cv=new ContentValues();
			cv.put(CURRENT_PAGE, page);
			mDatabase.update(TABLE_MARKET_PLACE, cv,null, null);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public long getRowCount(int area_id)
	{
		String rawQuery="SELECT COUNT("+ID+") AS ROW_COUNT FROM "+TABLE_MARKET_PLACE +" WHERE "+AREA_ID+" = "+area_id;
		long rowcount=0;
		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return rowcount;

	}

	public long getRowCount(String area)
	{
		String rawQuery="SELECT COUNT("+ID+") AS ROW_COUNT FROM "+TABLE_MARKET_PLACE +" WHERE "+AREA+" like '"+area+"'";
		Log.i("ROW COUNT","ROW COUNT QUERY "+ rawQuery);
		long rowcount=0;
		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		Log.i("ROW COUNT","ROW COUNT "+ rowcount);
		return rowcount;

	}

	//Fav row count
	public long getFavRowCount()
	{
		String rawQuery="SELECT COUNT("+ID+") AS ROW_COUNT FROM "+TABLE_FAVOURITE_STORES;
		long rowcount=0;
		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return rowcount;

	}

	//Fav row count
	public long getFavRowCount(int place_parent)
	{
		String rawQuery="SELECT COUNT("+ID+") AS ROW_COUNT FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent;
		long rowcount=0;
		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return rowcount;

	}

	/*
	 * Getting Store name by id from Geo-Text table
	 */

	public String getGeoStoreName(int store_id)
	{
		String title = "";
		try
		{

			String query="SELECT "+STORE_NAME+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title=cursor.getString(cursor.getColumnIndex(STORE_NAME));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllGeoStoreName(int area_id)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+STORE_NAME+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(STORE_NAME)));

			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	public ArrayList<String> getAllGeoStoreName(String area)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+STORE_NAME+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(STORE_NAME)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}


	public ArrayList<String> getAllGeoStoreDesc(int area_id)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+DESCRIPTION+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	public ArrayList<String> getAllGeoStoreDesc(String area)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+DESCRIPTION+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	public ArrayList<String> getAllTotalLikes(int area_id)
	{
		ArrayList<String> total_like = new ArrayList<String>();
		try
		{

			String query="SELECT "+TOTAL_LIKE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_like.add(cursor.getString(cursor.getColumnIndex(TOTAL_LIKE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_like;

	}

	public ArrayList<String> getAllTotalLikes(String area)
	{
		ArrayList<String> total_like = new ArrayList<String>();
		try
		{

			String query="SELECT "+TOTAL_LIKE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				total_like.add(cursor.getString(cursor.getColumnIndex(TOTAL_LIKE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return total_like;

	}


	/*
	 * Getting Photo url by id from Geo-Text table
	 */

	public String getGeoPhoto(int store_id)
	{
		String photo = "";
		try
		{


			String query="SELECT "+LOGO+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo=cursor.getString(cursor.getColumnIndex(LOGO));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllGeoPhoto(int area_id)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+LOGO+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(LOGO)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}


	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllGeoPhoto(String area)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+LOGO+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(LOGO)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllDistances(int area_id)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+DISTANCE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(DISTANCE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}

	public ArrayList<String> getAllDistances(String area)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+DISTANCE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(DISTANCE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;
	}

	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllGeoAreas(int area_id)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getString(cursor.getColumnIndex(AREA)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}

	public ArrayList<String> getAllGeoAreas(String area_name)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area_name+"'";
			Log.i("SEARCH QUERY", "SEARCH QUERY "+query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getString(cursor.getColumnIndex(AREA)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}

	public ArrayList<String> getAllGeoAreaId(String area_name)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA_ID+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area_name+"'";
			Log.i("SEARCH QUERY", "SEARCH QUERY "+query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getInt(cursor.getColumnIndex(AREA_ID))+"");
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}

	public ArrayList<String> getAllGeoAreaId(int area_id)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA_ID+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Log.i("SEARCH QUERY", "SEARCH QUERY "+query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getInt(cursor.getColumnIndex(AREA_ID))+"");
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}


	/*
	 * Get all City from Geo-Text table
	 */

	public ArrayList<String> getAllGeoCity(int area_id)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			String query="SELECT "+CITY+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				city.add(cursor.getString(cursor.getColumnIndex(CITY)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return city;

	}

	public ArrayList<String> getAllGeoCity(String area)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			String query="SELECT "+CITY+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				city.add(cursor.getString(cursor.getColumnIndex(CITY)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return city;

	}



	/*
	 * Get all Id's Geo-Text table
	 */

	public ArrayList<String> getAllGeoStoreId(int area_id)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+ID+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}

	public ArrayList<String> getAllGeoStoreId(String area)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+ID+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		for(int i=0;i<ids.size();i++)
		{

		}
		return ids;


	}


	/*
	 * Get all Id's Geo-Text table
	 */

	public ArrayList<String> getAllGeoStoreParentId(int area)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+PLACE_PARENT+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(PLACE_PARENT)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}


	public ArrayList<String> getAllGeoStoreParentId(String area)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+PLACE_PARENT+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Log.i("QUERY", "QUERY "+query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(PLACE_PARENT)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}

	/*
	 * Get all Offers Geo-Text table
	 */

	public ArrayList<String> getAllStoreOffers(int area_id)
	{
		ArrayList<String> offer_title = new ArrayList<String>();
		try
		{

			String query="SELECT "+OFFER_TITLE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				offer_title.add(cursor.getString(cursor.getColumnIndex(OFFER_TITLE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return offer_title;

	}

	public ArrayList<String> getAllStoreOffers(String area)
	{
		ArrayList<String> offer_title = new ArrayList<String>();
		try
		{

			String query="SELECT "+OFFER_TITLE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				offer_title.add(cursor.getString(cursor.getColumnIndex(OFFER_TITLE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return offer_title;

	}

	/*Has offers */
	public String hasOffers(int store_id)
	{
		String hasOffer = "";
		try
		{

			String query="SELECT "+HAS_OFFER+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				hasOffer=cursor.getString(cursor.getColumnIndex(HAS_OFFER));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return hasOffer;

	}

	/*offers */
	public String getPerticularOffers(int store_id)
	{
		String offer = "";
		try
		{

			String query="SELECT "+OFFER_TITLE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				offer=cursor.getString(cursor.getColumnIndex(OFFER_TITLE));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return offer;

	}

	/*
	 * Get all Offers flag Geo-Text table
	 */

	public ArrayList<String> getAllOffersFlag(int area_id)
	{
		ArrayList<String> offer_flag = new ArrayList<String>();
		try
		{

			String query="SELECT "+HAS_OFFER+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				offer_flag.add(cursor.getString(cursor.getColumnIndex(HAS_OFFER)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return offer_flag;

	}

	public ArrayList<String> getAllOffersFlag(String area)
	{
		ArrayList<String> offer_flag = new ArrayList<String>();
		try
		{

			String query="SELECT "+HAS_OFFER+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				offer_flag.add(cursor.getString(cursor.getColumnIndex(HAS_OFFER)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return offer_flag;

	}

	/*
	 * Get City by id from Geo-Text table
	 */

	public String getGeoCity(int store_id)
	{
		String city = "";
		try
		{

			String query="SELECT "+CITY+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				city=cursor.getString(cursor.getColumnIndex(CITY));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return city;
	}

	/*
	 * Get Area by id from Geo-Text table
	 */
	public String getGeoArea(int store_id)
	{
		String area = "";
		try
		{

			String query="SELECT "+AREA+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area=cursor.getString(cursor.getColumnIndex(AREA));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}

	/*
	 * Get Street by id from Geo-Text table
	 */

	public String getGeoStreet(int store_id)
	{
		String area = "";
		try
		{

			String query="SELECT "+STREET+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area=cursor.getString(cursor.getColumnIndex(STREET));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}


	/*
	 * Get Mobile Numbers by id from Geo-Text table
	 */

	public String getGeoMobileNos(int store_id)
	{
		String mobileno = "";
		try
		{

			String query="SELECT "+MOB_NO_1+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno=cursor.getString(cursor.getColumnIndex(MOB_NO_1));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	/*
	 * Getting all Mobile No's name from Geo-Text table
	 */

	public ArrayList<String> getGeoAllMobileNos(int area_id)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_1+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos2(int area_id)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_2+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos3(int area_id)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_3+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos(String area)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_1+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos2(String area)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_2+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos3(String area)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_3+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllTELNos(int area_id)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_1+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}
	public ArrayList<String> getGeoAllTELNos2(int area_id)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_2+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos3(int area_id)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_3+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA_ID+" = "+area_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos(String area)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_1+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos2(String area)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_2+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos3(String area)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_3+" FROM "+TABLE_MARKET_PLACE+" WHERE "+AREA+" like '"+area+"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}


	/*
	 * GetLandmark by id from Geo-Text table
	 */

	public String getGeoLandmark(int store_id)
	{
		String landmark = "";
		try
		{

			String query="SELECT "+LANDMARK+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				landmark=cursor.getString(cursor.getColumnIndex(LANDMARK));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return landmark;

	}


	/*
	 * Get Building by id from Geo-Text table
	 */

	public String getGeoBuilding(int store_id)
	{
		String building = "";
		try
		{

			String query="SELECT "+BUILDING+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				building=cursor.getString(cursor.getColumnIndex(BUILDING));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return building;

	}

	/*
	 * Get Distance by id from Geo-Text table
	 */

	public String getGeoDistance(int store_id)
	{
		String distance = "";
		try
		{

			String query="SELECT "+DISTANCE+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				distance=cursor.getString(cursor.getColumnIndex(DISTANCE));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return distance;

	}

	/*
	 * Get Distance by id from Geo-Text table
	 */

	public boolean isGeoIdExists(int store_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			String query="SELECT "+ID+" FROM "+TABLE_MARKET_PLACE+" WHERE "+ID+"=" + store_id;
			Cursor  cursor  = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				id=cursor.getInt(cursor.getColumnIndex(ID))+"";
				if(id.equalsIgnoreCase(store_id+""))
				{
					exists=true;
					break;
				}

			}
			cursor.close();

		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{


		}
		Log.i("ID EXISTIS","IS ID EXISTS " +exists);
		return exists;

	}

	public boolean isNewsFeedPostIdExists(int post_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			String query="SELECT "+POST_ID+" FROM "+TABLE_OFFERS+" WHERE "+POST_ID+"=" + post_id;
			Cursor  cursor  = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				id=cursor.getInt(cursor.getColumnIndex(POST_ID))+"";
				if(id.equalsIgnoreCase(post_id+""))
				{
					exists=true;
					break;
				}
			}
			cursor.close();

		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{


		}
		return exists;

	}



	void deleteGeoText(int area_id)
	{
		try
		{

			mDatabase.delete(TABLE_MARKET_PLACE, AREA_ID+" = "+area_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}

	}
	void deleteGeoText(String area)
	{
		try
		{

			int res=mDatabase.delete(TABLE_MARKET_PLACE, AREA+" like '"+area+"'", null);
			Log.i("DELETED ", "DELETED  : "+res);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
	}

	//Delete GeoFav
	void deleteAllGeoFavText(int place_parent)
	{
		try
		{

			mDatabase.delete(TABLE_FAVOURITE_STORES, PLACE_PARENT+" = "+place_parent, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText(String area)
	{
		try
		{

			mDatabase.delete(TABLE_FAVOURITE_STORES, AREA+" like '"+area+"'", null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText()
	{
		try
		{

			mDatabase.delete(TABLE_FAVOURITE_STORES,null,null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
	}


	public long getGeoRowCount()
	{
		long rowCount=0;
		try
		{

			rowCount=DatabaseUtils.queryNumEntries(mDatabase, TABLE_MARKET_PLACE);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return rowCount ;
	}




	/*
	 * Getting all Store name from Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreName()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+STORE_NAME+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(STORE_NAME)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoStoreName(int place_parent)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+STORE_NAME+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(STORE_NAME)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;
	}

	public ArrayList<String> getAllFavGeoStoreDESC()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+DESCRIPTION+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	public ArrayList<String> getAllFavGeoStoreDESC(int place_parent)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			String query="SELECT "+DESCRIPTION+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				title.add(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return title;

	}

	/*
	 * Get all Fav Id's Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreId()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+ID+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}
	/*
	 * Get all Fav Id's Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreId(int place_parent)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			String query="SELECT "+ID+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				ids.add(cursor.getString(cursor.getColumnIndex(ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return ids;

	}

	/*
	 * Getting all Mobile No's name from Geo-Text Fav table
	 */

	public ArrayList<String> getGeoFavAllMobileNos()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_1+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoFavAllMobileNos2()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_2+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoFavAllMobileNos3()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_3+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoFavAllTelNos()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_1+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(TEL_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}

	public ArrayList<String> getGeoFavAllTelNos2()
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_2+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_2)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}

	public ArrayList<String> getGeoFavAllTelNos3()
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			String query="SELECT "+TEL_NO_3+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				tel.add(cursor.getString(cursor.getColumnIndex(TEL_NO_3)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return tel;

	}
	public ArrayList<String> getGeoFavAllMobileNos(int place_parent)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			String query="SELECT "+MOB_NO_1+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				mobileno.add(cursor.getString(cursor.getColumnIndex(MOB_NO_1)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return mobileno;

	}



	/*
	 * Get all City from Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoCity()
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			String query="SELECT "+CITY+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				city.add(cursor.getString(cursor.getColumnIndex(CITY)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return city;

	}

	/*
	 * Get all City from Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoCity(int place_parent)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			String query="SELECT "+CITY+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				city.add(cursor.getString(cursor.getColumnIndex(CITY)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return city;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoPhoto()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+LOGO+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(LOGO)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoPhoto(int place_parent)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+LOGO+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(LOGO)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavDistances()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+DISTANCE+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(DISTANCE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavDistances(int place_parent)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			String query="SELECT "+DISTANCE+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				photo.add(cursor.getString(cursor.getColumnIndex(DISTANCE)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return photo;

	}


	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoAreas()
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getString(cursor.getColumnIndex(AREA)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}
	public ArrayList<String> getAllFavGeoAreaID()
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA_ID+" FROM "+TABLE_FAVOURITE_STORES+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getString(cursor.getColumnIndex(AREA_ID)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}
	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoAreas(int place_parent)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			String query="SELECT "+AREA+" FROM "+TABLE_FAVOURITE_STORES+" WHERE "+PLACE_PARENT+" = "+place_parent+" ORDER BY "+STORE_NAME;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				area.add(cursor.getString(cursor.getColumnIndex(AREA)));
			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		return area;

	}

	/*
	 * Delete Fav by id
	 */
	void deleteGeoFavText(int store_id)
	{
		try
		{

			mDatabase.delete(TABLE_FAVOURITE_STORES, ID+"="+store_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}

	}




	/*
	 *  Queries for Area Table ======================================
	 */

	public long createArea(Integer id, String name, String latitude, String longitude, String pincode, String city, String country, String countryCode, String isoCode, String placeId,String locality,String state,int active,int published_status){

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}

		Log.d("GETINAREA","GETINAREA" + name);

		ContentValues cv=new ContentValues();
		cv.put(AREA_ID, id);
		cv.put(AREA, name);
		cv.put(LATITUDE, latitude);
		cv.put(LONGITUDE, longitude);
		cv.put(PINCODE, pincode);
		cv.put(CITY, city);
		cv.put(COUNTRY, country);
		cv.put(COUNTRY_CODE, countryCode);
		cv.put(ISO_CODE, isoCode);
		cv.put(ID, placeId);
		cv.put(LOCALITY, locality);
		cv.put(STATE, state);
		cv.put(ACTIVE_AREA, active);
		cv.put(PUBLISHED_STATE, published_status);

		return mDatabase.insertWithOnConflict(TABLE_AREAS, null, cv,SQLiteDatabase.CONFLICT_REPLACE);
	}

	public Integer updatePlaceStatus(UpadateAreaListener obj,int id,int state)
	{
		int rows_affected=0;
		try
		{

			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}

			ContentValues cv2=new ContentValues();
			cv2.put(ACTIVE_AREA, 0);
			mDatabase.update(TABLE_AREAS,cv2,null, null);

			ContentValues cv=new ContentValues();
			cv.put(ACTIVE_AREA, state);
			rows_affected=mDatabase.update(TABLE_AREAS,cv,AREA_ID+"="+id, null);

			Log.i("ROW UPDATED", "ROW UPDATED "+rows_affected);
			obj.areaUpdated(id);
			/*	getAllCurrentPages();*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
			return rows_affected;
		}
		return rows_affected;

	}


	public String getAreaNameById(int areaId)
	{
		String addr  = "";
		String query="SELECT "+AREA+" FROM "+TABLE_AREAS +" WHERE "+ AREA_ID+" = " + areaId;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(AREA));
		}
		cursor.close();

		return addr;
	}


	public String getAreaStatus(int areaId)
	{
		String area_active  = "";
		String query="SELECT "+ACTIVE_AREA+" FROM "+TABLE_AREAS +" WHERE "+ AREA_ID+" = " + areaId;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area_active=cursor.getString(cursor.getColumnIndex(area_active));
		}
		cursor.close();

		return area_active;
	}

	public String getActiveArea()
	{
		String area  = "";
		String query="SELECT "+AREA+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;
		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}
		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(AREA));
		}
		cursor.close();

		return area;
	}

	public String getMerchantCountForActiveArea(){
		String count  = "";
		String query="SELECT "+MERCHANT_COUNT+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;
		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}
		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			count=cursor.getString(cursor.getColumnIndex(MERCHANT_COUNT));
		}
		cursor.close();

		return count;		
	}

	public String getActiveAreaPin()
	{
		String area  = "";
		String query="SELECT "+PINCODE+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(PINCODE));
		}
		cursor.close();

		return area;
	}
	public String getActiveAreaCity()
	{
		String area  = "";
		String query="SELECT "+CITY+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(CITY));
		}
		cursor.close();

		return area;
	}

	public String getAreaLat()
	{
		String lat  = "";
		String query="SELECT "+LATITUDE+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			lat=cursor.getString(cursor.getColumnIndex(LATITUDE));
		}
		cursor.close();

		return lat;
	}
	public String getAreaLong()
	{
		String longitude  = "";
		String query="SELECT "+LONGITUDE+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			longitude=cursor.getString(cursor.getColumnIndex(LONGITUDE));
		}
		cursor.close();

		return longitude;
	}

	public String getActiveAreaID()
	{
		String area  = "";
		String query="SELECT "+AREA_ID+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}
		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(AREA_ID));
		}
		cursor.close();

		return area;
	}
	
	public String getActiveSlug()
	{
		String slug  = "";
		String query="SELECT "+SLUG+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}
		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			slug=cursor.getString(cursor.getColumnIndex(SLUG));
		}
		cursor.close();

		return slug;
	}

	public String getActiveAreaPlace()
	{
		String area  = "";
		String query="SELECT "+ID+" FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(ID));
		}
		cursor.close();

		return area;
	}

	public ArrayList<String> getAllAreas()
	{
		ArrayList<String>  allAreas =new ArrayList<String>();
		String query="SELECT "+AREA+" FROM "+TABLE_AREAS;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreas.add(cursor.getString(cursor.getColumnIndex(AREA)));
		}
		cursor.close();

		return allAreas;
	}
	public ArrayList<String> getAllPublishedAreas(int status)
	{
		ArrayList<String>  allAreas =new ArrayList<String>();
		String query="SELECT "+AREA+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreas.add(cursor.getString(cursor.getColumnIndex(AREA)));
		}
		cursor.close();

		return allAreas;
	}

	public ArrayList<String> getAllAreaIds()
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();
		String query="SELECT "+AREA_ID+" FROM "+TABLE_AREAS;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getInt(cursor.getColumnIndex(AREA_ID))+"");
		}
		cursor.close();

		return allAreasIds;
	}

	public ArrayList<String> getAllPublishedAreaIds(int status)
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();
		String query="SELECT "+AREA_ID+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getInt(cursor.getColumnIndex(AREA_ID))+"");
		}
		cursor.close();

		return allAreasIds;
	}

	public ArrayList<String> getAllAreaLat()
	{
		ArrayList<String>  allAreaslats =new ArrayList<String>();
		String query="SELECT "+LATITUDE+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslats.add(cursor.getString(cursor.getColumnIndex(LATITUDE)));
		}

		cursor.close();

		return allAreaslats;
	}

	public ArrayList<String> getAllPublishedAreaLat(int status)
	{
		ArrayList<String>  allAreaslats =new ArrayList<String>();
		String query="SELECT "+LATITUDE+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslats.add(cursor.getString(cursor.getColumnIndex(LATITUDE)));
		}
		cursor.close();

		return allAreaslats;
	}

	public ArrayList<String> getAllAreaLong()
	{
		ArrayList<String>  allAreaslong=new ArrayList<String>();
		String query="SELECT "+LONGITUDE+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslong.add(cursor.getString(cursor.getColumnIndex(LONGITUDE)));
		}
		cursor.close();

		return allAreaslong;
	}

	public ArrayList<String> getAllPublishedAreaLong(int status)
	{
		ArrayList<String>  allAreaslong=new ArrayList<String>();
		String query="SELECT "+LONGITUDE+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslong.add(cursor.getString(cursor.getColumnIndex(LONGITUDE)));
		}
		cursor.close();

		return allAreaslong;
	}


	public ArrayList<String> getAllAreaPin()
	{
		ArrayList<String>  allAreasPin=new ArrayList<String>();
		String query="SELECT "+PINCODE+" FROM "+TABLE_AREAS;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPin.add(cursor.getString(cursor.getColumnIndex(PINCODE)));
		}
		cursor.close();

		return allAreasPin;
	}

	public ArrayList<String> getAllPublishedAreaPin(int status)
	{
		ArrayList<String>  allAreasPin=new ArrayList<String>();
		String query="SELECT "+PINCODE+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPin.add(cursor.getString(cursor.getColumnIndex(PINCODE)));
		}
		cursor.close();

		return allAreasPin;
	}

	public ArrayList<String> getAllAreaCity()
	{
		ArrayList<String>  allAreasCity=new ArrayList<String>();
		String query="SELECT "+CITY+" FROM "+TABLE_AREAS;

		if(mDatabase==null || !mDatabase.isOpen())
		{
			openDB();
		}

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCity.add(cursor.getString(cursor.getColumnIndex(CITY)));
		}
		cursor.close();

		return allAreasCity;
	}

	public ArrayList<String> getAllPublishedAreaCity(int status)
	{
		ArrayList<String>  allAreasCity=new ArrayList<String>();
		String query="SELECT "+CITY+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCity.add(cursor.getString(cursor.getColumnIndex(CITY)));
		}
		cursor.close();

		return allAreasCity;
	}

	public ArrayList<String> getAllAreaCountries()
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();
		String query="SELECT "+COUNTRY+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(COUNTRY)));
		}
		cursor.close();

		return allAreasCountry;
	}



	public ArrayList<String> getAllPublishedAreaCountries(int status)
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();
		String query="SELECT "+COUNTRY+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(COUNTRY)));
		}
		cursor.close();

		return allAreasCountry;
	}

	public ArrayList<String> getAllAreaCountryCodes()
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();
		String query="SELECT "+COUNTRY_CODE+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(COUNTRY_CODE)));
		}
		cursor.close();

		return allAreasCountry;
	}

	public ArrayList<String> getAllPublishedAreaCountryCodes(int status)
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();
		String query="SELECT "+COUNTRY_CODE+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(COUNTRY_CODE)));
		}
		cursor.close();

		return allAreasCountry;
	}


	public ArrayList<String> getAllAreaCodes()
	{
		ArrayList<String>  allAreasCodes=new ArrayList<String>();
		String query="SELECT "+COUNTRY_CODE+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCodes.add(cursor.getString(cursor.getColumnIndex(COUNTRY_CODE)));
		}
		cursor.close();

		return allAreasCodes;
	}



	public ArrayList<String> getAllAreaIsoCode()
	{
		ArrayList<String>  allAreasIsoCode=new ArrayList<String>();
		String query="SELECT "+ISO_CODE+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIsoCode.add(cursor.getString(cursor.getColumnIndex(ISO_CODE)));
		}
		cursor.close();

		return allAreasIsoCode;
	}

	public ArrayList<String> getAllPublishedAreaIsoCode(int status)
	{
		ArrayList<String>  allAreasIsoCode=new ArrayList<String>();
		String query="SELECT "+ISO_CODE+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIsoCode.add(cursor.getString(cursor.getColumnIndex(ISO_CODE)));
		}
		cursor.close();

		return allAreasIsoCode;
	}

	public ArrayList<String> getAllAreaPlaceId()
	{
		ArrayList<String>  allAreasPlaceIds=new ArrayList<String>();
		String query="SELECT "+ID+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPlaceIds.add(cursor.getString(cursor.getColumnIndex(ID)));
		}
		cursor.close();

		return allAreasPlaceIds;
	}
	public ArrayList<String> getAllPublishedAreaPlaceId(int status)
	{
		ArrayList<String>  allAreasPlaceIds=new ArrayList<String>();
		String query="SELECT "+ID+" FROM "+TABLE_AREAS+" where "+PUBLISHED_STATE+" = "+status;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPlaceIds.add(cursor.getString(cursor.getColumnIndex(ID)));
		}
		cursor.close();

		return allAreasPlaceIds;
	}

	public ArrayList<String> getAllAreaLocality()
	{
		ArrayList<String>  allAreasLocality=new ArrayList<String>();
		String query="SELECT "+LOCALITY+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasLocality.add(cursor.getString(cursor.getColumnIndex(LOCALITY)));
		}
		cursor.close();

		return allAreasLocality;
	}

	public ArrayList<String> getAllAreaState()
	{
		ArrayList<String>  allAreasState=new ArrayList<String>();
		String query="SELECT "+STATE+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasState.add(cursor.getString(cursor.getColumnIndex(STATE)));
		}
		cursor.close();

		return allAreasState;
	}

	public ArrayList<String> getAllAreaActive()
	{
		ArrayList<String>  allAreasActive=new ArrayList<String>();
		String query="SELECT "+ACTIVE_AREA+" FROM "+TABLE_AREAS;

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasActive.add(cursor.getString(cursor.getColumnIndex(ACTIVE_AREA)));
		}
		cursor.close();

		return allAreasActive;
	}

	public void deleteAreasTable()
	{
		try
		{
			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}
			mDatabase.delete(TABLE_AREAS, null, null);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void deletePlacesTable()
	{
		try
		{

			mDatabase.delete(TABLE_MERCHANT_STORES, null, null);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public String getDateTypeById(String dateId)
	{
		String dateType  = "";
		String query="SELECT "+DATE_NAME+" FROM "+TABLE_DATE_CATEGORIES +" WHERE "+ ID+" = '"+ dateId+"'";

		Cursor  cursor = mDatabase.rawQuery(query,null);
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			dateType=cursor.getString(cursor.getColumnIndex(DATE_NAME));
		}
		cursor.close();

		return dateType;
	}

	public void deleteTableDateCategory()
	{
		try
		{

			mDatabase.delete(TABLE_DATE_CATEGORIES, null, null);

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void openDB()
	{		
		mDatabase=mInstance.getWritableDatabase();
	}
	public long getAreaRowCount()
	{
		String rawQuery="SELECT COUNT("+AUTO_ID+") AS ROW_COUNT FROM "+TABLE_AREAS ;
		Log.i("ROW COUNT","ROW COUNT AREA QUERY "+ rawQuery);
		long rowcount=0;
		try
		{

			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		Log.i("ROW COUNT","ROW COUNT AREA "+ rowcount);
		return rowcount;
	}
	
	public long getCategoryRowCount()
	{
		String rawQuery="SELECT COUNT("+ID+") AS ROW_COUNT FROM "+TABLE_CATEGORIES ;
		Log.i("ROW COUNT","ROW COUNT AREA QUERY "+ rawQuery);
		long rowcount=0;
		try
		{

			if(mDatabase==null || !mDatabase.isOpen())
			{
				openDB();
			}

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		Log.i("ROW COUNT","ROW COUNT AREA "+ rowcount);
		return rowcount;
	}

	public long getActiveAreaRowCount()
	{
		String rawQuery="SELECT COUNT("+AUTO_ID+") AS ROW_COUNT FROM "+TABLE_AREAS +" WHERE "+ ACTIVE_AREA+" = " + 1;
		Log.i("ROW COUNT","ROW COUNT AREA QUERY "+ rawQuery);
		long rowcount=0;
		try
		{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

		}
		Log.i("ROW COUNT","ROW COUNT AREA "+ rowcount);
		return rowcount;
	}
}
