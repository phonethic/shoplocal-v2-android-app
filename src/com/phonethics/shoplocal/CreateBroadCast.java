package com.phonethics.shoplocal;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;


public class CreateBroadCast extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener 
{
	private Activity mContext;

	private byte[] mByteArr = null ;

	private  final int REQUEST_CODE_CUSTOMCAMERA = 8;
	private  final int REQUEST_CODE_POSTGALLERY = 9;
	private  final int REQUEST_CODE_POSTCROPGALLERY = 10;
	private  final int REQUEST_CODE_TAKE_PICTURE = 7;

	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="";

	private ImageView mImgPreview;
	private ImageView mReviewImage;
	private ImageView mDeleteImage;

	private Button mEditButton;
	private Button mPostButton;
	private Button mBtnChooseImage;
	private Button mBtnTakeImage;
	private Button   mBtnReview;

	//CreateBroadcast
	private EditText mEdtBroadTitle;
	private EditText mEdtBroadDesc;
	private EditText mEdtTags;
	private EditText mEdtImageCaption;


	private CheckBox mChkIsOffer;

	String is_offered="";


	private boolean mbIsImagePost;
	private boolean mbIsReuse;

	private ScrollView mScrollMakePostScroll;
	private ScrollView mScrollViewPostScroll;
	private ImageView mImgSepratorCheckBox;

	private TextView mTxtViewOfferDate;
	private TextView mTxtViewOfferDetail;
	private TextView mTxtViewOfferTitle;

	private TextView mTextOfferDate;
	private TextView mTextOfferTime;

	private TableRow mTblImageCaptionRow;
	private TableRow mTblDateRow;
	private TableRow mTblTimeRow;

	private ImageView mImgCaptionSep;
	private ProgressBar mProgPost;

	private Calendar dateTime;
	private Calendar dateTimeCurrent;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();

	private String msPostTitle="";
	private String msPostDescription="";
	private String msPostImageUrl="";

	private Bitmap mBitmapImage;

	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;

	long mlMiliseconds;

	private ScrollView mPostScroll;
	private ScrollView mViewScroll;
	
	private Map<String,String> eventMap=new HashMap<String, String>();

	//File cacheDir;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_broad_cast);

		mContext=this;
		dateTime=Calendar.getInstance();
		dateTimeCurrent=Calendar.getInstance();
		imageLoader=ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		setTitle(getResources().getString(R.string.createBroadCastHeader));

		mScrollMakePostScroll=(ScrollView)findViewById(R.id.makePostScroll);
		mScrollViewPostScroll=(ScrollView)findViewById(R.id.viewPostScroll);

		mEditButton=(Button)findViewById(R.id.editButton);
		mPostButton=(Button)findViewById(R.id.postButton);
		mBtnChooseImage=(Button)findViewById(R.id.btnChooseImage);
		mBtnTakeImage=(Button)findViewById(R.id.btnCapture);
		mBtnReview=(Button)findViewById(R.id.btnReview);

		mEditButton.setText(getString(R.string.edit));

		mReviewImage=(ImageView)findViewById(R.id.reviewImage);
		mImgPreview=(ImageView)findViewById(R.id.imgThumbPreview);
		mDeleteImage=(ImageView)findViewById(R.id.deleteImage);
		mImgCaptionSep=(ImageView)findViewById(R.id.imageCaptionSep);

		mImgSepratorCheckBox=(ImageView)findViewById(R.id.sepratorCheck);

		mTxtViewOfferDate=(TextView)findViewById(R.id.txtViewOfferDate);
		mTxtViewOfferDetail=(TextView)findViewById(R.id.txtViewOfferDetail);
		mTxtViewOfferTitle=(TextView)findViewById(R.id.txtViewOfferTitle);

		mTblImageCaptionRow=(TableRow)findViewById(R.id.imageCaptionRow);

		mEdtBroadTitle=(EditText)findViewById(R.id.edtBroadTitle);
		mEdtBroadDesc=(EditText)findViewById(R.id.edtBroadDesc);
		mEdtTags=(EditText)findViewById(R.id.edtTags);
		mEdtImageCaption=(EditText)findViewById(R.id.edtImageTitle);

		mChkIsOffer=(CheckBox)findViewById(R.id.chkIsOffer);

		mProgPost=(ProgressBar)findViewById(R.id.progPost);

		mTextOfferDate=(TextView)findViewById(R.id.offerDate);
		mTextOfferTime=(TextView)findViewById(R.id.offerTime);

		mTblDateRow=(TableRow)findViewById(R.id.tblSelectDate);
		mTblTimeRow=(TableRow)findViewById(R.id.tblselectTime);


		Date cdt=new Date();

		dateTimeCurrent.setTime(cdt);

		DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss a");

		mTblDateRow.setVisibility(View.GONE);
		mTblTimeRow.setVisibility(View.GONE);

		mTextOfferDate.setText(dt.format(cdt));
		mTextOfferTime.setText(dt2.format(cdt.getTime()));

		mEdtBroadTitle.setTypeface(tf);
		mEdtBroadDesc.setTypeface(tf);
		mEdtTags.setTypeface(tf);

		mTextOfferDate.setTypeface(tf);
		mTextOfferTime.setTypeface(tf);

		mTxtViewOfferDate.setTypeface(tf);
		mTxtViewOfferDetail.setTypeface(tf);
		mTxtViewOfferTitle.setTypeface(tf);

		try
		{
			Bundle b=getIntent().getExtras();
			if(b!=null)
			{


				mArrStoreId=b.getStringArrayList("SELECTED_STORE_ID");
				msPostTitle=b.getString("title");
				msPostDescription=b.getString("description");
				msPostImageUrl=b.getString("photourl");
				if(msPostTitle!="")
				{
					mEdtBroadTitle.setText(msPostTitle);
				}

				if(msPostDescription!="")
				{
					mEdtBroadDesc.setText(msPostDescription);
				}
				if(msPostImageUrl!=null && !msPostImageUrl.equals(""))
				{


					msPostImageUrl=msPostImageUrl.replaceAll(" ","%20");

					if(!msPostImageUrl.equalsIgnoreCase("notfound"))
					{
						//load images and display it 
						mImgPreview.setVisibility(View.VISIBLE);
						mReviewImage.setVisibility(View.VISIBLE);
						mDeleteImage.setVisibility(View.VISIBLE);

						imageLoader.displayImage(Config.IMAGE_URL+msPostImageUrl, mImgPreview, new ImageLoadingListener() {

							@Override
							public void onLoadingStarted(String arg0, View arg1) {
							}

							@Override
							public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
							}

							@Override
							public void onLoadingComplete(String arg0, View arg1, Bitmap bitmap) {
								mBitmapImage=bitmap;
								FILE_TYPE="image/jpeg";
								FILE_NAME="temp_photo.jpg";
								mReviewImage.setImageBitmap(bitmap);
								try
								{
									ByteArrayOutputStream stream = new ByteArrayOutputStream();
									mBitmapImage.compress(Bitmap.CompressFormat.JPEG, 80, stream);
									mByteArr = stream.toByteArray();
								}catch(Exception ex)
								{
									ex.printStackTrace();
								}
							}
							@Override
							public void onLoadingCancelled(String arg0, View arg1) {
							}
						});

						mbIsImagePost=true;
						mbIsReuse=true;
					}
					else{
						mReviewImage.setVisibility(View.GONE);
						mImgPreview.setVisibility(View.GONE);
					}

				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();

		}


		mBtnChooseImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				eventMap.put("source", "Gallery");
				EventTracker.logEvent("NewPost_ImageAdded",eventMap );
				openGallery();
			}
		});

		mBtnTakeImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				eventMap.put("source", "Camera");
				EventTracker.logEvent("NewPost_ImageAdded",eventMap );
				takePictureFromCustomCamera();
			}
		});

		mChkIsOffer.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(mChkIsOffer.isChecked())
				{
					is_offered="1";
					mTblDateRow.setVisibility(View.VISIBLE);
					mTblTimeRow.setVisibility(View.VISIBLE);
					mImgSepratorCheckBox.setVisibility(View.VISIBLE);
				}else
				{
					is_offered="";
					mImgSepratorCheckBox.setVisibility(View.GONE);
					mTblTimeRow.setVisibility(View.GONE);
					mTblDateRow.setVisibility(View.GONE);
					mImgSepratorCheckBox.setVisibility(View.GONE);
				}
			}
		});

		mTextOfferDate.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				chooseDate();
			}
		});
		mTextOfferTime.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				chooseTime();	
			}
		});

		mBtnReview.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mEditButton.setText(getString(R.string.edit));
				if(mEdtBroadTitle.getText().toString().length()==0)
				{
					showToast(getResources().getString(R.string.titleAlert));
				}
				//If its offer
				else if(is_offered.equalsIgnoreCase("1"))
				{
					if(mTextOfferDate.getText().toString().length()==0){
						showToast(getResources().getString(R.string.offerDateAlert));
					}
					else if(mTextOfferDate.getText().toString().length()==0){
						showToast(getResources().getString(R.string.offerTimeAlert));
					}
					else
					{

						mScrollMakePostScroll.setVisibility(ViewGroup.GONE);
						mScrollViewPostScroll.setVisibility(ViewGroup.VISIBLE);

						mTxtViewOfferDate.setText("Offer valid till "+mTextOfferDate.getText().toString()+mTextOfferTime.getText().toString());
						mTxtViewOfferDetail.setText(mEdtBroadDesc.getText().toString());
						mTxtViewOfferTitle.setText(mEdtBroadTitle.getText().toString());
					}
				}
				//If its not offer
				else
				{
					mTxtViewOfferDate.setText("Offer valid till "+mTextOfferDate.getText().toString()+mTextOfferTime.getText().toString());
					mTxtViewOfferDetail.setText(mEdtBroadDesc.getText().toString());
					mTxtViewOfferTitle.setText(mEdtBroadTitle.getText().toString());
					mScrollMakePostScroll.setVisibility(ViewGroup.GONE);
					mScrollViewPostScroll.setVisibility(ViewGroup.VISIBLE);
				}

				if(mScrollViewPostScroll.isShown())
				{
					if(is_offered.equalsIgnoreCase("1")){
						mTxtViewOfferDate.setVisibility(View.VISIBLE);
					}
					else{
						mTxtViewOfferDate.setVisibility(View.INVISIBLE);
					}
				}

			}
		});

		//delete image
		mDeleteImage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mImgPreview.setVisibility(View.GONE);
				mReviewImage.setVisibility(View.GONE);
				mByteArr=null;
				FILE_PATH="";
				FILE_NAME="";
				mbIsImagePost=false;
				mDeleteImage.setVisibility(View.GONE);
			}
		});

		//Allow user to Edit Broadcast
		mEditButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				if(!mProgPost.isShown()){
					mEditButton.setText(getString(R.string.edit));
					mScrollMakePostScroll.setVisibility(ViewGroup.VISIBLE);
					mScrollViewPostScroll.setVisibility(ViewGroup.GONE);
				}
			}
		});

		mPostButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mEdtBroadTitle.getText().toString().length()==0){
					showToast(getResources().getString(R.string.titleAlert));
				}
				//If its offer
				else if(is_offered.equalsIgnoreCase("1")){
					if(mTextOfferDate.getText().toString().length()==0){
						showToast( getResources().getString(R.string.offerDateAlert));
					}
					else if(mTextOfferTime.getText().toString().length()==0){
						showToast( getResources().getString(R.string.offerTimeAlert));
					}
					else{
						Log.i("IS OFFER", "IS OFFER "+is_offered);
						if(!mProgPost.isShown()){
							postBroadCast(true);
						}
						else{
							showToast(getResources().getString(R.string.postingAlert));
						}
					}
				}
				//If its not offer
				else{
					Log.i("IS OFFER", "IS OFFER "+is_offered);
					if(!mProgPost.isShown()){
						postBroadCast(false);
					}
					else{
						showToast(getResources().getString(R.string.postingAlert));
					}
				}
			}
		});
	}

	void postBroadCast(boolean isOffer)
	{
		if(mInternet.isNetworkAvailable()){
			try
			{
				String description  =  mEdtBroadDesc.getText().toString();
				if (description.trim().length() >= 250) {
					showToast("Description cannot be more than 250 characters");
					return;
				}
				
				JSONObject json=new JSONObject();
				JSONArray jsonArray=new JSONArray(mArrStoreId);
				JSONArray fileArray=new JSONArray();
				JSONObject fileobject = new JSONObject();

				json.put("place_id", jsonArray);
				
				
				if(mbIsImagePost==true){
					if(mByteArr!=null){
						String user_file= Base64.encodeToString(mByteArr, Base64.DEFAULT);
						fileobject.put("filetype", FILE_TYPE);
						fileobject.put("userfile", user_file);
						fileobject.put("title", mEdtImageCaption.getText().toString());
						fileArray.put(fileobject);
						json.put("file",fileArray);
					}
				}
				if(isOffer==false){
					json.put("is_offered", "0");
				}
				else{
					json.put("is_offered","1");

				}
				json.put("url", "");
				json.put("offer_date_time", mTextOfferDate.getText().toString()+mTextOfferTime.getText().toString());
				json.put("user_id", Config.MERCHANT_USER_ID);
				json.put("auth_id", Config.MERCHANT_AUTH_ID	);
				json.put("title", mEdtBroadTitle.getText().toString());
				json.put("description", mEdtBroadDesc.getText().toString());
				json.put("tags", mEdtTags.getText().toString());
				json.put("type", "photo");
				json.put("state", "published");
				json.put("format", "html");

				String msUrl=Config.POST_OFFER_URL;
				new ShoplocalAsyncTask(this, "post", "merchant", false,json, "postOffer").execute(msUrl);

			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void debug(String msg){
		Log.i("Talk Now","Talk Now "+msg);
	}

	public void onBackPressed() {
		finishto();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finishto();
		return true;
	}

	void finishto()
	{
		if(mEdtBroadTitle.getText().toString().length()!=0 || mEdtBroadDesc.getText().toString().length()==0)
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle("Talk to Customer");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.discardMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					Intent intent=new Intent(mContext, MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
				}
			})
			;

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		else if(!mProgPost.isShown())
		{
			Intent intent=new Intent(mContext, MerchantTalkHome.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		else
		{
			showToast("Please wait while posting");
		}
	}

	private void takePictureFromCustomCamera() {
		try {
			Intent intent = new Intent(mContext,CameraView.class);
			intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);
		} catch (ActivityNotFoundException e) {
			Log.d("", "cannot take picture", e);
		}
	}

	private void openGallery() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY);
	}

	public void chooseDate(){
		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(mContext, d, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
	}
	public void chooseTime(){
		new TimePickerDialog(mContext, t, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show();
	}

	DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDate();
		}
	};
	TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateTime();
		}
	};
	private void updateTime() {
		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss");
		mlMiliseconds=dateTime.getTimeInMillis();
		mTextOfferTime.setText(dt2.format(dateTime.getTime()));
	}
	private void updateDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));

		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0)
		{
			showToast(getResources().getString(R.string.pastDateAlert));
			//reseting calender
			dateTime=Calendar.getInstance();
			chooseDate();
		}
		else
		{
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMMM-yyyy");
			mTextOfferDate.setText(dt.format(dateTime.getTime()));
		}
	}

	public long dateDiff(Date d1,Date d2)
	{
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		Log.i("daysBetween", "daysBetween "+datediff);
		return datediff;
	}


	@Override
	public void onAsyncStarted() {
		mProgPost.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mProgPost.setVisibility(View.GONE);
		if(tag.equalsIgnoreCase("postOffer"))
		{
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status==null)return;
				if(status.equalsIgnoreCase("false")){
					EventTracker.logEvent("NewPost_PostFailed", false);
					String message=jsonObject.getString("message");
					showToast(message);
					if(jsonObject.has("error_code")){
						String error_code=jsonObject.getString("error_code");
						if(error_code.equalsIgnoreCase("-116")){
							invalidAuthFinish();
						}
					}
					return;
				}
				if(status.equalsIgnoreCase("true")){
					EventTracker.logEvent("NewPost_Posted", false);
					String message=jsonObject.getString("message");
					try
					{
						DateFormat format; format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
						mLocalNotification.clearPostPrefs();
						mLocalNotification.checkPref();
						mLocalNotification.setPostPref(format.format(new Date()),mLocalNotification.isPostAlarmFired());
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
					
					showToast(message);
					Intent intent=new Intent(mContext, MerchantTalkHome.class);
					intent.putExtra("STORE_ID",mArrStoreId.get(0));
					startActivity(intent);
					finish();
					
				}


			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
	}

	@Override
	public void onError(int errorCode) {
		mProgPost.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}

	void invalidAuthFinish()
	{
		try
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
					try
					{
						mSessionManager.logoutUser();
						Intent intent=new Intent(mContext, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		try
		{
			switch (requestCode) {

			case REQUEST_CODE_CUSTOMCAMERA:
				if(resultCode == Activity.RESULT_OK){

					try{
						Bitmap bitmap = null;

						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							mByteArr = stream.toByteArray();
							cameraImageSave.deleteFromFile();
							cameraImageSave = null;
							bitmap = null;
						}
						setPhoto(BitmapFactory.decodeByteArray(mByteArr , 0, mByteArr.length));
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				break;

			case REQUEST_CODE_POSTGALLERY:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();
					cropCapturedImage(imageUri);

//					Intent intent=new Intent(CreateBroadCast.this, ZoomCroppingActivity.class);
//					intent.putExtra("imagepath", picturePath);
//					intent.putExtra("comingfrom", "gallery");
//					startActivityForResult(intent, REQUEST_CODE_POSTCROPGALLERY);
				}

				break;
				
		

			case REQUEST_CODE_POSTCROPGALLERY:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";
					
					  //Create an instance of bundle and get the returned data
					   Bundle extras = data.getExtras();
					   //get the cropped bitmap from extras
					   Bitmap thePic = extras.getParcelable("data");
					   
					CameraImageSave cameraSaveImage = new CameraImageSave();

					try
					{
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}

					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
							cameraSaveImage.deleteFromFile();
							mByteArr = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						setPhoto(BitmapFactory.decodeByteArray(mByteArr , 0,mByteArr.length));
					}
					catch(Exception e){
						Log.e("Exception","Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;
			}

			super.onActivityResult(requestCode, resultCode, data);
		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	
	 public void cropCapturedImage(Uri picUri){
		  //call the standard crop action intent 
		  Intent cropIntent = new Intent("com.android.camera.action.CROP");
		  //indicate image type and Uri of image
		  cropIntent.setDataAndType(picUri, "image/*");
		  //set crop properties
		  cropIntent.putExtra("crop", "true");
		  //indicate aspect of desired crop
		  cropIntent.putExtra("aspectX", 1);
		  cropIntent.putExtra("aspectY", 1);
		  //indicate output X and Y
		  cropIntent.putExtra("outputX", 256);
		  cropIntent.putExtra("outputY", 256);
		  //retrieve data on return
		  cropIntent.putExtra("return-data", true);
		  //start the activity - we handle returning in onActivityResult
		  startActivityForResult(cropIntent, REQUEST_CODE_POSTCROPGALLERY);
		 }
		

	void setPhoto(Bitmap bitmap)
	{
		try
		{
			FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";

			mImgPreview.setImageBitmap(bitmap);
			mImgPreview.setScaleType(ScaleType.FIT_XY);
			mReviewImage.setImageBitmap(bitmap);
			mReviewImage.setScaleType(ScaleType.FIT_XY);
			mDeleteImage.setVisibility(View.VISIBLE);
			mImgPreview.setVisibility(View.VISIBLE);
			mReviewImage.setVisibility(View.VISIBLE);

			mEditButton.setText(getString(R.string.edit));
			mEditButton.setTextColor(Color.WHITE);
			mEditButton.setGravity(Gravity.CENTER);

			mbIsImagePost=true;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}

