package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.picasso.Picasso;

public class OffersAdapter extends ShoplocalAdapter implements DBListener  {

	int ITEMS_PER_PAGE = 50;
	private int mPage = 0;
	private PullToRefreshListView mList;
	private TextView mTxtCounter;
	private boolean mbRefreshing = false;
	private Typeface  tf = null;
	private ProgressDialog mProgressDialog;
	private ArrayList<String> msPostId=new ArrayList<String>();
	private NetworkCheck mInernet;
	private ListClickListener mListClick;
	
	
	public OffersAdapter(FragmentActivity activity, PullToRefreshListView list,TextView txtCounter,ListClickListener lsClick){
		super(activity, R.layout.newsfeedrow);
		mList = list;
		this.mTxtCounter=txtCounter;
		this.mListClick=lsClick;
		mInernet=new NetworkCheck(activity);
		tf=Typeface.createFromAsset(mActivity.getAssets(), "fonts/GOTHIC_0.TTF");
		mProgressDialog=new ProgressDialog(mActivity);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");
	}

	public void setmPage(int mPage) {
		this.mPage = mPage;
	}

	public boolean isMbRefreshing() {
		return mbRefreshing;
	}

	public void setMbRefreshing(boolean mbRefreshing) {
		this.mbRefreshing = mbRefreshing;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {

		String sQuery="Select O._id,O.post_id,O.id,O.store_name,O.title,O.description,O.post_date,O.has_offer,O.offer_date_time,O.image_url_1,O.total_like,O.total_share,O.mob_no_1,O.mob_no_2,O.mob_no_3,O.tel_no_1,O.tel_no_2,O.tel_no_3,R.total_record,R.total_page,R.current_page"
				+ " from OFFERS as O INNER JOIN OFFERS_RECORDS R on O.area_id=R.area_id where O.area_id="+Config.MY_AREA+" and R.area_id="+Config.MY_AREA;
		return new ShoplocalCursorLoader(mActivity, sQuery);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		debug("onFinish");
		try{
			if(mProgressDialog!=null){
				mProgressDialog.dismiss();
			}
			if(cursor==null || cursor.getCount()==0){
				String sURL = Config.OFFERS_STREAM_URL +"&area_id="+Config.MY_AREA + "&page=" + (mPage+1) + "&count="+ITEMS_PER_PAGE;
				if(mInernet.isNetworkAvailable()){
					getData(sURL,"",false,"Offers");
				}else{
					showToast(mActivity.getResources().getString(R.string.noInternetConnection));
				}
				return;
			}
			changeCursor(cursor);
			mCursor = cursor;
			mCount = cursor.getCount();
			debug("total = " + mCount);
			try
			{
				msPostId.clear();
				for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
					msPostId.add(cursor.getString(cursor.getColumnIndex("post_id")));
				}
				cursor.moveToFirst();
			}catch(Exception ex){
				ex.printStackTrace();
			}

			notifyDataSetChanged();
		}catch(Exception ex){
			ex.printStackTrace();
		}


	}


	void showToast(String msg){
		Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
	}

	private void debug(String s) {
		Log.v("OffersAdpter " , "OffersAdpter " + s);
	}

	@Override
	public void bindView(View convertView, Context context, final Cursor cursor) {
		if(convertView == null) {
			convertView=LayoutInflater.from(mActivity).inflate(R.layout.newsfeedrow,null);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();

		if(holder==null){
			holder=new ViewHolder();
			holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
			holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
			holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);
			holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
			holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);
			holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);
			holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);
			holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);
			holder.imgCallStore=(ImageView)convertView.findViewById(R.id.imgCallStore);

			holder.newsFeedOffersText.setTypeface(tf,Typeface.BOLD);
			holder.newsFeedOffersDesc.setTypeface(tf,Typeface.BOLD);
			holder.newsFeedTotalLike.setTypeface(tf,Typeface.BOLD);
			holder.newsFeedTotalShare.setTypeface(tf,Typeface.BOLD);
			holder.newsFeedStoreName.setTypeface(tf,Typeface.BOLD);
			convertView.setTag(holder);
		}

		String postImage=cursor.getString(0);
		int index=-1;

		index=cursor.getColumnIndex("image_url_1");

		if(index>0){
			postImage=cursor.getString(index);
		}
		if(postImage==null){
			holder.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
		}else{
			String sImageUrl= postImage.replaceAll(" ", "%20");
			try{
				Picasso.with(mActivity).load(Config.IMAGE_URL+sImageUrl)
				.placeholder(R.drawable.ic_offers_placeholder)
				.error(R.drawable.ic_offers_placeholder)
				.into(holder.imgNewsFeedLogo);
			}
			catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){ 
				e.printStackTrace();
			}
		}

		int like=0;
		index=cursor.getColumnIndex("total_like");
		if(index>0){
			like=cursor.getInt(index);
		}
		if(like>0){
			holder.newsFeedTotalLike.setText(like+"");
		}
		else{
			holder.newsFeedTotalLike.setText("0");
		}

		int share=0;
		index=cursor.getColumnIndex("total_share");
		if(index>0){
			share=cursor.getInt(index);
		}
		if(share>0){
			holder.newsFeedTotalShare.setText(share+"");
		}else{
			holder.newsFeedTotalShare.setText("0");
		}

		index = cursor.getColumnIndex("has_offer");
		if(index > 0) {
			int has_offer = cursor.getInt(index);
			if(has_offer == 1) {
				holder.newsIsOffer.setVisibility(View.VISIBLE);
			}else {
				holder.newsIsOffer.setVisibility(View.GONE);		    
			}
		}else {
			holder.newsIsOffer.setVisibility(View.GONE);		    
		}

		index = cursor.getColumnIndex("store_name");
		if(index > 0) {
			String sName = cursor.getString(index);
			holder.newsFeedStoreName.setText(sName.toUpperCase(Locale.US));
		}else {
			holder.newsFeedStoreName.setVisibility(View.GONE);
		}

		index = cursor.getColumnIndex("description");
		if(index > 0) {
			String sDescription = cursor.getString(index);
			holder.newsFeedOffersDesc.setText(sDescription);
		}else {
			holder.newsFeedOffersDesc.setVisibility(View.GONE);
		}

		index=cursor.getColumnIndex("title");
		if(index>0){
			String sTitle=cursor.getString(index);
			holder.newsFeedOffersText.setText(sTitle);
		}else{
			holder.newsFeedOffersText.setText("");
		}


		index=cursor.getColumnIndex("post_date");
		if(index > 0) {
			String sPost_date = cursor.getString(index);

			DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dt2=new SimpleDateFormat("MMM");
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			DateFormat date_format=new SimpleDateFormat("hh:mm a");

			try {
				Date date_con = (Date) dt.parse(sPost_date);
				Date date=dateFormat.parse(sPost_date);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				holder.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}

		}else {
			holder.newsFeedOffersDate.setVisibility(View.GONE);
		}
	}



	@Override
	public View getView(final int position, View view, ViewGroup viewGroup) {
		View tmpView=super.getView(position, view, viewGroup);
		TextView txtLike=(TextView)tmpView.findViewById(R.id.newsFeedTotalLike);
		TextView txtShare=(TextView)tmpView.findViewById(R.id.newsFeedTotalShare);
		ImageView imgCallStore=(ImageView)tmpView.findViewById(R.id.imgCallStore);
		txtLike.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				likePost(position);
			}
		});
		txtShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sharePost(position);
			}
		});
		imgCallStore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				callStore(position);
			}
		});
		return tmpView;
	}



	private class ViewHolder
	{
		TextView newsFeedOffersDate;
		TextView newsFeedOffersMonth;
		TextView newsFeedOffersText;
		TextView newsFeedOffersDesc;
		TextView newsFeedTotalLike;
		TextView newsFeedTotalShare;
		ImageView imgNewsFeedLogo;
		View newsFeedviewOverLay;
		TextView newFeedband;
		ImageView newsIsOffer;
		ImageView imgCallStore;
		TextView newsFeedStoreName;
	}



	@Override
	public void onCompleted(String sURL, String sJson,String tag) {
		debug(sURL);
		debug(sJson);
		if(mProgressDialog!=null){
			mProgressDialog.dismiss();
		}
		if(tag.equalsIgnoreCase("Offers")){
			try{

				

				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
	
				if(status == null) return;

				if(status.equals("false")){

					return;
				}

				if(isMbRefreshing()==true){
					Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from OFFERS_RECORDS where area_id="+Config.MY_AREA).loadInBackground();
					Cursor c=new ShoplocalCursorLoader(mActivity, "Delete from OFFERS where area_id="+Config.MY_AREA).loadInBackground();
					if(c!=null && c1!=null){
						debug("Deleted "+c.getCount());
						setMbRefreshing(false);
						mList.onRefreshComplete();
						msPostId.clear();
					}
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");
				if(dataObject == null) return;

				int records = dataObject.getInt("total_record");
				int page = dataObject.getInt("total_page");

				JSONArray jsonArray = dataObject.getJSONArray("record");
				if(jsonArray == null)  return;

				int count = jsonArray.length();
				ContentValues[] values = new ContentValues[count];

				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					if(offerObject != null) {
						ContentValues value = new ContentValues();
						value.put("id",offerObject.getInt("id"));
						value.put("area_id",offerObject.getString("area_id"));
						value.put("store_name",offerObject.getString("name"));

						value.put("latitude",offerObject.getString("latitude"));
						value.put("longitude",offerObject.getString("longitude"));
						value.put("distance",offerObject.getString("distance"));

						value.put("mob_no_1",offerObject.getString("mob_no1"));
						value.put("mob_no_2",offerObject.getString("mob_no2"));
						value.put("mob_no_3",offerObject.getString("mob_no3"));

						value.put("tel_no_1",offerObject.getString("tel_no1"));
						value.put("tel_no_2",offerObject.getString("tel_no2"));
						value.put("tel_no_3",offerObject.getString("tel_no3"));

						value.put("title",offerObject.getString("title"));

						value.put("post_type",offerObject.getString("type"));
						value.put("post_id",offerObject.getString("post_id"));
						msPostId.add(offerObject.getString("post_id"));
						value.put("description",offerObject.getString("description"));

						value.put("image_url_1",offerObject.getString("image_url1"));
						value.put("image_url_2",offerObject.getString("image_url2"));
						value.put("image_url_3",offerObject.getString("image_url3"));

						value.put("thumb_url_1",offerObject.getString("thumb_url1"));
						value.put("thumb_url_2",offerObject.getString("thumb_url2"));
						value.put("thumb_url_3",offerObject.getString("thumb_url3"));

						value.put("post_date",offerObject.getString("date"));
						value.put("has_offer",offerObject.getString("is_offered"));

						value.put("offer_date_time",offerObject.getString("offer_date_time"));

						value.put("total_like",offerObject.getString("total_like"));
						value.put("total_share",offerObject.getString("total_share"));

						value.put("verified",offerObject.getString("verified"));

						values[i] = value;
					}
				}

				//Inserting In RECORD_STATUS
				ContentValues [] contentValues=new ContentValues[1];
				ContentValues value=new ContentValues();
				value.put("area_id",Config.MY_AREA);
				value.put("current_page",mPage+1);
				value.put("total_record",records);
				value.put("total_page",page);
				contentValues[0]=value;

				DBUtil dbUtil = DBUtil.getInstance(mActivity.getApplicationContext());
				dbUtil.replaceOrUpdate("OFFERS_RECORDS", contentValues);
				dbUtil.replaceOrUpdate(this,"OFFERS",values,"offers");

			}catch(Exception ex){
				ex.printStackTrace();
				debug("Exception = " + ex.toString());
			}
		}
		if(tag.equalsIgnoreCase("likePost")){

		}
		if(tag.equalsIgnoreCase("sharePost")){
			
		}

	}

	void likePost(int position)
	{
		mListClick.listItemClick(position,msPostId.get(position), "like");
	}

	void sharePost(int position)
	{
		mListClick.listItemClick(position,msPostId.get(position), "share");
	}
	void callStore(int position)
	{
		mListClick.listItemClick(position, msPostId.get(position),"call");
	}

	private void showProgressDialog(){
		mProgressDialog.show();
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}

	@Override
	public void onError(int errorCode) {
		if(mProgressDialog!=null){
			mProgressDialog.dismiss();
			if(isMbRefreshing()==true){
				mList.onRefreshComplete();
				setMbRefreshing(false);
			}
			Toast.makeText(mActivity, "Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onCompleteInsertion(String tag) {
//		mActivity.getSupportLoaderManager().restartLoader(R.id.MARKET_LOADER,null,this);		
		//mActivity.getSupportLoaderManager().getLoader(R.id.MARKET_LOADER).forceLoad();
	}

}
