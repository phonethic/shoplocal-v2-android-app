package com.phonethics.shoplocal;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.CallListDialog;
import com.phonethics.adapters.MyShoplocalAdapter;

public class MyShoplocal extends ShoplocalActivity implements ListClickListener, OnItemClickListener, 
OnLastItemVisibleListener, OnRefreshListener<ListView>, ShoplocalAsyncTaskStatusListener
{
	private PullToRefreshListView mList;
	private TextView mTxtCounter;

	private int  miTotalRecords;
	private int  miTotalPages;
	private int  miCurrentPage=0;
	private int  miItemPerPage=50;
	private int miTempPosition=0;

	private boolean mbIsRefreshing=false;	

	private String msTempPostId="";
	private Activity mContext;

	private String msStoreId="";
	private String msPostId="";
	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msStoreName="";
	private String msImageLink="";
	private String msIsOffer="";
	private String msOfferValidTill="";
	private String msPostTitle="";
	private String msPostDescription="";
	private String msShareVia="";
	private String msCallType="";
	private String msActiveAreaName="";

	private MyShoplocalAdapter mAdapter;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo1=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo2=new ArrayList<String>();
	private ArrayList<String> mArrMobileNo3=new ArrayList<String>();

	private ArrayList<String> mArrTelNo1=new ArrayList<String>();
	private ArrayList<String> mArrTelNo2=new ArrayList<String>();
	private ArrayList<String> mArrTelNo3=new ArrayList<String>();

	private ArrayList<String> mArrPostId=new ArrayList<String>();
	private ArrayList<String> mArrPostTitle=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrPostDesc=new ArrayList<String>();
	private ArrayList<String> mArrPostDate=new ArrayList<String>();
	private ArrayList<String> mArrPostImage=new ArrayList<String>();
	private ArrayList<String> mArrPostHasOffer=new ArrayList<String>();
	private ArrayList<String> mArrTotalLike=new ArrayList<String>();
	private ArrayList<String> mArrTotalShare=new ArrayList<String>();
	private ArrayList<String> mArrPostValidityDate= new ArrayList<String>();
	private ArrayList<String> mArrCall= new ArrayList<String>();

	private ProgressDialog mProgressDialog;
	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;
	
	//private TextView txtEmptyMessage;
	
	private long back_pressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=this;
		mList=(PullToRefreshListView)findViewById(R.id.dafault_listSearchResult);
		((TextView) findViewById(R.id.viewOffers)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
				if(!event.equals("")){
					EventTracker.logEvent(event, true);
				}
				msDrawerItem="";
				Intent intent= new Intent(MyShoplocal.this,NewsFeedActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		
		((TextView) findViewById(R.id.addBusiness)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
				if(!event.equals("")){
					EventTracker.logEvent(event, true);
				}
				Intent intent=new Intent(MyShoplocal.this,MarketPlaceActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		mTxtCounter=(TextView)findViewById(R.id.txtCounter);
		//txtEmptyMessage=(TextView)findViewById(R.id.txtEmptyMessage);
		setTitle(getResources().getString(R.string.itemMyShoplocalOffers));
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		mList.setLayoutAnimation(controller);
		
		mProgressDialog=new ProgressDialog(mContext);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");
		
		if(mSessionManager.isLoggedInCustomer()==true){
			getMyShoplocalOffers();
		}else{
			mList.setVisibility(View.GONE);
			showLoginLayout(true);
		}
		
		
		
		msActiveAreaName=mDbUtil.getActiveArea();

		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());
		
		mAdapter=new MyShoplocalAdapter(mContext, 0, 0, mArrPostId, mArrPostTitle, mArrPostDesc, mArrStoreName, 
				mArrPostHasOffer, mArrPostImage, mArrPostDate, 
				mArrTotalLike, mArrTotalShare, this);
		mList.setAdapter(mAdapter);
		mList.setOnLastItemVisibleListener(this);
		mList.setOnRefreshListener(this);
		mList.setOnItemClickListener(this);

	}


	private void showProgressDialog(){

		mProgressDialog.show();
	}

	void getMyShoplocalOffers()
	{
		if(mInternet.isNetworkAvailable()){
			EventTracker.logEvent("MyShoplocal_Fetch", false);
			
			String msUrl=Config.MY_SHOPLOCAL_URL+ "?page=" + (miCurrentPage+1) + "&count="+miItemPerPage;
			new ShoplocalAsyncTask(this, "get", "customer", true, "myShoplocal").execute(msUrl);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(mSessionManager.isLoggedInCustomer()){
			SubMenu subMenu1 = menu.addSubMenu("Extra Settings");
			subMenu1.add(getResources().getString(R.string.itemMyProfile));
			subMenu1.add(getResources().getString(R.string.itemFavouriteStores));
			MenuItem subMenu1Item = subMenu1.getItem();
			subMenu1Item.setIcon(R.drawable.ic_drawer_profile);
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getTitle().toString().equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			EventTracker.logEvent("Tab_CustomerProfile", false);
			Intent intent=new Intent(mContext,CustomerProfile.class);
			startActivity(intent);
		}
		if(item.getTitle().toString().equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			EventTracker.logEvent("Tab_FavouriteBusiness", false);
			Intent intent=new Intent(mContext,MerchantFavouriteStores.class);
			startActivity(intent);
		}
		if(item.getItemId()==android.R.id.home){
			toggleDrawer();
		}
		return true;
	}

	void invalidateActionBar()
	{
		this.supportInvalidateOptionsMenu();
	}

	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		this.msTempPostId=msPostId;
		miTempPosition=position;
		if(tag.equalsIgnoreCase("like")){
			if(mSessionManager.isLoggedInCustomer()){
				likePost(msPostId);
			}else{
				loginAlert(getResources().getString(R.string.postFavAlert), 1);
			}
		}else if(tag.equalsIgnoreCase("share")){
			if(mInternet.isNetworkAvailable()){
				showShareDialog(position);
			}else{
				showToast(getString(R.string.noInternetConnection));
			}
		}else if(tag.equalsIgnoreCase("call")){
			
			mArrCall.clear();
			
			mCalldailog.setTitle(mArrStoreName.get(position));
			if(mArrMobileNo1.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo1.get(position).toString());
			}
			if(mArrMobileNo2.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo2.get(position).toString());
			}
			if(mArrMobileNo3.get(position).length()>5){
				mArrCall.add("+"+mArrMobileNo3.get(position).toString());
			}
			if(mArrTelNo1.get(position).length()>5){
				mArrCall.add(mArrTelNo1.get(position).toString());
			}
			if(mArrTelNo2.get(position).length()>5){
				mArrCall.add(mArrTelNo2.get(position).toString());
			}
			if(mArrTelNo3.get(position).length()>5){
				mArrCall.add(mArrTelNo3.get(position).toString());
			}
			mCallAdapter.notifyDataSetChanged();
			
			if(mArrCall.size()!=0){
				
				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("Source","MyShoplocal");
				callClicked.put("storeName",mArrStoreName.get(miTempPosition));
				debug(mArrStoreName.get(miTempPosition));
				EventTracker.logEvent("StoreCall", callClicked);
				
				msCallType="StoreCall";
				callContactApi(msPostId);
				mCalldailog.show();
			}else{
				showToast("No numbers to call");
			}
		}
	}
	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			
			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("Source","MyShoplocal");
			callClicked.put("storeName",mArrStoreName.get(miTempPosition));
			callClicked.put("Number",mArrCall.get(position));
			debug(mArrStoreName.get(miTempPosition));
			EventTracker.logEvent("StoreCallDone", callClicked);
			
			likePlace(mArrStoreId.get(miTempPosition));
			
			msCallType="StoreCallDone";
			callContactApi(msTempPostId);
			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();
			
		}
		
	}
	
	void likePlace(String postId)
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", postId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlaceOnCall").execute(msUrl);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	void callContactApi(String postId)
	{
		try
		{
			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
					
					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put("post_id", postId);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);
					
					debug(jsonObject.toString());
					
					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void showShareDialog(int position){
		try
		{
			msMobNo1=mArrMobileNo1.get(position);
			msMobNo2=mArrMobileNo2.get(position);
			msMobNo3=mArrMobileNo3.get(position);

			msTelNo1=mArrTelNo1.get(position);
			msTelNo2=mArrTelNo2.get(position);
			msTelNo3=mArrTelNo3.get(position);

			msIsOffer=mArrPostHasOffer.get(position);
			msOfferValidTill=mArrPostValidityDate.get(position);

			msPostTitle=mArrPostTitle.get(position);
			msPostDescription=mArrPostDesc.get(position);
			msStoreName=mArrStoreName.get(position);

			msImageLink=mArrPostImage.get(position);

			String shareBody= "";
			String validTill="";
			String contact="";
			String contact_lable="\nContact:";
			if(msIsOffer.length()!=0 && msIsOffer.equalsIgnoreCase("1")){
				try
				{
					DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
					DateFormat dt2=new SimpleDateFormat("MMM");
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

					Date date_con = (Date) dt.parse(msOfferValidTill.toString());
					Date date=dateFormat.parse(msOfferValidTill.toString());

					DateFormat date_format=new SimpleDateFormat("hh:mm a");
					Calendar cal = Calendar.getInstance();
					cal.setTime(date_con);
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH);
					int day = cal.get(Calendar.DAY_OF_MONTH);
					validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);

				}catch(Exception ex)
				{
					ex.printStackTrace();
					validTill="";
				}
			}
			else{
				validTill="";
			}
			try
			{
				if(msMobNo1.length()>0){
					contact="+"+msMobNo1;
				} 
				if(msMobNo2.length()>0){
					contact+=" +"+msMobNo2;
				}
				if(msMobNo3.length()>0){
					contact+=" +"+msMobNo3;
				}
				if(msTelNo1.length()>0){
					contact+=" "+msTelNo1;
				}
				if(msTelNo2.length()>0){
					contact+=" "+msTelNo2;
				}
				if(msTelNo3.length()>0){
					contact+=" "+msTelNo3;
				}
				if(contact.length()==0){
					contact_lable="";
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			shareBody=msStoreName + " - " + "Offers" + " : " + "\n\n"+msPostTitle + "\n" + 
					msPostDescription +validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
			offerShareDialog(msStoreName, msImageLink, shareBody);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==1)
				{
					Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
					mContext.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		EventTracker.logEvent("MyShoplocal_Details", false);
		int tempPos=position-1;
		Intent intent=new Intent(this, OffersBroadCastDetails.class);
		intent.putExtra("storeName",mArrStoreName.get(tempPos));
		intent.putExtra("title",mArrPostTitle.get(tempPos));
		intent.putExtra("body",mArrPostDesc.get(tempPos));
		intent.putExtra("PLACE_ID",mArrStoreId.get(tempPos));
		intent.putExtra("POST_ID",mArrPostId.get(tempPos));
		intent.putExtra("fromCustomer", true);
		intent.putExtra("mob1",mArrMobileNo1.get(tempPos));
		intent.putExtra("mob2",mArrMobileNo2.get(tempPos));
		intent.putExtra("mob3",mArrMobileNo3.get(tempPos));
		intent.putExtra("tel1",mArrTelNo1.get(tempPos));
		intent.putExtra("tel2",mArrTelNo2.get(tempPos));
		intent.putExtra("tel3",mArrTelNo3.get(tempPos));
		intent.putExtra("photo_source",mArrPostImage.get(tempPos));
		intent.putExtra("USER_LIKE","-1");
		startActivity(intent);
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	}

	@Override
	public void onLastItemVisible() {
		if(mInternet.isNetworkAvailable()){
			if(miCurrentPage<miTotalPages){
				String msUrl=Config.MY_SHOPLOCAL_URL+ "?page=" + (miCurrentPage+1) + "&count="+miItemPerPage;
				new ShoplocalAsyncTask(this, "get", "customer", true, "myShoplocal").execute(msUrl);
			}
			else{
				debug("current page "+miCurrentPage+" total pages "+miTotalPages);
			}
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		mbIsRefreshing=true;
		if(mInternet.isNetworkAvailable()){
			miCurrentPage=0;
			String msUrl=Config.MY_SHOPLOCAL_URL+ "?page=" + (miCurrentPage+1) + "&count="+miItemPerPage;
			new ShoplocalAsyncTask(this, "get", "customer", true, "myShoplocal").execute(msUrl);

		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void clearData()
	{
		mArrStoreId.clear();
		mArrStoreName.clear();
		mArrMobileNo1.clear();
		mArrMobileNo2.clear();
		mArrMobileNo3.clear();
		mArrTelNo1.clear();
		mArrTelNo2.clear();
		mArrTelNo3.clear();
		mArrPostTitle.clear();
		mArrPostId.clear();
		mArrPostDesc.clear();
		mArrPostImage.clear();
		mArrPostDate.clear();
		mArrPostHasOffer.clear();
		mArrTotalLike.clear();
		mArrTotalShare.clear();
		mArrPostValidityDate.clear();

		miTotalPages=0;
		miTotalRecords=0;
		miCurrentPage=0;

	}

	@Override
	public void onBackPressed() {
		if(back_pressed+2000 >System.currentTimeMillis()){
			mLocalNotification.alarm();
			if(isAppRated()==false){
			setSession(getSessionLength()+getTimeDiff()); //Add session  length.
			}
			mDbUtil.close();
			AppLaunchPrefs.clearPrefs(mContext);
			finish();
		}
		else{
			showToast(getResources().getString(R.string.backpressMessage));
			back_pressed=System.currentTimeMillis();
		}
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		if(tag.equalsIgnoreCase("myShoplocal")){
			try
			{
				showEmptyLayout(false);
				mList.setVisibility(View.VISIBLE);
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					//showToast(msg);
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
						mList.setVisibility(View.GONE);
						showLoginLayout(true);
					}else if(code.equalsIgnoreCase("-240")){
						showEmptyLayout(true);
						//txtEmptyMessage.setText(msg);
						mList.setVisibility(View.GONE);
					}
					return;
				}
				
				if(mbIsRefreshing==true){
					mbIsRefreshing=false;
					clearData();
					mList.onRefreshComplete();
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");
				if(dataObject == null) return;

				miCurrentPage++;
				miTotalRecords = dataObject.getInt("total_record");
				miTotalPages = dataObject.getInt("total_page");

				JSONArray jsonArray = dataObject.getJSONArray("record");
				if(jsonArray == null)  return;

				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					if(offerObject != null) {
						mArrStoreId.add(offerObject.getInt("id")+"");
						
						String desc = offerObject.getString("name").trim();
						String seperator = "\\\\'";
						String []tempText = desc.split(seperator);
						
				        String b= "";
				        for (String c : tempText) {
				        	b +=c+"\'";
				        }
				        b = b.substring(0, b.length()-1);
						
						mArrStoreName.add(b);
						mArrMobileNo1.add(offerObject.getString("mob_no1"));
						mArrMobileNo2.add(offerObject.getString("mob_no2"));
						mArrMobileNo3.add(offerObject.getString("mob_no3"));
						mArrTelNo1.add(offerObject.getString("tel_no1"));
						mArrTelNo2.add(offerObject.getString("tel_no2"));
						mArrTelNo3.add(offerObject.getString("tel_no3"));
						
						desc = offerObject.getString("title").trim();
						seperator = "\\\\'";
						tempText = desc.split(seperator);
						
				        b= "";
				        for (String c : tempText) {
				        	b +=c+"\'";
				        }
				        b = b.substring(0, b.length()-1);
						
						mArrPostTitle.add(b);
						mArrPostId.add(offerObject.getString("post_id"));
						
						desc = offerObject.getString("description").trim();
						seperator = "\\\\'";
						tempText = desc.split(seperator);
						
				        b= "";
				        for (String c : tempText) {
				        	b +=c+"\'";
				        }
				        b = b.substring(0, b.length()-1);
						
						mArrPostDesc.add(b);
						mArrPostImage.add(offerObject.getString("image_url1"));
						mArrPostDate.add(offerObject.getString("date"));
						mArrPostValidityDate.add(offerObject.getString("offer_date_time"));
						mArrPostHasOffer.add(offerObject.getString("is_offered"));
						mArrTotalLike.add(offerObject.getString("total_like"));
						mArrTotalShare.add(offerObject.getString("total_share"));
					}
				}

				mTxtCounter.setText(mArrPostId.size()+"/"+miTotalRecords);

				mAdapter.notifyDataSetChanged();

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("likePost")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){
					
					try
					{
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(mArrStoreName.get(miTempPosition));
						if(mArrPostImage.get(miTempPosition).length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+mArrPostImage.get(miTempPosition));
						}
						fbObj.setStoreDescription(mArrPostDesc.get(miTempPosition));
						String encodedurl = URLEncoder.encode(mArrStoreName.get(miTempPosition),"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+mArrStoreId.get(miTempPosition));
						FacebookOperations.FavThisOnFb(mActivity, fbObj, getString(R.string.fb_offer_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					String message=jsonObject.getString("message");
					showToast(message);
				}

			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("sharePost")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;

				if(status.equalsIgnoreCase("true")){

					if(mbFaceBookShare==false)
					{
						try
						{
							FbObject fbObj=new FbObject();
							fbObj.setStoreTitle(mArrStoreName.get(miTempPosition));
							if(mArrPostImage.get(miTempPosition).length()==0){
								fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
							}else{
								fbObj.setStoreImage(Config.IMAGE_URL+mArrPostImage.get(miTempPosition));
							}
							fbObj.setStoreDescription(mArrPostDesc.get(miTempPosition));
							String encodedurl = URLEncoder.encode(mArrStoreName.get(miTempPosition),"UTF-8");
							debug(Config.FACEBOOK_SHARE_URL+msSlug);
							fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+mArrStoreId.get(miTempPosition));
							FacebookOperations.shareThisInfo(mActivity, fbObj, getString(R.string.fb_offer_object));
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}


	@Override
	public void onError(int errorCode) {
		if(mProgressDialog.isShowing())
		{
			mProgressDialog.dismiss();
		}
		showToast(getResources().getString(R.string.errorMessage));
	}

	void likePost(String msPostId)
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				EventTracker.logEvent("MyShoplocal_Liked", false);
				
				JSONObject json = new JSONObject();
				json.put("user_id", Config.CUST_USER_ID);
				json.put("post_id", msTempPostId);
				json.put("auth_id", Config.CUST_AUTH_ID);
				String msUrl=Config.POST_LIKE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false,json,"likePost").execute(msUrl);
			}else
			{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void sharePost(String msPostId)
	{
		try
		{
			try {
				if(mInternet.isNetworkAvailable()){
					EventTracker.logEvent("MyShoplocal_Shared", false);
					
					JSONObject json = new JSONObject();
					json.put("post_id", msTempPostId);
					json.put("via", msShareVia);
					String msUrl=Config.POST_SHARE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"sharePost").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void debug(String msg){
		Log.d("MyShoplocal","MyShoplocal "+msg);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
			if(mSessionManager.isLoggedInCustomer())
			{
				invalidateActionBar();
				//	EventTracker.logEvent("MyShoplocal_LoginSuccess", false);
				mList.setVisibility(View.VISIBLE);
				getMyShoplocalOffers();
				showLoginLayout(false);
			}
			else
			{
				showLoginLayout(true);
			}
		}
		if(requestCode==4)
		{
			if(mSessionManager.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();
				}
				else
				{
					Intent intent=new Intent(mContext,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
		if(requestCode == 3){
			//		VIA="Android";
			//	callShareApi(TEMP_POST_ID);
			msShareVia="Android";
			sharePost(msPostId);
		}

		try
		{
			mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {

				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}


				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					//				VIA="Android-Facebook";
					//				callShareApi(TEMP_POST_ID);
					msShareVia="Android-Facebook";
					sharePost(msPostId);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}