package com.phonethics.shoplocal;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.localnotification.LocalNotification;

public class FavouriteStoresAdapter extends ShoplocalAdapter implements DBListener  {

	private ProgressDialog mProgressDialog;

	int ITEMS_PER_PAGE = 50;
	private int mPage = 0;
	private PullToRefreshListView mList;
	private RelativeLayout mEmptyData;
	private boolean mbRefreshing = false;
	private Typeface  tf = null;
	private NetworkCheck mInernet;
	private SessionManager mSessionManager;
	private LocalNotification mLocalNotification;
	private ListClickListener mListener;
	private int mFavRecord=0;
	private int mFavStore=0;
	//private TextView txtEmptyMessage;
	public FavouriteStoresAdapter(FragmentActivity activity, PullToRefreshListView list,RelativeLayout mEmptyData,
			/*TextView txtEmptyMessage,*/ListClickListener mListener)  {
		super(activity, R.layout.layout_marketplace);
		debug("Constructor 1");
		mList = list;
		this.mEmptyData=mEmptyData;
		//this.txtEmptyMessage=txtEmptyMessage;
		
		mProgressDialog=new ProgressDialog(mActivity);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setTitle("Shoplocal");
		mProgressDialog.setMessage("Loading please wait");
		
		mInernet=new NetworkCheck(activity);
		mSessionManager=new SessionManager(activity);
		tf=Typeface.createFromAsset(mActivity.getAssets(), "fonts/GOTHIC_0.TTF");
		mLocalNotification=new LocalNotification(mActivity.getApplicationContext());
		this.mListener=mListener;
		

	}

	public void setmPage(int mPage) {
		this.mPage = mPage;
	}

	public boolean isMbRefreshing() {
		return mbRefreshing;
	}

	public void setMbRefreshing(boolean mbRefreshing) {
		this.mbRefreshing = mbRefreshing;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		String sQuery = "Select F._id,F.id,F.logo,F.total_like,F.distance,F.store_name,F.mob_no_1,F.mob_no_2,F.mob_no_3,F.tel_no_1,F.tel_no_2,F.tel_no_3,F.description"
				+" from FAVOURITE_STORES as F ";
		return new ShoplocalCursorLoader(mActivity,sQuery);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		debug("onFinish "+cursor+" count "+cursor.getCount());
		try{
			if(mProgressDialog!=null){
				mProgressDialog.dismiss();
			}
			if(cursor==null || cursor.getCount()==0){
				String sURL = Config.ALL_FAVOURITE_PLACES_URL +"page=" + (mPage+1) + "&count="+ITEMS_PER_PAGE;
				if(mInernet.isNetworkAvailable()){
					getData(sURL,"customer",true,"favourtie");
				}else{
					showToast(mActivity.getResources().getString(R.string.noInternetConnection));
				}
				return;
			}
			changeCursor(cursor);
			mCursor = cursor;
			mCount = cursor.getCount();
			debug("total = " + mCount);
//			cursor.moveToNext();
//			int index=cursor.getColumnIndex("total_record");
//			mTxtCounter.setText(mCount+"/"+cursor.getString(index));
			notifyDataSetChanged();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void showToast(String msg){
		Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
	}

	private void debug(String s) {
		Log.v("FavouriteAdapter " , "FavouriteAdapter " + s);
	}


	@Override
	public void bindView(View convertView, Context context, Cursor cursor) {

		if(convertView == null) {
			convertView=LayoutInflater.from(mActivity).inflate(R.layout.layout_marketplace,null);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();

		if(holder == null) {
			holder=new ViewHolder();
			holder.imgPlaceLogo=(ImageView)convertView.findViewById(R.id.imgPlaceLogo);
			holder.txtStoreName=(TextView)convertView.findViewById(R.id.txtStoreName);
			holder.txtStoreDistance=(TextView)convertView.findViewById(R.id.txtStoreDistance);
			holder.txtStoreDescription=(TextView)convertView.findViewById(R.id.txtStoreDescription);
			holder.txtStoreLikeCount=(TextView)convertView.findViewById(R.id.txtStoreLikeCount);
			holder.imgOfferBand=(ImageView)convertView.findViewById(R.id.imgOfferBand);

			holder.txtStoreName.setTypeface(tf, Typeface.BOLD);
			holder.txtStoreDescription.setTypeface(tf);
			holder.txtStoreDistance.setTypeface(tf);

			convertView.setTag(holder);
		}


		int index = -1;

		int likes = 0;
		index = cursor.getColumnIndex("total_like");
		if(index > 0) {
			likes = cursor.getInt(index);	    
		}
		if(likes > 0) {
			holder.txtStoreLikeCount.setVisibility(View.VISIBLE);
			holder.txtStoreLikeCount.setText(""+likes);
		}else {
			holder.txtStoreLikeCount.setVisibility(View.GONE);
		}
		holder.txtStoreDistance.setVisibility(View.GONE);
//		index = cursor.getColumnIndex("distance");
//
//		if(index > 0) {
//			String sDistance = cursor.getString(index);
//			if(sDistance != null) {
//				double distance = Double.parseDouble(sDistance);
//
//				if(distance < 1 && distance >0) {
//					DecimalFormat format = new DecimalFormat("##.##");
//					String formatted = format.format(distance*100);
//					holder.txtStoreDistance.setText(""+formatted+" m.");
//				} else {
//					DecimalFormat format = new DecimalFormat("##.##");
//					String formatted = format.format(distance);
//					if((distance*100)>8000) {
//						holder.txtStoreDistance.setVisibility(View.GONE);
//					} else {
//						holder.txtStoreDistance.setVisibility(View.VISIBLE);
//						holder.txtStoreDistance.setText(""+formatted+" km.");
//					}
//				}
//			}
//		}

		index = cursor.getColumnIndex("has_offer");
		if(index > 0) {
			int has_offer = cursor.getInt(index);
			if(has_offer == 1) {
				holder.imgOfferBand.setVisibility(View.VISIBLE);
			}else {
				holder.imgOfferBand.setVisibility(View.GONE);		    
			}
		}else {
			holder.imgOfferBand.setVisibility(View.GONE);		    
		}


		index = cursor.getColumnIndex("store_name");
		if(index > 0) {
			String sName = cursor.getString(index).trim();
			String seperator = "\\\\'";
			String []tempText = sName.split(seperator);
			
			/*if (tip.contains(seperator)){*/
	        String b= "";
	        for (String c : tempText) {
	        	b +=c+"\'";
	        }
	        b = b.substring(0, b.length()-1);
			holder.txtStoreName.setText(/*sName*/b.toUpperCase(Locale.US));
		}else {
			holder.txtStoreName.setVisibility(View.GONE);
		}

		index = cursor.getColumnIndex("description");
		if(index > 0) {
			String sDescription = cursor.getString(index);
			holder.txtStoreDescription.setText(sDescription);
		}else {
			holder.txtStoreDescription.setVisibility(View.GONE);
		}


	}

	private class ViewHolder {
		ImageView imgPlaceLogo;
		ImageView imgOfferBand;
		TextView txtStoreName;
		TextView txtStoreDescription;
		TextView txtStoreDistance;
		TextView txtStoreLikeCount;
	}
	

	@Override
	public View getView(final int position, View view, ViewGroup viewGroup) {
		View tmpView=super.getView(position, view, viewGroup);
		ImageView imgCall=(ImageView)tmpView.findViewById(R.id.imgPlaceLogo);
		imgCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showCall(position);
			}
		});
		return tmpView;
	}
	
	void showCall(int position){
		mListener.listItemClick(position, "", "call");
		
	}

	@Override
	public void onCompleted(String sURL, String sJson,String tag) {
		debug(sURL);
		debug(sJson);
		try {
			if(mProgressDialog!=null){
				mProgressDialog.dismiss();
			}

			mEmptyData.setVisibility(View.GONE);
			mList.setVisibility(View.VISIBLE);
			JSONObject jsonObject = new JSONObject(sJson);
			String status = jsonObject.getString("success");
			if(status == null) return;

			if(status.equals("false")){
				String code=jsonObject.getString("code");
				String msg=jsonObject.getString("message");
				if(code.equalsIgnoreCase("-221")){
					mSessionManager.logoutCustomer();
					showToast(msg);
				}
				//No Records Found
				if(code.equalsIgnoreCase("-241"))
				{
					mEmptyData.setVisibility(View.VISIBLE);
					mLocalNotification.checkPref();
					mLocalNotification.setFavPref(false, mLocalNotification.isFavAlarmFired());
					showToast(msg);
					//txtEmptyMessage.setText(msg);
					if(isMbRefreshing()==true){
						Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from FAVOURITE_RECORDS").loadInBackground();
						Cursor c=new ShoplocalCursorLoader(mActivity, "Delete from FAVOURITE_STORES").loadInBackground();
						if(c!=null && c1!=null){
							debug("Deleted "+c.getCount());
							setMbRefreshing(false);
							mList.onRefreshComplete();
							mList.setVisibility(View.GONE);
							
						}
					}
				}
				return;
			}

			if(isMbRefreshing()==true){
				Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from FAVOURITE_RECORDS").loadInBackground();
				Cursor c=new ShoplocalCursorLoader(mActivity, "Delete from FAVOURITE_STORES").loadInBackground();
				if(c!=null && c1!=null){
					debug("Deleted "+c.getCount());
					setMbRefreshing(false);
					mList.onRefreshComplete();
				}
			}
			
			JSONObject dataObject = jsonObject.getJSONObject("data");
			if(dataObject == null) return;

			int records = dataObject.getInt("total_record");
			int page = dataObject.getInt("total_page");

			JSONArray jsonArray = dataObject.getJSONArray("record");
			if(jsonArray == null)  return;

			int count = jsonArray.length();
			ContentValues[] values = new ContentValues[count];

			for(int i=0;i<count;i++) {
				JSONObject storeObject = jsonArray.getJSONObject(i);
				if(storeObject != null) {
					ContentValues value = new ContentValues();
					value.put("id",storeObject.getInt("id"));
					value.put("place_parent",storeObject.getInt("place_parent"));
					value.put("store_name",storeObject.getString("name"));
					value.put("description",storeObject.getString("description"));
					value.put("building",storeObject.getString("building"));
					value.put("street",storeObject.getString("street"));
					value.put("landmark",storeObject.getString("landmark"));
					value.put("area_id",storeObject.getInt("area_id"));
					value.put("area",storeObject.getString("area"));
					value.put("city",storeObject.getString("city"));

					value.put("tel_no_1",storeObject.getString("tel_no1"));
					value.put("tel_no_2",storeObject.getString("tel_no2"));
					value.put("tel_no_3",storeObject.getString("tel_no3"));

					value.put("mob_no_1",storeObject.getString("mob_no1"));
					value.put("mob_no_2",storeObject.getString("mob_no2"));
					value.put("mob_no_3",storeObject.getString("mob_no3"));

					value.put("logo",storeObject.getString("image_url"));
					value.put("email_id",storeObject.getString("email"));
					value.put("website",storeObject.getString("website"));	
					value.put("total_like",storeObject.getInt("total_like"));
					values[i] = value;
				}
			}
			mFavRecord=0;
			mFavStore=0;
			//Inserting In RECORD_STATUS
			ContentValues [] contentValues=new ContentValues[1];
			ContentValues value=new ContentValues();
			value.put("area_id",Config.MY_AREA);
			value.put("current_page",mPage+1);
			value.put("total_record",records);
			value.put("total_page",page);
			contentValues[0]=value;

			DBUtil dbUtil = DBUtil.getInstance(mActivity.getApplicationContext());
			dbUtil.replaceOrUpdate(this,"FAVOURITE_RECORDS", contentValues,"favourit-records");
			dbUtil.replaceOrUpdate(this,"FAVOURITE_STORES",values,"favourite-stores");

			mLocalNotification.checkPref();
			mLocalNotification.setFavPref(true, mLocalNotification.isFavAlarmFired());
			//mTxtCounter.setText(mCount+"/"+records);
		}catch(Exception e) {
			e.printStackTrace();
			debug("Exception = " + e.toString());
		}

	}

	@Override
	public void onError(int errorCode) {

		if(mProgressDialog!=null){
			mProgressDialog.dismiss();
			if(isMbRefreshing()==true){
				mList.onRefreshComplete();
				setMbRefreshing(false);
			}
			Toast.makeText(mActivity, "Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
		}

	}

	private void showProgressDialog(){
		mProgressDialog.show();
	}

	@Override
	public void onAsyncStarted() {
		showProgressDialog();
	}


	@Override
	public void onCompleteInsertion(String tag) {
		debug("Database callback fav rec"+mFavRecord+" fav store"+ mFavStore);
//		mActivity.getSupportLoaderManager().restartLoader(R.id.MARKET_LOADER,null,this);		
		if(tag.equalsIgnoreCase("favourit-records"))
		{
			mFavRecord=1;
			debug("Database callback "+tag);
		}
		if(tag.equalsIgnoreCase("favourite-stores")){
			mFavStore=1;
			debug("Database callback "+tag);
		}
		if(mFavRecord==1 && mFavStore==1){
			debug("Database callback Forceload"+mFavRecord+" fav store"+ mFavStore);
			mActivity.getSupportLoaderManager().getLoader(R.id.MARKET_LOADER).forceLoad();
		}
	}

}
