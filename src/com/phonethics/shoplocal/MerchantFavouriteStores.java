package com.phonethics.shoplocal;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.CallListDialog;

public class MerchantFavouriteStores extends ShoplocalActivity implements OnLastItemVisibleListener, 
OnRefreshListener<ListView>, OnItemClickListener,ListClickListener, ShoplocalAsyncTaskStatusListener
{
	private PullToRefreshListView mList;
	private RelativeLayout mEmptyData;
	private ProgressBar progress;
	private FavouriteStoresAdapter mAdapter=null;
	private String msQuery="";
	private int index=-1;
	private int currentPage = 0;
	private int totalPages=0;

	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msStoreName="";
	private String msStoreId="";
	private String msCallType="";
	private String msActiveAreaName="";

	private ArrayList<String> mArrCall=new ArrayList<String>();

	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;
	//private TextView txtEmptyMessage;

	private Activity mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=this;
		setTitle(getString(R.string.itemFavouriteStores));
		mList=(PullToRefreshListView)findViewById(R.id.dafault_listSearchResult);
		mEmptyData=(RelativeLayout)findViewById(R.id.emptyData);
		//txtEmptyMessage=(TextView)findViewById(R.id.txtEmptyMessage);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		mList.setLayoutAnimation(controller);

		msActiveAreaName=mDbUtil.getActiveArea();

		((TextView) findViewById(R.id.viewOffers)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
				if(!event.equals("")){
					EventTracker.logEvent(event, true);
				}
				msDrawerItem="";
				Intent intent= new Intent(MerchantFavouriteStores.this,NewsFeedActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		
		((TextView) findViewById(R.id.addBusiness)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
				if(!event.equals("")){
					EventTracker.logEvent(event, true);
				}
				Intent intent=new Intent(MerchantFavouriteStores.this,MarketPlaceActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		
		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());

		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		hideDrawerLayout();

		if(mSessionManager.isLoggedInCustomer()==true){
			EventTracker.logEvent("FavouriteStore_Fetch", false);
			mAdapter=new FavouriteStoresAdapter(this,mList,mEmptyData,/*txtEmptyMessage,*/this);
			mList.setAdapter(mAdapter);
		}else{
			mList.setVisibility(View.GONE);
			showLoginLayout(true);
		}
		mList.setOnLastItemVisibleListener(this);
		mList.setOnRefreshListener(this);
		mList.setOnItemClickListener(this);
	}

	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {

			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("Source","FavouriteStore");
			callClicked.put("storeName",msStoreName);
			callClicked.put("Number",mArrCall.get(position));
			debug(msStoreName);
			EventTracker.logEvent("StoreCallDone", callClicked);

			msCallType="StoreCallDone";
			callContactApi(msStoreId);
			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();
		}

	}
	

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		String storeId="";
		storeId=getStoreId(position);
		if(storeId.length()>0){
			Intent intent=new Intent(this, StoreDetailsActivity.class);
			intent.putExtra("STORE_ID", storeId);
			startActivity(intent);
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		refreshData();
		super.onResume();
	}
	
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		refreshData();
	}

	private void refreshData() {
		if(mInternet.isNetworkAvailable()){
			EventTracker.logEvent("FavouriteStore_Fetch", false);
			mAdapter.setMbRefreshing(true);
			mAdapter.setmPage(0);
			mAdapter.getData(Config.ALL_FAVOURITE_PLACES_URL + "page=" +0 
					+ "&count="+mAdapter.ITEMS_PER_PAGE,"customer",true,"Favourite");
		}else{
			showToast(getString(R.string.noInternetConnection));
		}
	}
	@Override
	public void onLastItemVisible() {
		if(mInternet.isNetworkAvailable()){
			Cursor cursor=null;
			try{
				msQuery="Select * from FAVOURITE_RECORDS ";
				cursor=new ShoplocalCursorLoader(this, msQuery).loadInBackground();

				if(cursor!=null){

					cursor.moveToFirst();

					if(cursor.getCount()!=0){
						index=cursor.getColumnIndex("current_page");
						debug(cursor.getCount()+" index "+index);

						if(index>=0){
							currentPage=cursor.getInt(index);
						}

						index=cursor.getColumnIndex("total_page");

						if(index>=0){
							totalPages=cursor.getInt(index);
						}

						debug("current page "+currentPage+" total pages "+totalPages);

						if(currentPage<totalPages){
							mAdapter.setmPage(currentPage);
							mAdapter.getData(Config.ALL_FAVOURITE_PLACES_URL + "page="
									+ (currentPage+1) + "&count="+mAdapter.ITEMS_PER_PAGE,"customer",true,"Favourite");
							EventTracker.logEvent("FavouriteStore_Fetch", false);
						}
						else{
							debug("current page "+currentPage+" total pages "+totalPages);
						}
					}
					cursor.close();
				}
			}catch(CursorIndexOutOfBoundsException c){
				c.printStackTrace();
			}
			finally{
				cursor.close();
			}
		}else{
			showToast(getString(R.string.noInternetConnection));
		}

	}

	void debug(String s){
		Log.i("FavouriteActivity", "FavouriteActivity "+s);
	}

	String  getStoreId(int position)
	{
		String storeId="";
		//Pull to refresh listview index starts from 1
		int index=position-1;
		try
		{
			Cursor cursor=mAdapter.getCursor();
			if(cursor!=null && cursor.getCount()!=0){
				cursor.moveToPosition(index);
				storeId=cursor.getString(cursor.getColumnIndex("id"));
			}
		}catch(Exception ex){
			ex.printStackTrace();
			storeId="";
		}
		return storeId;
	}

	void getContactNumbers(int position)
	{
		try
		{
			int index=position-1;
			Cursor cursor=mAdapter.getCursor();
			if(cursor!=null && cursor.getCount()!=0){
				cursor.moveToPosition(index);
				msStoreId=cursor.getString(cursor.getColumnIndex("id"));
				msStoreName=cursor.getString(cursor.getColumnIndex("store_name"));
				msMobNo1=cursor.getString(cursor.getColumnIndex("mob_no_1"));
				msMobNo2=cursor.getString(cursor.getColumnIndex("mob_no_2"));
				msMobNo3=cursor.getString(cursor.getColumnIndex("mob_no_3"));
				msTelNo1=cursor.getString(cursor.getColumnIndex("tel_no_1"));
				msTelNo2=cursor.getString(cursor.getColumnIndex("tel_no_2"));
				msTelNo3=cursor.getString(cursor.getColumnIndex("tel_no_3"));
			}
			mArrCall.clear();
			mCalldailog.setTitle(msStoreName);
			if(msMobNo1.length()>5){
				mArrCall.add("+"+msMobNo1);
			}
			if(msMobNo2.length()>5){
				mArrCall.add("+"+msMobNo2);
			}
			if(msMobNo3.length()>5){
				mArrCall.add("+"+msMobNo3);
			}
			if(msTelNo1.length()>5){
				mArrCall.add(msTelNo1);
			}
			if(msTelNo2.length()>5){
				mArrCall.add(msTelNo2);
			}
			if(msTelNo3.length()>5){
				mArrCall.add(msTelNo3);
			}
			mCallAdapter.notifyDataSetChanged();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void callContactApi(String storeId)
	{
		try
		{
			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);

					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put("place_id", storeId);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);

					debug(jsonObject.toString());

					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2){

			if(mbShowLoginLayout==true){
				mLoginInfoLayout.setVisibility(View.GONE);
				mbShowLoginLayout=false;
				mList.setVisibility(View.VISIBLE);
				EventTracker.logEvent("FavouriteStore_Fetch", false);
				mAdapter=new FavouriteStoresAdapter(this,mList,mEmptyData,/*txtEmptyMessage,*/this);
				mList.setAdapter(mAdapter);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		return true;
	}

	@Override
	public void finish() {
		super.finish();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		if(tag.equalsIgnoreCase("call")){
			getContactNumbers(position+1);
			if(mArrCall.size()!=0){
				
				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("Source","FavouriteStore");
				callClicked.put("storeName",msStoreName);
				debug(msStoreName);
				EventTracker.logEvent("StoreCall", callClicked);
				
				msCallType="StoreCall";
				callContactApi(msStoreId);
				mCalldailog.show();
			}else{
				showToast("No numbers to call");
			}
		}
	}

	@Override
	public void onAsyncStarted() {

	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		debug(sURL);
	}

	@Override
	public void onError(int errorCode) {
		showToast(getResources().getString(R.string.errorMessage));
	}
}