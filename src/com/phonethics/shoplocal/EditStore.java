package com.phonethics.shoplocal;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.adapters.LocationListAdapter;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;
import com.phonethics.shoplocal.EditStoreFragment.EditFragmentClickListener;
import com.phonethics.shoplocal.TimeFragment.UpdateTimeInterface;
import com.squareup.picasso.Picasso;

public class EditStore extends ShoplocalActivity implements EditFragmentClickListener,
ShoplocalAsyncTaskStatusListener,DBListener,UpdateTimeInterface,LocationListener
{
	private Activity mContext;

	private ViewPager mViewPager;
	private GridView mGridView;

	private ProgressBar mProgress;

	private ListView mListLocation;
	private ListView mCategoryList;

	private PagerTitleStrip mPagerTitleStrip;

	private Button mBtnEdit;
	private Button mBtnDelete;
	private Button mBtnCancel;
	private Button mBtnSelectCategory;

	private LinearLayout mEditLayout;
	private LinearLayout mEditLayoutDone;
	private RelativeLayout mGalleryChooser;

	private Dialog mLocationDialog;
	private Dialog mCategoryDailog;
	private Dialog mGalleryDialog;
	private Dialog mLogoDialog;

	private boolean mbIsNewStore;
	private boolean mbIsEditModeOn;
	private boolean mbIsGpsEnabled=false;
	private boolean mbIsNetworkEnabled=false;

	private int miManageGallery;
	private int miRemoveLogo=0;

	private ArrayList<String> mArrPages= new ArrayList<String>();
	private ArrayList<String>mArrStoreDay=new ArrayList<String>();
	private ArrayList<String>mArrPlaceTimingStatus=new ArrayList<String>();
	private ArrayList<String>mArrOpenTime=new ArrayList<String>();
	private ArrayList<String>mArrCloseTime=new ArrayList<String>();

	private ArrayList<String>mArrAreaName=new ArrayList<String>();
	private ArrayList<String>mArrCity=new ArrayList<String>();
	private ArrayList<String>mArrAreaId=new ArrayList<String>();
	private ArrayList<String>mArrAreaPincode=new ArrayList<String>();

	private ArrayList<String>mArrCategoryId=new ArrayList<String>();
	private ArrayList<String>mArrCategoryName=new ArrayList<String>();
	private ArrayList<String>mArrTempCategoryId=new ArrayList<String>();
	private ArrayList<String>mArrTempCategoryName=new ArrayList<String>();
	private ArrayList<String>mArrSelectedCategoryId=new ArrayList<String>();

	private ArrayList<String>mArrDeleteIds=new ArrayList<String>();

	private String LOCATION="Location";
	private String DETAILS="Details";
	private String CONTACT="Contact";
	private String DESCRIPTION="Description";
	private String OPERATIONS="Operations";
	private String GALLERY="Gallery";
	private String msStoreId;
	private String msStoreName="";
	private String msPincode="";
	private String msAreaId="";
	private String msAreaName="";
	private String msStoreDescription="";
	private String msStoreLatitude="";
	private String msStoreLongitude="";
	private String msStoreLogo="";
	private String msStoreMobileNo1="";
	private String msStoreMobileNo2="";
	private String msStoreMobileNo3="";
	private String msStoreTelNo1="";
	private String msStoreTelNo2="";
	private String msStoreTelNo3="";
	private String msStoreEmail="";
	private String msStoreFb="";
	private String msStoreTwitter="";
	private String msStoreWebsite="";
	private String msStreet;
	private String msLandmark="";
	private String msCity="";
	private String msState="";
	private String msCountry="";
	private String msBuilding="";
	private String msUserLike="-1";
	private String msOpenTime="";
	private String msCloseTime="";
	private String msContact="";
	private String msMapLinkLable="";
	private String msMapLink="";
	private String mCountryCode="";
	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="image/jpeg";
	private String mdLatitude="";
	private String mdLongitude="";
	private String mSelectedCategory="";
	private String msGalleryImageTitle="";
	private String msStoreTimeFrom="00:00";
	private String msStoreTimeTo="00:00";

	private String msUrltoAppend="";
	private String msUrlIds="";
	private String msDeleteGalleryUrlParam = "&gallery_id=";

	private String msLatitude="0.0";
	private String msLongitude="0.0";
	private String msPlaceStatus="1";

	private Calendar mCurrentTime = Calendar.getInstance(); 

	private ArrayList<String> mArrGalleryId=new ArrayList<String>();
	private ArrayList<String> mArrTitle=new ArrayList<String>();
	private ArrayList<String> mArrImageUrl=new ArrayList<String>();
	private ArrayList<String> mArrThumbUrl=new ArrayList<String>();

	private GridAdapter mAdapter=null;

	private final int REQUEST_CODE_TAKE_PICTURE = 7;
	private final int REQ_CODE_GALLERY_SELECTGALLERY = 18; //Receive image from gallery - gallery
	private final int REQ_CODE_LOGO_SELECTGALLERYCROP = 17; //Recieve image from gallery after crop - logo
	private final int REQ_CODE_LOGO_SELECTGALLERY = 16; //Receive image from gallery - logo
	private final int REQ_CODE_GETCLICKEDIMAGE = 12;
	private final int REQ_CODE_GAlLERY_SELECTGALLERYCROP = 19;

	private byte [] pager_byte=null;
	private  byte[] gallery_byte = null ;

	private File mFileTemp;

	private LocationManager mLocationManager;
	private Location mLocation;

	private Map<String, String> eventMap=new HashMap<String, String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		msPlaceStatus="1";
		mContext=this;
		setContentView(R.layout.activity_edit_store);
		hideDrawerLayout();

		Bundle b=getIntent().getExtras();
		if(b!=null){
			mbIsNewStore=b.getBoolean("isNew");
			msStoreId=b.getString("STORE_ID");
			miManageGallery=b.getInt("manageGallery");
		}
		mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mLocationDialog=new Dialog(mContext);
		mLocationDialog.setContentView(R.layout.layout_location_chooser_dialog);
		mLocationDialog.setCancelable(true);
		mLocationDialog.setTitle("Choose Location");
		mLocationDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mCategoryDailog=new Dialog(mContext);
		mCategoryDailog.setContentView(R.layout.categorylistdialog);
		mCategoryDailog.setCancelable(true);
		mCategoryDailog.setTitle("Choose Category");
		mCategoryDailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mGalleryDialog=new Dialog(mContext); 
		mGalleryDialog.setContentView(R.layout.gallery_image_title);
		mGalleryDialog.setCancelable(true);
		mGalleryDialog.setTitle("Set Title");
		mGalleryDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mLogoDialog=new Dialog(mContext); 
		mLogoDialog.setContentView(R.layout.logopickerdialog);
		mLogoDialog.setCancelable(true);
		mLogoDialog.setTitle("Select an Option");
		mLogoDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mCategoryList=(ListView)mCategoryDailog.findViewById(R.id.categDilogList);
		mListLocation=(ListView)mLocationDialog.findViewById(R.id.locationList);

		mListLocation.setOnItemClickListener(new LocationDialog());

		mProgress=(ProgressBar)findViewById(R.id.progUpload);

		mPagerTitleStrip=(PagerTitleStrip)findViewById(R.id.pagerTabs);
		mViewPager=(ViewPager)findViewById(R.id.viewPagerManageStore);

		mGridView=(GridView)findViewById(R.id.imageGrid);

		mEditLayoutDone = (LinearLayout) findViewById(R.id.editDoneLay);
		mEditLayout = (LinearLayout) findViewById(R.id.editLay);
		mGalleryChooser=(RelativeLayout)findViewById(R.id.galleryImageChooser);

		mBtnDelete = (Button) findViewById(R.id.done);
		mBtnCancel= (Button) findViewById(R.id.Cancel);
		mBtnEdit = (Button) findViewById(R.id.Edit);
		mBtnSelectCategory=(Button)mCategoryDailog.findViewById(R.id.btnDoneSelection);

		mLocationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		mLocation=mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		mbIsGpsEnabled=mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		mbIsNetworkEnabled=mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		//		if(mbIsGpsEnabled==false){
		//			showLocationAlert("GPS is disabled please enable it get better accurracy.");
		//		}else if(mbIsGpsEnabled==false && mbIsNetworkEnabled==false){
		//			showLocationAlert("Please enable location services.");
		//		}

		if(mLocation==null){
			mLocation=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if(mLocation!=null){	
			}else{
				if(miManageGallery!=1){
					showToast(getResources().getString(R.string.locationAlert));
				}
			}
		}
		getGallery();
		if(miManageGallery!=1){
			mBtnEdit.setVisibility(View.GONE);
			mViewPager.setVisibility(View.VISIBLE);
			mGridView.setVisibility(View.GONE);
			mGalleryChooser.setVisibility(View.GONE);
			if(mbIsNewStore==false){
				getStoreDetails();
			}
			else{
				loadCategories();
			}
		}
		if(miManageGallery==1){
			setTitle(getResources().getString(R.string.manageGalleryTitle));
			mBtnEdit.setVisibility(View.VISIBLE);
			mGridView.setVisibility(View.VISIBLE);
			mViewPager.setVisibility(View.GONE);
			mPagerTitleStrip.setVisibility(View.GONE);
			mBtnEdit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mEditLayout.setVisibility(View.VISIBLE);
					mBtnEdit.setVisibility(View.GONE);
					mbIsEditModeOn = true;
					mAdapter=new GridAdapter(mContext, mArrThumbUrl,mArrGalleryId,mArrTitle);
					mAdapter.notifyDataSetChanged();
				}
			});

			mBtnCancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mEditLayout.setVisibility(View.GONE);
					mArrDeleteIds.clear();
					msDeleteGalleryUrlParam = "";

					mBtnEdit.setVisibility(View.VISIBLE);
					mbIsEditModeOn = false;

					mAdapter=new GridAdapter(mContext, mArrThumbUrl,mArrGalleryId,mArrTitle);
					mGridView.setAdapter(mAdapter);
					//					imageGrid.setAdapter(adapterImg);
				}
			});
			mBtnDelete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					if(mArrDeleteIds.size()>0){
						AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
						alertDialogBuilder3.setTitle("Shoplocal");
						alertDialogBuilder3
						.setMessage(getResources().getString(R.string.deleteGalleryImage))
						.setCancelable(false)
						.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								for(int i=0;i<mArrDeleteIds.size();i++){
									Log.d("IDSTODELETE", "IDSTODELETE " + mArrDeleteIds.get(i));
									msUrltoAppend = msUrltoAppend + mArrDeleteIds.get(i);  
									if(i!=mArrDeleteIds.size()-1){
										msUrltoAppend = msUrltoAppend + ",";
									}
									//callDeleteImage();
								}

								Log.d("URLTOSEND","URLTOSEND "  +msDeleteGalleryUrlParam + msUrltoAppend);
								msUrlIds = msDeleteGalleryUrlParam + msUrltoAppend;
								deleteGalleryImages(msUrlIds);
							}
						})
						.setNegativeButton("No",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});
						AlertDialog alertDialog3 = alertDialogBuilder3.create();
						alertDialog3.show();
					}
					else{
						//showToast(getResources().getString(R.string.deleteImageValidation));
					}
				}
			});


		}
		if(mDbUtil.getAreaRowCount()!=0){
			mArrCity=mDbUtil.getAllAreaCity();
			mArrAreaId=mDbUtil.getAllAreaIds();
			mArrAreaPincode=mDbUtil.getAllAreaPin();
			mArrAreaName=mDbUtil.getAllAreas();
			mListLocation.setAdapter(new LocationListAdapter(mContext, 0, 0, mArrAreaName));
		}else{
			checkNearestArea();
		}

		//ViewPager
		mArrPages.add("Location");
		mArrPages.add("Details");
		mArrPages.add("Contact");
		mArrPages.add("Description");
		mArrPages.add("Operations");
		mArrPages.add("Gallery");

		mViewPager.setAdapter(new EditPageFragment(getSupportFragmentManager(), getFragments(), mArrPages));
		mViewPager.setOffscreenPageLimit(mArrPages.size());
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if(position==1){
					try
					{
						if(mbIsNewStore==false){
							Button mBtnLocationSave=(Button)mViewPager.findViewById(R.id.btnManageLocationtSave);
							mBtnLocationSave.setVisibility(View.VISIBLE);
						}
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
				if(position==4)
				{
					try
					{
						EditText edtManageStoreName=(EditText)mViewPager.findViewById(R.id.edtManageStoreName);
						EditText edtManageDesc2=(EditText)mViewPager.findViewById(R.id.edtManageDesc2);

						TextView txtOperationStoreName=(TextView)mViewPager.findViewById(R.id.txtOperationStoreName);;
						TextView txtOperationDesc=(TextView)mViewPager.findViewById(R.id.txtOperationDesc);

						txtOperationStoreName.setText(edtManageStoreName.getText().toString());
						txtOperationDesc.setText(edtManageDesc2.getText().toString());
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	void showLocationAlert(String msg){
		AlertDialog.Builder alertDialog=new AlertDialog.Builder(mContext);
		alertDialog.setTitle("Location");
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("Enable",new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});
		alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alert=alertDialog.create();
		alert.show();
	}

	@Override
	public void onClickView(String tag) {
		if(tag.equalsIgnoreCase("location-save")){
			updateStoreData(1);
		}
		if(tag.equalsIgnoreCase("location-skip")){
			skipToPage();
		}
		if(tag.equalsIgnoreCase("location-mapImage")){
			if(mbIsNewStore==false)
			{
				AlertDialog.Builder alert=new AlertDialog.Builder(mContext);
				alert.setTitle(getResources().getString(R.string.changeLocationAlert));
				alert.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//										locationService();
						if(mLocation!=null){
							onLocationChanged(mLocation);
						}else{
							showToast(getString(R.string.locationAlert));
						}
						dialog.dismiss();
					}
				});
				alert.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				alert.show();
			}
			else
			{
				AlertDialog.Builder alert=new AlertDialog.Builder(mContext);
				alert.setTitle("Are you at the store?");
				alert.setPositiveButton("I am at the Store", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						EventTracker.logEvent("Store_DropPin", false);
						//										locationService();
						if(mLocation!=null){
							onLocationChanged(mLocation);
						}else{
							showToast(getString(R.string.locationAlert));
						}
						dialog.dismiss();
					}
				});
				alert.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				alert.show();
			}
		}
		if(tag.equalsIgnoreCase("details-category")){
		}
		if(tag.equalsIgnoreCase("details-btnSave")){
			updateStoreData(1);
		}
		if(tag.equalsIgnoreCase("details-area")){
			mLocationDialog.show();
		}
		if(tag.equalsIgnoreCase("contact-skip")){
			skipToPage();
		}
		if(tag.equalsIgnoreCase("contact-save")){
			updateStoreData(2);
		}
		if(tag.equalsIgnoreCase("description-save")){
			updateStoreData(3);
		}
		if(tag.equalsIgnoreCase("description-skip")){
			skipToPage();
		}
		if(tag.equalsIgnoreCase("timing-logo")){
			try
			{
				TextView chooseExisting=(TextView)mLogoDialog.findViewById(R.id.chooseExisting);
				TextView takePic=(TextView)mLogoDialog.findViewById(R.id.takePic);
				TextView chooseShoplocal=(TextView)mLogoDialog.findViewById(R.id.chooseShoplocal);
				TextView removeLogo=(TextView)mLogoDialog.findViewById(R.id.removeLogo);
				final ImageView imgLogo=(ImageView)mViewPager.findViewById(R.id.imgManageStore_ThumbPreview);
				mLogoDialog.show();
				if(pager_byte!=null){
					removeLogo.setVisibility(View.VISIBLE);
				}
				else if(!msStoreLogo.equals("") && msStoreLogo.length()!=0){
					removeLogo.setVisibility(View.VISIBLE);
				}
				else{
					removeLogo.setVisibility(View.GONE);
				}

				removeLogo.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if(pager_byte!=null){
							miRemoveLogo=1;
							mLogoDialog.dismiss();
							imgLogo.setImageResource(R.drawable.add_store_logo);
						}
						else if(msStoreLogo.length()!=0){
							miRemoveLogo=1;
							mLogoDialog.dismiss();
							imgLogo.setImageResource(R.drawable.add_store_logo);
						}
						mLogoDialog.dismiss();
					}
				});

				chooseExisting.setOnClickListener(new android.view.View.OnClickListener() {
					@Override
					public void onClick(View v) {
						eventMap.put("source", "Gallery");
						EventTracker.logEvent("Store_LogoAdd", eventMap);
						openGallery();
						mLogoDialog.dismiss();
					}
				});
				takePic.setOnClickListener(new android.view.View.OnClickListener() {
					@Override
					public void onClick(View v) {
						eventMap.put("source", "Camera");
						EventTracker.logEvent("Store_LogoAdd", eventMap);
						mLogoDialog.dismiss();
						takePictureForLogoFromCamera();
					}
				});
				chooseShoplocal.setOnClickListener(new android.view.View.OnClickListener() {
					@Override
					public void onClick(View v) {
						eventMap.put("source", "Shoplocal");
						EventTracker.logEvent("Store_LogoAdd", eventMap);	
						mLogoDialog.dismiss();
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("timing-from")){
			TimeFragment.newInstance("time-from").show(getSupportFragmentManager(), "time-from");
		}
		if(tag.equalsIgnoreCase("timing-to")){
			TimeFragment.newInstance("time-to").show(getSupportFragmentManager(), "time-to");
		}
		if(tag.equalsIgnoreCase("timing-skip")){
			skipToPage();
		}
		if(tag.equalsIgnoreCase("timing-save")){
			updateStoreData(4);
		}
		if(tag.equalsIgnoreCase("gallery-save")){
			if(mbIsNewStore==false){
				Intent intent=new Intent(mContext,ShowPlaceMessage.class);
				intent.putExtra("STOREID", msStoreId);
				startActivity(intent);
				mContext.finish();
				mContext.overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
			}else{
				showToast("Please create a store first");
			}
		}
		if(tag.equalsIgnoreCase("gallery-skip")){
		}
	}

	void skipToPage()
	{
		try
		{
			mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void takePictureForLogoFromCamera() {
		try {
			Intent intent=new Intent(mContext, CameraView.class);
			mContext.startActivityForResult(intent, REQ_CODE_GETCLICKEDIMAGE);
		} catch (ActivityNotFoundException e) {
			Log.d("", "cannot take picture", e);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private void openGallery() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQ_CODE_LOGO_SELECTGALLERY);
	}


	private class LocationDialog implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			try
			{
				TextView tempArea=(TextView)mViewPager.findViewById(R.id.edtManageArea);
				TextView tempCity=(TextView)mViewPager.findViewById(R.id.edtManageCity);
				TextView tempPinCode=(TextView)mViewPager.findViewById(R.id.edtManagePinCode);

				tempArea.setText(mArrAreaName.get(position));
				tempPinCode.setText(mArrAreaPincode.get(position));
				tempCity.setText(mArrCity.get(position));
				msAreaId=mArrAreaId.get(position);
				Log.i("Selected Area Id ", "Selected Area Id "+msAreaId);
				mLocationDialog.dismiss();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	void deleteGalleryImages(String msIds)
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.GALLERY_URL+"place_id="+msStoreId+msIds;
				new ShoplocalAsyncTask(this, "delete", "merchant", true, "delete").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void loadCategories()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.CATEGORIES_URL;
				new ShoplocalAsyncTask(this, "get", "customer", false, "categories").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void checkNearestArea()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.NEAREST_AREA+"latitude="+mdLatitude+"&longitude="+mdLongitude;
				new ShoplocalAsyncTask(this, "get", "customer", false, "nearestArea").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void getStoreDetails()
	{
		try
		{
			String msUrl="";
			if(mSessionManager.isLoggedInCustomer()==true){
				msUrl=Config.PLACE_DETAIL_URL+"place_id="+msStoreId+"&customer_id="+Config.CUST_USER_ID;	
			}else{
				msUrl=Config.PLACE_DETAIL_URL+"place_id="+msStoreId;
			}
			if(mInternet.isNetworkAvailable()){
				new ShoplocalAsyncTask(this, "get", "merchant", true, "storeDetails").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void getGallery()
	{
		try
		{
			String msUrl="";
			msUrl=Config.GALLERY_URL+"place_id="+msStoreId;	
			if(mInternet.isNetworkAvailable()){
				new ShoplocalAsyncTask(this, "get", "gallery", false, "gallery").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if(mbIsNewStore==false && miManageGallery!=1)
		{

			SubMenu subMenu1 = menu.addSubMenu("Extra Settings");
			if(msPlaceStatus.equalsIgnoreCase("0")){
				subMenu1.add("Show store");
			}
			if (msPlaceStatus.equalsIgnoreCase("1")){
				subMenu1.add("Hide store");
			}
			subMenu1.add("Delete store");
			MenuItem subMenu1Item = subMenu1.getItem();
			subMenu1Item.setIcon(R.drawable.ic_delete_store);
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getTitle().toString().equalsIgnoreCase("Delete store"))
		{
			try
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.deleteStoreMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Delete",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						deleteStore();
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}
		else if(item.getTitle().toString().equalsIgnoreCase("Show store"))
		{
			try
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.showStore))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Show",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						msPlaceStatus="1";
						toggleStore();
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}catch(Exception exception)
			{
				exception.printStackTrace();
			}
		}
		else if(item.getTitle().toString().equalsIgnoreCase("Hide store"))
		{
			try
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.hideStoreMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Hide",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						msPlaceStatus="0";
						toggleStore();
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else if(item.getItemId()==android.R.id.home)
		{
			finishTo();
		}

		return true;
	}

	private List<Fragment> getFragments()
	{
		List<Fragment> fList = new ArrayList<Fragment>();
		fList.add(EditStoreFragment.newInstance(LOCATION,mbIsNewStore));
		fList.add(EditStoreFragment.newInstance(DETAILS,mbIsNewStore));
		fList.add(EditStoreFragment.newInstance(CONTACT,mbIsNewStore));
		fList.add(EditStoreFragment.newInstance(DESCRIPTION,mbIsNewStore));
		fList.add(EditStoreFragment.newInstance(OPERATIONS,mbIsNewStore));
		fList.add(EditStoreFragment.newInstance(GALLERY,mbIsNewStore));
		return fList;
	}

	private class EditPageFragment extends FragmentStatePagerAdapter
	{
		ArrayList<String> pages;
		List<Fragment>fragments;
		public EditPageFragment(FragmentManager fm,List<Fragment>fragments,ArrayList<String> pages) {
			super(fm);
			this.pages=pages;
			this.fragments=fragments;
		}
		@Override
		public CharSequence getPageTitle(int position) {
			return pages.get(position);
		}
		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}
		@Override
		public int getCount() {
			return fragments.size();
		}
	}

	@Override
	public void onAsyncStarted() {
		mProgress.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mProgress.setVisibility(View.GONE);
		if(tag.equalsIgnoreCase("storeDetails")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status==null) return;

				if(status.equalsIgnoreCase("false")){
					String code=jsonObject.getString("code");
					if(code.equalsIgnoreCase("-116")){
						invalidAuthFinish();
						return;
					}
				}

				JSONArray dataArray = jsonObject.getJSONArray("data");
				if(dataArray == null) return;

				int count = dataArray.length();
				for(int i=0;i<count;i++)
				{
					JSONObject dataObject=dataArray.getJSONObject(i);
					JSONArray getTime=dataObject.getJSONArray("time");
					JSONArray getCategoires=dataObject.getJSONArray("categories");

					msStoreName=dataObject.getString("name");
					msAreaId=dataObject.getString("area_id");
					msAreaName=dataObject.getString("area");
					msStoreDescription=dataObject.getString("description");
					msStoreLatitude=dataObject.getString("latitude");
					msStoreLongitude=dataObject.getString("longitude");
					msStoreLogo=dataObject.getString("image_url");
					msStoreMobileNo1=dataObject.getString("mob_no1");
					msStoreMobileNo2=dataObject.getString("mob_no2");
					msStoreMobileNo3=dataObject.getString("mob_no3");
					msStoreTelNo1=dataObject.getString("tel_no1");
					msStoreTelNo2=dataObject.getString("tel_no2");
					msStoreTelNo3=dataObject.getString("tel_no3");
					msStoreEmail=dataObject.getString("email");
					msStoreFb=dataObject.getString("facebook_url");
					msStoreTwitter=dataObject.getString("twitter_url");
					msStreet=dataObject.getString("street");
					msLandmark=dataObject.getString("landmark");
					msCity=dataObject.getString("city");
					msState=dataObject.getString("state");
					msCountry=dataObject.getString("country");
					msBuilding=dataObject.getString("building");
					msStoreWebsite=dataObject.getString("website");
					msPincode=dataObject.getString("pincode");
					msPlaceStatus=dataObject.getString("place_status");
					if(dataObject.has("user_like")){
						msUserLike=dataObject.getString("user_like");
					}else{
						msUserLike="-1";
					}
					for(int index=0;index<getTime.length();index++)
					{
						JSONObject getData=getTime.getJSONObject(index);
						mArrStoreDay.add(getData.getString("day"));
						mArrPlaceTimingStatus.add(getData.getString("is_closed"));
						mArrOpenTime.add(getData.getString("open_time"));
						mArrCloseTime.add(getData.getString("close_time"));
					}
					for(int j=0;j<getCategoires.length();j++){
						mArrSelectedCategoryId.add(getCategoires.getString(j));
					}
				}
				String time = "" ;
				String time2="";
				try
				{
					//Open Time
					time =mArrOpenTime.get(0);
					DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					Date date = sdf.parse(time);
					DateFormat timeFormat=new SimpleDateFormat("hh:mm aa");
					//Close Time
					time2 = mArrCloseTime.get(0);
					Date date2 = sdf.parse(time2);
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");

					msOpenTime=timeFormat.format(date);
					msCloseTime=timeFormat.format(date2);
					msStoreTimeFrom=outFormat.format(sdf.parse(time));
					msStoreTimeTo=outFormat.format(sdf.parse(time2));
					debug("Time From 24 "+msStoreTimeFrom + " Time To 24 "+msStoreTimeTo);

				}catch(Exception ex){
					ex.printStackTrace();
				}
				msLatitude=msStoreLatitude;
				msLongitude=msStoreLongitude;
				invalidateActionBar();
				loadCategories();
				setTitle("Edit Store- "+msStoreName);
				if(msStoreLatitude.length()!=0 && msStoreLongitude.length()!=0)
					setImage(msStoreLatitude,msStoreLongitude);

				setDataToPager(msStoreId,msStoreName,msStoreDescription,msBuilding,msStreet,msLandmark,msAreaName
						,msPincode,msCity,msState,msCountry,msStoreLatitude,msStoreLongitude,msStoreTelNo1,msStoreTelNo2,msStoreTelNo3,
						msStoreMobileNo1,msStoreMobileNo2,msStoreMobileNo3,
						msStoreLogo,msStoreEmail,msStoreWebsite,msStoreFb,msStoreTwitter);

				setOperationData(mArrOpenTime,mArrCloseTime,mArrStoreDay,mArrPlaceTimingStatus);
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		if(tag.equalsIgnoreCase("areas")){

			try
			{
				JSONObject jsonobject=new JSONObject(sJson);
				JSONObject data = jsonobject.getJSONObject("data");
				JSONArray jsonArray = data.getJSONArray("areas");

				if(jsonArray == null)  return;

				int count = jsonArray.length();
				ContentValues[] values = new ContentValues[count];
				for(int i=0;i<jsonArray.length();i++){
					JSONObject areaObject=jsonArray.getJSONObject(i);
					if(areaObject!=null){
						ContentValues value=new ContentValues();
						mArrAreaId.add(areaObject.getString("id"));
						mArrAreaName.add(areaObject.getString("sublocality"));
						value.put("area_id", areaObject.getInt("id"));
						value.put("area",  areaObject.getString("sublocality"));
						value.put("slug", areaObject.getString("slug"));
						value.put("locality",  areaObject.getString("locality"));
						value.put("latitude",  areaObject.getString("latitude"));
						value.put("longitude",  areaObject.getString("longitude"));
						value.put("pincode",  areaObject.getString("pincode"));
						mArrAreaPincode.add(areaObject.getString("pincode"));
						value.put("city",  areaObject.getString("city"));
						mArrCity.add(areaObject.getString("city"));
						value.put("state",  areaObject.getString("state"));
						value.put("country",  areaObject.getString("country"));
						value.put("iso_code",  areaObject.getString("iso_code"));
						value.put("merchant_count",  areaObject.getString("merchant_count"));
						value.put("published_state",  areaObject.getInt("published_status"));
						value.put("active_area",  0);
						values[i]=value;
					}

					Cursor c1=new ShoplocalCursorLoader(mActivity, "Delete from AREAS").loadInBackground();
					if(c1!=null){
						debug("deleted previous entries"); 
						mDbUtil.replaceOrUpdate(this,"AREAS", values,"areas");
						mProgress.setVisibility(View.VISIBLE);
					}
					mListLocation.setAdapter(new LocationListAdapter(mContext, 0, 0, mArrAreaName));
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("gallery")){
			try
			{
				mArrGalleryId.clear();
				mArrTitle.clear();
				mArrImageUrl.clear();
				mArrThumbUrl.clear();

				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equalsIgnoreCase("false")){

					mArrGalleryId.add(0, "-1");
					mArrTitle.add(0, "Add Image");
					mArrImageUrl.add(0, "");
					mArrThumbUrl.add(0, "");
					mAdapter=new GridAdapter(mContext, mArrThumbUrl,mArrGalleryId,mArrTitle);

					if(miManageGallery==1){
						mGridView.setAdapter(mAdapter);
						mGridView.setOnItemClickListener(new GridItemClickListener(true));
					}else{
						GridView mPageGrid=(GridView)mViewPager.findViewById(R.id.imageGridMerchant);
						mPageGrid.setAdapter(mAdapter);
						mPageGrid.setOnItemClickListener(new GridItemClickListener(true));
					}

					return;
				}
				JSONArray dataArray = jsonObject.getJSONArray("data");
				if(dataArray == null) return;

				int count = dataArray.length();
				for(int i=0;i<count;i++)
				{
					JSONObject dataObject=dataArray.getJSONObject(i);
					mArrGalleryId.add(dataObject.getString("id"));
					mArrTitle.add(dataObject.getString("title"));
					mArrThumbUrl.add(dataObject.getString("thumb_url"));
					mArrImageUrl.add(dataObject.getString("image_url"));
				}

				mArrGalleryId.add(0, "-1");
				mArrTitle.add(0, "Add Image");
				mArrImageUrl.add(0, "");
				mArrThumbUrl.add(0, "");

				Log.d("SIZEOFID","SIZEOFID " + mArrGalleryId.size());
				mAdapter=new GridAdapter(mContext, mArrThumbUrl,mArrGalleryId,mArrTitle);
				try
				{
					if(miManageGallery==1){
						mGridView.setAdapter(mAdapter);
						mGridView.setOnItemClickListener(new GridItemClickListener(true));
					}else{
						GridView mPageGrid=(GridView)mViewPager.findViewById(R.id.imageGridMerchant);
						mPageGrid.setAdapter(mAdapter);
						mPageGrid.setOnItemClickListener(new GridItemClickListener(true));
					}
					//					if(manageGallery!=1){
					//						imageGridMerchant=(GridView)mPager.findViewById(R.id.imageGridMerchant);
					//						imageGridMerchant.setAdapter(adapterImg);
					//						imageGridMerchant.setOnItemClickListener(new GridItemClickListener(true));
					//					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("categories")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");

				if(status==null || status.equalsIgnoreCase("false")) return;

				JSONArray jsonArray=jsonObject.getJSONArray("data");
				int count=jsonArray.length();

				for(int i=0;i<count;i++){
					JSONObject json=jsonArray.getJSONObject(i);
					mArrCategoryName.add(json.getString("label"));
					mArrCategoryId.add(json.getString("id"));
				}
				mCategoryList.setAdapter(new CategoriesAdapter(mContext, 0, 0, mArrCategoryName));
				categoryDropDown(mArrCategoryName,mArrCategoryId);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("updateStore")){

			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");

				if(status==null)return;
				if(status.equalsIgnoreCase("true")){
					JSONObject jsonData=jsonObject.getJSONObject("data");
					msStoreId=jsonData.getString("place_id");
					showToast(msg);
					if(mbIsNewStore==true){
						EventTracker.logEvent("Store_Added", false);
					}else{
						EventTracker.logEvent("Store_Modified", false);
					}
					mbIsNewStore=false;
					invalidateActionBar();
					setPlaceRefresh();
					updateStoreTime();

				}
				if(status.equalsIgnoreCase("false")){
					String code=jsonObject.getString("code");
					if(code.equalsIgnoreCase("-116")){
						invalidAuthFinish();
						return;
					}
					showToast(msg);

					return;
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}if(tag.equalsIgnoreCase("updateTime")){

			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");

				if(status==null)return;
				if(status.equalsIgnoreCase("true")){
					skipToPage();
				}
				if(status.equalsIgnoreCase("false")){
					try
					{
						String code=jsonObject.getString("code");
						if(code.equalsIgnoreCase("-116")){
							invalidAuthFinish();
							return;
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
					showToast(msg);
					return;
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("postGallery")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");

				if(status==null)return;
				if(status.equalsIgnoreCase("true")){
					showToast(msg);
					getGallery();
				}
				if(status.equalsIgnoreCase("false")){
					showToast(msg);
					return;
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		if(tag.equalsIgnoreCase("delete")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");
				String msg=jsonObject.getString("message");
				if(status==null)return;
				if(status.equalsIgnoreCase("true")){
					mArrDeleteIds.clear();
					msUrltoAppend = "";
					getGallery();
				}
				if(status.equalsIgnoreCase("false")){
					showToast(msg);
					return;
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("deleteStore"))
		{
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equalsIgnoreCase("false")) 
				{
					setPlaceRefresh();
					String msg=jsonObject.getString("message");
					showToast(msg);
				}
				if(status.equalsIgnoreCase("true")){
					String msg=jsonObject.getString("message");
					showToast(msg);
					setPlaceRefresh();
					finishOnDelete();
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("toggleStatus"))
		{
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null) return;
				setPlaceRefresh();
				if(status.equalsIgnoreCase("true")){
					setPlaceRefresh();
					invalidateActionBar();
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	void invalidAuthFinish()
	{
		try
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
					try
					{
						mSessionManager.logoutUser();
						Intent intent=new Intent(mContext, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void finishOnDelete()
	{
		Intent intent=new Intent(mContext, MerchantTalkHome.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	void setPlaceRefresh()
	{
		SharedPreferences prefs=mContext.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putBoolean("isPlaceRefreshRequired", true);
		editor.commit();
	}

	//New set Category
	void categoryDropDown(final ArrayList<String> ALL_CATEGORY_NAME,final ArrayList<String> ALL_CATEGORY_ID){
		try{
			final TextView categDropDown	=	(TextView)mViewPager.findViewById(R.id.txtManageCategory);
			String categ = "";
			Log.i("Inside New Categ","onDropDown");
			for(int i=0;i<ALL_CATEGORY_NAME.size();i++){
				Log.i("Inside New Categ","onDropDown -"+ALL_CATEGORY_NAME.get(i)+" +++ " +ALL_CATEGORY_ID.get(i));
			}
			final ListView listCateg=(ListView)mCategoryDailog.findViewById(R.id.categDilogList);
			listCateg.setAdapter(new CategoryAdapter(mContext,ALL_CATEGORY_NAME, ALL_CATEGORY_ID));
			mCategoryDailog.setTitle("Select Category");

			//Setting Selected Categories
			ArrayList<String> names = new ArrayList<String>();

			for(int i=0;i<ALL_CATEGORY_ID.size();i++){
				if(mArrSelectedCategoryId.contains(ALL_CATEGORY_ID.get(i))){
					names.add(ALL_CATEGORY_NAME.get(i));
				}else{
				}
			}
			mSelectedCategory=names.toString();
			mSelectedCategory=mSelectedCategory.substring(1, mSelectedCategory.length()-1).trim();
			if(mSelectedCategory.equalsIgnoreCase("")){
				mSelectedCategory = "Select Category *";
			}
			categDropDown.setText(mSelectedCategory);
			categDropDown.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mCategoryDailog.show();		
				}
			});
			Button btnDone=(Button)mCategoryDailog.findViewById(R.id.btnDoneSelection);
			btnDone.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					//Removing duplicates
					mSelectedCategory=mArrTempCategoryName.toString();
					mSelectedCategory=mSelectedCategory.substring(1, mSelectedCategory.length()-1).trim();
					if(mSelectedCategory.equalsIgnoreCase("")){
						mSelectedCategory = "Select Category *";
					}
					categDropDown.setText(mSelectedCategory);
					mCategoryDailog.dismiss();
					mArrSelectedCategoryId=mArrTempCategoryId;
				}
			});

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void setImage(String latitude,String longitude)
	{
		try
		{
			ImageView staticMap=(ImageView)mViewPager.findViewById(R.id.mapImage);
			Button btnSkip=(Button)mViewPager.findViewById(R.id.skipButton);
			TextView shopDropMessage2=(TextView)mViewPager.findViewById(R.id.shopDropMessage2);

			shopDropMessage2.setVisibility(View.VISIBLE);
			shopDropMessage2.setText("Location Found.Swipe to continue.");
			shopDropMessage2.setTextColor(Color.BLACK);
			btnSkip.setText("Skip");

			String url="http://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&zoom=16&size="+300+"x"+150+"&maptype=roadmap&markers=color:blue|label:S|"+latitude+","+longitude+"&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g";
			staticMap.setScaleType(ScaleType.FIT_XY);
			staticMap.setAdjustViewBounds(true);
			/*imageLoader.DisplayImage(url, staticMap);*/
			try{
				Picasso.with(mContext).load(url)
				.placeholder(R.drawable.ic_place_holder)
				.error(R.drawable.ic_place_holder)
				.into(staticMap);
			}
			catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){ 
				e.printStackTrace();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void getCountryCode()
	{
		SharedPreferences pref=getSharedPreferences("merchantCountryCode",0);
		mCountryCode=pref.getString("mCountryCode", "91");

		Log.i("Countrycode found","Countrycode found "+mCountryCode);
	}

	void debug(String msg){
		Log.d("EditStore ", "EditStore "+msg);
	}

	@Override
	public void onError(int errorCode) {
		mProgress.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}

	void setDataToPager(String id,String store_name,String desc,String building,String street,String landmark,
			String area,String pincode,String city,String state,String country,String lati,String logi,String telno1,String telno2,String telno3,
			String mobno1,String mobno2,String mobno3,String photo,String email,String web,String fb,String twitter)
	{
		try
		{
			EditText edtManageStoreName=(EditText)mViewPager.findViewById(R.id.edtManageStoreName);
			EditText edtManageDesc=(EditText)mViewPager.findViewById(R.id.edtManageDesc2);
			EditText edtManageStreet=(EditText)mViewPager.findViewById(R.id.edtManageStreet);
			EditText edtManageLandMark=(EditText)mViewPager.findViewById(R.id.edtManageLandMark);
			TextView edtManageArea=(TextView)mViewPager.findViewById(R.id.edtManageArea);
			TextView edtManageCity=(TextView)mViewPager.findViewById(R.id.edtManageCity);
			TextView edtManagePinCode=(TextView)mViewPager.findViewById(R.id.edtManagePinCode);
			TextView txtMangeContactStoreName=(TextView)mViewPager.findViewById(R.id.txtMangeContactStoreName);

			EditText edtManageLandline=(EditText)mViewPager.findViewById(R.id.edtManageLandline);
			EditText edtManageLandline2=(EditText)mViewPager.findViewById(R.id.edtManageLandline2);
			EditText edtManageLandline3=(EditText)mViewPager.findViewById(R.id.edtManageLandline3);
			EditText edtManageMobile=(EditText)mViewPager.findViewById(R.id.edtManageMobile);
			EditText edtManageMobile2=(EditText)mViewPager.findViewById(R.id.edtManageMobile2);
			EditText edtManageMobile3=(EditText)mViewPager.findViewById(R.id.edtManageMobile3);
			EditText edtManageFb=(EditText)mViewPager.findViewById(R.id.edtManageFb);
			EditText edtManageTwitter=(EditText)mViewPager.findViewById(R.id.edtManageTwitter);
			EditText edtManageEmailId=(EditText)mViewPager.findViewById(R.id.edtManageEmailId);
			EditText edtManageWebSite=(EditText)mViewPager.findViewById(R.id.edtManageWebSite);


			TableRow landlinerow2=(TableRow)mViewPager.findViewById(R.id.landlinerow2);
			TableRow landlinerow3=(TableRow)mViewPager.findViewById(R.id.landlinerow3);
			TableRow mobileRow2=(TableRow)mViewPager.findViewById(R.id.mobileRow2);
			TableRow mobileRow3=(TableRow)mViewPager.findViewById(R.id.mobileRow3);

			Button tadd1=(Button)mViewPager.findViewById(R.id.add1);
			Button tadd4=(Button)mViewPager.findViewById(R.id.add4);

			TextView txtStoreName=(TextView)mViewPager.findViewById(R.id.txtOperationStoreName);
			TextView txtStoreDescription=(TextView)mViewPager.findViewById(R.id.txtOperationDesc);

			txtStoreName.setText(store_name);
			txtStoreDescription.setText(desc);
			edtManageStoreName.setText(store_name);
			edtManageDesc.setText(desc);
			edtManageStreet.setText(street);
			edtManageLandMark.setText(landmark);
			edtManageArea.setText(area);
			edtManageCity.setText(city);
			edtManagePinCode.setText(pincode);
			edtManageFb.setText(fb);
			edtManageTwitter.setText(twitter);
			edtManageEmailId.setText(email);
			edtManageWebSite.setText(web);

			if(telno2.length()>= 10 ){
				landlinerow2.setVisibility(View.VISIBLE);
			}
			if(telno3.length()>= 10){
				landlinerow3.setVisibility(View.VISIBLE);
			}
			if( mobno2.length()>= 10 ){
				mobileRow2.setVisibility(View.VISIBLE);
			}

			if(mobno3.length()>= 10){
				mobileRow3.setVisibility(View.VISIBLE);
			}

			if(landlinerow2.isShown() && landlinerow3.isShown()){
				tadd1.setVisibility(View.GONE);
			}
			if(mobileRow3.isShown() && mobileRow3.isShown()){
				tadd4.setVisibility(View.GONE);
			}
			txtMangeContactStoreName.setText(store_name);

			if(mobno1!=null && mobno1.length()>0){
				mobno1=mobno1.substring(mCountryCode.length());
			}
			if(mobno2!=null && mobno2.length()>0)
			{
				mobno2=mobno2.substring(mCountryCode.length());
			}
			if(mobno3!=null && mobno3.length()>0)
			{
				mobno3=mobno3.substring(mCountryCode.length());
			}

			if(mobno1.length()==10){
				edtManageMobile.setText(mobno1);
			}
			if(mobno2.length()==10){
				edtManageMobile2.setText(mobno2);
			}
			if(mobno3.length()==10)	{
				edtManageMobile3.setText(mobno3);
			}
			if(telno1.length()==11){
				edtManageLandline.setText(telno1);
			}
			if(telno2.length()==11){
				edtManageLandline2.setText(telno2);
			}
			if(telno3.length()==11)	{
				edtManageLandline3.setText(telno3);
			}

			if(email.equalsIgnoreCase("null")){
				edtManageEmailId.setText("");
			}
			if(web.equalsIgnoreCase("null")){
				edtManageWebSite.setText("");
			}

			ImageView imgManageStore_ThumbPreview=(ImageView)mViewPager.findViewById(R.id.imgManageStore_ThumbPreview);

			imgManageStore_ThumbPreview.setScaleType(ScaleType.FIT_XY);
			Log.i("PHOTO IN", "PHOTO IN"+photo);
			photo=photo.replaceAll(" ", "%20");
			Log.i("PHOTO IN", "PHOTO IN After"+photo);

			String iphoto=Config.IMAGE_URL+photo;
			if(photo.length()!=0)
			{
				Log.i("PHOTO IN", "PHOTO IN After inside"+photo);
				/*imageLoader.DisplayImage(iphoto, imgManageStore_ThumbPreview);*/
				try{
					Picasso.with(mContext).load(iphoto)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(imgManageStore_ThumbPreview);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
		}catch(IndexOutOfBoundsException ex)
		{
			ex.printStackTrace();	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void setOperationData(ArrayList<String> Time1_OPEN,ArrayList<String> Time1_Close,ArrayList<String> DAY,ArrayList<String> PLACE_STATUS)
	{
		try
		{

			//time page.
			TextView txtMangeStoreTimeFrom=(TextView)mViewPager.findViewById(R.id.txtMangeStoreTimeFrom);
			TextView txtMangeStoreTimeTo=(TextView)mViewPager.findViewById(R.id.txtMangeStoreTimeTo);
			ToggleButton bManagetoggleSun=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleSun);
			ToggleButton bManagetoggleMon=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleMon);
			ToggleButton bManagetoggleTue=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleTue);
			ToggleButton bManagetoggleWed=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleWed);
			ToggleButton bManagetoggleThu=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleThu);
			ToggleButton bManagetoggleFri=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleFri);
			ToggleButton bManagetoggleSat=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleSat);


			for(int i=0;i<Time1_OPEN.size();i++)
			{
				msOpenTime=Time1_OPEN.get(i);
			}
			for(int i=0;i<Time1_Close.size();i++)
			{
				msCloseTime=Time1_Close.get(i);
			}

			try
			{
				//Open Time
				String time = Time1_OPEN.get(0);
				DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
				Date date = sdf.parse(time);
				System.out.println("Time Converted open: " + sdf.format(date));
				DateFormat time_open=new SimpleDateFormat("hh:mm aa");
				System.out.println("Time Converted open: " + time_open.format(date));
				txtMangeStoreTimeFrom.setText(time_open.format(date));
				//Close Time
				String time2 = Time1_Close.get(0);
				DateFormat sdf2 = new SimpleDateFormat("hh:mm:ss");
				Date date2 = sdf.parse(time2);
				System.out.println("Time Converted Close: " + sdf2.format(date));
				DateFormat time_close=new SimpleDateFormat("hh:mm aa");
				System.out.println("Time Converted Close: " + time_close.format(date2));
				txtMangeStoreTimeTo.setText(time_close.format(date2));

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			for(int i=0;i<DAY.size();i++)
			{
				Log.i("DAY","DAY : "+DAY.get(i)+" STATUS : "+PLACE_STATUS.get(i));
			}
			for(int i=0;i<DAY.size();i++)
			{
				if(DAY.get(i).equalsIgnoreCase("SUNDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleSun.setChecked(true);
					}
					else{
						bManagetoggleSun.setChecked(false);
					}

				}
				if(DAY.get(i).equalsIgnoreCase("MONDAY")){
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleMon.setChecked(true);
					}
					else{
						bManagetoggleMon.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("TUESDAY")){
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleTue.setChecked(true);
					}
					else{
						bManagetoggleTue.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("WEDNESDAY")){
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleWed.setChecked(true);
					}
					else{
						bManagetoggleWed.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("THURSDAY")){
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleThu.setChecked(true);
					}
					else{
						bManagetoggleThu.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("FRIDAY")){
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleFri.setChecked(true);
					}
					else{
						bManagetoggleFri.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("SATURDAY")){
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0")){
						bManagetoggleSat.setChecked(true);
					}
					else{
						bManagetoggleSat.setChecked(false);
					}
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	String getTime(String time)
	{
		//Splitting string
		String [] splitstring;
		String time_status="";
		//Splitting string based on comma
		splitstring=time.split( ",\\s*");
		ArrayList<String>output=new ArrayList<String>();
		//Adding string to arraylist
		for(String string:splitstring)
		{
			output.add(string);
		}
		for(int i=0;i<output.size();i++)
		{
			if(!output.get(i).equalsIgnoreCase("closed")){
				time_status=output.get(i);
			}
			Log.i("Closed","Closed seprated "+output.get(i));
		}

		Log.i("Closed","Opened at "+time_status);

		return time_status;
	}
	public void onBackPressed() {
		finishTo();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	void finishTo()
	{
		try
		{
			if(miManageGallery!=1){
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage("Are you sure you want to leave this screen?")
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
						try
						{
							//						clearAllData();
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
						Intent intent=new Intent(mContext, MerchantTalkHome.class);
						intent.putExtra("isSplitLogin",true);
						intent.putExtra("USER_ID", Config.MERCHANT_USER_ID);
						intent.putExtra("AUTH_ID", Config.MERCHANT_AUTH_ID);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();
				alertDialog3.show();
			}
			else{
				Intent intent=new Intent(mContext, MerchantTalkHome.class);
				intent.putExtra("isSplitLogin",true);
				intent.putExtra("USER_ID", Config.MERCHANT_USER_ID);
				intent.putExtra("AUTH_ID", Config.MERCHANT_AUTH_ID);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Intent intent=new Intent(mContext, MerchantTalkHome.class);
			intent.putExtra("isSplitLogin",true);
			intent.putExtra("USER_ID", Config.MERCHANT_USER_ID);
			intent.putExtra("AUTH_ID", Config.MERCHANT_AUTH_ID);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
	}

	private class CategoryAdapter extends ArrayAdapter<String> 
	{
		LayoutInflater inflator;
		ArrayList<String>cat_name;
		ArrayList<String>cat_id;
		Activity context;
		public CategoryAdapter(Activity context, ArrayList<String>cat_name,ArrayList<String>cat_id) {
			super(context, R.layout.categorylistlayout, cat_name);
			this.context=context;
			this.cat_name=cat_name;
			this.cat_id=cat_id;
			inflator=context.getLayoutInflater();
		}
		@Override
		public int getCount() {
			return cat_name.size();
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
			if(convertView==null)
			{
				ViewHolder viewHolder=new ViewHolder();
				inflator=context.getLayoutInflater();
				convertView=inflator.inflate(R.layout.edit_categorylistlayout, null);
				viewHolder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				viewHolder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);

				viewHolder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

						if(isChecked){
							if(mArrTempCategoryId.contains(cat_id)){
							}else{
								mArrTempCategoryId.add(cat_id.get(position));
								mArrTempCategoryName.add(cat_name.get(position));
							}
						}else{
							mArrTempCategoryId.remove(cat_id.get(position));
							mArrTempCategoryName.remove(cat_name.get(position));
						}
					}
				});
				convertView.setTag(viewHolder);
				//viewHolder.chkBox.setTag(list.get(position));
			}

			ViewHolder holder=(ViewHolder)convertView.getTag();
			holder.txtCategory.setText(cat_name.get(position));
			Log.i("CATEGORY","CATEGORY "+cat_name.get(position));
			if(mArrSelectedCategoryId.contains(cat_id.get(position))){
				holder.chkBox.setChecked(true);
			}else{
				holder.chkBox.setChecked(false);
			}
			return convertView;
		}
	}
	class ViewHolder
	{
		TextView txtCategory;
		CheckBox chkBox;
	}


	class GridAdapter extends BaseAdapter
	{
		ArrayList<String>photo,title,id;
		Activity context;
		LayoutInflater inflate;
		String image;

		public GridAdapter(Activity context,ArrayList<String>photo,ArrayList<String>id,ArrayList<String>title)
		{
			this.context=context;
			this.photo=photo;
			this.title=title;
			this.id=id;
			inflate=context.getLayoutInflater();
		}

		@Override
		public int getCount() {

			return photo.size();
		}

		@Override
		public Object getItem(int position) {

			return photo.get(position);
		}

		@Override
		public long getItemId(int position) {

			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {

			final int pos = position;
			ViewHolder holder;

			if(convertView==null)
			{

				Log.d("INIF", "INIF");
				holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.gridimagelayout,null);

				holder.photo=(ImageView)convertView.findViewById(R.id.imgGridImage);
				holder.txtImageTitle=(TextView)convertView.findViewById(R.id.txtImageTitle);
				holder.toChkDelete=(CheckBox)convertView.findViewById(R.id.toChkDelete);
				holder.tmpView=(View)convertView.findViewById(R.id.tmpView);
				holder.imageTitle = (TextView) convertView.findViewById(R.id.imageTitle);
				convertView.setTag(holder);
			}
			else{
				Log.d("INIF", "INELSE");	
			}



			holder=(ViewHolder)convertView.getTag();

			if(mbIsEditModeOn){
				holder.toChkDelete.setVisibility(View.VISIBLE);
			}
			//logic to check selected images
			holder.toChkDelete.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(buttonView.isChecked()){
						mArrDeleteIds.add(mArrGalleryId.get(pos));
					}
					else{
						mArrDeleteIds.remove(mArrGalleryId.get(pos));
					}
				}
			});

			if(photo.get(position)!=null || photo.get(position).length()!=0)
			{
				image=photo.get(position).replaceAll(" ", "%20");
				/*imageLoader.DisplayImage(PHOTO_URL+image,hold.photo);*/
				try{
					if(position!=0){
						Picasso.with(context).load(Config.IMAGE_URL+image)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(holder.photo);
					}
					else{
						holder.photo.setImageResource(R.drawable.photo);
					}

				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			if(position==0)
			{
				holder.photo.setImageResource(R.drawable.photo);
				holder.toChkDelete.setVisibility(View.GONE);
				if(miManageGallery==1){
					holder.tmpView.setVisibility(View.VISIBLE);
				}
			}else if(mbIsEditModeOn==false){
				holder.toChkDelete.setVisibility(View.GONE);
				holder.tmpView.setVisibility(View.GONE);
			}
			else{
				holder.toChkDelete.setVisibility(View.VISIBLE);
				holder.tmpView.setVisibility(View.GONE);
			}
			//	hold.txtImageTitle.setText(title.get(position).toString());
			holder.txtImageTitle.setVisibility(View.GONE);
			
			if (position > 0) {
				try {
					String titleText = title.get(position).trim();
					if (titleText.length() > 0) {
						holder.imageTitle.setVisibility(View.VISIBLE);
						
						String desc = titleText.trim();
						String seperator = "\\\\'";
						String []tempText = desc.split(seperator);
						
				        String b= "";
				        for (String c : tempText) {
				        	b +=c+"\'";
				        }
				        b = b.substring(0, b.length()-1);
				        
				        desc = b.trim();
						seperator = "\\\\n";
						tempText = desc.split(seperator);
						
				        b = "";
				        for (String c : tempText) {
				        	b +=c+"\n";
				        }
				        titleText = b.substring(0, b.length()-1);
						
						holder.imageTitle.setText(titleText);
					} else {
						holder.imageTitle.setVisibility(View.GONE);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				holder.imageTitle.setVisibility(View.GONE);
			}
			
			return convertView;
		}

		class ViewHolder
		{
			ImageView photo;
			TextView txtImageTitle, imageTitle;
			CheckBox toChkDelete;
			View tmpView;
		}

	}

	public class GridItemClickListener implements OnItemClickListener
	{
		boolean isPagerGallery;
		public GridItemClickListener(boolean isPagerGallery)
		{
			this.isPagerGallery=isPagerGallery;
			Log.i("isPagerGallery", "isPagerGallery "+isPagerGallery);
		}
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

			if(position==0)
			{
				if(mbIsNewStore==false)
				{
					if(!mProgress.isShown())
					{
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
						alertDialogBuilder.setTitle("Shoplocal");
						alertDialogBuilder.setMessage("Photo");
						//null should be your on click listener
						alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

								if(isPagerGallery)
								{
									eventMap.put("source", "Camera");
									EventTracker.logEvent("Store_ImageAdd",eventMap );
								}
								else
								{
									eventMap.put("source", "Camera");
									EventTracker.logEvent("StoreGallery_ImageAdd",eventMap );
								}
								/** ANirudh gallery change */
								takePictureForGALLERY();
							}
						});
						alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								if(isPagerGallery)
								{
									eventMap.put("source", "Gallery");
									EventTracker.logEvent("Store_ImageAdd",eventMap );	
								}
								else
								{
									eventMap.put("source", "Gallery");
									EventTracker.logEvent("StoreGallery_ImageAdd",eventMap );	
								}

								openGalleryForGrid();
							}
						});
						alertDialogBuilder.show();
					}
				}else
				{
					showToast(mContext.getResources().getString(R.string.storeCreateAlert));
					mViewPager.setCurrentItem(0);
				}
			}
			else
			{
				Intent intent=new Intent(mContext,StorePagerGallery.class);
				intent.putExtra("photo_source", mArrImageUrl);
				intent.putExtra("position", position);
				intent.putStringArrayListExtra("imageTitles", mArrTitle);
				mContext.startActivity(intent);
			}

		}
	}

	void takePictureForGALLERY() {

		try {
			try {
				Intent intent = new Intent(mContext,CameraView.class);
				intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
				mContext.startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}

	private  void openGalleryForGrid() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		mContext.startActivityForResult(photoPickerIntent, REQ_CODE_GALLERY_SELECTGALLERY);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			boolean deleted = mFileTemp.delete();
			Log.i("deleted ", "deleted "+mFileTemp.getAbsolutePath().toString()+mFileTemp.getName()+" "+deleted);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			Log.i("RESULT CODE","REQUEST CODE"+requestCode+" Result Code"+resultCode);
			// After taking picture. Image gets cropped and received here for Place-Gallery
			if(requestCode==REQUEST_CODE_TAKE_PICTURE && resultCode == RESULT_OK)
			{
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				Bitmap bitmap = null;
				CameraImageSave cameraImageSave = new CameraImageSave();

				try{
					bitmap = cameraImageSave.getBitmapFromFile(480);

					if(bitmap != null){

						ByteArrayOutputStream stream = new ByteArrayOutputStream();

						cameraImageSave.deleteFromFile();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						gallery_byte = stream.toByteArray();

						cameraImageSave.deleteFromFile();
						cameraImageSave = null;
						bitmap = null;

						setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0, gallery_byte.length));
					}
				}
				catch(Exception e){
					Log.e("Exception","EditStore.java -> onActivityForResult() -> REQ_CODE = REQUEST_CODE_TAKE_PICTURE");
					e.printStackTrace();
				}
			}

			// After taking picture. Image gets cropped and received here for Place-logo
			if(requestCode == REQ_CODE_GETCLICKEDIMAGE && resultCode == RESULT_OK){
				try{

					CameraImageSave cameraImageSave = new CameraImageSave();
					Bitmap bitmap = cameraImageSave.getBitmapFromFile(150);


					if(bitmap != null){
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						pager_byte = stream.toByteArray();
						cameraImageSave.deleteFromFile();
						bitmap = null;
						cameraImageSave = null;
					}

					setPhoto(BitmapFactory.decodeByteArray(pager_byte , 0, pager_byte.length), true);

				}
				catch(Exception e){ }
			}


			// After an Image is selected for place-logo it goes for cropping.
			if(requestCode == REQ_CODE_LOGO_SELECTGALLERY){
				if(resultCode == RESULT_OK){
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					cropCapturedImage(imageUri, REQ_CODE_LOGO_SELECTGALLERYCROP);

					//					Intent intent=new Intent(EditStore.this, ZoomCroppingActivity.class);
					//					intent.putExtra("imagepath", picturePath);
					//					intent.putExtra("comingfrom", "gallery");
					//					startActivityForResult(intent, REQ_CODE_LOGO_SELECTGALLERYCROP);
				}
			}

			// for selected Image from gallery. After cropping results get received here. 
			if(requestCode == REQ_CODE_LOGO_SELECTGALLERYCROP){
				if(resultCode == RESULT_OK){

					//Create an instance of bundle and get the returned data
					Bundle extras = data.getExtras();
					//get the cropped bitmap from extras
					Bitmap thePic = extras.getParcelable("data");

					CameraImageSave cameraSaveImage = new CameraImageSave();

					try
					{
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}


					String filePath = Environment.getExternalStorageDirectory()
							+ File.separator + "temp_photo_crop.jpg";
					try{
						FileInputStream in = new FileInputStream(filePath);
						BufferedInputStream buf = new BufferedInputStream(in);
						Bitmap bitmap = BitmapFactory.decodeStream(buf);

						bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, true);

						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						pager_byte = stream.toByteArray();
						bitmap = null;

						setPhoto(BitmapFactory.decodeByteArray(pager_byte , 0,pager_byte.length),true);
					}
					catch(Exception e){}
				}
			}

			// After an Image is selected for place-gallery it goes for cropping.
			if(requestCode == REQ_CODE_GALLERY_SELECTGALLERY){
				if(resultCode == RESULT_OK){
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					cropCapturedImage(imageUri, REQ_CODE_GAlLERY_SELECTGALLERYCROP);

					//					Intent intent=new Intent(EditStore.this, ZoomCroppingActivity.class);
					//					intent.putExtra("imagepath", picturePath);
					//					intent.putExtra("comingfrom", "gallery");
					//					startActivityForResult(intent, REQ_CODE_GAlLERY_SELECTGALLERYCROP);
				}
			}

			// for selected Image from gallery for place-gallery. After cropping results get received here. 
			if(requestCode == REQ_CODE_GAlLERY_SELECTGALLERYCROP){
				if(resultCode == RESULT_OK){

					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";
					
					//Create an instance of bundle and get the returned data
					Bundle extras = data.getExtras();
					//get the cropped bitmap from extras
					Bitmap thePic = extras.getParcelable("data");

					CameraImageSave cameraSaveImage = new CameraImageSave();

					try
					{
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					
					try{
						String filePath = cameraSaveImage.getImagePath();


						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						if(bitmap != null){
							Uri uri = Uri.fromFile(new File(filePath));

							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							gallery_byte = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
					}
					catch(Exception e) {
						Log.e("Exception","EditStore.java -> onActivityForResult() -> RequestCode = REQ_CODE_GAlLERY_SELECTGALLERYCROP");
						e.printStackTrace();
					}

					setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0, gallery_byte.length));
				}
			}

		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void cropCapturedImage(Uri picUri,int REQ_CODE){
		//call the standard crop action intent 
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		//indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		//set crop properties
		cropIntent.putExtra("crop", "true");
		//indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		//indicate output X and Y
		cropIntent.putExtra("outputX", 256);
		cropIntent.putExtra("outputY", 256);
		//retrieve data on return
		cropIntent.putExtra("return-data", true);
		//start the activity - we handle returning in onActivityResult
		startActivityForResult(cropIntent, REQ_CODE);
	}

	void setPhoto(Bitmap bitmap)
	{

		//Posting image for gallery.
		try
		{

			mGalleryDialog.setCancelable(false);
			mGalleryDialog.setTitle("Set Title for image");
			Button galleryImageDone=(Button)mGalleryDialog.findViewById(R.id.galleryImageDone);
			Button galleryImageCancel=(Button)mGalleryDialog.findViewById(R.id.galleryImageCancel);
			final EditText galleryImageTitle=(EditText)mGalleryDialog.findViewById(R.id.galleryImageTitle);
			mGalleryDialog.show();
			galleryImageDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View view) {

					msGalleryImageTitle=galleryImageTitle.getText().toString();
					galleryImageTitle.setText("");
					postPhotoToGallery();
					mGalleryDialog.dismiss();
				}
			});
			galleryImageCancel.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View view) {

					galleryImageTitle.setText("");
					msGalleryImageTitle="";
					postPhotoToGallery();
					mGalleryDialog.dismiss();
				}
			});


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void setPhoto(Bitmap bitmap,boolean isPager)
	{
		try
		{
			miRemoveLogo=0;
			ImageView imgThumb=(ImageView)mViewPager.findViewById(R.id.imgManageStore_ThumbPreview);
			imgThumb.setImageBitmap(bitmap);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void postPhotoToGallery()
	{
		try
		{
			if(mbIsNewStore==false){
				String encodePath="";
				if(gallery_byte!=null)
				{
					encodePath=Base64.encodeToString(gallery_byte, Base64.DEFAULT);
					JSONObject json = new JSONObject();
					json.put("place_id", msStoreId);
					json.put("filetype", FILE_TYPE);
					json.put("userfile", encodePath);
					json.put("title", msGalleryImageTitle);
					json.put("user_id", Config.MERCHANT_USER_ID);
					json.put("auth_id", Config.MERCHANT_AUTH_ID	);
					String msUrl=Config.GALLERY_URL;
					new ShoplocalAsyncTask(this, "post","merchant", true, json, "postGallery").execute(msUrl);
				}
			}else{
				showToast("Please create store first");
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	void updateStoreData(int page){

		try
		{
			EditText tempStoreName=(EditText)mViewPager.findViewById(R.id.edtManageStoreName);
			EditText tempStreet=(EditText)mViewPager.findViewById(R.id.edtManageStreet);
			EditText tempLandMark=(EditText)mViewPager.findViewById(R.id.edtManageLandMark);
			TextView tempArea=(TextView)mViewPager.findViewById(R.id.edtManageArea);
			TextView tempCity=(TextView)mViewPager.findViewById(R.id.edtManageCity);
			TextView tempPinCode=(TextView)mViewPager.findViewById(R.id.edtManagePinCode);
			EditText tempDesc=(EditText)mViewPager.findViewById(R.id.edtManageDesc2);

			EditText tempLandline=(EditText)mViewPager.findViewById(R.id.edtManageLandline);
			EditText tempMobile=(EditText)mViewPager.findViewById(R.id.edtManageMobile);
			EditText tempFax=(EditText)mViewPager.findViewById(R.id.edtManageFax);
			EditText tempTollFree=(EditText)mViewPager.findViewById(R.id.edtManageTollFree);
			EditText tempEmailId=(EditText)mViewPager.findViewById(R.id.edtManageEmailId);
			EditText tempWebsite=(EditText)mViewPager.findViewById(R.id.edtManageWebSite);

			EditText tempLandline2=(EditText)mViewPager.findViewById(R.id.edtManageLandline2);
			EditText tempLandline3=(EditText)mViewPager.findViewById(R.id.edtManageLandline3);


			EditText tempMobile2=(EditText)mViewPager.findViewById(R.id.edtManageMobile2);
			EditText tempMobile3=(EditText)mViewPager.findViewById(R.id.edtManageMobile3);

			EditText tempFb=(EditText)mViewPager.findViewById(R.id.edtManageFb);
			EditText tempTwitter=(EditText)mViewPager.findViewById(R.id.edtManageTwitter);


			String msUrl=Config.MERCHANT_ADD_PLACE;

			JSONObject json=new JSONObject();

			if(mbIsNewStore==true){
				json.put("place_id", "");				
			}else{
				json.put("place_id", msStoreId);				
			}
			json.put("user_id", Config.MERCHANT_USER_ID);
			json.put("auth_id", Config.MERCHANT_AUTH_ID);
			json.put("place_status", "1");
			if(page==1){
				if(isValidDetails()==true){
					JSONArray jsonArr=new JSONArray(mArrTempCategoryId);
					json.put("name", tempStoreName.getText().toString());
					json.put("place_parent", "0");
					json.put("category", jsonArr);

					json.put("building", "");
					json.put("street", tempStreet.getText().toString());

					json.put("landmark", tempLandMark.getText().toString());
					json.put("area", tempArea.getText().toString());
					json.put("area_id", msAreaId);

					json.put("pincode", tempPinCode.getText().toString());
					json.put("city", tempCity.getText().toString());

					json.put("latitude", msLatitude);
					json.put("longitude", msLongitude);

					if(mInternet.isNetworkAvailable()){
						if(mbIsNewStore==true){
							new ShoplocalAsyncTask(this, "post","merchant", true, json, "updateStore").execute(msUrl);
						}else{
							new ShoplocalAsyncTask(this, "put","merchant", true, json, "updateStore").execute(msUrl);
						}
					}else{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}
			}
			if(page==2){
				JSONArray jsonArr=new JSONArray(mArrTempCategoryId);
				json.put("name", tempStoreName.getText().toString());
				json.put("place_parent", "0");
				json.put("category", jsonArr);

				json.put("building", "");
				json.put("street", tempStreet.getText().toString());

				json.put("landmark", tempLandMark.getText().toString());
				json.put("area", tempArea.getText().toString());
				json.put("area_id", msAreaId);
				json.put("pincode", tempPinCode.getText().toString());
				json.put("city", tempCity.getText().toString());

				json.put("latitude", msLatitude);
				json.put("longitude", msLongitude);

				json.put("landline", tempLandline.getText().toString());
				json.put("landline2", tempLandline2.getText().toString());
				json.put("landline3", tempLandline3.getText().toString());

				if(tempMobile.getText().toString().length()!=0){
					json.put("mobile", tempMobile.getText().toString());
				}
				else{
					json.put("mobile", "");
				}

				if(tempMobile2.getText().toString().length()!=0){
					json.put("mobile2", tempMobile2.getText().toString());
				}
				else{
					json.put("mobile2", "");
				}
				if(tempMobile3.getText().toString().length()!=0){
					json.put("mobile3", tempMobile3.getText().toString());
				}
				else{
					json.put("mobile3", "");
				}

				json.put("email",tempEmailId.getText().toString());
				json.put("website",tempWebsite.getText().toString());
				json.put("facebook_url",tempFb.getText().toString());
				json.put("twitter_url",tempTwitter.getText().toString());

				if(isValidDetails()==true && isValidContactDetails()==true){
					if(mInternet.isNetworkAvailable()){
						if(mbIsNewStore==true){
							new ShoplocalAsyncTask(this, "post","merchant", true, json, "updateStore").execute(msUrl);
						}else{
							new ShoplocalAsyncTask(this, "put","merchant", true, json, "updateStore").execute(msUrl);
						}
					}else{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}
			}
			if(page==3){
				if(isValidDetails()==true && isValidContactDetails()==true ){

					JSONArray jsonArr=new JSONArray(mArrTempCategoryId);
					json.put("name", tempStoreName.getText().toString());
					json.put("place_parent", "0");
					json.put("category", jsonArr);

					json.put("building", "");
					json.put("street", tempStreet.getText().toString());

					json.put("landmark", tempLandMark.getText().toString());
					json.put("area", tempArea.getText().toString());
					json.put("area_id", msAreaId);
					json.put("pincode", tempPinCode.getText().toString());
					json.put("city", tempCity.getText().toString());

					json.put("latitude", msLatitude);
					json.put("longitude", msLongitude);

					json.put("landline", tempLandline.getText().toString());
					json.put("landline2", tempLandline2.getText().toString());
					json.put("landline3", tempLandline3.getText().toString());

					if(tempMobile.getText().toString().length()!=0){
						json.put("mobile", tempMobile.getText().toString());
					}
					else{
						json.put("mobile", "");
					}

					if(tempMobile2.getText().toString().length()!=0){
						json.put("mobile2", tempMobile2.getText().toString());
					}
					else{
						json.put("mobile2", "");
					}
					if(tempMobile3.getText().toString().length()!=0){
						json.put("mobile3", tempMobile3.getText().toString());
					}
					else{
						json.put("mobile3", "");
					}

					json.put("email",tempEmailId.getText().toString());
					json.put("website",tempWebsite.getText().toString());
					json.put("facebook_url",tempFb.getText().toString());
					json.put("twitter_url",tempTwitter.getText().toString());


					json.put("description", tempDesc.getText().toString());
					if(mInternet.isNetworkAvailable()){
						if(mbIsNewStore==true){
							new ShoplocalAsyncTask(this, "post","merchant", true, json, "updateStore").execute(msUrl);
						}else{
							new ShoplocalAsyncTask(this, "put","merchant", true, json, "updateStore").execute(msUrl);
						}
					}else{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}
			}
			if(page==4){

				JSONArray jsonArr=new JSONArray(mArrTempCategoryId);
				json.put("name", tempStoreName.getText().toString());
				json.put("place_parent", "0");
				json.put("category", jsonArr);

				json.put("building", "");
				json.put("street", tempStreet.getText().toString());

				json.put("landmark", tempLandMark.getText().toString());
				json.put("area", tempArea.getText().toString());
				json.put("area_id", msAreaId);
				json.put("pincode", tempPinCode.getText().toString());
				json.put("city", tempCity.getText().toString());

				json.put("latitude", msLatitude);
				json.put("longitude", msLongitude);

				json.put("landline", tempLandline.getText().toString());
				json.put("landline2", tempLandline2.getText().toString());
				json.put("landline3", tempLandline3.getText().toString());

				if(tempMobile.getText().toString().length()!=0){
					json.put("mobile", tempMobile.getText().toString());
				}
				else{
					json.put("mobile", "");
				}

				if(tempMobile2.getText().toString().length()!=0){
					json.put("mobile2", tempMobile2.getText().toString());
				}
				else{
					json.put("mobile2", "");
				}
				if(tempMobile3.getText().toString().length()!=0){
					json.put("mobile3", tempMobile3.getText().toString());
				}
				else{
					json.put("mobile3", "");
				}

				json.put("email",tempEmailId.getText().toString());
				json.put("website",tempWebsite.getText().toString());
				json.put("facebook_url",tempFb.getText().toString());
				json.put("twitter_url",tempTwitter.getText().toString());


				json.put("description", tempDesc.getText().toString());


				String encodedPagerpath="";
				if(pager_byte!=null){
					encodedPagerpath=Base64.encodeToString(pager_byte, Base64.DEFAULT);
					if(encodedPagerpath==null || encodedPagerpath.equals("")){
						encodedPagerpath="";
					}
					if(miRemoveLogo==1){
						json.put("userfile","");
					}else{
						json.put("userfile",encodedPagerpath);
						json.put("filetype",FILE_TYPE);
					}
				}else if(msStoreLogo.length()!=0){
					if(miRemoveLogo==1){
						json.put("userfile","");
					}
				}

				if(isValidDetails()==true && isValidContactDetails()==true ){
					if(mInternet.isNetworkAvailable()){
						if(mbIsNewStore==true){
							new ShoplocalAsyncTask(this, "post","merchant", true, json, "updateStore").execute(msUrl);
						}else{
							new ShoplocalAsyncTask(this, "put","merchant", true, json, "updateStore").execute(msUrl);
						}
					}else{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}
			}
			debug(json.toString());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void deleteStore()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				List<NameValuePair> name=new ArrayList<NameValuePair>();
				name.add(new BasicNameValuePair("place_id",msStoreId));
				String param=URLEncodedUtils.format(name, "UTF-8");
				String msUrl=Config.MERCHANT_ADD_PLACE+"?"+param;
				JSONObject json = new JSONObject();
				new ShoplocalAsyncTask(this, "delete","merchant", true, json, "deleteStore").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void invalidateActionBar()
	{
		this.supportInvalidateOptionsMenu();
	}
	void toggleStore()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				JSONObject json = new JSONObject();
				json.put("user_id", Config.MERCHANT_USER_ID);
				json.put("auth_id", Config.MERCHANT_AUTH_ID);
				json.put("place_id", msStoreId);
				json.put("place_status", msPlaceStatus);
				String msUrl=Config.MERCHANT_ADD_PLACE;
				new ShoplocalAsyncTask(this, "put","merchant", true, json, "toggleStatus").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	void updateStoreTime(){
		try
		{
			if(mInternet.isNetworkAvailable()){
				JSONObject json=new JSONObject();
				String startTime="00:00";
				String closeTime="00:00";

				TextView txtTempTimeFrom=(TextView)mViewPager.findViewById(R.id.txtMangeStoreTimeFrom);
				TextView txtTempTimeTo=(TextView)mViewPager.findViewById(R.id.txtMangeStoreTimeTo);
				ToggleButton TemptoggleSun=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleSun);
				ToggleButton TemptoggleMon=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleMon);
				ToggleButton TemptoggleTue=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleTue);
				ToggleButton TemptoggleWed=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleWed);
				ToggleButton TemptoggleThu=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleThu);
				ToggleButton TemptoggleFri=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleFri);
				ToggleButton TemptoggleSat=(ToggleButton)mViewPager.findViewById(R.id.bManagetoggleSat);

				String msUrl=Config.MERCHANT_ADD_PLACE_TIME;
				json.put("place_id",msStoreId);

				if(TemptoggleMon.isChecked()==true){
					json.put("mon_time1_status","0");
				}
				else{ 
					json.put("mon_time1_status","1");
				}

				if(TemptoggleTue.isChecked()==true){
					json.put("tue_time1_status","0");
				}
				else{
					json.put("tue_time1_status","1");
				}
				if(TemptoggleWed.isChecked()==true){
					json.put("wed_time1_status","0");
				}
				else{
					json.put("wed_time1_status","1");
				}
				if(TemptoggleThu.isChecked()==true){
					json.put("thu_time1_status","0");
				}
				else{
					json.put("thu_time1_status","1");
				}

				if(TemptoggleFri.isChecked()==true){
					json.put("fri_time1_status","0");
				}else{
					json.put("fri_time1_status","1");
				}
				if(TemptoggleSat.isChecked()==true){
					json.put("sat_time1_status","0");
				}
				else{
					json.put("sat_time1_status","1");
				}
				if(TemptoggleSun.isChecked()==true){
					json.put("sun_time1_status","0");
				}			
				else{
					json.put("sun_time1_status","1");
				}

				if(txtTempTimeFrom.getText().toString().length()==0){
					startTime="00:00";
				}else{
					startTime=msStoreTimeFrom;
				}
				if(txtTempTimeTo.getText().toString().length()==0){
					closeTime="00:00";
				}else{
					closeTime=msStoreTimeTo;
				}

				debug("Edit Store times "+startTime+" "+closeTime);

				json.put("monday","monday");
				json.put("mon_time1_open",startTime);
				json.put("mon_time1_close",closeTime);

				json.put("tuesday","tuesday");
				json.put("tue_time1_open",startTime);
				json.put("tue_time1_close",closeTime);

				json.put("wednesday","wednesday");
				json.put("wed_time1_open",startTime);
				json.put("wed_time1_close",closeTime);

				json.put("thursday","thursday");
				json.put("thu_time1_open",startTime);
				json.put("thu_time1_close",closeTime);

				json.put("friday","friday");
				json.put("fri_time1_open",startTime);
				json.put("fri_time1_close",closeTime);

				json.put("saturday","saturday");
				json.put("sat_time1_open",startTime);
				json.put("sat_time1_close",closeTime);

				json.put("sunday","sunday");
				json.put("sun_time1_open",startTime);
				json.put("sun_time1_close",closeTime);


				json.put("user_id", Config.MERCHANT_USER_ID);
				json.put("auth_id", Config.MERCHANT_AUTH_ID);

				debug("Store Timing Json "+json.toString());
				new ShoplocalAsyncTask(this, "put","merchant", true, json, "updateTime").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	boolean isValidDetails()
	{
		EditText tempStoreName=(EditText)mViewPager.findViewById(R.id.edtManageStoreName);
		EditText tempStreet=(EditText)mViewPager.findViewById(R.id.edtManageStreet);
		EditText tempLandMark=(EditText)mViewPager.findViewById(R.id.edtManageLandMark);
		TextView tempArea=(TextView)mViewPager.findViewById(R.id.edtManageArea);
		TextView tempCity=(TextView)mViewPager.findViewById(R.id.edtManageCity);
		TextView tempPinCode=(TextView)mViewPager.findViewById(R.id.edtManagePinCode);
		EditText tempDesc=(EditText)mViewPager.findViewById(R.id.edtManageDesc2);

		if(tempStoreName.getText().toString().equalsIgnoreCase("") || tempStoreName.getText().toString().length()==0){
			showToast(getResources().getString(R.string.storeNameAlert));
			return false;
		}
		else if(msAreaId==null || msAreaId.length()==0){
			showToast("Area cannot be blank");
			return false;
		}
		else if(tempArea.getText().toString().equalsIgnoreCase("") || tempArea.getText().toString().length()==0 ){
			showToast("Area cannot be blank");
			return false;
		}
		else if(tempCity.getText().toString().equalsIgnoreCase("") || tempCity.getText().toString().length()==0 ){
			showToast("City cannot be blank");
			return false;
		}
		else if(tempPinCode.getText().toString().equalsIgnoreCase("") || tempPinCode.getText().toString().length()==0 ){
			showToast("Pincode cannot be blank");
			return false;
		}
		else if(tempPinCode.getText().toString().length()<6){
			showToast("Pincode must be 6 digit");
			return false;
		}
		else if(mArrTempCategoryId.size()==0 && mArrSelectedCategoryId.size()==0){
			showToast(getResources().getString(R.string.storeCategoryAlert));
			return false;
		}
		else
		{
			return true;
		}

	}

	//			void showToast(String text)
	//			{
	//				Toast.makeText(context, text,Toast.LENGTH_LONG).show();
	//			}
	boolean isValidContactDetails()
	{

		try
		{
			EditText tempLandline=(EditText)mViewPager.findViewById(R.id.edtManageLandline);
			EditText tempLandline2=(EditText)mViewPager.findViewById(R.id.edtManageLandline2);
			EditText tempLandline3=(EditText)mViewPager.findViewById(R.id.edtManageLandline3);

			EditText tempMobile=(EditText)mViewPager.findViewById(R.id.edtManageMobile);
			EditText tempMobile2=(EditText)mViewPager.findViewById(R.id.edtManageMobile2);
			EditText tempMobile3=(EditText)mViewPager.findViewById(R.id.edtManageMobile3);

			EditText tempEmail=(EditText)mViewPager.findViewById(R.id.edtManageEmailId);

			if(tempMobile.getText().toString().length()>0
					&& tempMobile.getText().toString().length()!=10){
				showToast(getResources().getString(R.string.mobileValidationAlert));
				return false;
			}
			/*		else if(store_mobile_no2.length()>2 && store_mobile_no2.length()!=12)*/
			else if(tempMobile2.getText().toString().length()>0 
					&& tempMobile2.getText().toString().length()!=10){
				showToast(getResources().getString(R.string.mobileValidationAlert));
				return false;
			}
			/*	else if(store_mobile_no3.length()>2 && store_mobile_no3.length()!=12)*/
			else if(tempMobile3.getText().toString().length()>0 
					&& tempMobile3.getText().toString().length()!=10){
				showToast(getResources().getString(R.string.mobileValidationAlert));
				return false;
			}
			else if(tempLandline.length()>0 
					&& tempLandline.length()!=11){
				showToast(getResources().getString(R.string.landlineValidationAlert));
				return false;
			}
			else if(tempLandline2.length()>0 
					&& tempLandline2.length()!=11){
				showToast(getResources().getString(R.string.landlineValidationAlert));
				return false;
			}
			else if(tempLandline3.length()>0
					&& tempLandline3.length()!=11){
				showToast(getResources().getString(R.string.landlineValidationAlert));
				return false;
			}
			else if(tempEmail.getText().toString().length()>0 
					&& !isValidEmail(tempEmail.getText().toString())){
				showToast(getResources().getString(R.string.emailIdAlert));
				return false;
			}

			else if(((tempMobile.getText().toString().length()==0) && (tempMobile2.getText().toString().length()==0) 
					&& (tempMobile3.getText().toString().length()==0)) 
					&& ((tempLandline.getText().toString().length()==0) && (tempLandline2.getText().toString().length()==0)
							&& (tempLandline3.getText().toString().length()==0))){
				showToast(getResources().getString(R.string.contactInfo));
				return false;
			}
			else{
				return true;
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
	}

	//Validating Email id.
	public  boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}   

	@Override
	public void onCompleteInsertion(String tag) {

	}

	@Override
	public void updateTime(String tag, Calendar time) {
		TextView txtTempTimeFrom=(TextView)mViewPager.findViewById(R.id.txtMangeStoreTimeFrom);
		TextView txtTempTimeTo=(TextView)mViewPager.findViewById(R.id.txtMangeStoreTimeTo);

		if(tag.equalsIgnoreCase("time-from")){
			DateFormat  dt=new SimpleDateFormat("hh:mm");
			try {
				//Formating time int 24 clock format.
				String now = new SimpleDateFormat("hh:mm aa").format(time.getTime());
				System.out.println("time in 12 hour format : " + now);
				SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
				SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
				String time24;
				time24 = outFormat.format(inFormat.parse(now));

				System.out.println("time in 24 hour format : " + time24);
				msStoreTimeFrom=time24;
				/*txtMangeStoreTimeFrom.setText(time24);*/
				txtTempTimeFrom.setText(now);

			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("time-to")){
			DateFormat  dt=new SimpleDateFormat("hh:mm");

			try {
				//Formating time int 24 clock format.
				String now = new SimpleDateFormat("hh:mm aa").format(time.getTime());
				System.out.println("time in 12 hour format : " + now);
				SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
				SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
				String time24;
				time24 = outFormat.format(inFormat.parse(now));
				System.out.println("time in 24 hour format : " + time24);
				/*txtMangeStoreTimeTo.setText(time24);*/
				txtTempTimeTo.setText(now);
				msStoreTimeTo=time24;
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		if(location.getLatitude()==0.0 || location.getLongitude()==0.0){
			showToast(getResources().getString(R.string.locationAlert));
		}else{
			msLatitude=location.getLatitude()+"";
			msLongitude=location.getLongitude()+"";
			setImage(msLatitude, msLongitude);
			skipToPage();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onLocationFound(String lat, String longi) {
		msLatitude=lat;
		msLongitude=longi;
		skipToPage();
	}

}




