package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.CallListDialog;

public class MarketPlaceActivity extends ShoplocalActivity implements ListClickListener,OnLastItemVisibleListener,
ShoplocalAsyncTaskStatusListener,OnRefreshListener<ListView>, OnItemClickListener {
	
	private PullToRefreshListView mList;
	private TextView mTxtCounter;
	private ProgressBar progress;
	private MarketPlaceAdapter mAdapter=null;
	private String msQuery="";
	private int index=-1;
	private int currentPage = 0;
	private int totalPages=0;

	private Activity mContext;
	private long mlBackPressed;

	private String msMobNo1;
	private String msMobNo2;
	private String msMobNo3;
	private String msTelNo1;
	private String msTelNo2;
	private String msTelNo3;
	private String msStoreName="";
	private String msStoreId="";
	private String msCallType="";
	private String msActiveAreaName="";
	private ArrayList<String> mArrCall=new ArrayList<String>();

	private Dialog mCalldailog;
	private ListView mListCall;
	private CallListDialog mCallAdapter;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(getResources().getString(R.string.itemAllStores));

		mContext=this;
		msActiveAreaName=mDbUtil.getActiveArea();

		mCalldailog=new Dialog(mContext);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		mCallAdapter=new CallListDialog(mContext, 0, mArrCall);
		mListCall.setAdapter(mCallAdapter);
		mListCall.setOnItemClickListener(new StoreCallListener());

		progress=(ProgressBar)findViewById(R.id.progress);
		mList=(PullToRefreshListView)findViewById(R.id.dafault_listSearchResult);
		mTxtCounter=(TextView)findViewById(R.id.txtCounter);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		
		mList.setLayoutAnimation(controller);
		
		mAdapter=new MarketPlaceAdapter(this,mList,mTxtCounter,this);
		mList.setAdapter(mAdapter);
		
		mList.setOnLastItemVisibleListener(this);
		mList.setOnRefreshListener(this);
		mList.setOnItemClickListener(this);
		
		createNavigationList();
		showSearchBox(false);
	}
	
	private class StoreCallListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {

			Map<String, String> callClicked= new HashMap<String, String>();
			callClicked.put("AreaName",msActiveAreaName);
			callClicked.put("Source","MarketPlace");
			callClicked.put("storeName",msStoreName);
			debug(msStoreName);
			callClicked.put("Number",mArrCall.get(position));
			EventTracker.logEvent("StoreCallDone", callClicked);
			likePlace(msStoreId);
			msCallType="StoreCallDone";
			callContactApi(msStoreId);
			Intent call = new Intent(android.content.Intent.ACTION_DIAL);
			call.setData(Uri.parse("tel:"+mArrCall.get(position)));
			startActivity(call);
			mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			mCalldailog.dismiss();
		}

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{

			if(mSessionManager.isLoggedInCustomer())
			{
				createDrawer();
			}
		}
		if(requestCode==4)
		{
			if(mSessionManager.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();
				}
				else
				{
					Intent intent=new Intent(mContext,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}

	@Override
	public void onLastItemVisible() {
		if(mInternet.isNetworkAvailable()){
			Cursor cursor=null;
			try{
				msQuery="Select * from ALL_STORES_RECORDS where area_id="+Config.MY_AREA;
				cursor=new ShoplocalCursorLoader(this, msQuery).loadInBackground();

				if(cursor!=null){

					cursor.moveToFirst();

					if(cursor.getCount()!=0){
						
						index=cursor.getColumnIndex("current_page");
						debug(cursor.getCount()+" index "+index);

						if(index>=0){
							currentPage=cursor.getInt(index);
						}

						index=cursor.getColumnIndex("total_page");

						if(index>=0){
							totalPages=cursor.getInt(index);
						}

						debug("current page "+currentPage+" total pages "+totalPages);

						if(currentPage<totalPages){
							mAdapter.setmPage(currentPage);
							mAdapter.getData(Config.MARKET_PLACE_URL + "latitude="+Config.LATITUDE + "&longitude=" + Config.LONGITUDE 
									+ "&distance="+Config.DISTANCE +"&area_id="+Config.MY_AREA 
									+ "&page=" + (currentPage+1) + "&count="+mAdapter.ITEMS_PER_PAGE,"",false,"MarktePlace");
						}
						else{
							debug("current page "+currentPage+" total pages "+totalPages);
						}
					}
					cursor.close();
				}

			}catch(CursorIndexOutOfBoundsException c){
				c.printStackTrace();
			}
			finally{
				cursor.close();
			}
		}else{
			showToast(getString(R.string.noInternetConnection));
		}
	}

	void debug(String s){
		Log.i("MarketPlaceActivity", "MarketPlaceActivity "+s);
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		refreshData();
	}
	
	private void refreshData() {
		if(mInternet.isNetworkAvailable()){
			mAdapter.setMbRefreshing(true);
			mAdapter.setmPage(0);
			mAdapter.getData(Config.MARKET_PLACE_URL + "latitude="+Config.LATITUDE + "&longitude=" + Config.LONGITUDE 
					+ "&distance="+Config.DISTANCE +"&area_id="+Config.MY_AREA 
					+ "&page=" + 0 + "&count="+mAdapter.ITEMS_PER_PAGE,"",false,"MarketPlace");
		}else{
			showToast(getString(R.string.noInternetConnection));
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		String storeId="";
		storeId=getStoreData(position);
		if(storeId.length()>0){
			EventTracker.logEvent("MarketPlace_Details", false);
			Intent intent=new Intent(this, StoreDetailsActivity.class);
			intent.putExtra("STORE_ID", storeId);
			startActivity(intent);
		}
	}

	String  getStoreData(int position)
	{
		String storeId="";
		//Pull to refresh listview index starts from 1
		int index=position-1;
		try
		{
			Cursor cursor=mAdapter.getCursor();
			if(cursor!=null && cursor.getCount()!=0){
				cursor.moveToPosition(index);
				storeId=cursor.getString(cursor.getColumnIndex("id"));
			}
		}catch(Exception ex){
			ex.printStackTrace();
			storeId="";
		}
		return storeId;
	}

	void getContactNumbers(int position)
	{
		try
		{
			int index=position-1;
			Cursor cursor=mAdapter.getCursor();
			if(cursor!=null && cursor.getCount()!=0){
				cursor.moveToPosition(index);
				msStoreId=cursor.getString(cursor.getColumnIndex("id"));
				msStoreName=cursor.getString(cursor.getColumnIndex("store_name"));
				msMobNo1=cursor.getString(cursor.getColumnIndex("mob_no_1"));
				msMobNo2=cursor.getString(cursor.getColumnIndex("mob_no_2"));
				msMobNo3=cursor.getString(cursor.getColumnIndex("mob_no_3"));
				msTelNo1=cursor.getString(cursor.getColumnIndex("tel_no_1"));
				msTelNo2=cursor.getString(cursor.getColumnIndex("tel_no_2"));
				msTelNo3=cursor.getString(cursor.getColumnIndex("tel_no_3"));
			}
			mArrCall.clear();
			mCalldailog.setTitle(msStoreName);
			if(msMobNo1.length()>5){
				mArrCall.add("+"+msMobNo1);
			}
			if(msMobNo2.length()>5){
				mArrCall.add("+"+msMobNo2);
			}
			if(msMobNo3.length()>5){
				mArrCall.add("+"+msMobNo3);
			}
			if(msTelNo1.length()>5){
				mArrCall.add(msTelNo1);
			}
			if(msTelNo2.length()>5){
				mArrCall.add(msTelNo2);
			}
			if(msTelNo3.length()>5){
				mArrCall.add(msTelNo3);
			}
			mCallAdapter.notifyDataSetChanged();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		if(mlBackPressed+2000 >System.currentTimeMillis()){
			finishTo();
		}else{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			mlBackPressed=System.currentTimeMillis();
		}
	}

	void finishTo()
	{
		mLocalNotification.alarm();
		if(isAppRated()==false){
		setSession(getSessionLength()+getTimeDiff()); //Add session  length.
		}
		mDbUtil.close();
		AppLaunchPrefs.clearPrefs(mContext);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		if(tag.equalsIgnoreCase("call")){ 
			getContactNumbers(position+1);
			if(mArrCall.size()!=0){
				msCallType="StoreCall";
				callContactApi(msStoreId);
				Map<String, String> callClicked= new HashMap<String, String>();
				callClicked.put("AreaName",msActiveAreaName);
				callClicked.put("Source","MarketPlace");
				callClicked.put("storeName",msStoreName);
				debug(msStoreName);
				EventTracker.logEvent("StoreCall", callClicked);
				
				mCalldailog.show();
			}else{
				showToast("No numbers to call");
			}
		}
	}

	@Override
	public void onAsyncStarted() {

	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		debug(sURL);
		if(tag.equalsIgnoreCase("callApi"))
		{

		}
	}

	@Override
	public void onError(int errorCode) {
		showToast(getResources().getString(R.string.errorMessage));
	}	

	void callContactApi(String storeId)
	{
		try
		{
//			if(mSessionManager.isLoggedInCustomer()){
				if(mInternet.isNetworkAvailable()){
					PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
					
					String msUrl=Config.PLACE_CALL_URL;
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("user_id", Config.CUST_USER_ID);
					jsonObject.put("auth_id", Config.CUST_AUTH_ID);
					jsonObject.put("place_id", storeId);
					jsonObject.put("via", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "
							+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
					jsonObject.put("call_type", msCallType);
					jsonObject.put("active_area", Config.MY_AREA);
					
					debug(jsonObject.toString());
					
					new ShoplocalAsyncTask(this,"post", "customer", false, jsonObject,"callApi").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
//			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	void likePlace(String postId)
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", postId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlaceOnCall").execute(msUrl);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
}