package com.phonethics.shoplocal;

public interface ShoplocalAsyncTaskStatusListener {
	public void onAsyncStarted();
    public void onCompleted(String sURL,String sJson,String tag);
    public void onError(int errorCode);
}