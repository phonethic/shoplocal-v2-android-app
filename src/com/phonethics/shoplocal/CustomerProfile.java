package com.phonethics.shoplocal;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils.TruncateAt;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager.BadTokenException;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.flurry.android.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;


public class CustomerProfile extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener
{

	private Activity mContext;

	private EditText mEdtCustName;
	private EditText mEdtCustEmail;
	private EditText mEdtCustCity;
	private EditText mEdtCustState;

	private TextView mTxtCustDob;
	private TextView mTxtMoreDetails;
	private TextView mTxtAddMore;

	private ImageView mImgProfileThumb;

	private Button mBtnCreatProfile;
	private Button mBtnSynFb;
	private Button mBtnChooseImage;

	private ProgressBar mProgerss;

	private RadioButton mRadioMale;
	private RadioButton mRadioFemale;

	private RadioGroup mRadioGroup;
	private LinearLayout mDateLayout;

	private ArrayList<LinearLayout> mLayouts;
	private ArrayList<String> mArrDateIds=new ArrayList<String>();
	private ArrayList<String> mArrDatesLables=new ArrayList<String>();
	private ArrayList<String> mArrSelectedIds=new ArrayList<String>();
	private ArrayList<String> mArrSeletedDateLables=new ArrayList<String>();
	private List<String> PERMISSIONS = Arrays.asList("email","user_birthday","user_location");

	private String [] msArrDateIds;
	private String [] msArrDatesLabels;
	private String msCurrentDate="";
	private String msGender="";
	private String msFbUserid="";
	private String msFbAccessToken="";
	private String msProfilePicUrl;
	private String msUserIdFB;
	private String msAccessTokenFB;
	private String FILE_PATH="";
	private String FILE_NAME="";
	private String FILE_TYPE="";
	private String msEncodedPath="";

	private int [] miArrStatus;
	private int mIndexes = 0;
	private int miTextPos;
	private int miChkCount = 0;
	private int mAge = 0;

	private boolean mbIsError=false;
	private boolean mbDateClicked=false;
	private boolean mbLoginFb=false;
	private boolean pendingPublishReauthorization = false;

	//
	private TextView[]  mDatePicker;
	private TextView[]  mDateTypePicker;
	private LinearLayout[] mChildLayout;
	private RelativeLayout[] mAddMoreBtnLayout;
	private ImageView [] mDeleteImg;

	//Activity request
	private final int REQ_CODE_CAMERA = 5;
	private final int REQ_CODE_GALLERY = 6;
	private final int REQ_CODE_GALLERYCROP = 7;

	private byte [] mProfileByte;
	private byte  mByteUserGender;

	private Calendar mDateTimeCal=Calendar.getInstance();

	//Image loader
	ImageLoader mImageLoader;
	DisplayImageOptions mOptions;

	private ScrollView mViewProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=this;
		hideDrawerLayout();
		setContentView(R.layout.activity_customer_profile);
		setTitle(getResources().getString(R.string.itemMyProfile));
		Date currentDate=new Date();
		msCurrentDate=new SimpleDateFormat("yyyy-MM-dd").format(currentDate);

		mImageLoader=ImageLoader.getInstance();
		mOptions = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		mLayouts=new ArrayList<LinearLayout>();

		mViewProfile=(ScrollView)findViewById(R.id.viewProfile);

		mImgProfileThumb=(ImageView)findViewById(R.id.profileThumb);

		mProgerss=(ProgressBar)findViewById(R.id.pBar);

		mRadioMale=(RadioButton)findViewById(R.id.genMale);
		mRadioFemale=(RadioButton)findViewById(R.id.genFemale);
		mRadioGroup=(RadioGroup)findViewById(R.id.radioGroup);

		mEdtCustName=(EditText)findViewById(R.id.customerName);
		mEdtCustEmail=(EditText)findViewById(R.id.customerEmail);
		mEdtCustCity=(EditText)findViewById(R.id.customerCity);
		mEdtCustState=(EditText)findViewById(R.id.customerState);

		mTxtCustDob=(TextView)findViewById(R.id.customerDob);
		mTxtMoreDetails=(TextView)findViewById(R.id.moreDetailsLabel);
		mTxtAddMore=(TextView)findViewById(R.id.AddMore);

		mBtnCreatProfile=(Button)findViewById(R.id.btnCreateProfile);
		mBtnSynFb=(Button)findViewById(R.id.btnCreateProfileFb);
		mBtnChooseImage=(Button)findViewById(R.id.btnProfChooseImage);

		mDateLayout = (LinearLayout) findViewById(R.id.dateViewParent);

		mEdtCustName.setTypeface(tf);
		mEdtCustEmail.setTypeface(tf);
		mEdtCustCity.setTypeface(tf);
		mEdtCustState.setTypeface(tf);
		mTxtCustDob.setTypeface(tf);
		mTxtMoreDetails.setTypeface(tf);
		mTxtAddMore.setTypeface(tf);
		mBtnCreatProfile.setTypeface(tf);
		mBtnSynFb.setTypeface(tf);
		mBtnChooseImage.setTypeface(tf);

		if(mSessionManager.isLoggedInCustomer()){
			getProfileDates();	
		}else{
			showToast("Kindly Login first to View/Update profile");
			Intent intent=new Intent(mActivity,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}


		mBtnChooseImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				choosePicture();
			}
		});

		mImgProfileThumb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				choosePicture();
			}
		});


		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if(checkedId==mRadioMale.getId()){
					msGender=mRadioMale.getText().toString();
				}
				else if(checkedId==mRadioFemale.getId()){
					msGender=mRadioFemale.getText().toString();
				}
			}
		});

		mBtnSynFb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mbLoginFb = true;
				try {
					PackageInfo info = getPackageManager().getPackageInfo(
							"com.phonethics.shoplocal", 
							PackageManager.GET_SIGNATURES);
					for (Signature signature : info.signatures) {
						MessageDigest md = MessageDigest.getInstance("SHA");
						md.update(signature.toByteArray());
						//Log.d("MY KEY HASH:", "MY KEY HASH:" +Base64.encodeToString(md.digest(), Base64.DEFAULT));
					}
				} catch (NameNotFoundException e) {

				} catch (NoSuchAlgorithmException e) {

				}
				if(mInternet.isNetworkAvailable())
				{
					login_facebook();
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		mBtnCreatProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mInternet.isNetworkAvailable())
				{
					mArrSelectedIds.clear();
					mArrSeletedDateLables.clear();
					for(int i=0;i<msArrDateIds.length;i++){
						debug("msArrDateIds.length "+msArrDateIds.length+" "+msArrDateIds[i]+" "+msArrDatesLabels[i]);
						if(!(msArrDateIds[i].equalsIgnoreCase("0")) && !(msArrDatesLabels[i].equalsIgnoreCase("0"))){
							mArrSelectedIds.add(msArrDateIds[i]);
							mArrSeletedDateLables.add(msArrDatesLabels[i]);
						}
					}

					if(mSessionManager.isLoggedInCustomer()==false){
						showToast(getResources().getString(R.string.customerLoginAlert));
						Intent intent=new Intent(mContext,LoginSignUpCustomer.class);
						mContext.startActivityForResult(intent,2);
						overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
					}
					else{
						if(mEdtCustEmail.getText().length()>0 && !isValidEmail(mEdtCustEmail.getText())){
							showToast(getResources().getString(R.string.emailIdAlert));
						}

						else{
							for(int i=0; i<mDatePicker.length;i++){
								if (mDateTypePicker[i].getVisibility() == View.VISIBLE){
									if(mDateTypePicker[i].getText().toString().trim().length() == 0){
										if(mDatePicker[i].getText().toString().length()!=0){
											mbIsError = true;
											break;
										}
									}
									else{
										mbIsError = false;
									}
								}
							}
							if(mbIsError){
								showToast(getResources().getString(R.string.dateTypeAlert));
							}
							else{

								for(int i=0;i<mDatePicker.length;i++){

									if(mDatePicker[i].getVisibility()==View.VISIBLE){

										if((mDatePicker[i].getText().toString().length())>0){
											miChkCount = 1;
											break;
										}
									}
								}

								if(miChkCount!=1){
									// show alert/prompt to fill date
									AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
									alertDialog.setIcon(R.drawable.ic_launcher);
									alertDialog.setTitle("Shoplocal");
									alertDialog.setMessage("Entering important dates allows merchants to send you special offers around those dates.");
									alertDialog.setIcon(R.drawable.ic_launcher);
									alertDialog.setCancelable(true);
									alertDialog.setPositiveButton("Skip", new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											updateProfile();
										}
									});
									alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
										}
									});
									AlertDialog alert = alertDialog.create();
									alert.show();
								}
								else{
									updateProfile();
								}
							}
						}
					}
				}
				else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}

			}
		});
	}

	void choosePicture()
	{
		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Shoplocal");
		alertDialog.setMessage("Choose Image From :");
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				msEncodedPath="";
				mbLoginFb = false;
				openGallery();
			}
		});
		alertDialog.setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				msEncodedPath="";
				mbLoginFb = false;
				takePicture();
			}
		});
		AlertDialog alert = alertDialog.create();
		alert.show();
	}

	private void openGallery() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQ_CODE_GALLERY);
	}


	private void takePicture() {
		Intent intent = new Intent(CustomerProfile.this, CameraView.class);
		startActivityForResult(intent, REQ_CODE_CAMERA);
	}

	public void login_facebook(){
		try
		{
			mProgerss.setVisibility(View.VISIBLE);
			Session.openActiveSession(mContext, true, new Session.StatusCallback() {
				public void call(final Session session, SessionState state, Exception exception) {
					if(session.isOpened()){
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();
						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(mContext, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}
						Request request = Request.newMeRequest(session,new Request.GraphUserCallback() {
							public void onCompleted(GraphUser user, Response response) {
								if(user!=null){
									try{
										msFbUserid=user.getId();
										msFbAccessToken=session.getAccessToken();
										mProgerss.setVisibility(View.INVISIBLE);

										String name = user.getName();
										String dob = user.getBirthday();
										Log.i("User Birthday","birthday "+dob);
										msGender = user.asMap().get("gender").toString();

										if(msGender.equalsIgnoreCase("male")){
											mRadioMale.setChecked(true);
										}
										else{
											mRadioFemale.setChecked(true);
										}

										String email =user.getProperty("email").toString();
										String city = user.getLocation().getProperty("name").toString();
										String url="http://graph.facebook.com/"+user.getId()+"/picture?type=large";
										setData(name, dob, email, city, "","",url);

										msUserIdFB = user.getId();
										msAccessTokenFB = session.getAccessToken();

									}catch(Exception ex){
										ex.printStackTrace();
									}
								}
								else{
									mProgerss.setVisibility(View.INVISIBLE);
								}
							}
						});
						request.executeAsync();
					}


					if(session.isClosed()){
						mProgerss.setVisibility(View.VISIBLE);
						EventTracker.logEvent("CSignup_LoginFailedFB", false);
						try {
							if ((exception instanceof FacebookOperationCanceledException ||
									exception instanceof FacebookAuthorizationException)) {
								new AlertDialog.Builder(CustomerProfile.this)
								.setTitle("Login Failed")
								.setMessage(exception.getMessage().toString())
								.setPositiveButton("OK", null)
								.show();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						Log.d("EXCEPTION","EXCEPTION " + exception);
					}
				}
			});
		}catch(NullPointerException npx)
		{
			npx.printStackTrace();
		}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void setData(String Name,String DOB, String emailId, String city, String state,String gender,String imgeUrl){
		mEdtCustName.setText(Name);
		DateFormat  dt=new SimpleDateFormat("yyyy-mm-dd");
		DateFormat df = new SimpleDateFormat("mm/dd/yyyy"); 
		Date startDate;
		String newDateString = null ;
		try {
			startDate = df.parse(DOB);
			newDateString = dt.format(startDate);
			System.out.println(newDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		for(int i=0;i<mArrDatesLables.size();i++){
			if(i!=0 && mDateTypePicker[i].getText().toString().equalsIgnoreCase(mArrDatesLables.get(0))){
			}
			else{
				mChildLayout[0].setVisibility(View.VISIBLE);
				mDatePicker[0].setText(newDateString);
				mDateTypePicker[0].setText(mArrDatesLables.get(0));
				msArrDateIds[0] = "1";
				msArrDatesLabels[0] = newDateString;
				break;

			}
		}
		mEdtCustEmail.setText(emailId);
		mEdtCustCity.setText(city);
		mEdtCustState.setText(state);
		mImageLoader.displayImage(imgeUrl, mImgProfileThumb, new ImageLoadingListener() {
			public void onLoadingStarted(String arg0, View arg1) {
				mProgerss.setVisibility(View.VISIBLE);
			}
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				mProgerss.setVisibility(View.GONE);
			}
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				mProgerss.setVisibility(View.GONE);
			}
			public void onLoadingCancelled(String arg0, View arg1) {
				mProgerss.setVisibility(View.GONE);
			}
		});
		mBtnCreatProfile.setText("Update Profile");
		msProfilePicUrl = imgeUrl;
	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			if(requestCode==2){
				getProfileDates();	
				createDrawer();
			}
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}catch(IllegalStateException ilgx)
		{
			ilgx.printStackTrace();
		}
		catch (NullPointerException e) {
			e.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		if (resultCode != RESULT_OK) {
			return;
		}
		try
		{
			Bitmap bitmap;
			switch (requestCode) {
			// CAMERA - A	
			case REQ_CODE_CAMERA:
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				bitmap = null;
				CameraImageSave cameraImageSave = new CameraImageSave();
				String filePath = cameraImageSave.getImagePath();
				try{
					bitmap = cameraImageSave.getBitmapFromFile(480);
					Uri uri = Uri.fromFile(new File(filePath));

					FileInputStream in = new FileInputStream(filePath);
					BufferedInputStream buf = new BufferedInputStream(in);
					bitmap = BitmapFactory.decodeStream(buf);


					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
					mProfileByte = stream.toByteArray();

					cameraImageSave.deleteFromFile();
					bitmap = null;
					cameraImageSave = null;

					setPhoto(BitmapFactory.decodeByteArray(mProfileByte , 0, mProfileByte.length));
				}
				catch(Exception e){
					Log.e("Exception","Cusomer profile class");
					e.printStackTrace();
				}
				break;

				// GET IMAGE FROM GALLERY - A
			case REQ_CODE_GALLERY:
				if(resultCode == RESULT_OK){
					Uri imageUri=data.getData();

					cropCapturedImage(imageUri);

					//					Intent intent=new Intent(CustomerProfile.this, ZoomCroppingActivity.class);
					//					intent.putExtra("imagepath", picturePath);
					//					intent.putExtra("comingfrom", "gallery");
					//					startActivityForResult(intent, REQ_CODE_GALLERYCROP);
				}
				break;

			case REQ_CODE_GALLERYCROP:
				if(resultCode == RESULT_OK){

					//Create an instance of bundle and get the returned data
					Bundle extras = data.getExtras();
					//get the cropped bitmap from extras
					Bitmap thePic = extras.getParcelable("data");

					CameraImageSave cameraSaveImage = new CameraImageSave();

					try
					{
						cameraSaveImage.saveBitmapToFile(thePic);
					}catch(Exception ex){
						ex.printStackTrace();
					}


					String filePath1 = Environment.getExternalStorageDirectory()
							+ File.separator + "temp_photo_crop.jpg";
					try{
						FileInputStream in = new FileInputStream(filePath1);
						BufferedInputStream buf = new BufferedInputStream(in);
						bitmap = BitmapFactory.decodeStream(buf);

						bitmap = Bitmap.createScaledBitmap(bitmap, 480, 480, true);

						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						mProfileByte = stream.toByteArray();
						bitmap = null;

						setPhoto(BitmapFactory.decodeByteArray(mProfileByte , 0, mProfileByte.length));
					}
					catch(Exception e){}
				}
				break;
			}

		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void cropCapturedImage(Uri picUri){
		//call the standard crop action intent 
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		//indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		//set crop properties
		cropIntent.putExtra("crop", "true");
		//indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		//indicate output X and Y
		cropIntent.putExtra("outputX", 256);
		cropIntent.putExtra("outputY", 256);
		//retrieve data on return
		cropIntent.putExtra("return-data", true);
		//start the activity - we handle returning in onActivityResult
		startActivityForResult(cropIntent, REQ_CODE_GALLERYCROP);
	}

	void setPhoto(Bitmap bitmap)
	{
		try{
			mbFacebook=false;
			mImgProfileThumb.setImageBitmap(bitmap);
			mImgProfileThumb.setScaleType(ScaleType.FIT_CENTER);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}   

	void getUserProfile()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("user_id", Config.CUST_USER_ID));
				String param=URLEncodedUtils.format(nameValuePairs, "utf-8");
				String msUrl=Config.LOAD_USER_PROFILE+param;
				new ShoplocalAsyncTask(this, "get","customer",true, "getProfile").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	void updateProfile()
	{
		try
		{
			if(mAge!=0){
				EventTracker.setUserAge(CustomerProfile.this, mAge);
			}

			EventTracker.setUserGender(mByteUserGender);

			if(mInternet.isNetworkAvailable()){
				JSONObject json=new JSONObject();
				json.put("active_area",Config.MY_AREA);
				json.put("name", mEdtCustName.getText().toString());
				json.put("email",mEdtCustEmail.getText().toString());
				json.put("gender", msGender);
				json.put("city", mEdtCustCity.getText().toString());
				json.put("state",mEdtCustState.getText().toString());
				json.put("user_id", Config.CUST_USER_ID);
				json.put("auth_id", Config.CUST_AUTH_ID);

				if(mArrSeletedDateLables.size()!=0 && mArrSelectedIds.size()!=0){
					JSONArray dates = new JSONArray();
					for(int i=0;i<mArrSeletedDateLables.size();i++){
						JSONObject obj = new JSONObject();
						obj.put("date_type", mArrSelectedIds.get(i));
						obj.put("date", mArrSeletedDateLables.get(i));
						dates.put(obj);
					}
					json.put("dates", dates);
				}
				else{
					json.put("dates", "");
				}

				if(mbLoginFb==true){
					json.put("image_url",msProfilePicUrl);
					json.put("facebook_user_id",msFbUserid);
					json.put("facebook_access_token",msFbAccessToken);
				}else{
					if(mProfileByte!=null){
						msEncodedPath=Base64.encodeToString(mProfileByte, Base64.DEFAULT);
						Log.i("Encode ", "Details : "+msEncodedPath);
						if(FILE_NAME!=null && msEncodedPath!=null){
							json.put("filename",FILE_NAME);
							json.put("filetype",FILE_TYPE);
							json.put("userfile",msEncodedPath);
						}
					}
					else{
						msEncodedPath="";
					}

				}
				debug("Update JSON "+json.toString());
				String msUrl=Config.UPDATE_USER_PROFILE_URL;
				new ShoplocalAsyncTask(this, "put", "customer", false, json, "updateProfile").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	void getProfileDates()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.DATE_CATEGORIES;
				new ShoplocalAsyncTask(this, "get","customer",true, "getDates").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(mViewProfile.isShown()==false){
			menu.add("View").setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}else{
			menu.add("Edit").setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home){
			finish();	
		}
		if(item.getTitle().toString().equalsIgnoreCase("Edit")){
			invalidateActionBar();
			mViewProfile.setVisibility(View.GONE);
		}
		if(item.getTitle().toString().equalsIgnoreCase("View")){
			invalidateActionBar();
			mViewProfile.setVisibility(View.VISIBLE);
		}

		return true;
	}
	
	void invalidateActionBar()
	{
		this.supportInvalidateOptionsMenu();
	}

	@Override
	public void onAsyncStarted() {
		mProgerss.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		debug(sURL);
		mProgerss.setVisibility(View.GONE);
		if(tag.equalsIgnoreCase("updateProfile")){

			try
			{
				JSONObject json=new JSONObject(sJson);

				String status=json.getString("success");
				String msg=json.getString("message");
				if(status==null) return;

				if(status.equalsIgnoreCase("true")){
					AlertDialog.Builder  alertDialog = new AlertDialog.Builder(mContext);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Shoplocal");
					alertDialog.setMessage(getResources().getString(R.string.profileUpdate));
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setCancelable(true);
					alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

					AlertDialog alert = alertDialog.create();
					alert.show();
				}else{
					String code=json.getString("code");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
					}
					showToast(msg);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}


		}
		if(tag.equalsIgnoreCase("getProfile")){

			try
			{
				JSONObject json=new JSONObject(sJson);

				String status=json.getString("success");
				String msg=json.getString("message");
				if(status==null) return;

				if(status.equalsIgnoreCase("true")){
					JSONObject jsonData=json.getJSONObject("data");
					String	id="";
					String	name="";
					String	mobile="";
					String	email="";
					String	gender="";
					String	city="";
					String	state="";
					String	imageUrl="";
					String fbid ="";
					ArrayList<String> datesLables = new ArrayList<String>();
					ArrayList<String> datesId = new ArrayList<String>();

					id=jsonData.getString("id");
					name=jsonData.getString("name");
					mobile=jsonData.getString("mobile");
					email=jsonData.getString("email");
					gender=jsonData.getString("gender");
					city=jsonData.getString("city");
					state=jsonData.getString("state");
					imageUrl=jsonData.getString("image_url");
					fbid=jsonData.getString("facebook_user_id");

					JSONArray jsonArry=jsonData.getJSONArray("dates");
					for(int i=0;i<jsonArry.length();i++)
					{
						JSONObject jsonObj=jsonArry.getJSONObject(i);
						datesId.add(jsonObj.getString("date_category"));
						datesLables.add(jsonObj.getString("date"));
					}
					debug("Profile Dates "+datesId.toString());
					if(datesId.size()>1){
						mLocalNotification.checkPref();
						mLocalNotification.setProfilePref(true, mLocalNotification.isProfilAlarmFired());
					}
					else{
						mLocalNotification.checkPref();
						mLocalNotification.setProfilePref(false, mLocalNotification.isProfilAlarmFired());
					}

					// check whether facebook user_id is present or not
					if(fbid.equalsIgnoreCase("0")){
						mBtnSynFb.setText("Connect with facebook");
					}
					else{
						mBtnSynFb.setText("Sync profile from facebook");
					}
					mByteUserGender = getGender(gender);

					setProfile(name, "", email, city, state, gender, imageUrl);
					if(datesLables.size() == 1 && datesLables.get(0).equalsIgnoreCase("0000-00-00")){
						for(int i=0;i<mArrDatesLables.size();i++){
							if(i == 3){
								break;
							}
							else{
								mChildLayout[i].setVisibility(View.VISIBLE);
								mDatePicker[i].setText("");
								mDateTypePicker[i].setText(mArrDatesLables.get(i));
								msArrDateIds[i] = mArrDateIds.get(i);
							}
						}
					}
					else{
						for(int i=0;i<datesLables.size();i++){
							debug("Dates Lable"+datesLables.get(i));
							mChildLayout[i].setVisibility(View.VISIBLE);
							mDatePicker[i].setText(datesLables.get(i));
							miChkCount = 1;
							for(int j=0;j< mArrDatesLables.size();j++){
								if(datesId.get(i).equalsIgnoreCase(mArrDateIds.get(j))){
									mDateTypePicker[i].setText(mArrDatesLables.get(j));
									msArrDatesLabels[i] = datesLables.get(i);
									msArrDateIds[i] = mArrDateIds.get(j);
									if(mArrDatesLables.get(j).equalsIgnoreCase("birthday")){
										mAge = getCurrentAge(datesLables.get(i));
										Log.i("Birthday","My birthday "+mAge);
									}
									break;
								}
							}
						}
					}
				}
				else{
					showToast(msg);
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("getDates")){
			try
			{
				JSONObject json=new JSONObject(sJson);
				String status=json.getString("success");
				if(status==null) return;

				if(status.equalsIgnoreCase("true")){
					getUserProfile();
					JSONObject jsonData=json.getJSONObject("data");
					JSONArray jsonArray=jsonData.getJSONArray("date_categories");
					int count=jsonArray.length();
					for(int i=0;i<count;i++)
					{
						JSONObject dateObj= jsonArray.getJSONObject(i);
						mArrDatesLables.add(dateObj.getString("name"));
						mArrDateIds.add(dateObj.getString("id"));
					}

					int size = mArrDatesLables.size();
					debug("Size  "+size);
					mDatePicker = new TextView[size];
					mDateTypePicker = new TextView[size];
					mChildLayout = new LinearLayout[size];
					mAddMoreBtnLayout = new RelativeLayout[size];
					mDeleteImg = new ImageView[size];
					miArrStatus = new int[size];
					msArrDateIds = new String[size];
					msArrDatesLabels = new String[size];

					// FOR INITIAL STATUS OF IDTOPASS AND DATETOPASS ARRAYLIST
					for(int indx=0; indx < msArrDateIds.length; indx++){
						msArrDateIds[indx] = "0";
						msArrDatesLabels[indx] = "0";
					}

					// FOR INITIAL STATUS OF ADD AND DELETE 
					for(int k=0;k<miArrStatus.length;k++){
						if(k==0){
							miArrStatus[k] = 10;
						}
						else{
							miArrStatus[k] = 5;
						}
					}

					for(int k=0;k<size;k++){

						mDatePicker[k] = new TextView(mContext);
						mDateTypePicker[k] = new TextView(mContext);
						mChildLayout[k]  = new LinearLayout(mContext);
						mAddMoreBtnLayout[k] = new RelativeLayout(mContext);
						mDeleteImg[k] = new ImageView(mContext);
					}

					creatLayout(size);

					// TO SHOW ONLY ONE VIEW AT THE VERY FIRST TIME
					for(int chk=0;chk<size;chk++){
						if(chk!=0){
							mChildLayout[chk].setVisibility(View.GONE);
						}
						else{
						}
					}

					// TO ADD A NEW VIEW ON CLICK OF ADD MORE BUTTON
					mTxtAddMore.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							mIndexes++;
							for(int m=0;m<miArrStatus.length;m++){
								if(miArrStatus[m] == 5){
									miArrStatus[m] = 10;
									mChildLayout[m].setVisibility(View.VISIBLE);
									break;
								}
							}
							Log.d("Value","Value" +  mIndexes);
							if(mIndexes==mArrDatesLables.size()-1){
								mTxtAddMore.setVisibility(View.INVISIBLE);
							}
						}
					});

					// TO DELETE ANY RECORDS
					for(int j=0;j<size;j++){

						final int copy = j;
						mDeleteImg[j].setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								if(mbIsError){
									mbIsError = false;
								}
								mIndexes = mIndexes-1;
								mDateTypePicker[copy].setText("");
								mDatePicker[copy].setText("");
								mChildLayout[copy].setVisibility(View.GONE);
								msArrDateIds[copy] = "0";
								msArrDatesLabels[copy] = "0";
								miArrStatus[copy] = 5;
								mTxtAddMore.setVisibility(View.VISIBLE);
							}
						});
					}
					// TO GET CLICK OF ALL DATEPICKERS 
					for( int m = 0 ; m< size ;m++){
						final int pos = m;
						mDatePicker[m].setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								mbDateClicked = true;
								chooseDate(pos);
							}
						});
					}

					// TO GET CLICK OF ALL DATE TYPE PICKER AND TO SET THEM
					for(int l =0 ; l <size; l++){

						final int copy = l;
						mDateTypePicker[l].setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								final CharSequence[] cs = mArrDatesLables.toArray(new CharSequence[mArrDatesLables.size()]);
								AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
								builder.setTitle("Make your selection");
								builder.setIcon(R.drawable.ic_launcher);
								builder.setItems(cs, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int which) {
										checkForAvailability(cs[which].toString(), copy, which);
									}
								});
								AlertDialog alert = builder.create();
								alert.show();
							}
						});
					}
				}
				else{
					//showToast(message);
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
	}

	int getCurrentAge(String date){
		Calendar cal = Calendar.getInstance();
		date = date.substring(0, 4);
		Log.i("Year","Current year "+date);
		int age = Integer.parseInt(date) - cal.get(Calendar.YEAR);
		return Math.abs(age);
	}

	byte getGender(String gender){
		if(gender.equalsIgnoreCase("male")){
			return Constants.MALE;
		}
		else {
			return Constants.FEMALE;
		}

	}

	public void setProfile(String Name,String DOB, String emailId, String city, String state,String gender,String imgeUrl){
		try
		{
			Log.d("GENDER","GENDER" + gender);
			mEdtCustName.setText(Name);
			mEdtCustEmail.setText(emailId);
			mEdtCustCity.setText(city);
			mEdtCustState.setText(state);
			if(gender.equalsIgnoreCase("Male")){
				mRadioMale.setChecked(true);
				msGender=mRadioMale.getText().toString().toLowerCase();
			}
			else{
				mRadioFemale.setChecked(true);
				msGender=mRadioFemale.getText().toString().toLowerCase();
			}
			if(imgeUrl.length()!=0)
			{
				mImageLoader.displayImage(Config.IMAGE_URL+imgeUrl, mImgProfileThumb, new ImageLoadingListener() {
					public void onLoadingStarted(String arg0, View arg1) {
						mProgerss.setVisibility(View.VISIBLE);
					}
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						mProgerss.setVisibility(View.GONE);
					}
					public void onLoadingComplete(String arg0, View arg1, Bitmap bitmap) {
						mProgerss.setVisibility(View.GONE);
						msEncodedPath=encodeTobase64(bitmap);
						FILE_TYPE="image/jpeg";
						FILE_NAME="temp_photo.jpg";
					}
					public void onLoadingCancelled(String arg0, View arg1) {
						mProgerss.setVisibility(View.GONE);
					}
				});
			}
			mBtnCreatProfile.setText("Update Profile");
			msProfilePicUrl = imgeUrl;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public static String encodeTobase64(Bitmap image)
	{
		Bitmap immagex=image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);
		Log.e("LOOK", imageEncoded);
		return imageEncoded;
	}
	public static Bitmap decodeBase64(String input) 
	{
		byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}

	public void chooseDate(){
		new DatePickerDialog(mContext, dateDialog, mDateTimeCal.get(Calendar.YEAR),mDateTimeCal.get(Calendar.MONTH),mDateTimeCal.get(Calendar.DAY_OF_MONTH)).show();
	}

	public void chooseDate(int pos){
		miTextPos  = pos;
		new DatePickerDialog(mContext, dateDialog, mDateTimeCal.get(Calendar.YEAR),mDateTimeCal.get(Calendar.MONTH),mDateTimeCal.get(Calendar.DAY_OF_MONTH)).show();
	}

	DatePickerDialog.OnDateSetListener dateDialog=new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			mDateTimeCal.set(Calendar.YEAR,year);
			mDateTimeCal.set(Calendar.MONTH, monthOfYear);
			mDateTimeCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");


			String ct = DateFormat.getDateInstance().format(new Date());

			String setDate = dt.format(mDateTimeCal.getTime());

			Log.d("SETDATE","SETDATE" +  setDate);

			Log.d("SETDATE","SETDATE" +  setDate);
			if(setDate.compareTo(msCurrentDate)>0){
				showToast(getResources().getString(R.string.dateAlert));
				if(mbDateClicked){
					for(int i=0; i<mArrDatesLables.size();i++){
						if(i==miTextPos)
							mDatePicker[i].setText("");
					}
				}
			}
			else{
				if(mbDateClicked){
					for(int i=0; i<mArrDatesLables.size();i++){
						if(i==miTextPos){
							mDatePicker[i].setText(dt.format(mDateTimeCal.getTime()));
							msArrDatesLabels[i] = dt.format(mDateTimeCal.getTime());
						}
					}
				}
			}

		}
	};

	public void checkForAvailability(String string, int position, int textPos) {
		boolean passIt = false;
		for(int chk = 0; chk< mArrDatesLables.size(); chk++){
			if(position == chk){
			}
			else{
				if(mDateTypePicker[chk].getText().toString().equalsIgnoreCase(string)){
					passIt = false;
					showToast(getResources().getString(R.string.dateTypeAlert));
					mDateTypePicker[position].setText("");
					break;
				}
				else{
					passIt = true;
					mDateTypePicker[position].setText(string);
				}
			}
		}

		if(passIt){
			msArrDateIds[position] = mArrDateIds.get(textPos);
		}

	}


	private void creatLayout(int size) {

		for(int index=0; index< size; index++){
			mChildLayout[index].setOrientation(LinearLayout.HORIZONTAL);

			LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp2.setMargins(10, 10, 10, 10);
			mChildLayout[index].setLayoutParams(lp2);

			// FOR TEXT BOX TO SELECT DATE FROM DATEPICKER
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
			lp.setMargins(0, 10, 5, 10);


			mDatePicker[index].setLayoutParams(lp);
			mDatePicker[index].setGravity(Gravity.CENTER);
			//datePick.setPadding(5, 5, 5, 5);

			mDatePicker[index].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdownicon, 0);
			mDatePicker[index].setBackgroundResource(R.drawable.shadow_effect);
			mDatePicker[index].setHint("Select Date");

			mChildLayout[index].addView(mDatePicker[index]);

			// FOR TEXTBOX TO SELECT TYPE OF DATE FROM LIST
			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.9f);
			lp1.setMargins(0, 10, 5, 10);


			mDateTypePicker[index].setLayoutParams(lp1);
			//dateType.setPadding(5, 5, 5, 5);
			mDateTypePicker[index].setGravity(Gravity.CENTER);

			mDateTypePicker[index].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdownicon, 0);
			mDateTypePicker[index].setBackgroundResource(R.drawable.shadow_effect);
			mDateTypePicker[index].setHint("select type of date");
			mDateTypePicker[index].setTextSize(12);
			mDateTypePicker[index].setEllipsize(TruncateAt.END);
			mDateTypePicker[index].setSingleLine(true);

			mChildLayout[index].addView(mDateTypePicker[index]);


			// ADD DELETE BUTTON IN LINEAR LAYOUT
			LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp3.setMargins(0, 10, 5, 10);

			mDeleteImg[index].setLayoutParams(lp3);
			mDeleteImg[index].setImageResource(R.drawable.deletebtn);
			mChildLayout[index].addView(mDeleteImg[index]);

			// FOR ADDMORE BUTTON LAYOUT AND ITS TEXT 
			RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			param.setMargins(20, 10, 0, 0);
			// FOR ADDMORE BUTTON TEXTVIEW INSIDE ABOVE RELATIVE LAYOUT
			mDateLayout.addView(mChildLayout[index]);
		}
	}


	@Override
	public void onError(int errorCode) {
		mProgerss.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}

	void debug(String msg){
		Log.d("CustomerProfile ", "CustomerProfile "+msg);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}


}


