package com.phonethics.shoplocal;

import android.util.Log;

/**
 * 
 * @author manoj
 *	All server url's 
 */
public class Config {
	private static boolean IN_DEBUG = false;
	
	public static  String DEVICE_ID="";
	
	public static final String APP_LAUNCH_API_CALLED="APP_LAUNCH_API_CALLED";
	
	public static final String IS_APP_LAUNCHED_CALLED="IS_APP_LAUNCHED_CALLED";
	
	public static final String APP_LAUNCH_PREF="APP_LAUNCH_PREF";
	
	public static final String APP_LAUNCH_KEY="APP_LAUNCH_KEY";
	
	public static final String SHARE_FB_PREF="SHARE_FB_PREF";
	
	public static final String SHARE_TOGGLE="SHARE_TOGGLE";
	
	public static final String APP_START_TIME="APP_START_TIME";
	
	public static final String APP_START_TIME_KEY="APP_DATE_TIME_STAMP";
	
	public static final String APP_SESSION="APP_SESSION";
	
	public static final String APP_SESSION_KEY="APP_SESSION_TIME";
	
	public static final String APP_RATED_KEY="IS_APP_RATED";
	
	public static final String APP_CATEGORY="CATEGORY_PREF";
	
	public static final String APP_CATEGORY_KEY="CATEGORY_DATE_STAMP";
	
	/*public static final String BASE_URL="http://stage.phonethics.in/shoplocal/api-3";*/ 
	
	//Live server url
	public static final String BASE_URL="http://shoplocal.co.in/api-3";
	
	/*public static final String IMAGE_URL="http://stage.phonethics.in/shoplocal";*/
	
	//Live server url
	public static final String IMAGE_URL="http://shoplocal.co.in/";
	
	/*public static final String FACEBOOK_SHARE_URL="http://stage.phonethics.in/shoplocal/marketplace/store";*/
	
	//Live server url
	public static final String FACEBOOK_SHARE_URL="http://shoplocal.co.in/marketplace/store/";
	
	public static final String API_HEADER="X-API-KEY";
	
	public static final String API_HEADER_VALUE="Fool";
	
	public static final String DEVICE_ID_KEY="DEVICE_ID";
	
	public static String LATITUDE="0.0";
	
	public static  String LONGITUDE="0.0";
	
	public static  String DISTANCE="-1";
	
	public static  String MY_AREA="";
	
	public static String CUST_USER_ID="";
	
	public static String CUST_AUTH_ID="";
	
	public static String MERCHANT_USER_ID="";
	
	public static String MERCHANT_AUTH_ID="";
	
	//public static final String BASE_URL="http://stage.phonethics.in/shoplocal/api-3/"; //Debug url
	
	public static final String CALL_COUNT_API= BASE_URL + "/broadcast_api/callcount?place_id=";
	
	public static final String APP_LAUNCH_URL= BASE_URL + "/device_api/device";
	
	public static final String MARKET_PLACE_URL=BASE_URL + "/place_api/search?";
	
	public static final String CATEGORIES_URL=BASE_URL + "/place_api/place_category";
	
	public static final String OFFERS_STREAM_URL=BASE_URL + "/broadcast_api/search?";
	
	public static final String REGISTER_USER_URL=BASE_URL + "/user_api/new_user";
	
	public static final String SET_PASSWORD_URL=BASE_URL + "/user_api/new_set_password";
	
	public static final String LOGIN_URL=BASE_URL + "/user_api/validate_credentials";
	
	public static final String DATE_CATEGORIES=BASE_URL + "/user_api/date_categories";
	
	public static final String MY_SHOPLOCAL_URL=BASE_URL + "/user_api/favourite_posts";
	
	public static final String LOAD_USER_PROFILE=BASE_URL + "/user_api/user?";
	
	public static final String UPDATE_USER_PROFILE_URL=BASE_URL + "/user_api/user";
	
	public static final String POST_LIKE_URL=BASE_URL + "/broadcast_api/like";
	
	public static final String POST_SHARE_URL=BASE_URL + "/broadcast_api/share";
	
	public static final String PLACE_LIKE_URL=BASE_URL + "/place_api/like";
	
	public static final String PLACE_CALL_URL=BASE_URL + "/broadcast_api/call";
	
	public static final String PLACE_SHARE_URL=BASE_URL + "/place_api/share";
	
	public static final String PLACE_DETAIL_URL=BASE_URL + "/place_api/places?";
	
	public static final String LOAD_ALL_POST_URL=BASE_URL + "/broadcast_api/broadcasts?";
	
	public static final String GALLERY_URL=BASE_URL + "/place_api/place_gallery?";
	
	public static final String AREA_URL=BASE_URL + "/place_api/areas?";
	
	public static final String NEAREST_AREA=BASE_URL + "/place_api/nearest_areas?";
	
	public static final String ALL_FAVOURITE_PLACES_URL=BASE_URL + "/user_api/favourite_places?";
	
	public static final String POST_DETAILS_URL=BASE_URL + "/broadcast_api/broadcasts?";
	
	public static final String POST_OFFER_URL=BASE_URL + "/broadcast_api/broadcast";
	
	public static final String MERCHANT_REGISTER_URL=BASE_URL + "/merchant_api/new_merchant/";
	
	public static final String MERCHANT_SET_PASSWORD_URL=BASE_URL + "/merchant_api/new_set_password";
	
	public static final String MERCHANT_LOGIN_URL=BASE_URL + "/merchant_api/validate_credentials";
	
	public static final String PEOPLE_COUNT_URL=BASE_URL + "/user_api/count?";
	
	public static final String MERCHANT_POST_BROADCAST_URL=BASE_URL + "/broadcast_api/broadcast";
	
	public static final String MERCHANT_ALL_PLACES_URL=BASE_URL + "/place_api/places";
	
	public static final String MERCHANT_ADD_PLACE=BASE_URL + "/place_api/place";
	
	public static final String MERCHANT_ADD_PLACE_TIME=BASE_URL + "/place_api/place_time";
	
	public static final String MERCHANT_POST_GALLERY=BASE_URL + "/place_api/place_gallery";
	
	public static final String MERCHANT_VALIDATE_LOGIN=BASE_URL + "/merchant_api/validate_login";
	
	public static void debug(String tag,String msg){
		if (IN_DEBUG)
			Log.i(tag, msg);
	}
}

