package com.phonethics.shoplocal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.adapters.MerchantDashboardAdapter;
import com.phonethics.model.DatePreferencesClass;

public class MerchantTalkHome extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener, DBListener
{

	private Activity mContext;
	private static long back_pressed;

	private DatePreferencesClass mDatePref;

	private ListView mPagerList;

	private ImageView mImgReportIcon;
	private TextView mTxtReportBig;

	private ImageView mImgPostIcon;
	private TextView mTxtTalkNowBig;

	private ImageView mImgViewIcon;
	private TextView mTxtViewPostBig;
	private TextView mTxtMoreBig;

	private SharedPreferences mPrefs;
	private Editor mEditor;

	private boolean mbIsPlaceRefreshRequired;

	private String VALIDATE_CREED_PREF="VALIDATE_CREED_PREF";
	private String VALIDATE_CREED_DATE="VALIDATE_CREED_DATE";

	//Fetch Dialog
	private AlertDialog.Builder mFetchDialogBuilder;
	private AlertDialog mFetchDialog;

	//Retry Dialog
	private AlertDialog.Builder mRetryDialogBuilder;
	private AlertDialog mRetryDialog;

	private ArrayList<String> mArrStoreId=new ArrayList<String>();
	private ArrayList<String> mArrStoreName=new ArrayList<String>();
	private ArrayList<String> mArrStoreLogo=new ArrayList<String>();
	private ArrayList<String> mArrStoreDesc=new ArrayList<String>();
	private ArrayList<String> mArrMenu=new ArrayList<String>();
	private ArrayList<Integer> mArrIcon=new ArrayList<Integer>();

	private String API_HEADER;
	private String API_VALUE;

	private String USER_ID="";
	private String AUTH_ID="";
	//User Id
	private String KEY_USER_ID="user_id";
	//Auth Id
	private String KEY_AUTH_ID="auth_id";

	private	ValidateCredentials validateCredentials;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merchant_talk_home);
		mContext=this;
		hideDrawerLayout();
		setTitle("Business Dashboard");

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//Initializing Date Pref Class
		mDatePref=new DatePreferencesClass(mContext, VALIDATE_CREED_PREF, VALIDATE_CREED_DATE);
		mPagerList=(ListView)findViewById(R.id.merchantPagerList);

		mImgReportIcon=(ImageView)findViewById(R.id.reportIcon);
		mTxtReportBig=(TextView)findViewById(R.id.txtReportBig);

		mImgPostIcon=(ImageView)findViewById(R.id.postIcon);
		mTxtTalkNowBig=(TextView)findViewById(R.id.txtTalkNowBig);


		mImgViewIcon=(ImageView)findViewById(R.id.viewIcon);
		mTxtViewPostBig=(TextView)findViewById(R.id.txtViewPostBig);

		mTxtMoreBig=(TextView)findViewById(R.id.txtMoreBig);

		mPrefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
		mEditor=mPrefs.edit();

		//Ui Dialogs
		mFetchDialogBuilder = new AlertDialog.Builder(mContext);
		mFetchDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));

		validateCredentials=new ValidateCredentials();

		//Retry Dialog
		mRetryDialogBuilder = new AlertDialog.Builder(mContext);
		mRetryDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));

		mTxtTalkNowBig.setTypeface(tf,Typeface.BOLD);

		mArrMenu.add("Reports");
		mArrMenu.add("View Post");
		mArrMenu.add("Add Store");
		mArrMenu.add("Edit Store");
		mArrMenu.add("Manage Gallery");
		mArrMenu.add("Contact Us");
		mArrMenu.add("Logout");
		//mArrMenu.add("Sure Shop");

		mArrIcon.add(R.drawable.ic_dashboard_reports);
		mArrIcon.add(R.drawable.ic_dashboard_viewpost);
		mArrIcon.add(R.drawable.ic_dashboard_addstore);
		mArrIcon.add(R.drawable.ic_dashboard_editstore);
		mArrIcon.add(R.drawable.ic_dashboard_gallery);
		mArrIcon.add(R.drawable.ic_setting_email);
		mArrIcon.add(R.drawable.ic_setting_logout);
		//mArrIcon.add(R.drawable.ic_dashboard_reports);

		mPagerList.setAdapter(new MerchantDashboardAdapter(mContext, 0, mArrMenu, mArrIcon));

		try
		{

			mDatePref.checkDateDiff();
			//Checking whether Validate api is getting called for first time only. If not then check if it is called and 
			//date time differnce is more than a day.
			if(mDatePref.isPrefDatePresent()==false || (mDatePref.isPrefDatePresent()==true && Math.abs(mDatePref.getDiffDays())>=1))
			{
				//Execute Validate Credentials 
				validateCredential();
				validateCredentials.execute(Config.MERCHANT_VALIDATE_LOGIN);
			}
			HashMap<String,String>user_details=mSessionManager.getUserDetails();
			USER_ID=user_details.get(KEY_USER_ID).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID).toString();

			SharedPreferences prefs=mContext.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
			mbIsPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);

			Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+mbIsPlaceRefreshRequired);

			if(!mSessionManager.isLoggedIn())
			{
				try
				{
					String active_place;
					active_place=mDbUtil.getActiveAreaID();
					Log.i("Active Area Count", "Active Area Count "+active_place);
					if(active_place.length()==0)
					{
						Intent intent=new Intent(mContext,LocationActivity.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
					else
					{
						Intent intent=new Intent(mContext,NewsFeedActivity.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}

			mFetchDialogBuilder
			.setMessage(getResources().getString(R.string.fetchDataMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false);

			mFetchDialog = mFetchDialogBuilder.create();
			mRetryDialogBuilder
			.setMessage(getResources().getString(R.string.retryDataMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Retry",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					try
					{
						getAllStores();
						//						loadAllStoreForUser();
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					try
					{
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});

			mRetryDialog = mRetryDialogBuilder.create();

			if(mbIsPlaceRefreshRequired==false)
			{
				mArrStoreId=mDbUtil.getAllPlacesIDs();
				mArrStoreName=mDbUtil.getAllPlacesNames();
				mArrStoreLogo=mDbUtil.getAllPlacesPhotoUrl();
				mArrStoreDesc=mDbUtil.getAllPlacesDesc();
			}
			else
			{
				getAllStores();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			if(hasOwnTag()==false)
			{
				setTag(mArrStoreId);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mImgReportIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				reports();
			}
		});

		mTxtReportBig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				reports();
			}
		});

		mImgPostIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				talkNow();
			}
		});
		mTxtTalkNowBig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				talkNow();
			}
		});
		mTxtViewPostBig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				viewPost();
			}
		});

		mImgViewIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				viewPost();
			}
		});


		mTxtMoreBig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent intent=new Intent(mContext,MerchantsHomeGrid.class);
				EventTracker.logEvent("Tab_MerchantMoreButton", true);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		mPagerList.setOnItemClickListener(new MerchantListItemClickListener());
	}//onCreate Ends here

	void addStore()
	{
		EventTracker.logEvent("Tab_AddStore",false );
		Intent intent=new Intent(mContext, EditStore.class);
		intent.putExtra("isNew", true);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	}
	void editStore(){
		//Add Store
		if(mArrStoreId.size()==0){
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.noStoreMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					try
					{
						Intent intent=new Intent(mContext, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						mContext.finish();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int arg1) {
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}
		//EditStore
		else if(mArrStoreId.size()==1){
			EventTracker.logEvent("Tab_EditStore",false );	
			Intent intent=new Intent(mContext, EditStore.class);
			intent.putExtra("isSplitLogin",true);
			intent.putExtra("STORE_NAME", mArrStoreName.get(0));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(0));
			intent.putExtra("STORE_ID", mArrStoreId.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		//Place Chooser
		else{
			EventTracker.logEvent("Tab_EditStore",false );	
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("viewPost",2);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
	}
	void manageGallery()
	{
		//Add Store
		if(mArrStoreId.size()==0){
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.noStoreMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					try
					{
						Intent intent=new Intent(mContext, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						mContext.finish();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int arg1) {
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}
		//Gallery
		else if(mArrStoreId.size()==1){
			EventTracker.logEvent("Tab_ManageGallery",false );	
			Intent intent=new Intent(mContext,EditStore.class);
			intent.putExtra("manageGallery",1);
			intent.putExtra("STORE_NAME", mArrStoreName.get(0));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(0));
			intent.putExtra("STORE_ID", mArrStoreId.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		//Place chooser
		else{
			EventTracker.logEvent("Tab_ManageGallery",false );	
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("viewPost",2);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",1);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

	}
	void facebook()
	{
		EventTracker.logEvent("Tab_MerchantFacebook",false );	
		Intent intent=new Intent(mContext,FbTwitter.class);
		intent.putExtra("isMerchant", true);
		intent.putExtra("isFb", true);
		intent.putExtra("fb", "facebook");
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	}
	void twitter()
	{
		EventTracker.logEvent("Tab_MerchantTwitter",false );	
		Intent intent=new Intent(mContext,FbTwitter.class);
		intent.putExtra("isMerchant", true);
		intent.putExtra("isFb", false);
		intent.putExtra("twitter", "twitter");
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	}
	void contactUs()
	{
		try
		{
			EventTracker.logEvent("Tab_MerchantContactUs",false );	
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
					"mailto",getResources().getString(R.string.contact_email), null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
			startActivity(Intent.createChooser(emailIntent, "Send email..."));
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}
	void logOut()
	{
		if(mSessionManager.isLoggedIn())
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.logoutmessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Confirm",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();

					SharedPreferences prefs;
					Editor editor;
					prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
					editor=prefs.edit();
					editor.clear();
					mSessionManager.logoutUser();

					String active_place;
					active_place=Config.MY_AREA;
					Log.i("Active Area Count", "Active Area Count "+active_place);
					mLocalNotification.clearPlacePrefs();
					mLocalNotification.clearPostPrefs();
					if(active_place.length()==0){
						Intent intent=new Intent(mContext,LocationActivity.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
					else{
						Intent intent=new Intent(mContext,NewsFeedActivity.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
				}
			})
			;
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}else{
			Intent intent=new Intent(mContext,SplitLoginSignUp.class);
			startActivity(intent);
			finish();

		}
	}
	private class MerchantListItemClickListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			
			if(position==0){
				reports();
			}
			if(position==1){
				viewPost();
			}
			if(position==2){
				addStore();
			}
			if(position==3){
				editStore();
			}
			if(position==4){
				manageGallery();
			}
			if(position==5){
				contactUs();
			}
			if(position==6){
				logOut();
			}
			if(position==7){
				sureShop();
			}
		}

	}
	
	void sureShop()
	{
		if(mArrStoreId.size()==0)
		{
			Intent intent=new Intent(mContext, EditStore.class);
			intent.putExtra("isNew", true);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		else if(mArrStoreId.size()==1)
		{
			Intent intent=new Intent(mContext,SureShop.class);
			intent.putExtra("SureShop",1);
			intent.putExtra("STORE_NAME", mArrStoreName.get(0));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(0));
			intent.putExtra("STORE_ID", mArrStoreId.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("report",0);
			intent.putExtra("sure_shop",1);
			intent.putExtra("viewPost",-1);
			intent.putExtra("AddStore",-1);
			intent.putExtra("manageGallery",-1);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

	}
	


	@Override
	public void onAsyncStarted() {
		if(!mFetchDialog.isShowing())
		{
			mFetchDialog.show();
		}
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		debug(sURL);
		mFetchDialog.dismiss();
		debug("Dialog is showing ? "+mFetchDialog.isShowing());
		if(tag.equalsIgnoreCase("allStores")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);

				String status=jsonObject.getString("success");
				if(status==null) return;

				if(status.equalsIgnoreCase("false")){
					String code=jsonObject.getString("code");
					if(code.equalsIgnoreCase("-116")){
						invalidAuthFinish();
						return;
					}
				}

				if(status.equalsIgnoreCase("true")){

					ArrayList<String>mArrLat=new ArrayList<String>();
					ArrayList<String>mArrLong=new ArrayList<String>();

					JSONArray jsonArray = jsonObject.getJSONArray("data");
					if(jsonArray == null)  return;

					int count = jsonArray.length();
					ContentValues[] values = new ContentValues[count];

					for(int i=0;i<count;i++){
						JSONObject storeObject=jsonArray.getJSONObject(i);
						if(storeObject!=null){
							ContentValues value=new ContentValues();
							value.put("id", storeObject.getInt("id"));
							mArrStoreId.add(storeObject.getInt("id")+"");
							value.put("place_parent", storeObject.getInt("place_parent"));
							value.put("store_name", storeObject.getString("name"));
							mArrStoreName.add(storeObject.getString("name"));
							value.put("description", storeObject.getString("description"));
							mArrStoreDesc.add(storeObject.getString("description"));
							value.put("building", storeObject.getString("building"));
							value.put("street", storeObject.getString("street"));
							value.put("landmark", storeObject.getString("landmark"));
							value.put("area", storeObject.getString("pincode"));
							value.put("pincode", storeObject.getString("pincode"));
							value.put("city", storeObject.getString("city"));
							value.put("state", storeObject.getString("state"));
							value.put("country", storeObject.getString("country"));
							value.put("latitude",storeObject.getString("latitude"));
							mArrLat.add(storeObject.getString("latitude"));
							value.put("longitude",storeObject.getString("longitude"));
							mArrLong.add(storeObject.getString("longitude"));
							value.put("tel_no_1", storeObject.getString("tel_no1"));
							value.put("tel_no_2", storeObject.getString("tel_no2"));
							value.put("tel_no_3", storeObject.getString("tel_no3"));
							value.put("mob_no_1", storeObject.getString("mob_no1"));
							value.put("mob_no_2", storeObject.getString("mob_no2"));
							value.put("mob_no_3", storeObject.getString("mob_no3"));
							value.put("logo", storeObject.getString("image_url"));
							mArrStoreLogo.add(storeObject.getString("image_url"));
							value.put("email_id", storeObject.getString("email"));
							value.put("website", storeObject.getString("website"));
							value.put("facebook", storeObject.getString("facebook_url"));
							value.put("twitter", storeObject.getString("twitter_url"));
							value.put("store_status", storeObject.getString("place_status"));
							value.put("published_state", storeObject.getString("published"));
							value.put("total_like", storeObject.getString("total_like"));
							value.put("total_share", storeObject.getString("total_share"));
							value.put("total_view", storeObject.getString("total_view"));

							values[i] = value;
						}
					}

					Cursor c1=new ShoplocalCursorLoader(mContext, "Delete from MERCHANT_STORES").loadInBackground();
					if(c1!=null){
						debug("deleted previous entries "+c1.getCount()); 
						DBUtil dbUtil = DBUtil.getInstance(mContext.getApplicationContext());
						debug("Dialog is showing before inserting ? "+mFetchDialog.isShowing());
						if(!mFetchDialog.isShowing())
						{
							mFetchDialog.show();
						}
						dbUtil.replaceOrUpdate(this,"MERCHANT_STORES", values,"merchant-stores");
						debug("Dialog is showing before inserting is it visible ? "+mFetchDialog.isShowing());
					}

					//Checking For Empty content
					//And Setting Alarm Based on the empty place details.
					try
					{
						mLocalNotification.checkPref();
						for(int i=0;i<mArrStoreId.size();i++)
						{
							if(mArrLat.get(i).toString().length()==0 || mArrLong.get(i).toString().length()==0
									|| mArrStoreLogo.get(i).length()==0 || mArrStoreDesc.get(i).length()==0)
							{
								Log.i("PREF ID ","PREF ID NAME "+mArrStoreId.get(i)+" "+mArrStoreName.get(i));
								mLocalNotification.setPlacePref(mArrStoreId.get(i),true, mLocalNotification.isPlaceAlarmFired());
								break;
							}
							else
							{
								mLocalNotification.setPlacePref(mArrStoreId.get(i),false, mLocalNotification.isPlaceAlarmFired());
							}
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}	
		}
	}

	@Override
	public void onCompleteInsertion(String tag) {
		debug("FetchComplete");
		debug("Dialog is showing inside onComplete ? "+mFetchDialog.isShowing());
		if(mFetchDialog.isShowing()){
			debug("FetchComplete 0");
			mFetchDialog.dismiss();
		}
		setPlaceRefresh(false);
	}

	void setPlaceRefresh(boolean status)
	{
		SharedPreferences prefs=mContext.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putBoolean("isPlaceRefreshRequired", status);
		editor.commit();
	}

	void debug(String msg){
		Log.i("MerchantTalk", "MerchantTalk "+msg);
	}

	@Override
	public void onError(int errorCode) {
		mFetchDialog.dismiss();
		setPlaceRefresh(true);
		showToast(getResources().getString(R.string.errorMessage));

	}

	void validateCredential()
	{
		//		if(mInternet.isNetworkAvailable()){
		//			String msUrl=Config.MERCHANT_ALL_PLACES_URL;
		//			new ShoplocalAsyncTask(this, "get", "merchant", true, "allStores").execute(msUrl);;
		//		}else{
		//			showToast(getResources().getString(R.string.noInternetConnection));
		//		}
	}

	void getAllStores()
	{
		if(mInternet.isNetworkAvailable()){
			String msUrl=Config.MERCHANT_ALL_PLACES_URL;
			new ShoplocalAsyncTask(this, "get", "merchant", true, "allStores").execute(msUrl);;
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	} 

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuItem extra=menu.add("Extra Settings").setIcon(R.drawable.switch_view);
		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		try
		{

			String active_place;
			active_place=mDbUtil.getActiveAreaID();
			if(active_place.length()==0)
			{
				Intent intent=new Intent(mContext,LocationActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
			else
			{
				Intent intent=new Intent(mContext,NewsFeedActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}

	void Debug(String msg){
		Log.i("MerchantTalkHome ","MerchantTalkHome "+msg);
	}

	public void onBackPressed() {
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			mLocalNotification.alarm();
			if(isAppRated()==false){
			setSession(getSessionLength()+getTimeDiff());//Add session  length. 
			}
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), "Press again to exit!", Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}

	//Validate Credential
	private class ValidateCredentials extends AsyncTask<String, Void, String>
	{
		protected void onCancelled() {
			super.onCancelled();
		}
		protected void onPreExecute() {

			super.onPreExecute();
		}
		protected String doInBackground(String... params) {
			String status = "";
			try
			{
				PackageInfo pinfo = mContext.getPackageManager().getPackageInfo(getPackageName(), 0);
				BufferedReader bufferedReader = null;
				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
				HttpPost httpPost=new HttpPost(params[0]);

				Log.i("", "Service Response URL "+params[0]);

				//Adding header.
				/*httpPost.addHeader("X-API-KEY", "Fool");*/
				httpPost.addHeader("X-HTTP-Method-Override", "PUT");
				httpPost.addHeader(API_HEADER, API_VALUE);

				JSONObject json = new JSONObject();

				json.put("user_id", USER_ID);
				json.put("auth_id", AUTH_ID);
				try
				{
					json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				StringEntity se = new StringEntity( json.toString());  
				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);

				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
				status=stringBuffer.toString();


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return  status;
		}




		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			try
			{
				String store_status;
				String store_msg = "";
				store_status=getStatus(result);
				if(store_status.equalsIgnoreCase("true"))
				{
					store_msg=getMessage(result);
					try
					{

						//Setting Current Date to Pref
						mDatePref.setDateInPref();

						//Tagging Merchant
						/*Set<String> tags = new HashSet<String>(); 
						tags=PushManager.shared().getTags();
						tags.add("merchant");
						PushManager.shared().setTags(tags);*/

						//Tracking Event
						EventTracker.logEvent("Merchant_ValidateLogin", false);


					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
				else
				{
					store_msg=getMessage(result);
					String error_code=getErrorCode(result);
					showToast(store_msg);
					if(error_code.equalsIgnoreCase("-116"))
					{
						mSessionManager.logoutUser();
						invalidAuthFinish();
					}
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {

				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		String getErrorCode(String status)
		{
			String error_code="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				error_code=jsonobject.getString("code");

			} catch (JSONException e) {

				e.printStackTrace();
				error_code="-1";
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				error_code="-1";
			}
			return error_code;
		}

		String getMessage(String status)
		{
			String message="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				message=jsonobject.getString("message");

			} catch (JSONException e) {

				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return message;
		}
	}


	void invalidAuthFinish()
	{
		try
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
					try
					{
						try
						{
							String active_place;
							active_place=mDbUtil.getActiveAreaID();
							//		Log.i("Active Area Count", "Active Area Count in Drawer");
							Log.i("Active Area Count", "Active Area Count "+active_place);
							if(active_place.length()==0)
							{
								Intent intent=new Intent(mContext,LocationActivity.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
							}
							else
		 					{
								Intent intent=new Intent(mContext,NewsFeedActivity.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	@Override
	public void finish() {
		super.finish();
		mDbUtil.close();
		AppLaunchPrefs.clearPrefs(mContext);
	}

	void setTag(ArrayList<String>ID)
	{
		try
		{
			boolean isMisMatch=false;
			//Adding Tags
			Set<String> tags = new HashSet<String>(); 
			//tags=PushManager.shared().getTags();
			ArrayList<String>tempIdList=new ArrayList<String>();

			String temp="";
			String replace="";
			if(tags.size()!=0)
			{
				//Getting All Tags
				Iterator<String> iterator=tags.iterator();

				//Traversing Tags to find out if there are any tags existing with own_id
				while(iterator.hasNext())
				{
					temp=iterator.next();

					Log.i("TAG", "Urban TAG PRINT "+temp);
					//If tag contains own_
					if(temp.startsWith("own_"))
					{
						//Replace own_ with "" and compare it with ID
						replace=temp.replaceAll("own_", "");
						tempIdList.add(replace);
						Log.i("TAG", "Urban TAG ID "+replace);

						//If Merchants Places ID arrayList does not contain ID which is already in tag list .
						//Set insMisMatch to true
						if(ID.contains(replace)==false)
						{
							isMisMatch=true;
							break;
						}
					}
				}
				if(isMisMatch==false)
				{
					//Check if Tag list has the Merchant Place ID's or not 
					for(int i=0;i<ID.size();i++)
					{
						if(tempIdList.contains(ID.get(i))==false)
						{
							isMisMatch=true;
							break;
						}
					}
				}
				Log.i("ID", "UAIR ID "+ID.toString());
				Log.i("ID", "UAIR ID TAG "+tempIdList.toString()+" "+isMisMatch);
				if(isMisMatch==true)
				{
					//Clear own_tag from Tag List
					for(int i=0;i<tempIdList.size();i++)
					{
						tags.remove("own_"+tempIdList.get(i));
					}

					//Add all ID's of Merchant Places to Tag list.
					for(int i=0;i<ID.size();i++)
					{
						tags.add("own_"+ID.get(i));
					}
					Log.i("TAG", "Urban TAG AFTER Merchant "+tags.toString());
					//Set Tags to Urban Airship
					//PushManager.shared().setTags(tags);
				}
			}
		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	boolean hasOwnTag()
	{

		boolean hasTag=false;
		//Adding Tags
		/*Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();
		String temp="";
		if(tags.size()!=0)
		{
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();
			//Traversing Tags to find out if there are any tags existing with own_id
			while(iterator.hasNext())
			{
				temp=iterator.next();
				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains own_
				if(temp.startsWith("own_"))
				{
					hasTag=true;
					break;
				}

			}

		}*/
		return hasTag;
	}



	void reports()
	{

		//Reports Tab
		if(mArrStoreId.size()==0){
			addNewStore();
		}
		else if(mArrStoreId.size()==1)
		{
			EventTracker.logEvent("Tab_Report", false);
			Intent intent=new Intent(mContext,Reports.class);
			intent.putExtra("STORE_NAME", mArrStoreName.get(0));
			intent.putExtra("STORE_LOGO", mArrStoreLogo.get(0));
			intent.putExtra("STORE_ID", mArrStoreId.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			EventTracker.logEvent("Tab_Report", false);
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("report",1);
			intent.putExtra("viewPost",-1);
			intent.putExtra("AddStore",-1);
			intent.putExtra("manageGallery",-1);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
	}
	void talkNow()
	{
		//Talk Now
		if(mArrStoreId.size()==0){
			addNewStore();
		}
		else if(mArrStoreId.size()==1)
		{
			EventTracker.logEvent("Tab_TalkNow", false);
			Intent intent=new Intent(mContext, CreateBroadCast.class);
			intent.putStringArrayListExtra("SELECTED_STORE_ID",mArrStoreId);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			EventTracker.logEvent("Tab_TalkNow", false);
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 2);
			intent.putExtra("viewPost", 0);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

	}
	void viewPost()
	{

		//View Post
		if(mArrStoreId.size()==0){
			addNewStore();
		}
		else if(mArrStoreId.size()==1)
		{
			EventTracker.logEvent("Tab_ViewPost", false);
			Intent intent=new Intent(mContext, ViewPost.class);
			intent.putExtra("storeName", mArrStoreName.get(0));
			intent.putExtra("STORE_ID", mArrStoreId.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			EventTracker.logEvent("Tab_ViewPost", false);
			Intent intent=new Intent(mContext,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("viewPost",1);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

	}

	void addNewStore()
	{

		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
		alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.noStoreMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				try
				{
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog3 = alertDialogBuilder3.create();
		alertDialog3.show();
	}



}
