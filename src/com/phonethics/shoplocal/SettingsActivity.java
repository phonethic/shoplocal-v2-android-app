package com.phonethics.shoplocal;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Session;
import com.facebook.SessionState;


public class SettingsActivity extends ShoplocalActivity {

	private Activity mContext;

	private TextView mTxtLogOutLable;
	private TextView mTxtLogOut;
	private TextView mTxtContactLable;
	private TextView mTxtContactNo;
	private TextView mTxtContactEmail;
	private TextView mTxtSocailConnect;
	private TextView mTextPermission;
	private	TextView mTxtClear;
	private TextView mTxtAboutUs;

	private TextView mTxtSocailLayer;
	private ImageView mImgSocailLogo;

	private LinearLayout mLogoutLayout;

	private  String KEY_USER_NAME_CUSTOMER="mobileno_CUSTOMER";

	private File mFile1;
	private File mFile2;
	private File mFile3;
	private File mFile4;
	private File mFile5;

	private double mdTotalLength;

	private DrawerLayout mDrawerLayout;
	private ExpandableListView mDrawerList;

	private Dialog mSocialDialog;
	private ListView mSocial;

	private String msFacebookUrl="https://www.facebook.com/shoplocal.co.in";
	private String msTwitterUrl="https://twitter.com/myshoplocal";
	private String msGooglePlus="https://plus.google.com/108704719894873121741/posts";
	private String msPineterest="http://www.pinterest.com/shoplocalindia/";

	private static long back_pressed;

	private RelativeLayout mShareLayout;
	private CheckBox mcbx;

	private Map<String, String> settings= new HashMap<String, String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		mContext=this;
		setTitle(getResources().getString(R.string.itemSettings));
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		mDrawerList=(ExpandableListView)findViewById(R.id.drawerList);

		setDrawer(mDrawerLayout,mDrawerList);
		createDrawer();

		mSocialDialog=new Dialog(mContext);
		mSocialDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
		mSocialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.activity_background));
		mSocialDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mSocialDialog.setContentView(R.layout.layout_socail_dialog);
		mSocialDialog.setCancelable(true);
		mSocialDialog.setTitle("Choose Location");
		mSocial=(ListView)mSocialDialog.findViewById(R.id.lstSocail);

		mTextPermission=(TextView)findViewById(R.id.txtPermission);
		mTxtLogOutLable=(TextView)findViewById(R.id.logoutLable);
		mTxtLogOut=(TextView)findViewById(R.id.logout);
		mTxtContactLable=(TextView)findViewById(R.id.contactLable);
		mTxtContactNo=(TextView)findViewById(R.id.pref_number);
		mTxtContactEmail=(TextView)findViewById(R.id.pref_ContactMail);
		mTxtSocailConnect=(TextView)findViewById(R.id.txtSocailConnect);
		mTxtAboutUs=(TextView)findViewById(R.id.txtAboutUs);
		mTxtClear=(TextView)findViewById(R.id.clear);
		mcbx=(CheckBox)findViewById(R.id.chkFb);

		mTxtAboutUs.setTypeface(tf);
		mTxtLogOutLable.setTypeface(tf);
		mTxtLogOut.setTypeface(tf);
		mTxtContactLable.setTypeface(tf);
		mTxtContactNo.setTypeface(tf);
		mTxtContactEmail.setTypeface(tf);
		mTxtSocailConnect.setTypeface(tf);
		mTxtClear.setTypeface(tf);
		mTextPermission.setTypeface(tf);

		mLogoutLayout=(LinearLayout)findViewById(R.id.logoutLayout);
		mShareLayout=(RelativeLayout)findViewById(R.id.fbPermissionLayout);

		if(!mSessionManager.isLoggedInCustomer()){
			mLogoutLayout.setVisibility(View.GONE);
			mShareLayout.setVisibility(View.GONE);
		}
		if(mSessionManager.isLoggedInFacebook()==true){
			mShareLayout.setVisibility(View.VISIBLE);
			if(getFbPerf()==true){
				mcbx.setChecked(true);
				sharePermission();	
			}else{
				mcbx.setChecked(false);
			}
		}else{
			mShareLayout.setVisibility(View.GONE);
			mcbx.setChecked(false);
		}

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			mFile1=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalGallery");
			mFile2=new File(android.os.Environment.getExternalStorageDirectory(),"/.hyperlocalcache");
			mFile3=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCustomerCache");
			mFile4=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCache");
			mFile5=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalWelcomeGallery");

			double total_length_file1 = getFolderSize(mFile1);
			double total_length_file2 = getFolderSize(mFile2);
			double total_length_file3 = getFolderSize(mFile3);
			double total_length_file4 = getFolderSize(mFile4);
			double total_length_file5 = getFolderSize(mFile5);

			mdTotalLength = total_length_file1 + total_length_file2 + total_length_file3 + total_length_file4 + total_length_file5;
			mdTotalLength = mdTotalLength/(1024*1024);

			DecimalFormat df = new DecimalFormat("#.##");
			Log.d("GINGERBREAD","GINGERBREAD " + mdTotalLength);

			mTxtClear.setText("Clear (File size " + df.format(mdTotalLength) + "MB)");
		}
		else
		{
			mFile1=mContext.getCacheDir();
			mFile2=mContext.getCacheDir();
			mFile3=mContext.getCacheDir();
			mFile4=mContext.getCacheDir();
			mFile5=mContext.getCacheDir();
		}

		mTxtClear.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				DeleteCache();
			}
		});

		mTxtLogOut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(mSessionManager.isLoggedInCustomer()){
					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
					alertDialogBuilder3.setTitle("Shoplocal");
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.logoutmessage))
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Confirm",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							try
							{
								mLocalNotification.checkPref();
								if(!mSessionManager.isLoggedInCustomer()){
									mLocalNotification.clearFavPrefs();
									mLocalNotification.clearProfilePrefs();
									mLocalNotification.clearAppLaunchPrefs();
									mLocalNotification.clearLoginPrefs();
								}
								if(mSessionManager.isLoggedInFacebook()){
									try{
										Session.openActiveSession(mContext, false, new Session.StatusCallback() {
											@Override
											public void call(Session session, SessionState state, Exception exception) {
												session.closeAndClearTokenInformation();
												Log.i("Clearing", "Token");
											}
										});
									} catch(NullPointerException npx) {
										npx.printStackTrace();
									} catch(Exception ex) {
										ex.printStackTrace();
									}
								}
								mSessionManager.logoutCustomer();
								if(mSessionManager.isLoggedInFacebook()==true){
									mShareLayout.setVisibility(View.VISIBLE);
								}else{
									SharedPreferences pref=mContext.getSharedPreferences(Config.SHARE_FB_PREF,MODE_PRIVATE);
									Editor  edit=pref.edit();
									edit.clear();
									edit.commit();
									mShareLayout.setVisibility(View.GONE);
								}
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}
							mLogoutLayout.setVisibility(View.GONE);
							createDrawer();
						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alertDialog3 = alertDialogBuilder3.create();
					alertDialog3.show();
				}
			}
		});

		mTxtContactNo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent call = new Intent(android.content.Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:"+getResources().getString(R.string.contact_no)));
				startActivity(call);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		mTxtContactEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
						"mailto",getResources().getString(R.string.contact_email), null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
				startActivity(Intent.createChooser(emailIntent, "Send email..."));

			}
		});

		mTxtSocailConnect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EventTracker.logEvent("Tab_SocialConnect", false);
				mSocialDialog.show();
			}
		});


		mTxtAboutUs.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EventTracker.logEvent("Tab_AboutShoplocal", false);
				Intent intent=new Intent(mContext, AboutUs.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		if(mSessionManager.isLoggedInFacebook()){
			mTxtLogOut.setText("Logout from facebook account");
		}
		else if(mSessionManager.isLoggedInCustomer()){
			HashMap<String,String>user_details=mSessionManager.getUserDetailsCustomer();
			String name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();
			mTxtLogOut.setText("Logout \t\t" + "(+" + name + ")");
		}


		mSocial.setAdapter(new SocailAdapter(mContext, 0, 0, getSocailContent(), getSocailIcons()));
		mSocial.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				mSocialDialog.dismiss();
				Intent intent=new Intent(mContext,AboutWebView.class);
				intent.putExtra("url", "Web");
				switch (position) {
				case 0:
					settings.put("Source","Facebook");
					EventTracker.logEvent("SocialConnect", settings);

					intent.putExtra("Web", msFacebookUrl);
					mContext.startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					break;
				case 1:
					settings.put("Source","Twitter");
					EventTracker.logEvent("SocialConnect", settings);

					intent.putExtra("Web", msTwitterUrl);
					mContext.startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					break;
				case 2:
					settings.put("Source","Google+");
					EventTracker.logEvent("SocialConnect", settings);

					intent.putExtra("Web", msGooglePlus);
					mContext.startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					break;
				case 3:
					settings.put("Source","Pinterest");
					EventTracker.logEvent("SocialConnect", settings);

					intent.putExtra("Web", msPineterest);
					mContext.startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					break;

				default:
					break;
				}
			}

		});
		
		mcbx.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sharePermission();
			}
		});

	}
	
	boolean getFbPerf()
	{
		boolean isAllowed=false;
		try
		{
			SharedPreferences pref=mContext.getSharedPreferences(Config.SHARE_FB_PREF,1);	
			isAllowed= pref.getBoolean(Config.SHARE_TOGGLE, false);
		}catch(Exception ex){
			ex.printStackTrace();
			return isAllowed;
		}
		return isAllowed;
		//Toast.makeText(context, pref.getBoolean(Config.SHARE_TOGGLE, false)+"", Toast.LENGTH_SHORT).show();
	}
	
	void sharePermission(){
		try{
			Config.debug("Settings ","Share Status "+mcbx.isChecked());
			SharedPreferences pref=mContext.getSharedPreferences(Config.SHARE_FB_PREF,MODE_PRIVATE);
			Editor  edit=pref.edit();
			if(mcbx.isChecked()==true){
				edit.putBoolean(Config.SHARE_TOGGLE, true);
			}else{
				edit.putBoolean(Config.SHARE_TOGGLE, false);	
			}
			edit.commit();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	ArrayList<String> getSocailContent()
	{ 
		ArrayList<String> mArr=new ArrayList<String>();
		mArr.add("Facebook");
		mArr.add("Twitter");
		mArr.add("Google+");
		mArr.add("Pinterest");
		return mArr;
	}

	ArrayList<Integer>getSocailIcons()
	{
		ArrayList<Integer> mArr=new ArrayList<Integer>();
		mArr.add(R.drawable.ic_facebook_permission);
		mArr.add(R.drawable.ic_social_twitter);
		mArr.add(R.drawable.ic_social_gplus);
		mArr.add(R.drawable.ic_social_pinterest);

		return mArr;
	}

	void DeleteCache()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage("Delete existing cache?")
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Clear",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				mdTotalLength = 0;
				mTxtClear.setText("Clear (File size " + mdTotalLength + "MB)");
				if(mFile1.exists()){
					DeleteRecursive(mFile1);
				}
				if(mFile2.exists()){
					DeleteRecursive(mFile2);
				}
				if(mFile3.exists()){
					DeleteRecursive(mFile3);
				}
				if(mFile4.exists()){
					DeleteRecursive(mFile4);
				}
				if(mFile5.exists()){
					DeleteRecursive(mFile5);
				}
			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	void DeleteRecursive(File file) {
		if(file.isDirectory()){
			//directory is empty, then delete it
			if(file.list().length==0){
				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{
				//list all the directory contents
				String files[] = file.list();
				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);
					//recursive delete
					DeleteRecursive(fileDelete);
				}
				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		toggleDrawer();
		return true;
	}

	public static double getFolderSize(File folderPath) {

		double totalSize = 0;
		if (folderPath == null) {
			return 0;
		}
		if (!folderPath.isDirectory()) {
			return 0;
		}
		File[] files = folderPath.listFiles();
		if(files != null){
			for (File file : files) {
				if (file.isFile()) {
					totalSize += file.length();
				} else if (file.isDirectory()) {
					totalSize += file.length();
					totalSize += getFolderSize(file);
				}
			}
		}
		return totalSize;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode==2)
		{
			createDrawer();
			if(mSessionManager.isLoggedInCustomer())
			{
				mLogoutLayout.setVisibility(View.VISIBLE);
			}
			
			if(mSessionManager.isLoggedInFacebook()==true){
				mShareLayout.setVisibility(View.VISIBLE);
			}else{
				mShareLayout.setVisibility(View.GONE);
			}

			if(mSessionManager.isLoggedInFacebook()){
				mTxtLogOut.setText("Logout from facebook account");
			}
			else if(mSessionManager.isLoggedInCustomer()){
				//Login Details
				HashMap<String,String>user_details=mSessionManager.getUserDetailsCustomer();
				String name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();
				mTxtLogOut.setText("Logout \t\t" + "(+" + name + ")");
			}
		}
		if(requestCode==4)
		{
			if(mSessionManager.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(mContext, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					mContext.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					mContext.finish();
				}
				else
				{
					Intent intent=new Intent(mContext,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}

	}

	private class SocailAdapter extends ArrayAdapter<String>{

		private ArrayList<String> mArrSocailList;
		private ArrayList<Integer>mArrImageSource;
		private Activity mContext;
		private LayoutInflater inflator;
		private int count;
		public SocailAdapter(Activity mContext, int resource,
				int textViewResourceId,  ArrayList<String> mArrSocailList,ArrayList<Integer>mArrImageSource) {
			super(mContext, resource, textViewResourceId, mArrSocailList);
			this.mContext=mContext;
			this.mArrSocailList=mArrSocailList;
			this.mArrImageSource=mArrImageSource;
			this.inflator=mContext.getLayoutInflater();
			this.count=mArrSocailList.size();
		}
		@Override
		public int getCount() {
			return count;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView=inflator.inflate(R.layout.layout_socail_layer, null);
			}
			ViewHolder holder = (ViewHolder) convertView.getTag();
			if(holder == null) {
				holder=new ViewHolder();
				holder.textSocailLayer=(TextView)convertView.findViewById(R.id.txtSocailLayer);
				holder.imgSocailLogo=(ImageView)convertView.findViewById(R.id.imgSocailLogo);
				convertView.setTag(holder);
			}
			holder.textSocailLayer.setText(mArrSocailList.get(position));
			holder.imgSocailLogo.setImageResource(mArrImageSource.get(position));
			return convertView;
		}

		class ViewHolder
		{
			TextView textSocailLayer;
			ImageView imgSocailLogo;
		}
	}

	@Override
	public void onBackPressed() {
		if(back_pressed+2000 >System.currentTimeMillis()){
			finishTo();
		}
		else{
			showToast(getResources().getString(R.string.backpressMessage));
			back_pressed=System.currentTimeMillis();
		}
	}

	void finishTo()
	{
		mLocalNotification.alarm();
		if(isAppRated()==false){
			setSession(getSessionLength()+getTimeDiff());//Add session  length.
		}
		mDbUtil.close();
		AppLaunchPrefs.clearPrefs(mContext);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}	

}