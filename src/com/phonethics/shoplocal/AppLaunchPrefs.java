package com.phonethics.shoplocal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppLaunchPrefs {
	static boolean isAppLaunchedApiCalled(Context context){
		boolean isApiCalled=false;
		SharedPreferences pref=context.getSharedPreferences(Config.APP_LAUNCH_API_CALLED, 1);
		isApiCalled=pref.getBoolean(Config.IS_APP_LAUNCHED_CALLED, false);
		Config.debug("AppLaunchPrefs", "AppLaunchPrefs called "+isApiCalled);
		return isApiCalled;
	}

	static  void appLaunchApi(Context mContext){
		Config.debug("AppLaunchPrefs", "AppLaunchPrefs called");
		setAppLaunchCalled(mContext, true);
		Intent appService=new Intent(mContext, AppLaunchApi.class);
		mContext.startService(appService);
	}
	static  void setAppLaunchCalled(Context mContext,boolean isAppLaunched){
		SharedPreferences pref=mContext.getSharedPreferences(Config.APP_LAUNCH_API_CALLED, 1);
		Editor edit=pref.edit();
		edit.putBoolean(Config.IS_APP_LAUNCHED_CALLED, isAppLaunched);
		edit.commit();
	}
	static void clearPrefs(Context mContext){
		SharedPreferences pref=mContext.getSharedPreferences(Config.APP_LAUNCH_API_CALLED, 1);
		Editor edit=pref.edit();
		edit.remove(Config.IS_APP_LAUNCHED_CALLED);
		edit.commit();
	}
}
