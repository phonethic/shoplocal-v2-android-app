package com.phonethics.shoplocal;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.ListView;

public abstract class ShoplocalAdapter extends ResourceCursorAdapter implements ShoplocalAsyncTaskStatusListener,LoaderManager.LoaderCallbacks<Cursor> {
    protected int mCount;
    protected FragmentActivity mActivity;
    protected ListView mList;
    protected Cursor mCursor;

    public ShoplocalAdapter(FragmentActivity activity, int layout) {
	super(activity,layout,null,2);
	this.mActivity = activity;
	activity.getSupportLoaderManager().initLoader(R.id.MARKET_LOADER,null,this);
    }

    protected void getData(String sURL,String userType,boolean isCredentialNeeded,String tag) {
	  new ShoplocalAsyncTask(this,"get",userType,isCredentialNeeded,tag).execute(sURL);
    }

    public int getCount() {
	return mCount;
    }
    
    public Cursor getCursor() {
	return mCursor;
    }

    public boolean isEnabled(int position) {
	return true;
    }
    
    public boolean areAllItemsEnabled() {
	return true;
    }
    
//    public void onError(int errorCode) {
//	
//    }

    public abstract void bindView(View convertView, Context context,Cursor cursor);
    public abstract void onCompleted(String sURL, String sJson,String tag);
    public abstract void onError(int errorCode);
    public abstract Loader<Cursor> onCreateLoader(int id, Bundle bundle);
    public abstract void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor);
    
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
    	changeCursor(null);
    }
    

    
}
