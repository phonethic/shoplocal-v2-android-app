package com.phonethics.shoplocal;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

public class TimeFragment extends DialogFragment implements OnTimeSetListener {

	private int miHour;
	private int miMin;
	private Calendar mTime = Calendar.getInstance(); 
	private String tag;
	private UpdateTimeInterface mFragmentListener;
	private static String EXTRA_MESSAGE="TIME";

	public TimeFragment()
	{

	}
	
	public static final TimeFragment newInstance(String Page)
	{
		TimeFragment f=new TimeFragment();
		Bundle bdl=new Bundle(1);
		bdl.putString(EXTRA_MESSAGE, Page);
		f.setArguments(bdl);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try{
			mFragmentListener=(UpdateTimeInterface)activity;
		}catch(ClassCastException ex){
			ex.printStackTrace();
		}
	}

	public interface UpdateTimeInterface
	{
		void updateTime(String tag,Calendar time);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		tag= getArguments().getString(EXTRA_MESSAGE);
		miHour=mTime.get(Calendar.HOUR_OF_DAY);
		miMin=mTime.get(Calendar.MONDAY);
		return new TimePickerDialog(getActivity(), this, 0, 0, false);
	}
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		mTime.set(Calendar.HOUR_OF_DAY,hourOfDay);
		mTime.set(Calendar.MINUTE,minute);
		mFragmentListener.updateTime(tag,mTime);
	}

}
