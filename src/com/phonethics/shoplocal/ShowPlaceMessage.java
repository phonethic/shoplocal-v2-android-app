package com.phonethics.shoplocal;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ShowPlaceMessage extends ShoplocalActivity {

	Activity context;
	ActionBar actionBar;
	ArrayList<String>STOREID=new ArrayList<String>();
	private TextView mTxtmsg;
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_place_message);
		context=this;
		setTitle(getResources().getString(R.string.actionBarTitle));
		
		mTxtmsg=(TextView)findViewById(R.id.msg);
		mTxtmsg.setTypeface(tf);

		Bundle b=getIntent().getExtras();
		Button btnPost=(Button)findViewById(R.id.btnPost);
		Button btnSkip=(Button)findViewById(R.id.btnSkip);
		if(b!=null)
		{
			try
			{
				String storeid=b.getString("STOREID");
				STOREID.add(storeid);

				btnPost.setOnClickListener(new OnClickListener() {

					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(context, CreateBroadCast.class);
						intent.putStringArrayListExtra("SELECTED_STORE_ID",STOREID);
						startActivity(intent);
						overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
						finish();
					}
				});

				btnSkip.setOnClickListener(new OnClickListener() {

					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(context,MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}



	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent=new Intent(context,MerchantTalkHome.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
		return true;

	}

	
	public void onBackPressed() {
		Intent intent=new Intent(context,MerchantTalkHome.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
	}



}
