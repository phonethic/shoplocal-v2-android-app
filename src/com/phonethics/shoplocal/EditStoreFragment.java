package com.phonethics.shoplocal;

import java.text.SimpleDateFormat;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ParseException;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class EditStoreFragment extends Fragment implements LocationListener{

	public static final String EXTRA_MESSAGE = "PAGE";
	public static final String EXTRA_STORE="STORE_TYPE";

	private String LOCATION="Location";
	private String DETAILS="Details";
	private String CONTACT="Contact";
	private String DESCRIPTION="Description";
	private String OPERATIONS="Operations";
	private String GALLERY="Gallery";

	private View mView;
	
	private ImageView mImgMap;
	
	private TextView mTxtLocationInfo;
	
	private Button mBtnLocationSave;
	private Button mBtnLocationSkip;
	
	private Typeface tf;

	private EditFragmentClickListener mFragmentListener;
	
	private boolean mbIsNewStore=false;
	private boolean mbIsGpsEnabled=false;
	private boolean mbIsNetworkEnabled=false;

	private LocationManager mLocationManager;
	private Location mLocation;
	
	public EditStoreFragment()
	{
	}

	public static final EditStoreFragment newInstance(String Page,boolean isNewStore)
	{
		EditStoreFragment f=new EditStoreFragment();
		Bundle bdl=new Bundle(1);
		bdl.putString(EXTRA_MESSAGE, Page);
		bdl.putBoolean(EXTRA_STORE,isNewStore);
		f.setArguments(bdl);
		return f;
	}

	public interface EditFragmentClickListener {
		void onClickView(String tag);
		void onLocationFound(String lat,String longi);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try{
			mFragmentListener=(EditFragmentClickListener)activity;
		}catch(ClassCastException ex){
			ex.printStackTrace();
		}

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tf=Typeface.createFromAsset(getActivity().getAssets(), "fonts/GOTHIC_0.TTF");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		String page = getArguments().getString(EXTRA_MESSAGE);
		mbIsNewStore=getArguments().getBoolean(EXTRA_STORE);
		if(page.equalsIgnoreCase(LOCATION))
		{
			mView=inflater.inflate(R.layout.merchant1, null);
			mTxtLocationInfo=(TextView)mView.findViewById(R.id.shopDropMessage2);
			mBtnLocationSave=(Button)mView.findViewById(R.id.btnManageLocationtSave);
			mBtnLocationSkip=(Button)mView.findViewById(R.id.skipButton);
			mImgMap=(ImageView)mView.findViewById(R.id.mapImage);
			
			mTxtLocationInfo.setTypeface(tf);
			mBtnLocationSave.setTypeface(tf);
			mBtnLocationSkip.setTypeface(tf);
			
			mLocationManager=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
			mLocation=mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			mbIsGpsEnabled=mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			mbIsNetworkEnabled=mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if(mbIsGpsEnabled==false){
				showLocationAlert("GPS is disabled please enable it to get better accurracy.");
			}else if(mbIsGpsEnabled==false && mbIsNetworkEnabled==false)
			{
				showLocationAlert("Please enable location services.");
			}

			if(mLocation==null){
				mLocation=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if(mLocation!=null)
				{	
					if(mbIsNewStore==true){
						onLocationChanged(mLocation);
					}
				}else{
					showToast(getResources().getString(R.string.locationAlert));
				}
			}else
			{
				if(mbIsNewStore==true){
					onLocationChanged(mLocation);
				}
			}
			
			mBtnLocationSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("location-save");
				}
			});
			mBtnLocationSkip.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("location-skip");
				}
			});
			mImgMap.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("location-mapImage");		
				}
			});

		}
		if(page.equalsIgnoreCase(DETAILS))
		{
			mView=inflater.inflate(R.layout.managestorelocation, null);
			TextView mTxtCategory=(TextView)mView.findViewById(R.id.txtManageCategory);
			Button 	btnMangeStoreSave=(Button)mView.findViewById(R.id.btnMangeStoreSave);
			EditText edtStoreName=(EditText)mView.findViewById(R.id.edtManageStoreName);
			EditText edtStoreStreet=(EditText)mView.findViewById(R.id.edtManageStreet);
			EditText edtStoreLandMark=(EditText)mView.findViewById(R.id.edtManageLandMark);
			TextView txtArea=(TextView)mView.findViewById(R.id.edtManageArea);
			TextView txtCity=(TextView)mView.findViewById(R.id.edtManageCity);
			TextView txtPincode=(TextView)mView.findViewById(R.id.edtManagePinCode);
			
			txtArea.setHint("Select Area *");
			txtPincode.setHint("Pincode");
			txtCity.setHint("City");
			
			mTxtCategory.setTypeface(tf);
			edtStoreName.setTypeface(tf);
			btnMangeStoreSave.setTypeface(tf);
			edtStoreLandMark.setTypeface(tf);
			txtArea.setTypeface(tf);
			txtCity.setTypeface(tf);
			edtStoreStreet.setTypeface(tf);
			txtPincode.setTypeface(tf);
			
			
			mTxtCategory.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("details-category");		
				}
			});
			
			btnMangeStoreSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("details-btnSave");		
				}
			});
			txtArea.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("details-area");		
				}
			});

		}
		if(page.equalsIgnoreCase(CONTACT))
		{
			mView=inflater.inflate(R.layout.managestorecontact, null);
			TextView txtMangeContactStoreName=(TextView)mView.findViewById(R.id.txtMangeContactStoreName);
			EditText edtLandline=(EditText)mView.findViewById(R.id.edtManageLandline);
			EditText edtMobile=(EditText)mView.findViewById(R.id.edtManageMobile);
			
			Button btnManageContactSave=(Button)mView.findViewById(R.id.btnManageContactSave);
			EditText edtManageEmailId=(EditText)mView.findViewById(R.id.edtManageEmailId);
			EditText edtManageWebSite=(EditText)mView.findViewById(R.id.edtManageWebSite);

			EditText edtManageFb=(EditText)mView.findViewById(R.id.edtManageFb);
			EditText edtManageTwitter=(EditText)mView.findViewById(R.id.edtManageTwitter);

			TextView txtTempHead=(TextView)mView.findViewById(R.id.txtTempHead);

			Button btnManageContactSkip=(Button)mView.findViewById(R.id.btnManageContactSkip);

			TableRow landlinerow1=(TableRow)mView.findViewById(R.id.landlinerow1);
			final TableRow landlinerow2=(TableRow)mView.findViewById(R.id.landlinerow2);
			final TableRow landlinerow3=(TableRow)mView.findViewById(R.id.landlinerow3);

			final TableRow mobileRow1=(TableRow)mView.findViewById(R.id.mobileRow1);
			final TableRow mobileRow2=(TableRow)mView.findViewById(R.id.mobileRow2);
			final TableRow mobileRow3=(TableRow)mView.findViewById(R.id.mobileRow3);

			final Button add1=(Button)mView.findViewById(R.id.add1);
			final Button add2=(Button)mView.findViewById(R.id.add2);
			final Button add3=(Button)mView.findViewById(R.id.add3);

			final Button add4=(Button)mView.findViewById(R.id.add4);
			final Button add5=(Button)mView.findViewById(R.id.add5);
			final Button add6=(Button)mView.findViewById(R.id.add6);

			final EditText edtLandline2=(EditText)mView.findViewById(R.id.edtManageLandline2);
			final EditText edtLandline3=(EditText)mView.findViewById(R.id.edtManageLandline3);


			final EditText edtMobile2=(EditText)mView.findViewById(R.id.edtManageMobile2);
			final EditText edtMobile3=(EditText)mView.findViewById(R.id.edtManageMobile3);

			add1.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					if(!landlinerow2.isShown()){
						landlinerow2.setVisibility(View.VISIBLE);
					}else if(!landlinerow3.isShown()){
						landlinerow3.setVisibility(View.VISIBLE);
					}
					if(landlinerow2.isShown() && landlinerow3.isShown()){
						add1.setVisibility(View.GONE);
					}
				}
			});
			add2.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if(landlinerow2.isShown()){
						landlinerow2.setVisibility(View.GONE);
						edtLandline2.setText("");
					}
					if(!landlinerow2.isShown()){
						add1.setVisibility(View.VISIBLE);
					}
				}
			});
			add3.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if(landlinerow3.isShown()){
						landlinerow3.setVisibility(View.GONE);
						edtLandline3.setText("");
					}
					if(!landlinerow3.isShown()){
						add1.setVisibility(View.VISIBLE);
					}
				}
			});

			add4.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					if(!mobileRow2.isShown()){
						mobileRow2.setVisibility(View.VISIBLE);
					}else if(!mobileRow3.isShown()){
						mobileRow3.setVisibility(View.VISIBLE);
					}
					if(mobileRow2.isShown() && mobileRow3.isShown()){
						add4.setVisibility(View.GONE);
					}
				}
			});
			add5.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					if(mobileRow2.isShown()){
						mobileRow2.setVisibility(View.GONE);
						edtMobile2.setText("");
					}
					if(!mobileRow2.isShown()){
						add4.setVisibility(View.VISIBLE);
					}
				}
			});

			add6.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					if(mobileRow3.isShown()){
						mobileRow3.setVisibility(View.GONE);
						edtMobile3.setText("");
					}
					if(!mobileRow3.isShown()){
						add4.setVisibility(View.VISIBLE);
					}
				}
			});

			txtTempHead.setTypeface(tf);
			txtMangeContactStoreName.setTypeface(tf);
			edtLandline.setTypeface(tf);
			edtMobile.setTypeface(tf);
			btnManageContactSave.setTypeface(tf);
			edtManageEmailId.setTypeface(tf);
			edtManageWebSite.setTypeface(tf);
			btnManageContactSkip.setTypeface(tf);

			edtLandline2.setTypeface(tf);
			edtLandline3.setTypeface(tf);

			edtMobile2.setTypeface(tf);
			edtMobile3.setTypeface(tf);

			edtManageFb.setTypeface(tf);
			edtManageTwitter.setTypeface(tf);

			btnManageContactSkip.setOnClickListener( new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("contact-skip");
				}
			});
			btnManageContactSave.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("contact-save");
				}
			});
		}
		if(page.equalsIgnoreCase(DESCRIPTION))
		{
			mView=inflater.inflate(R.layout.merchantdescription, null);
			EditText edtDesc=(EditText)mView.findViewById(R.id.edtManageDesc2);
			Button btnDescSave=(Button)mView.findViewById(R.id.btnMangeStoreDescSave);
			Button btnDescKip=(Button)mView.findViewById(R.id.btnMangeStoreDescSkip);

			edtDesc.setTypeface(tf);
			edtDesc.setTypeface(tf);
			btnDescKip.setTypeface(tf);

			btnDescSave.setOnClickListener(new android.view.View.OnClickListener(){
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("description-save");
				}
			});
			btnDescKip.setOnClickListener(new android.view.View.OnClickListener(){
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("description-skip");
				}
			});
		}
		
		if(page.equalsIgnoreCase(OPERATIONS))
		{
			mView=inflater.inflate(R.layout.managestoretimings, null);

			TextView txtTimeFrom=(TextView)mView.findViewById(R.id.txtMangeStoreTimeFrom);
			TextView txtTimeTo=(TextView)mView.findViewById(R.id.txtMangeStoreTimeTo);

			ImageView imgLogo=(ImageView)mView.findViewById(R.id.imgManageStore_ThumbPreview);

			Button btnSave=(Button)mView.findViewById(R.id.btnManageTime);
			Button btnSkip=(Button)mView.findViewById(R.id.btnManageTimeSkip);
	
			btnSkip.setTypeface(tf);
			txtTimeFrom.setTypeface(tf);
			txtTimeTo.setTypeface(tf);
			btnSave.setTypeface(tf);

			try {
				String now = new SimpleDateFormat("hh:mm aa").format(new java.util.Date().getTime());
				System.out.println("time in 12 hour format : " + now);
				SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
				SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
				String time24 = "";
				try {
					time24 = outFormat.format(inFormat.parse(now));
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
				System.out.println("time in 24 hour format : " + time24);
				txtTimeFrom.setText(now);
				txtTimeTo.setText(now);
			} catch (ParseException e) {

				e.printStackTrace();
			}
			imgLogo.setOnClickListener(new android.view.View.OnClickListener(){
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("timing-logo");
				}
			});

			txtTimeFrom.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("timing-from");
				}
			});
			txtTimeTo.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("timing-to");
				}
			});
			
			btnSkip.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("timing-skip");
				}
			});
			btnSave.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("timing-save");
				}
			});
			
		}
		if(page.equalsIgnoreCase(GALLERY))
		{
			mView=inflater.inflate(R.layout.merchantgallery, null);

			Button btnSave=(Button)mView.findViewById(R.id.btnMangeStoreGalleryave);
			Button btnSkip=(Button)mView.findViewById(R.id.btnMangeStoreGallerySkip);

			btnSave.setTypeface(tf);
			btnSkip.setTypeface(tf);
			
			btnSave.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("gallery-save");
				}
			});
			btnSkip.setOnClickListener(new android.view.View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mFragmentListener.onClickView("gallery-skip");
				}
			});
			
		}
		return mView;
	}

	@Override
	public void onLocationChanged(Location location) {
		setImage(location.getLatitude()+"", location.getLongitude()+"");
		mFragmentListener.onLocationFound(location.getLatitude()+"", location.getLongitude()+"");
	}
	
	void setImage(String latitude,String longitude)
	{
		try
		{

			mTxtLocationInfo.setVisibility(View.VISIBLE);
			mTxtLocationInfo.setText("Location Found.Swipe to continue.");
			mTxtLocationInfo.setTextColor(Color.BLACK);
			mBtnLocationSkip.setText("Proceed");

			String url="http://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&zoom=16&size="+300+"x"+150+"&maptype=roadmap&markers=color:blue|label:S|"+latitude+","+longitude+"&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g";
			mImgMap.setScaleType(ScaleType.FIT_XY);
			mImgMap.setAdjustViewBounds(true);
			/*imageLoader.DisplayImage(url, staticMap);*/
			try{
				Picasso.with(getActivity()).load(url)
				.placeholder(R.drawable.ic_place_holder)
				.error(R.drawable.ic_place_holder)
				.into(mImgMap);
			}
			catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){ 
				e.printStackTrace();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}
	
	void showLocationAlert(String msg){


		AlertDialog.Builder alertDialog=new AlertDialog.Builder(getActivity());
		alertDialog.setTitle("Location");
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton("Enable",new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);

			}
		});
		alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alert=alertDialog.create();
		alert.show();

	}

	void showToast(String msg)
	{
	Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();	
	}

}

