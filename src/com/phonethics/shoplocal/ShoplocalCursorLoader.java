package com.phonethics.shoplocal;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.CursorLoader;
import android.app.Activity;
import android.net.Uri;
import android.util.Log;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.Intent;

public class ShoplocalCursorLoader extends CursorLoader {
	private Activity mActivity;
	private String msQuery;
	private DBUtil mDbUtil;
	private String[] mSelectionArgs;

	public ShoplocalCursorLoader(Activity context, String query,String[] selectionArgs) {
		super(context);
		this.mActivity = context;
		this.msQuery = query;
		this.mSelectionArgs = selectionArgs;
		this.mDbUtil = DBUtil.getInstance(mActivity.getApplicationContext());
	}

	
	public ShoplocalCursorLoader(Activity context, String query) {
		this(context,query,null);
		debug(query);
	}
	
	public Cursor loadInBackground() {	
		debug("Loading in Background");
		Cursor cursor=null;
		Log.i("Queries From ShoplocalCursorLoader", msQuery);
		cursor = mDbUtil.rawQuery(msQuery,mSelectionArgs);
		debug(cursor.getCount()+" ");
		return cursor;
	}

	private void debug(String s) {
		Log.v("CursorLoader " , "CursorLoader " + s);
	}

	protected void onStartLoading() {
		forceLoad();
		debug("Started Loading");
	}

	protected void onStopLoading() {
		super.onStopLoading();
	}


}