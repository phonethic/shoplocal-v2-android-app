package com.phonethics.shoplocal;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

public class FacebookOperations {


	Context mContext1;
	private static final String TAG = "FacebookMethods";
	private static ProgressDialog progressDialog;
	private static final List<String> mListPERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private static boolean pendingPublishReauthorization = false;
	//private static final String objectName = "store";

	public FacebookOperations(Context mContext){
		this.mContext1 = mContext;
		
	}

	/**
	 * Method use to invite friends to use your app;
	 * @param mContext
	 */
	//	public static void sendAppReuest(final Context mContext){
	//		String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
	//		Bundle params = new Bundle();
	//		params.putString("message", "Watch the offers in your area. Happy Shopping!!");
	//		///params.putString("", value)
	//
	//		WebDialog requestDialog = ( new WebDialog.RequestsDialogBuilder(mContext, Session.getActiveSession(), params))
	//				.setOnCompleteListener(new OnCompleteListener() {
	//
	//					@Override
	//					public void onComplete(Bundle values, FacebookException error) {
	//					
	//						if (error!=null) {
	//							if (error instanceof FacebookOperationCanceledException) {
	//								Toast.makeText(mContext,"Request cancelled",Toast.LENGTH_SHORT).show();
	//							} else {
	//								Toast.makeText(mContext,"Network Error",Toast.LENGTH_SHORT).show();
	//							}
	//						} else {
	//							final String requestId = values.getString("request");
	//							Log.d("Shoplocal Friend Request ", "Request "+values.toString());
	//							if (requestId != null) {
	//								Toast.makeText(mContext,"Request sent",Toast.LENGTH_SHORT).show();
	//							} else {
	//								Toast.makeText(mContext,"Request cancelled",Toast.LENGTH_SHORT).show();
	//							}
	//						}
	//
	//					}
	//				})
	//				//params.putString("to",  "");
	//				.build();
	//		requestDialog.show();
	//
	//	}

	/*
	 * Method use to invite friends to use your app;
	 * 
	 */
	public static void sendAppReuest(final Context mContext){
		try
		{
			//Toast.makeText(mContext, "Inviting Friends 1", 0).show();
			final Bundle params = new Bundle();
			params.putString("message", "Watch the offers in your area. Happy Shopping!!");
			Session.openActiveSession((Activity) mContext, true, new Session.StatusCallback() {
				@Override
				public void call(Session session, SessionState state,
						Exception exception) {
					if(session.isOpened()) {
						WebDialog requestDialog = ( new WebDialog.RequestsDialogBuilder(mContext, Session.getActiveSession(), params))
								.setOnCompleteListener(new OnCompleteListener() {
									@Override
									public void onComplete(Bundle values, FacebookException error) {
										if (error!=null) {
											if (error instanceof FacebookOperationCanceledException) {
												Toast.makeText(mContext,"Request cancelled",Toast.LENGTH_SHORT).show();
											} else {
												Toast.makeText(mContext,"Network Error",Toast.LENGTH_SHORT).show();
											}
										} else {
											final String requestId = values.getString("request");
											Log.d("Inorbit Friend Request ", "Request "+values.toString());
											if (requestId != null) {
												Toast.makeText(mContext,"Request sent",Toast.LENGTH_SHORT).show();
											} else {
												Toast.makeText(mContext,"Request cancelled",Toast.LENGTH_SHORT).show();
											}
										}
									}
								})
								.build();
						Toast.makeText(mContext, "Inviting Friends 2", 0).show();
						requestDialog.show();
					}
				}
			});
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	/**
	 * 
	 * @param mContext
	 * @param fbObj
	 * @param objectName
	 * To share any store or offer on facebook Time-line
	 */
	public static void shareThisInfo(final Context mContext, final FbObject fbObj,final String fbObjectType){
		boolean allowPost=false;
		try
		{
			SharedPreferences pref=mContext.getSharedPreferences(Config.SHARE_FB_PREF,1);
			allowPost= pref.getBoolean(Config.SHARE_TOGGLE, false);
			Config.debug("Facebook ", "Facebook Share "+allowPost);
			//Toast.makeText(mContext, "Is SHare con"+pref.getBoolean(Config.SHARE_TOGGLE, false)+"", Toast.LENGTH_SHORT).show();
		}catch(Exception ex){
			ex.printStackTrace();
			allowPost=false;
		}
		//Toast.makeText(mContext, "isShare"+isShareAllowed, Toast.LENGTH_SHORT).show();
		if(allowPost==true){
			
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage("Do you want to share this on your Facebook timeline?")
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					
					try
					{
						String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
						Session session = Session.getActiveSession();
						if (session != null || !session.isOpened()) {
							// Check for publish permissions    
							List<String> permissions = session.getPermissions();
							if (!isSubsetOf(mListPERMISSIONS, permissions)) {
								pendingPublishReauthorization = true;
								Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest((Activity)mContext, mListPERMISSIONS);
								session.requestNewPublishPermissions(newPermissionsRequest);
								return;
							}

							// Show a progress dialog because the batch request could take a while.
							progressDialog = ProgressDialog.show(mContext, "", "Please Wait", true);
							try {
								// Create a batch request, firstly to post a new object and
								// secondly to publish the action with the new object's id.
								RequestBatch requestBatch = new RequestBatch();

								// Set up the JSON representing the book
								JSONObject store = new JSONObject();
								store.put("title", fbObj.getStoreTitle());
								store.put("image",fbObj.getStoreImage());
								store.put("url",fbObj.getStoreUrl());
								store.put("description", fbObj.getStoreDescription());

								Bundle objectParams = new Bundle();
								objectParams.putString("object", store.toString());
								// Set up the object request callback
								Request.Callback objectCallback = new Request.Callback() {

									@Override
									public void onCompleted(Response response) {
										// Log any response error
										Log.d("", "response 1 =="+response.toString());
										FacebookRequestError error = response.getError();
										if (error != null) {
											dismissProgressDialog();
											Log.i(TAG, error.getErrorMessage());
										}
									}
								};

								// Create the request for object creation
								Request objectRequest = new Request(Session.getActiveSession(),"me/objects/"+nameSpace+":"+fbObjectType,objectParams, HttpMethod.POST, objectCallback);

								// Set the batch name so you can refer to the result
								// in the follow-on publish action request
								objectRequest.setBatchEntryName("objectCreate");

								// Add the request to the batch
								requestBatch.add(objectRequest);

								// Request: Publish action request
								// --------------------------------------------
								Bundle actionParams = new Bundle();

								// Refer to the "id" in the result from the previous batch request
								actionParams.putString(fbObjectType, "{result=objectCreate:$.id}");

								// Turn on the explicit share flag
								actionParams.putString("fb:explicitly_shared", "true");

								// Set up the action request callback
								Request.Callback actionCallback = new Request.Callback() {

									@Override
									public void onCompleted(Response response) {
										dismissProgressDialog();
										Log.d("", "response =="+response.toString());

										FacebookRequestError error = response.getError();
										if (error != null) {
											Toast.makeText(mContext,error.getErrorMessage(),Toast.LENGTH_LONG).show();
										} else {
											String actionId = null;
											try {
												JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
												actionId = graphResponse.getString("id");
											} catch (JSONException e) {
												Log.i(TAG,"JSON error "+ e.getMessage());
											}
											Toast.makeText(mContext, actionId,Toast.LENGTH_LONG).show();
										}
									}
								};

								// Create the publish action request
								Request actionRequest = new Request(Session.getActiveSession(),"me/"+nameSpace+":share",actionParams, HttpMethod.POST,actionCallback);
								//Request actionRequest = new Request(Session.getActiveSession(),"me/"+nameSpace+":favourite",actionParams, HttpMethod.POST,actionCallback);
								// Add the request to the batch
								requestBatch.add(actionRequest);
								// Execute the batch request
								requestBatch.executeAsync();
							} catch (JSONException e) {
								Log.i(TAG,"JSON error "+ e.getMessage());
								dismissProgressDialog();
							}
						}
						
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
					
				}
			})
			.setNegativeButton("Not now",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();

			
		}


	}


	public static void FavThisOnFb(final Context mContext, final FbObject fbObj,final String fbObjectType){
		boolean allowPost=false;
		try
		{
			SharedPreferences pref=mContext.getSharedPreferences(Config.SHARE_FB_PREF,1);
			allowPost= pref.getBoolean(Config.SHARE_TOGGLE, false);
			Config.debug("Facebook ", "Facebook Share "+allowPost);
			//Toast.makeText(mContext, "Is SHare con"+pref.getBoolean(Config.SHARE_TOGGLE, false)+"", Toast.LENGTH_SHORT).show();
		}catch(Exception ex){
			ex.printStackTrace();
			allowPost=false;
		}
		//Toast.makeText(mContext, "isShare"+isShareAllowed, Toast.LENGTH_SHORT).show();
//		if(allowPost==true){
//
//
//			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
//			alertDialogBuilder3.setTitle("Shoplocal");
//			alertDialogBuilder3
//			.setMessage("Do you want to share this on your Facebook timeline?")
//			.setIcon(R.drawable.ic_launcher)
//			.setCancelable(false)
//			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog,int id) {
//					
//					String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
//					Session session = Session.getActiveSession();
//					if (session != null || !session.isOpened()) {
//						// Check for publish permissions    
//						List<String> permissions = session.getPermissions();
//						if (!isSubsetOf(mListPERMISSIONS, permissions)) {
//							pendingPublishReauthorization = true;
//							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest((Activity)mContext, mListPERMISSIONS);
//							session.requestNewPublishPermissions(newPermissionsRequest);
//							return;
//						}
//
//						// Show a progress dialog because the batch request could take a while.
//						progressDialog = ProgressDialog.show(mContext, "", "Please Wait", true);
//						try {
//							// Create a batch request, firstly to post a new object and
//							// secondly to publish the action with the new object's id.
//							RequestBatch requestBatch = new RequestBatch();
//
//							// Set up the JSON representing the book
//							JSONObject store = new JSONObject();
//							store.put("title", fbObj.getStoreTitle());
//							store.put("image",fbObj.getStoreImage());
//							store.put("url",fbObj.getStoreUrl());
//							store.put("description", fbObj.getStoreDescription());
//
//							Bundle objectParams = new Bundle();
//							objectParams.putString("object", store.toString());
//							// Set up the object request callback
//							Request.Callback objectCallback = new Request.Callback() {
//
//								@Override
//								public void onCompleted(Response response) {
//									// Log any response error
//									Log.d("", "response 1 =="+response.toString());
//									FacebookRequestError error = response.getError();
//									if (error != null) {
//										dismissProgressDialog();
//										Log.i(TAG, error.getErrorMessage());
//									}
//								}
//							};
//
//							// Create the request for object creation
//							Request objectRequest = new Request(Session.getActiveSession(),"me/objects/"+nameSpace+":"+fbObjectType,objectParams, HttpMethod.POST, objectCallback);
//
//							// Set the batch name so you can refer to the result
//							// in the follow-on publish action request
//							objectRequest.setBatchEntryName("objectCreate");
//
//							// Add the request to the batch
//							requestBatch.add(objectRequest);
//
//							// Request: Publish action request
//							// --------------------------------------------
//							Bundle actionParams = new Bundle();
//
//							// Refer to the "id" in the result from the previous batch request
//							actionParams.putString(fbObjectType, "{result=objectCreate:$.id}");
//
//							// Turn on the explicit share flag
//							actionParams.putString("fb:explicitly_shared", "true");
//
//							// Set up the action request callback
//							Request.Callback actionCallback = new Request.Callback() {
//
//								@Override
//								public void onCompleted(Response response) {
//									dismissProgressDialog();
//									Log.d("", "response =="+response.toString());
//
//									FacebookRequestError error = response.getError();
//									if (error != null) {
//										Toast.makeText(mContext,error.getErrorMessage(),Toast.LENGTH_LONG).show();
//									} else {
//										String actionId = null;
//										try {
//											JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
//											actionId = graphResponse.getString("id");
//										} catch (JSONException e) {
//											Log.i(TAG,"JSON error "+ e.getMessage());
//										}
//										Toast.makeText(mContext, actionId,Toast.LENGTH_LONG).show();
//									}
//								}
//							};
//
//							// Create the publish action request
//							//Request actionRequest = new Request(Session.getActiveSession(),"me/"+nameSpace+":share",actionParams, HttpMethod.POST,actionCallback);
//							Request actionRequest = new Request(Session.getActiveSession(),"me/"+nameSpace+":favourite",actionParams, HttpMethod.POST,actionCallback);
//							// Add the request to the batch
//							requestBatch.add(actionRequest);
//							// Execute the batch request
//							requestBatch.executeAsync();
//						} catch (JSONException e) {
//							Log.i(TAG,"JSON error "+ e.getMessage());
//							dismissProgressDialog();
//						}
//					}
//
//				}
//			})
//			.setNegativeButton("Not now",new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog,int id) {
//					dialog.dismiss();
//				}
//			});
//			AlertDialog alertDialog3 = alertDialogBuilder3.create();
//			alertDialog3.show();
//
//		}


	}

	/** 
	 * @param mContext
	 * @param fbObj
	 * @param objectName
	 * This Post store/offer on facebook timeline as when store or offer is liked.
	 */
	public static void likeOnFb(final Context mContext,FbObject fbObj,String fbObjectName){
		String nameSpace = mContext.getResources().getString(R.string.fb_name_space);
		Session session = Session.getActiveSession();
		if (session != null || !session.isOpened()) {
			// Check for publish permissions    
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(mListPERMISSIONS, permissions)) {
				pendingPublishReauthorization = true;
				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest((Activity)mContext, mListPERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				return;
			}

			// Show a progress dialog because the batch request could take a while.
			progressDialog = ProgressDialog.show(mContext, "", "Please Wait", true);
			try {
				// Create a batch request, firstly to post a new object and
				// secondly to publish the action with the new object's id.
				RequestBatch requestBatch = new RequestBatch();

				// Set up the JSON representing the book
				JSONObject store = new JSONObject();
				store.put("title", fbObj.getStoreTitle());
				store.put("image",fbObj.getStoreImage());
				store.put("url",fbObj.getStoreUrl());
				store.put("description", fbObj.getStoreDescription());

				Bundle objectParams = new Bundle();
				objectParams.putString("object", store.toString());
				// Set up the object request callback
				Request.Callback objectCallback = new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						// Log any response error
						Log.d("", "response 1 =="+response.toString());
						FacebookRequestError error = response.getError();
						if (error != null) {
							dismissProgressDialog();
							Log.i(TAG, error.getErrorMessage());
						}
					}
				};

				// Create the request for object creation
				Request objectRequest = new Request(Session.getActiveSession(),"me/objects/"+nameSpace+":"+fbObjectName,objectParams, HttpMethod.POST, objectCallback);

				// Set the batch name so you can refer to the result
				// in the follow-on publish action request
				objectRequest.setBatchEntryName("objectCreate");

				// Add the request to the batch
				requestBatch.add(objectRequest);

				// Request: Publish action request
				// --------------------------------------------
				Bundle actionParams = new Bundle();

				// Refer to the "id" in the result from the previous batch request
				actionParams.putString("object", "{result=objectCreate:$.id}");

				// Turn on the explicit share flag
				actionParams.putString("fb:explicitly_shared", "true");

				// Set up the action request callback
				Request.Callback actionCallback = new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						dismissProgressDialog();
						Log.d("", "response =="+response.toString());

						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(mContext,error.getErrorMessage(),Toast.LENGTH_LONG).show();
						} else {
							String actionId = null;
							try {
								JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
								actionId = graphResponse.getString("id");
							} catch (JSONException e) {
								Log.i(TAG,"JSON error "+ e.getMessage());
							}
							catch(NullPointerException npe){
								npe.printStackTrace();
							}
							//Toast.makeText(mContext, actionId,Toast.LENGTH_LONG).show();
						}
					}
				};

				// Create the publish action request
				//Request actionRequest = new Request(Session.getActiveSession(),"me/og.likes", actionParams, HttpMethod.POST,actionCallback);
				Request actionRequest = new Request(Session.getActiveSession(),"me/shop-local:favourite", actionParams, HttpMethod.POST,actionCallback);
				// Add the request to the batch
				requestBatch.add(actionRequest);

				// Execute the batch request
				requestBatch.executeAsync();
			} catch (JSONException e) {
				Log.i(TAG,"JSON error "+ e.getMessage());
				dismissProgressDialog();
			}
		}
	}





	private static void dismissProgressDialog() {
		// Dismiss the progress dialog
		if (progressDialog != null) { 
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	private static boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}


}
