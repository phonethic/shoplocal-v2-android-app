package com.phonethics.shoplocal;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.CallListDialog;
import com.phonethics.adapters.PostAdapter;
import com.phonethics.shoplocal.StoreDetailsFragment.FragmentActionListener;
import com.squareup.picasso.Picasso;

public class StoreDetailsActivity extends ShoplocalActivity implements FragmentActionListener,ShoplocalAsyncTaskStatusListener, OnPageChangeListener, DBListener 
{


	private String PAGE_ABOUT_STORE="About Store";
	private String PAGE_ADDRESS="Address";
	private String PAGE_POST="Post/Offers";
	private String PAGE_IMAGE_GALLERY="Photo Gallery";

	private ViewPager mViewPager;
	private StorePageAdapter mStoreAdapter;

	private ArrayList<String>pages=new ArrayList<String>();
	private boolean mbIsUserLoggedIn=false;
	private boolean mbGetLike=false;
	private String msStoreId;
	private String msUrl;
	String USER_ID;

	public String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	ProgressBar storeProgress;

	private ArrayList<String>msArrCallnumbers=new ArrayList<String>();

	private String msShareVia="Android";

	private String msStoreName="";
	private String msAreaId="";
	private String msAreaName="";
	private String msStoreDescription="";
	private String msStoreLatitude="";
	private String msStoreLongitude="";
	private String msStoreLogo="";
	private String msStoreMobileNo1="";
	private String msStoreMobileNo2="";
	private String msStoreMobileNo3="";
	private String msStoreTelNo1="";
	private String msStoreTelNo2="";
	private String msStoreTelNo3="";
	private String msStoreEmail="";
	private String msStoreFb="";
	private String msStoreTwitter="";
	private String msStoreWebsite="";
	private String msStoreTotalLike="";
	private String msStoreTotalShare="";
	private String msStreet;
	private String msLandmark="";
	private String msCity="";
	private String msState="";
	private String msCountry="";
	private String msBuilding="";
	private String msUserLike="-1";
	private String msOpenTime="";
	private String msCloseTime="";
	private String msContact="";
	private String msMapLinkLable="";
	private String msMapLink="";
	private String msActivaAreaName="";
	private String msCategName="";

	private ArrayList<String>mArrStoreDay=new ArrayList<String>();
	private ArrayList<String>mArrPlaceTimingStatus=new ArrayList<String>();
	private ArrayList<String>mArrOpenTime=new ArrayList<String>();
	private ArrayList<String>mArrCloseTime=new ArrayList<String>();

	private ArrayList<String>mArrPostId=new ArrayList<String>();
	private ArrayList<String>mArrPostTitle=new ArrayList<String>();
	private ArrayList<String>mArrPostDescription=new ArrayList<String>();
	private ArrayList<String>mArrPostDate=new ArrayList<String>();
	private ArrayList<String>mArrPostOfferDate=new ArrayList<String>();
	private ArrayList<String>mArrPostTotalLike=new ArrayList<String>();
	private ArrayList<String>mArrPostTotalShare=new ArrayList<String>();
	private ArrayList<String>mArrPostTotalViews=new ArrayList<String>();
	private ArrayList<String>mArrPostImageUrl1=new ArrayList<String>();
	private ArrayList<String> mArrCategoryLable=new ArrayList<String>();
	private ArrayList<String> mArrCategoryId=new ArrayList<String>();
	private ArrayList<String> mArrPostIsOffer = new ArrayList<String>();


	private ArrayList<String>mArrGalleryImage=new ArrayList<String>();
	private ArrayList<String>mArrGalleryThumb=new ArrayList<String>();
	private ArrayList<String>mArrGalleryTitle=new ArrayList<String>();

	private Map<String, String> call= new HashMap<String, String>();
	private Map<String, String> facebook = new HashMap<String, String>();
	private Map<String, String> twitter = new HashMap<String, String>();;
	private Map<String, String> website = new HashMap<String, String>();
	private Map<String, String> email = new HashMap<String, String>();
	private Map<String, String> viewmap = new HashMap<String, String>();
	private Map<String, String> directions = new HashMap<String, String>();
	private Map<String, String> viewoffer = new HashMap<String, String>();

	Activity context;

	Dialog mCalldailog;
	ListView mListCall;

	TextView tvActionBarTitle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_details);
		hideDrawerLayout();
		context=this;
		storeProgress=(ProgressBar)findViewById(R.id.storeProgress);
		mViewPager=(ViewPager)findViewById(R.id.storePager);

		msActivaAreaName=mDbUtil.getActiveArea();

		View actionBarView = getLayoutInflater().inflate(R.layout.actionbarlayoutforstoredetail, null);
		tvActionBarTitle = (TextView) actionBarView.findViewById(R.id.textViewActionBarTitle);
		mActionBar.setCustomView(actionBarView);

		pages.add(PAGE_ABOUT_STORE);
		pages.add(PAGE_ADDRESS);
		pages.add(PAGE_POST);
		pages.add(PAGE_IMAGE_GALLERY);

		mbIsUserLoggedIn=mSessionManager.isLoggedInCustomer();

		mStoreAdapter=new StorePageAdapter(getSupportFragmentManager(), getFragments(),pages);
		mViewPager.setAdapter(mStoreAdapter);
		mViewPager.setOnPageChangeListener(this);
		mViewPager.setOffscreenPageLimit(mStoreAdapter.getCount()-1);

		mCalldailog=new Dialog(context);
		mCalldailog.setContentView(R.layout.calldialog);
		mCalldailog.setTitle("Contact");
		mCalldailog.setCancelable(true);
		mCalldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		mListCall=(ListView)mCalldailog.findViewById(R.id.listCall);

		Bundle b=getIntent().getExtras();
		if(b!=null)	{
			msStoreId=b.getString("STORE_ID");
			debug(msStoreId);
			if(mbIsUserLoggedIn==true){
				mbGetLike=true;
				HashMap<String,String>user_details=mSessionManager.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				msUrl=Config.PLACE_DETAIL_URL+"place_id="+msStoreId+"&customer_id="+USER_ID;	
			}else{
				mbGetLike=false;
				msUrl=Config.PLACE_DETAIL_URL+"place_id="+msStoreId;
			}
			if(mInternet.isNetworkAvailable()){
				new ShoplocalAsyncTask(this, "get", "customer", false, "storeDetails").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		}
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
				if(like==1)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	@Override
	public void onCallClicked() {
		//showToast("Call");

		Map<String, String> callClicked= new HashMap<String, String>();
		callClicked.put("AreaName",msActivaAreaName);
		callClicked.put("Source","StoreDetails");
		EventTracker.logEvent("StoreCall", callClicked);

		mCalldailog.show();
		mListCall.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				call.put("AreaName",msActivaAreaName);
				call.put("Source","StoreDetails");
				EventTracker.logEvent("StoreCallDone", call);

				Intent callNumber = new Intent(android.content.Intent.ACTION_DIAL);
				callNumber.setData(Uri.parse("tel:"+msArrCallnumbers.get(position)));
				startActivity(callNumber);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				mCalldailog.dismiss();
			}
		});
	}

	@Override
	public void onStoreFavourited() {
		//showToast("Favourite");
		if(msUserLike.equalsIgnoreCase("1")){
			unLikePlace();
		}else{			
			likePlace();
		}
	}

	@Override
	public void onStoreShare() {
		//showToast("Share");
		//		shareThisPlace();
		if(mInternet.isNetworkAvailable()){
			showDialog();
		}else{
			showToast(getString(R.string.noInternetConnection));
		}
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}

	void shareThisPlace()
	{
		try{
			if(mInternet.isNetworkAvailable()){

				Map<String, String> share= new HashMap<String, String>();
				share.put("AreaName",msActivaAreaName);
				share.put("StoreName",msStoreName);
				EventTracker.logEvent("StoreDetail_Share", share);

				JSONObject json = new JSONObject();
				json.put("place_id", msStoreId);
				json.put("via", msShareVia);
				String msUrl=Config.PLACE_SHARE_URL;
				new ShoplocalAsyncTask(this, "post","customer",false, json, "sharePlace").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch (JSONException e) {
			e.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void likePlace()
	{
		try {
			if(mInternet.isNetworkAvailable()){
				if(mSessionManager.isLoggedInCustomer()==true){

					Map<String, String> favourite= new HashMap<String, String>();
					favourite.put("AreaName",msActivaAreaName);
					favourite.put("StoreName",msStoreName);
					EventTracker.logEvent("StoreDetail_Favourite", favourite);

					JSONObject json = new JSONObject();
					json.put("user_id", Config.CUST_USER_ID);
					json.put("place_id", msStoreId);
					json.put("auth_id", Config.CUST_AUTH_ID);

					String msUrl=Config.PLACE_LIKE_URL;
					new ShoplocalAsyncTask(this, "post","customer",false,json,"likePlace").execute(msUrl);
				}else{
					loginAlert(getResources().getString(R.string.placeFavAlert), 1);
				}
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	void unLikePlace()
	{
		if(mInternet.isNetworkAvailable()){

			Map<String, String> unfavourite= new HashMap<String, String>();
			unfavourite.put("AreaName",msActivaAreaName);
			unfavourite.put("StoreName",msStoreName);
			EventTracker.logEvent("StoreDetail_UnFavourite", unfavourite);

			String msUrl=Config.PLACE_LIKE_URL+"?place_id="+msStoreId;
			new ShoplocalAsyncTask(this, "delete","customer",true,null,"unlikePlace").execute(msUrl);
		}else{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public void onGetDirectionCliked() {
		//showToast("Get Direction");

		try
		{
			if((msStoreLatitude.length()!=0 && msStoreLongitude.length()!=0)&&(!msStoreLatitude.equalsIgnoreCase("0")&&(!msStoreLongitude.equalsIgnoreCase("0")))){
				directions.put("AreaName",msActivaAreaName);
				directions.put("StoreName",msStoreName);
				EventTracker.logEvent("StoreDetail_Directions", directions);

				/*String uri = "http://maps.google.com/maps?saddr=" + LAT +","+ LONG +"&daddr="+ latitued +","+ longitude;*/
				String uri = "http://maps.google.com/maps?saddr=" + Config.LATITUDE +","+ Config.LONGITUDE +"&daddr="+ msStoreLatitude +","+ msStoreLongitude;
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
				startActivity(intent);
				/*store_mapDialog.dismiss();*/
			}
			else{
				showToast("No Location");
			}

		}catch(ActivityNotFoundException ac)
		{
			ac.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public void onEmailClicked() {
		//		showToast("Email");
		if(msStoreEmail.length()!=0)
		{
			if(msActivaAreaName != null || msStoreName != null){
				email.put("AreaName",msActivaAreaName);
				email.put("StoreName",msStoreName);
				EventTracker.logEvent("StoreDetail_Email", email);
			}

			Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
					"mailto",msStoreEmail, null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "I found you on shoplocal");
			context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
		}
		else
		{
			showToast("No email address");
		}
	}

	@Override
	public void onFacebookClicked() {

		ImageView pagerFb=(ImageView)mViewPager.findViewById(R.id.pagerFacebook);
		pagerFb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(msStoreFb.length()!=0)
				{
					if(msActivaAreaName != null || msStoreName != null){
						facebook.put("AreaName",msActivaAreaName);
						facebook.put("StoreName",msStoreName);
						EventTracker.logEvent("StoreDetail_Facebook", facebook);
					}

					Intent intent=new Intent(StoreDetailsActivity.this,AboutWebView.class);
					intent.putExtra("url", "SocialURL");
					intent.putExtra("SocialURL", msStoreFb);
					intent.putExtra("isFb", true);
					context.startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else
				{
					showToast("Facebook not set");
				}
			}
		});
	}

	@Override
	public void onWebsiteClicked() {

		ImageView pagerWebsite=(ImageView)mViewPager.findViewById(R.id.pagerWebsite);
		pagerWebsite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(msStoreWebsite.length()!=0)
				{
					if(msActivaAreaName != null || msStoreName != null){
						website.put("AreaName",msActivaAreaName);
						website.put("StoreName",msStoreName);
						EventTracker.logEvent("StoreDetail_Website", website);
					}
					Intent intent=new Intent(context,AboutWebView.class);
					intent.putExtra("url", "Web");
					intent.putExtra("Web", msStoreWebsite);
					context.startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else
				{
					showToast("Website not set");
				}
			}
		});
	}

	@Override
	public void onTwitterClicked() {
		ImageView pagerTwitter=(ImageView)mViewPager.findViewById(R.id.pagerTwitter);
		pagerTwitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(msStoreTwitter.length()!=0)
				{
					if(msActivaAreaName != null || msStoreName != null){
						twitter.put("AreaName",msActivaAreaName);
						twitter.put("StoreName",msStoreName);
						EventTracker.logEvent("StoreDetail_Twitter", twitter);
					}

					Intent intent=new Intent(context,AboutWebView.class);
					intent.putExtra("url", "SocialURL");
					intent.putExtra("SocialURL", msStoreTwitter);
					intent.putExtra("isFb", false);
					context.startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else
				{
					showToast("Twitter not set");
				}
			}
		});
	}

	void showToast(String text)
	{
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	private List<Fragment> getFragments()
	{
		List<Fragment> fList = new ArrayList<Fragment>();
		fList.add(StoreDetailsFragment.newInstance(PAGE_ABOUT_STORE));
		fList.add(StoreDetailsFragment.newInstance(PAGE_ADDRESS));
		fList.add(StoreDetailsFragment.newInstance(PAGE_POST));
		fList.add(StoreDetailsFragment.newInstance(PAGE_IMAGE_GALLERY));
		return fList;
	}

	private class StorePageAdapter extends FragmentStatePagerAdapter
	{
		ArrayList<String> pages;
		List<Fragment>fragments;
		
		public StorePageAdapter(FragmentManager fm,List<Fragment>fragments,ArrayList<String> pages) {
			super(fm);
			this.pages=pages;
			this.fragments=fragments;
		}
		@Override
		public CharSequence getPageTitle(int position) {
			return pages.get(position);
		}
		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}
		@Override
		public int getCount() {
			return fragments.size();
		}
	}

	@Override
	public void onAsyncStarted() {
		storeProgress.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		storeProgress.setVisibility(View.GONE);
		debug(sURL);
		debug(tag);
		debug(sJson);

		if(tag.equalsIgnoreCase("storeDetails")){
			try
			{
				ArrayList<String>categId=new ArrayList<String>();

				mArrCategoryLable.clear();
				mArrCategoryId.clear();

				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				JSONArray dataArray = jsonObject.getJSONArray("data");
				if(dataArray == null) return;

				int count = dataArray.length();

				if(mDbUtil.getCategoryRowCount()==0){
					loadCategories();
				}else{
					mArrCategoryLable=mDbUtil.getData("CATEGORIES", "label");
					mArrCategoryId=mDbUtil.getData("CATEGORIES", "id");
				}
				for(int i=0;i<count;i++)
				{
					JSONObject dataObject=dataArray.getJSONObject(i);
					JSONArray getTime=dataObject.getJSONArray("time");
					JSONArray getCateg=dataObject.getJSONArray("categories");

					msStoreName=dataObject.getString("name");
					msAreaId=dataObject.getString("area_id");
					msAreaName=dataObject.getString("area");
					msStoreDescription=dataObject.getString("description");
					msStoreLatitude=dataObject.getString("latitude");
					msStoreLongitude=dataObject.getString("longitude");
					msStoreLogo=dataObject.getString("image_url");
					msStoreMobileNo1=dataObject.getString("mob_no1");
					msStoreMobileNo2=dataObject.getString("mob_no2");
					msStoreMobileNo3=dataObject.getString("mob_no3");
					msStoreTelNo1=dataObject.getString("tel_no1");
					msStoreTelNo2=dataObject.getString("tel_no2");
					msStoreTelNo3=dataObject.getString("tel_no3");
					msStoreEmail=dataObject.getString("email");
					msStoreFb=dataObject.getString("facebook_url");
					msStoreTwitter=dataObject.getString("twitter_url");
					msStreet=dataObject.getString("street");
					msLandmark=dataObject.getString("landmark");
					msCity=dataObject.getString("city");
					msState=dataObject.getString("state");
					msCountry=dataObject.getString("country");
					msBuilding=dataObject.getString("building");
					msStoreWebsite=dataObject.getString("website");
					msStoreTotalLike=dataObject.getString("total_like");
					msStoreTotalShare=dataObject.getString("total_share");
					if(dataObject.has("user_like")){
						msUserLike=dataObject.getString("user_like");
					}else{
						msUserLike="-1";
					}
					for(int index=0;index<getTime.length();index++)
					{
						JSONObject getData=getTime.getJSONObject(index);
						mArrStoreDay.add(getData.getString("day"));
						mArrPlaceTimingStatus.add(getData.getString("is_closed"));
						mArrOpenTime.add(getData.getString("open_time"));
						mArrCloseTime.add(getData.getString("close_time"));
					}
					for(int index=0;index<getCateg.length();index++){
						categId.add(getCateg.getString(index));
					}

				}

				for(int i=0;i<categId.size();i++){
					for(int j=0;j<mArrCategoryId.size();j++){
						if(mArrCategoryId.get(j).equalsIgnoreCase(categId.get(i))){
							msCategName=mArrCategoryLable.get(j);
							break;
						}
					}
				}
				debug("Category "+msCategName);

				String time = "" ;
				String time2="";
				try
				{
					//Open Time
					//Open Time
					time =mArrOpenTime.get(0);

					DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					Date date = sdf.parse(time);

					System.out.println("Time Converted open: " + sdf.format(date));

					DateFormat time_open=new SimpleDateFormat("hh:mm aa");
					System.out.println("Time Converted open: " + time_open.format(date));

					/*txtMangeStoreTimeFrom.setText(time_open.format(date));*/


					//Close Time
					time2 = mArrCloseTime.get(0);

					DateFormat sdf2 = new SimpleDateFormat("hh:mm:ss");
					Date date2 = sdf.parse(time2);

					System.out.println("Time Converted Close: " + sdf2.format(date));

					DateFormat time_closed=new SimpleDateFormat("hh:mm aa");
					System.out.println("Time Converted Close: " + time_closed.format(date2));

					msOpenTime=time_open.format(date);
					msCloseTime=time_closed.format(date2);
				}catch(Exception ex){
					ex.printStackTrace();
				}

				Map<String, String> view= new HashMap<String, String>();
				view.put("AreaName",msActivaAreaName);
				view.put("StoreName", msStoreName);
				EventTracker.logEvent("StoreDetail_View", view);

				tvActionBarTitle.setText(msStoreName);
				setTitle(msStoreName);
				if(mbGetLike==true && msUserLike.equalsIgnoreCase("1")){
					setDataToPager(msStoreLogo, msOpenTime, msCloseTime, "", msStoreDescription, true,msStoreTotalLike,msStoreTotalShare);
				}else{
					setDataToPager(msStoreLogo, msOpenTime, msCloseTime, "", msStoreDescription, false,msStoreTotalLike,msStoreTotalShare);
				}
				setAddressData(msStreet, msLandmark, msAreaName, msCity, msStoreMobileNo1, msStoreMobileNo2,
						msStoreMobileNo3, msStoreTelNo1, msStoreTelNo2, msStoreTelNo3, msStoreEmail);

			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		if(tag.equalsIgnoreCase("likePlace")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){

					try
					{
						
						FbObject fbObj=new FbObject();
						fbObj.setStoreTitle(msStoreName);
						if(msStoreLogo.length()==0){
							fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
						}else{
							fbObj.setStoreImage(Config.IMAGE_URL+msStoreLogo);
						}
						fbObj.setStoreDescription(msStoreDescription);
						String encodedurl = URLEncoder.encode(msStoreName,"UTF-8");
						debug(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+msStoreId);
						fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/all/"+encodedurl+"/"+msStoreId);
						FacebookOperations.FavThisOnFb(mActivity, fbObj, getString(R.string.fb_store_object));
					}catch(Exception ex){
						ex.printStackTrace();
					}

					String message=jsonObject.getString("message");
					//showToast(message);
					changeLikeImage(true);
					msUserLike="1";
				}
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("unlikePlace")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;

				if(status.equals("false")){
					String code=jsonObject.getString("code");
					String msg=jsonObject.getString("message");
					if(code.equalsIgnoreCase("-221")){
						mSessionManager.logoutCustomer();
						showToast(msg);
					}
					return;
				}

				if(status.equalsIgnoreCase("true")){
					String message=jsonObject.getString("message");
					//showToast(message);
					changeLikeImage(false);
					msUserLike="-1";
				}
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("sharePlace")){

			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");

				if(status == null) return;
				if(status.equalsIgnoreCase("true")){
					try
					{
						if(mbFaceBookShare==false)
						{
							FbObject fbObj=new FbObject();
							fbObj.setStoreTitle(msStoreName);
							if(msStoreLogo.length()==0){
								fbObj.setStoreImage("http://shoplocal.co.in/assets/images/main/shop.png");
							}else{
								fbObj.setStoreImage(Config.IMAGE_URL+msStoreLogo);
							}
							fbObj.setStoreDescription(msStoreDescription);
							String encodedurl = URLEncoder.encode(msStoreName,"UTF-8");
							fbObj.setStoreUrl(Config.FACEBOOK_SHARE_URL+msSlug+"/"+msCategName+"/"+encodedurl+"/"+msStoreId);
							FacebookOperations.shareThisInfo(mActivity, fbObj, getString(R.string.fb_store_object));
						}
					}catch(Exception ex){
						ex.printStackTrace();
					}

				}

			}catch(Exception ex){
				ex.printStackTrace();
			}
			
		}
		if(tag.equalsIgnoreCase("loadPosts")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equalsIgnoreCase("false")){
					String msg=jsonObject.getString("message");
					String code=jsonObject.getString("code");
					if(code.equalsIgnoreCase("-404")){
						RelativeLayout mPlaceHolder=(RelativeLayout)mViewPager.findViewById(R.id.placeHolderView);
						mPlaceHolder.setVisibility(View.VISIBLE);

						TextView mTxt=(TextView)mViewPager.findViewById(R.id.txtPlaceHolderMsg);
						mTxt.setText(msg);
					}else{
						showToast(msg);
					}
					return;
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");

				if(dataObject == null) return;

				JSONArray jsonArray = dataObject.getJSONArray("record");

				if(jsonArray == null)  return;



				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					mArrPostId.add(offerObject.getString("id"));
					mArrPostTitle.add(offerObject.getString("title"));
					mArrPostDescription.add(offerObject.getString("description"));
					mArrPostDate.add(offerObject.getString("date"));
					mArrPostOfferDate.add(offerObject.getString("offer_date_time"));
					mArrPostTotalLike.add(offerObject.getString("total_like"));
					mArrPostTotalShare.add(offerObject.getString("total_share"));
					mArrPostTotalViews.add(offerObject.getString("total_view"));
					mArrPostImageUrl1.add(offerObject.getString("image_url1"));
					mArrPostIsOffer.add(offerObject.getString("is_offered"));
				}

				setPostAdapters();
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
		
		if(tag.equalsIgnoreCase("loadGallery")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				String status = jsonObject.getString("success");
				if(status == null) return;

				if(status.equalsIgnoreCase("false")){
					String msg=jsonObject.getString("message");
					String code=jsonObject.getString("code");
					if(code.equalsIgnoreCase("-156")){
						RelativeLayout mPlaceHolder=(RelativeLayout)mViewPager.findViewById(R.id.placeHolderGalleryView);
						mPlaceHolder.setVisibility(View.VISIBLE);

						TextView mTxt=(TextView)mViewPager.findViewById(R.id.txtGalleryPlaceHolderMsg);
						mTxt.setText(msg);
					}
					else{
						showToast(msg);
					}
					return;
				}

				JSONArray jsonArray = jsonObject.getJSONArray("data");
				if(jsonArray == null)  return;

				int count = jsonArray.length();
				ArrayList<String> imageTitlesCustomer = new ArrayList<String>();
				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					mArrGalleryThumb.add(offerObject.getString("thumb_url"));
					mArrGalleryImage.add(offerObject.getString("image_url"));
					imageTitlesCustomer.add(offerObject.getString("title"));
				}
				setGalleryData(mArrGalleryThumb, imageTitlesCustomer);
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		if(tag.equalsIgnoreCase("categories")){
			try
			{
				JSONObject jsonObject=new JSONObject(sJson);
				String status=jsonObject.getString("success");

				if(status==null || status.equalsIgnoreCase("false")) return;

				JSONArray jsonArray=jsonObject.getJSONArray("data");
				int count=jsonArray.length();

				for(int i=0;i<count;i++){
					JSONObject json=jsonArray.getJSONObject(i);
					mArrCategoryLable.add(json.getString("label"));
					mArrCategoryId.add(json.getString("id"));
				}

				ContentValues[] values = new ContentValues[count];
				for(int i=0;i<count;i++) {
					ContentValues value = new ContentValues();
					value.put("label", mArrCategoryLable.get(i));
					value.put("id", mArrCategoryId.get(i));
					values[i] = value;
				}

				DBUtil dbUtil = DBUtil.getInstance(mActivity.getApplicationContext());
				dbUtil.replaceOrUpdate(this,"CATEGORIES",values,"loadCateg"); 
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	void loadCategories()
	{
		try
		{
			if(mInternet.isNetworkAvailable()){
				String msUrl=Config.CATEGORIES_URL;
				new ShoplocalAsyncTask(this, "get", "customer", false, "categories").execute(msUrl);
			}else{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void changeLikeImage(boolean status)
	{
		try
		{
			ImageView pagerStoreFav=(ImageView)mViewPager.findViewById(R.id.pagerStoreFav);
			if(status){
				pagerStoreFav.setImageResource(R.drawable.ic_store_liked);
			}
			else{
				pagerStoreFav.setImageResource(R.drawable.ic_store_unliked);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	void setPostAdapters()
	{
		try
		{
			ListView broadCastList=(ListView)mViewPager.findViewById(R.id.pagerPostList);

			PostAdapter adapter=new PostAdapter(this, R.drawable.ic_launcher, R.drawable.ic_launcher, mArrPostTitle,
					mArrPostImageUrl1,mArrPostDate,mArrPostTotalLike,mArrPostTotalViews,mArrPostTotalShare, mArrPostIsOffer, false);
			broadCastList.setAdapter(adapter);
			if(mArrPostId.size()==0){
				broadCastList.setVisibility(View.GONE);
			}else{
				broadCastList.setVisibility(View.VISIBLE);
			}
			broadCastList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {

					Intent intent=new Intent(context, OffersBroadCastDetails.class);
					intent.putExtra("storeName", msStoreName);
					intent.putExtra("title", mArrPostTitle.get(position));
					intent.putExtra("body", mArrPostDescription.get(position));
					intent.putExtra("POST_ID", mArrPostId.get(position));
					intent.putExtra("PLACE_ID", msStoreId);
					intent.putExtra("fromCustomer", true);

					intent.putExtra("mob1", msStoreMobileNo1);
					intent.putExtra("mob2", msStoreMobileNo2);
					intent.putExtra("mob3", msStoreMobileNo3);
					intent.putExtra("tel1", msStoreTelNo1);
					intent.putExtra("tel2", msStoreTelNo2);
					intent.putExtra("tel3", msStoreTelNo3);
					intent.putExtra("isFromStoreDetails", true);
					intent.putExtra("photo_source", mArrPostImageUrl1.get(position));
					intent.putExtra("USER_LIKE","-1");
					//										if(POST_USER_LIKE.size()==0)
					//										{
					//											intent.putExtra("USER_LIKE","-1");
					//										}else
					//										{
					//											intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
					//					
					//										}
					context.startActivityForResult(intent, 5);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
			});



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void setDataToPager(String photo_url, String open_time, String close_time,
			String open_close_status, String store_desc,boolean isLike,String total_like,String total_share)
	{
		try
		{
			//About Store
			ImageView pagerLogo=(ImageView)mViewPager.findViewById(R.id.pagerLogo);
			TextView pagerOpenCloseTime=(TextView)mViewPager.findViewById(R.id.pagerOpenCloseTime);
			TextView pagerOpenCloseDays=(TextView)mViewPager.findViewById(R.id.pagerOpenCloseDays);
			TextView pagerStoreDesc=(TextView)mViewPager.findViewById(R.id.pagerStoreDesc);
			ImageView pagerStoreFav=(ImageView)mViewPager.findViewById(R.id.pagerStoreFav);
			TextView pagerStoreStats=(TextView)mViewPager.findViewById(R.id.pagerStoreStats);
			
			pagerOpenCloseTime.setTypeface(tf);
			pagerOpenCloseDays.setTypeface(tf);
			pagerStoreDesc.setTypeface(tf);
			pagerStoreStats.setTypeface(tf);
			
			
			pagerStoreStats.setText("Total Likes : "+total_like+"\n\nTotal Shares : "+total_share);
			if(photo_url.length()!=0)
			{
				try{
					Picasso.with(this).load(Config.IMAGE_URL+photo_url)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(pagerLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				pagerLogo.setImageResource(R.drawable.ic_place_holder);
			}

			pagerOpenCloseTime.setText(open_time+" "+close_time);
			pagerOpenCloseDays.setText(open_close_status);
			pagerStoreDesc.setText(""+store_desc);

			if(isLike)
			{
				pagerStoreFav.setImageResource(R.drawable.ic_store_liked);
			}
			else
			{
				pagerStoreFav.setImageResource(R.drawable.ic_store_unliked);
			}

			if(photo_url.length()!=0)
			{
				final ArrayList<String> url=new ArrayList<String>();
				url.add(" ");
				url.add(photo_url);
				pagerLogo.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent=new Intent(context,StorePagerGallery.class);
						intent.putStringArrayListExtra("photo_source", url);
						intent.putExtra("position", 0+1);
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});
			}


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setAddressData(String street,String landmark,String area,String city,String mob1,
			String mob2,String mob3,String tel1,String tel2,String tel3,String email)
	{
		try
		{
			TextView pagerAddress=(TextView)mViewPager.findViewById(R.id.pagerAddress);
			TextView pagerPhoneNumbers=(TextView)mViewPager.findViewById(R.id.pagerPhoneNumbers);
			TextView pagerEmail=(TextView)mViewPager.findViewById(R.id.pagerEmail);
			

			ImageView pagerLocate=(ImageView)mViewPager.findViewById(R.id.pagerLocate);
			ImageView pagerSep1=(ImageView)mViewPager.findViewById(R.id.pagerSep1);
			
			pagerAddress.setTypeface(tf);
			pagerPhoneNumbers.setTypeface(tf);
			pagerEmail.setTypeface(tf);
			
			pagerAddress.setText(street+" "+landmark+"\n"+area+"\n"+city);
			

			if(Config.LATITUDE.length()!=0 && msStoreLatitude.length()!=0){
				if((msStoreLatitude.length()!=0 && msStoreLongitude.length()!=0)&&(!msStoreLatitude.equalsIgnoreCase("0")&&(!msStoreLongitude.equalsIgnoreCase("0")))){
					pagerLocate.setVisibility(View.VISIBLE);
					msMapLinkLable="\non Google Map : \n";
					msMapLink="http://maps.google.com/?q="+msStoreLatitude+","+msStoreLongitude;
				}else{
					pagerLocate.setVisibility(View.GONE);
				}
			}else{
				pagerLocate.setVisibility(View.GONE);
			}

			if(mob1.length()==0 && mob2.length()==0 && mob3.length()==0 && tel1.length()==0 && tel2.length()==0 && tel3.length()==0)
			{
				pagerPhoneNumbers.setVisibility(View.GONE);
				pagerSep1.setVisibility(View.GONE);
			}
			else
			{
				try
				{
					String contact_nos="";
					if(mob1.length()!=0)
					{
						contact_nos+="+"+mob1+"\n";
						msContact="+"+mob1;
					}
					if(mob2.length()!=0)
					{
						contact_nos+="+"+mob2+"\n";
						msContact=" +"+mob2;
					}
					if(mob3.length()!=0)
					{
						contact_nos+="+"+mob3+"\n";
						msContact=" +"+mob3;
					}
					if(tel1.length()!=0)
					{
						contact_nos+=tel1+"\n";
						msContact=" "+tel1;
					}
					if(tel2.length()!=0)
					{
						contact_nos+=tel2+"\n";
						msContact=" "+tel2;
					}
					if(tel3.length()!=0)
					{
						contact_nos+=tel3;
						msContact=" "+tel3;
					}

					pagerPhoneNumbers.setText(contact_nos);

					if(msStoreMobileNo1.length()>5)
					{
						msArrCallnumbers.add("+"+msStoreMobileNo1);
					}
					if(msStoreMobileNo2.length()>5)
					{
						msArrCallnumbers.add("+"+msStoreMobileNo2);
					}
					if(msStoreMobileNo3.length()>5)
					{
						msArrCallnumbers.add("+"+msStoreMobileNo3);
					}
					if(msStoreTelNo1.length()>5)
					{
						msArrCallnumbers.add(msStoreTelNo1);
					}
					if(msStoreTelNo2.length()>5)
					{
						msArrCallnumbers.add(msStoreTelNo2);
					}
					if(msStoreTelNo3.length()>5)
					{
						msArrCallnumbers.add(msStoreTelNo3);
					}

					mListCall.setAdapter(new CallListDialog(context, 0, msArrCallnumbers));
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}

			if(email.length()!=0)
			{
				pagerEmail.setText(email);
			}
			else
			{
				pagerEmail.setVisibility(View.GONE);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public void onError(int errorCode) {
		showToast(getResources().getString(R.string.errorMessage));

	}

	void debug(String msg)
	{
		Log.i("StoreDetails", "StoreDetails "+msg);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}
	@Override
	public void onPageSelected(int position) {
		if(position==2){
			if(mArrPostId.size()==0){
				if(mInternet.isNetworkAvailable()){
					String custId="";
					String msPostUrl="";
					if(mSessionManager.isLoggedInCustomer()){
						custId=Config.CUST_USER_ID;
						msPostUrl=Config.LOAD_ALL_POST_URL+"place_id="+msStoreId+"&customer_id="+custId;
					}else{
						msPostUrl=Config.LOAD_ALL_POST_URL+"place_id="+msStoreId;	
					}
					new ShoplocalAsyncTask(this, "get", "customer", false, "loadPosts").execute(msPostUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}

			}
		}
		if(position==3)	{
			if(mArrGalleryImage.size()==0){
				if(mInternet.isNetworkAvailable()){
					String mGalleryUrl=Config.GALLERY_URL+"place_id="+msStoreId;
					new ShoplocalAsyncTask(this, "get", "customer", false, "loadGallery").execute(mGalleryUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		}
	}



	class CallListViewHolder
	{
		TextView contactNo;
	}

	void setGalleryData(ArrayList<String>source, final ArrayList<String> imageTitlesCustomer)
	{
		try
		{
			GridView pagerGalleryGrid=(GridView)mViewPager.findViewById(R.id.pagerGalleryGrid);
			pagerGalleryGrid.setAdapter(new GridAdapter(context, 0, 0, source, imageTitlesCustomer));
			pagerGalleryGrid.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {

					Map<String, String> viewgallery= new HashMap<String, String>();
					viewgallery.put("AreaName",msActivaAreaName);
					viewgallery.put("StoreName",msStoreName);
					EventTracker.logEvent("StoreDetail_ViewGallery", viewgallery);

					if(mArrGalleryImage.size()!=0)
					{
						ArrayList<String> url=new ArrayList<String>();
						url.add(" ");
						for(int i=0;i<mArrGalleryImage.size();i++)
						{
							url.add(mArrGalleryImage.get(i));
						}

						Intent intent=new Intent(context,StorePagerGallery.class);
						intent.putStringArrayListExtra("photo_source", url);
						ArrayList<String> imageTitlesCustomerCopy = (ArrayList<String>) imageTitlesCustomer.clone();
						imageTitlesCustomerCopy.add(0, "");
						intent.putStringArrayListExtra("imageTitles", imageTitlesCustomerCopy);
						intent.putExtra("position", position+1);
						context.startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	class GridAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> imageTitlesCustomer;
		ArrayList<String> source;
		LayoutInflater inflator;
		Activity context;
		public GridAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> source, ArrayList<String> imageTitlesCustomer) {
			super(context, resource, textViewResourceId, source);
			this.context=context;
			inflator=context.getLayoutInflater();
			this.source=source;
			this.imageTitlesCustomer = imageTitlesCustomer;
		}
		@Override
		public int getCount() {

			return source.size();
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if(convertView==null)
			{
				GridViewHolder holder=new GridViewHolder();
				convertView=inflator.inflate(R.layout.store_details_gallery_image, null);
				holder.pagerGalleryImage=(ImageView)convertView.findViewById(R.id.pagerGalleryImage);
				holder.imageTitle = (TextView) convertView.findViewById(R.id.imageTitleCustomer);
				convertView.setTag(holder);
			}

			GridViewHolder hold=(GridViewHolder)convertView.getTag();
			String title = imageTitlesCustomer.get(position).trim();
			if (title.length() > 0) {
				
				String desc = title.trim();
				String seperator = "\\\\'";
				String []tempText = desc.split(seperator);
				
		        String b= "";
		        for (String c : tempText) {
		        	b +=c+"\'";
		        }
		        b = b.substring(0, b.length()-1);
		        
		        desc = b.trim();
				seperator = "\\\\n";
				tempText = desc.split(seperator);
				
		        b = "";
		        for (String c : tempText) {
		        	b +=c+"\n";
		        }
		        title = b.substring(0, b.length()-1);
				
				hold.imageTitle.setText(title);
				hold.imageTitle.setVisibility(View.VISIBLE);
			} else {
				hold.imageTitle.setVisibility(View.GONE);
			}
			
			try
			{
				try{
					Picasso.with(context).load(Config.IMAGE_URL+source.get(position))
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.pagerGalleryImage);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return convertView;
		}



	}
	static class GridViewHolder
	{
		ImageView pagerGalleryImage;
		TextView imageTitle;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2){
			onStoreFavourited();
		}
		if(requestCode==3){
			shareThisPlace();
		}
		try
		{
			mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}

				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					msShareVia="Android-Facebook";
					shareThisPlace();
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void showDialog(){

		try
		{
			String contactLable="\nContact : ";
			if(msContact.length()==0)
			{
				contactLable="";
			}

			String shareBody = "http://shoplocal.co.in/download"+"\n\n"+msStoreName+"\nAddress : "+msStreet+" "+msLandmark+"\n"+msAreaName+" "+msCity
					+contactLable+msContact+"\nTimings : "+msOpenTime+" To "+msCloseTime+"\nDescription : "+msStoreDescription
					+msMapLinkLable+msMapLink;

			storeShareDialog(msStoreName+" -store on Shoplocal", msStoreLogo,shareBody);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onCompleteInsertion(String tag) {

	}

}
