package com.phonethics.shoplocal;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.actionbarsherlock.view.Menu;
import com.phonethics.adapters.PostAdapter;



public class ViewPost extends ShoplocalActivity implements ShoplocalAsyncTaskStatusListener, OnItemClickListener, OnItemLongClickListener 
{

	private ListView mListViewOffers;
	private ProgressBar mProgOffers;
	private String msStoreId="";
	private String msStoreName="";

	private ArrayList<String>mArrPostId=new ArrayList<String>();
	private ArrayList<String>mArrPostTitle=new ArrayList<String>();
	private ArrayList<String>mArrPostDescription=new ArrayList<String>();
	private ArrayList<String>mArrPostDate=new ArrayList<String>();
	private ArrayList<String>mArrPostOfferDate=new ArrayList<String>();
	private ArrayList<String>mArrPostTotalLike=new ArrayList<String>();
	private ArrayList<String>mArrPostTotalShare=new ArrayList<String>();
	private ArrayList<String>mArrPostTotalViews=new ArrayList<String>();
	private ArrayList<String>mArrPostImageUrl1=new ArrayList<String>();
	private ArrayList<String> mArrPostIsOffer = new ArrayList<String>();

	Activity mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_post);
		mListViewOffers=(ListView)findViewById(R.id.listViewOffers);
		mProgOffers=(ProgressBar)findViewById(R.id.progOffers);
		mContext=this;
		hideDrawerLayout();
		setTitle("View Post");
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			msStoreId=b.getString("STORE_ID");
			msStoreName=b.getString("storeName");
			if(msStoreId!=""){
				if(mInternet.isNetworkAvailable()){
					String msUrl=Config.LOAD_ALL_POST_URL+"place_id="+msStoreId;
					new ShoplocalAsyncTask(this, "get", "merchant", true, "loadPosts").execute(msUrl);
				}else{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		}
	}

	@Override
	public void onAsyncStarted() {
		mProgOffers.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCompleted(String sURL, String sJson, String tag) {
		debug(sJson);
		mProgOffers.setVisibility(View.GONE);
		
		if(tag.equalsIgnoreCase("loadPosts")){
			try
			{
				JSONObject jsonObject = new JSONObject(sJson);
				
				String status = jsonObject.getString("success");
				if(status.equalsIgnoreCase("false") || status == null){
					String message=jsonObject.getString("message");
					showToast(message);
					if(jsonObject.has("error_code")){
						String error_code=jsonObject.getString("error_code");
						if(error_code.equalsIgnoreCase("-116")){
							invalidAuthFinish();
						}
					}
					return;
				}

				JSONObject dataObject = jsonObject.getJSONObject("data");

				if(dataObject == null) return;

				JSONArray jsonArray = dataObject.getJSONArray("record");

				if(jsonArray == null)  return;

				int count = jsonArray.length();

				for(int i=0;i<count;i++) {
					JSONObject offerObject = jsonArray.getJSONObject(i);
					mArrPostId.add(offerObject.getString("id"));
					mArrPostTitle.add(offerObject.getString("title"));
					mArrPostDescription.add(offerObject.getString("description"));
					mArrPostDate.add(offerObject.getString("date"));
					mArrPostOfferDate.add(offerObject.getString("offer_date_time"));
					mArrPostTotalLike.add(offerObject.getString("total_like"));
					mArrPostTotalShare.add(offerObject.getString("total_share"));
					mArrPostTotalViews.add(offerObject.getString("total_view"));
					mArrPostImageUrl1.add(offerObject.getString("image_url1"));
					mArrPostIsOffer.add(offerObject.getString("is_offered"));
				}
				PostAdapter adapter=new PostAdapter(this, R.drawable.ic_launcher, R.drawable.ic_launcher, mArrPostTitle,
						mArrPostImageUrl1,mArrPostDate,mArrPostTotalLike,mArrPostTotalViews,mArrPostTotalShare, mArrPostIsOffer,true);
				mListViewOffers.setAdapter(adapter);
				mListViewOffers.setOnItemClickListener(this);
				mListViewOffers.setOnItemLongClickListener(this);
				registerForContextMenu(mListViewOffers);
			}catch(JSONException je){
				je.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}

		}
	}

	void debug(String msg)
	{
		Log.i("ViewPost", "ViewPost "+msg);
	}

	@Override
	public void onError(int errorCode) {
		mProgOffers.setVisibility(View.GONE);
		showToast(getResources().getString(R.string.errorMessage));
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

		Intent intent=new Intent(mContext, OffersBroadCastDetails.class);
		intent.putExtra("PLACE_ID", msStoreId);
		intent.putExtra("storeName", msStoreName);
		intent.putExtra("POST_ID", mArrPostId.get(position));
		intent.putExtra("title", mArrPostTitle.get(position));
		intent.putExtra("body", mArrPostDescription.get(position));
		intent.putExtra("photo_source", mArrPostImageUrl1.get(position));
		startActivity(intent);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position,
			long arg3) {

		AlertDialog.Builder alertDialog=new AlertDialog.Builder(mContext);
		alertDialog.setTitle("Re-use post");
		alertDialog.setMessage(getResources().getString(R.string.reusePostAlert));
		alertDialog.setPositiveButton("Yes",new OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				ArrayList<String>SELECTED_STORE_ID=new ArrayList<String>();
				SELECTED_STORE_ID.add(msStoreId);
				dialog.cancel();
				Intent intent=new Intent(mContext, CreateBroadCast.class);
				intent.putExtra("POST_ID", mArrPostId.get(position));
				intent.putStringArrayListExtra("SELECTED_STORE_ID",SELECTED_STORE_ID);
				intent.putExtra("title", mArrPostTitle.get(position));
				intent.putExtra("description", mArrPostDescription.get(position));
				intent.putExtra("photourl", mArrPostImageUrl1.get(position));
				startActivity(intent);
				finish();
			}
		});
		alertDialog.setNegativeButton("No",new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alertDialog.show();


		return false;
	}

	void invalidAuthFinish(){
		try{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(mContext);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					dialog.dismiss();
					try{
						mSessionManager.logoutUser();
						Intent intent=new Intent(mContext, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();
			alertDialog3.show();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		finishTo();
		return true;
	}

	public void onBackPressed() {
		finishTo();
	}

	void finishTo(){
		Intent intent=new Intent(mContext,MerchantTalkHome.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

}
